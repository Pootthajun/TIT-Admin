﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Web.UI.WebControls
Imports System.Data.SqlClient

Namespace Common
    Public Class FunctionEng


        Public Shared Function GetSysConfig(ByVal ParaName As String) As String
            Dim ret As Boolean = False
            Dim trans As New TransactionDB
            Dim lnq As New MsSysconfigLinqDB

            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_PARM_NAME", ParaName)

            ret = lnq.ChkDataByWhere("config_name = @_PARM_NAME", trans.Trans, p)
            trans.CommitTransaction()
            Dim r As String = lnq.CONFIG_VALUE
            lnq = Nothing
            If ret = True Then
                Return r
            Else
                Return ""
            End If
        End Function

        Public Shared Sub CreateLogFile(ByVal TextMsg As String)
            Try
                Dim LogPath As String = System.Windows.Forms.Application.StartupPath & "\Logs\"
                If IO.Directory.Exists(LogPath) = False Then
                    IO.Directory.CreateDirectory(LogPath)
                End If
                Dim FILE_NAME As String = LogPath & DateTime.Now.ToString("yyyyMMddHH") & ".txt"
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fff") & " " & TextMsg & vbNewLine & vbNewLine)
                objWriter.Close()
            Catch ex As Exception

            End Try
            
        End Sub

        Public Shared Sub CreateErrorLog(ByVal TextMsg As String)
            Dim frame As StackFrame = New StackFrame(1, True)
            Dim ClassName As String = frame.GetMethod.ReflectedType.Name
            Dim FunctionName As String = frame.GetMethod.Name
            Dim LineNo As Integer = frame.GetFileLineNumber

            Try
                Dim LogFolder As String = System.Windows.Forms.Application.StartupPath & "\FaultErrorLog"
                If IO.Directory.Exists(LogFolder) = False Then
                    IO.Directory.CreateDirectory(LogFolder)
                End If
                Dim FILE_NAME As String = LogFolder & "\" & DateTime.Now.ToString("yyyyMMddHH") & ".txt"
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fff") & " " & ClassName & "." & FunctionName & " Line No : " & LineNo & vbNewLine & TextMsg & vbNewLine & vbNewLine)
                objWriter.Close()
            Catch ex As Exception

            End Try
        End Sub

        Public Shared Function cStrToDateTime(ByVal StrDate As String, ByVal StrTime As String) As DateTime 'Convert วันที่จาก yyyy-MM-dd HH:mm:ss เป็น DateTime
            Dim ret As New Date(1, 1, 1)
            If StrDate.Trim <> "" Then
                Dim vDate() As String = Split(StrDate, "-")
                If StrTime.Trim <> "" Then
                    Dim vTime() As String = Split(StrTime, ":")
                    ret = New Date(vDate(0), vDate(1), vDate(2), vTime(0), vTime(1), vTime(2))
                Else
                    ret = New Date(vDate(0), vDate(1), vDate(2))
                End If
            End If
            Return ret
        End Function

        Public Shared Function ExecuteDataTable(ByVal sql As String, parm() As SqlParameter) As DataTable
            Dim dt As New DataTable
            dt = SqlDB.ExecuteTable(sql, parm)
            Return dt
        End Function

        Public Shared Function ExecuteNonQuery(sql As String, parm() As SqlParameter) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Try
                ret = SqlDB.ExecuteNonQuery(sql, parm)
            Catch ex As Exception
                ret.IsSuccess = False
                ret.ErrorMessage = "Exception : " & ex.Message & ex.StackTrace
            End Try
            Return ret
        End Function

        Public Shared Sub SetComputerList(ddl As DropDownList)
            Dim sql As String = "select id, servername + ' (' + serverip + ')' server_name "
            sql += " from CF_CONFIG_COMPUTER_LIST "
            sql += " order by servername"
            Dim dt As DataTable = SqlDB.ExecuteTable(sql)
            If dt.Rows.Count > 0 Then
                ddl.DataTextField = "server_name"
                ddl.DataValueField = "id"
                ddl.DataSource = dt
                ddl.DataBind()
            End If
        End Sub

    End Class
End Namespace

