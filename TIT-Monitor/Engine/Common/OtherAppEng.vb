﻿Imports System.Reflection
Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Namespace Common
    Public Class OtherAppEng
        Public Shared Function SendAlarmOtherApp(MacAddress As String, alDt As DataTable, IsProblem As Boolean, LocationName As String, MailSubject As String, MailAlias As String) As ExecuteDataInfo
            ''### Current Class and Function name
            Dim m As MethodBase = MethodBase.GetCurrentMethod()
            Dim ThisClassName As String = m.ReflectedType.Name
            Dim ThisFunctionName As String = m.Name

            Dim ret As New ExecuteDataInfo
            Try
                'ข้อมูล Computer
                Dim kDt As DataTable = GetConfigComputerInfo(MacAddress)
                If kDt.Rows.Count > 0 Then
                    Dim kDr As DataRow = kDt.Rows(0)

                    'หาข้อมูล AlarmCode
                    If alDt.Rows.Count > 0 Then
                        Dim AlarmActivity = alDt.Rows(0)("alarm_problem")
                        Dim AlarmCode As String = alDt.Rows(0)("alarm_code")
                        Dim ServerName As String = kDr("ServerName")
                        Dim IPAddress As String = kDr("ServerIP")

                        'เก็บข้อมูลใน Alarm Waiting Clear ไว้ก่อน
                        Dim dt As DataTable = AlarmEng.GetAlarmWaitingClear(ServerName, AlarmActivity, "Alarm")
                        Dim lnq As New TbAlarmWaitingClearLinqDB

                        'ตรวจสอบว่า Flag ที่ส่งมาเป็นสถานะที่ต้อง Alarm หรือไม่
                        If IsProblem = True Then
                            If dt.Rows.Count > 0 Then   'ถ้ามี Alarm Waiting Clear อยู่แล้ว
                                Dim dr As DataRow = dt.Rows(0)

                                'Dim MacAddress As String = dr("MacAddress")
                                Dim SpecificProblem As String = dr("SpecificProblem")

                                'Update Alarm Qty
                                lnq.GetDataByPK(Convert.ToInt64(dr("id")), Nothing)
                                lnq.ALARMQTY += 1
                                lnq.LASTUPDATEDATE = DateTime.Now

                                If lnq.ID > 0 Then
                                    Dim trans As New TransactionDB
                                    ret = lnq.UpdateData(ThisClassName & "." & ThisFunctionName, trans.Trans)
                                    If ret.IsSuccess = True Then
                                        trans.CommitTransaction()

                                        AlarmEng.InsertAlarmLog(ServerName, IPAddress, MacAddress, AlarmCode, AlarmActivity, "CRITICAL", 0, "Alarm", "MAIL", SpecificProblem, lnq.ID)
                                    Else
                                        trans.RollbackTransaction()
                                        ret.IsSuccess = False
                                        ret.ErrorMessage = lnq.ErrorMessage
                                    End If
                                End If
                            Else
                                'Insert Alarm Waiting Clear
                                Dim SpecificProblem As String = "Alarm " & alDt.Rows(0)("sms_message").ToString.Replace("<Shop Short Name>", LocationName)
                                Dim _ID = AlarmEng.InsertAlarmWaitingClear(ServerName, IPAddress, MacAddress, AlarmCode, "CRITICAL", 0, AlarmActivity, "MAIL", SpecificProblem, MailAlias, MailSubject)
                                If _ID > 0 Then
                                    ret.IsSuccess = True
                                    ret.NewID = _ID
                                End If
                            End If
                        Else
                            'ถ้าไม่ใช่ Alarm ให้ตรวจสอบว่ายังมี Alarm ค้างอยู่หรือไม่
                            If dt.Rows.Count > 0 Then   'ถ้ามี Alarm Waiting Clear อยู่แล้ว ให้ทำการ Clear Alarm
                                'Clear Alarm
                                Dim ClearMsg As String = "Clear " & alDt.Rows(0)("sms_message").ToString.Replace("<Shop Short Name>", LocationName) 'alDt.Rows(0)("eng_desc")

                                ret = AlarmEng.SendClearAlarm(ServerName, IPAddress, MacAddress, AlarmCode, "CRITICAL", 0, "MAIL", AlarmActivity, ClearMsg, Convert.ToInt64(dt.Rows(0)("id")))
                            Else
                                ret.IsSuccess = True
                            End If
                        End If
                        dt.Dispose()
                    Else
                        ret.IsSuccess = "false"
                        ret.ErrorMessage = "Cannot find Alarm Problem"
                        Common.FunctionEng.CreateErrorLog("Cannot find Alarm Problem")
                    End If
                    alDt.Dispose()
                End If
            Catch ex As Exception
                Dim _err As String = "Exception : " & ex.Message & vbNewLine & ex.StackTrace & vbNewLine & " MacAddress=" & MacAddress
                ret.IsSuccess = False
                ret.ErrorMessage = _err
                Common.FunctionEng.CreateErrorLog(_err)
            End Try
            Return ret
        End Function

        Public Shared Function GetMonitorComputerInfo(MacAddress As String) As DataTable
            Dim dt As New DataTable
            Try
                Dim sql As String = "select cc.ServerName,cc.ServerIP,cc.MacAddress, cc.online_status,cc.today_available, cc.ActiveStatus, "
                sql += " cpu.cpupercent,cpu.cputemperature, ram.rampercent, (sum(d.TotalSizeGB-d.FreeSpaceGB)/sum(d.TotalSizeGB))*100 diskpercent "
                sql += " from CF_CONFIG_COMPUTER_LIST cc "
                sql += " inner join tb_cpu_info cpu On cc.macaddress=cpu.macaddress "
                sql += " inner join tb_ram_info ram on cc.macaddress=ram.macaddress "
                sql += " inner join TB_DRIVE_INFO d On cc.macaddress=d.macaddress "
                sql += " where cc.MacAddress=@_MACADDRESS"
                sql += " group by cc.ServerName,cc.ServerIP,cc.MacAddress, cc.online_status,cc.today_available, cc.ActiveStatus, "
                sql += " cpu.cpupercent,cpu.cputemperature, ram.rampercent"

                Dim p(1) As SqlParameter
                p(0) = SqlDB.SetText("@_MACADDRESS", MacAddress)
                dt = SqlDB.ExecuteTable(sql, p)
            Catch ex As Exception
                dt = New DataTable
            End Try

            dt.TableName = "GetMonitorComputerInfo"
            Return dt
        End Function

        Public Shared Function GetConfigComputerInfo(MacAddress As String) As DataTable
            Dim dt As New DataTable
            Try
                Dim sql As String = "select cc.ServerName,cc.ServerIP,cc.MacAddress, cc.system_code, cc.online_status,cc.today_available, cc.ActiveStatus "
                sql += " from CF_CONFIG_COMPUTER_LIST cc "
                sql += " where cc.MacAddress=@_MACADDRESS"

                Dim p(1) As SqlParameter
                p(0) = SqlDB.SetText("@_MACADDRESS", MacAddress)
                dt = SqlDB.ExecuteTable(sql, p)
            Catch ex As Exception
                dt = New DataTable
            End Try
            dt.TableName = "GetConfigComputerInfo"
            Return dt
        End Function

        Private Shared Function GetAlarmInfoByAlarmProblem(AlarmProblem As String) As DataTable
            Dim ret As DataTable
            Try
                Dim sql As String = "select alarm_problem, alarm_code,eng_desc, sms_message"
                sql += " from MS_MASTER_MONITORING_ALARM "
                sql += " where alarm_problem=@_ALARM_PROBLEM"
                Dim p(1) As SqlParameter
                p(0) = SqlDB.SetText("@_ALARM_PROBLEM", AlarmProblem)

                ret = SqlDB.ExecuteTable(sql, p)

            Catch ex As Exception
                ret = New DataTable
            End Try
            Return ret
        End Function

    End Class
End Namespace