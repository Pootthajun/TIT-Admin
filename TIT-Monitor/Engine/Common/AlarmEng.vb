﻿Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Namespace Common
    Public Class AlarmEng
        Public Shared Function GetAlarmWaitingClear(ByVal ServerName As String, ByVal AlarmActivity As String, ByVal FlagAlarm As String) As DataTable
            Dim ret As New DataTable
            Dim sql As String = "select * from TB_ALARM_WAITING_CLEAR "
            sql += " where ServerName=@_SERVERNAME and AlarmActivity=@_ALARMACTIVITY and FlagAlarm=@_FLAGALARM"
            Dim p(3) As SqlParameter
            p(0) = SqlDB.SetText("@_SERVERNAME", ServerName)
            p(1) = SqlDB.SetText("@_ALARMACTIVITY", AlarmActivity)
            p(2) = SqlDB.SetText("@_FLAGALARM", FlagAlarm)
            ret = SqlDB.ExecuteTable(sql, p)
            Return ret
        End Function

        Public Shared Function InsertAlarmWaitingClear(ByVal ServerName As String, ByVal IPAddress As String, MacAddress As String, AlarmCode As String, ByVal Severity As String, ByVal AlarmValue As Double, ByVal AlarmActivity As String, ByVal AlarmMethod As String, ByVal SpecificProblem As String, MailAlias As String, MailSubject As String) As Long
            Dim frame As StackFrame = New StackFrame(1, True)
            Dim ClassName As String = frame.GetMethod.ReflectedType.Name
            Dim FunctionName As String = frame.GetMethod.Name
            Dim LineNo As Integer = frame.GetFileLineNumber

            Dim ret As Long = 0
            Try
                Dim lnq As New TbAlarmWaitingClearLinqDB
                lnq.SERVERNAME = ServerName
                lnq.HOSTIP = IPAddress
                lnq.MACADDRESS = MacAddress
                lnq.ALARMACTIVITY = AlarmActivity
                lnq.ALARMCODE = AlarmCode
                lnq.SEVERITY = Severity
                lnq.ALARMVALUE = AlarmValue
                lnq.ALARMMETHOD = AlarmMethod
                lnq.FLAGALARM = "Alarm"
                lnq.SPECIFICPROBLEM = SpecificProblem
                lnq.ALARMQTY = 1
                lnq.ISSENDALARM = "N"
                lnq.ISSENDCLEAR = "Z"
                lnq.MAIL_ALIAS = MailAlias
                lnq.MAIL_SUBJECT = MailSubject

                Dim trans As New TransactionDB

                If lnq.InsertData(ClassName & "." & FunctionName, trans.Trans).IsSuccess = True Then
                    trans.CommitTransaction()
                    ret = lnq.ID
                    InsertAlarmLog(ServerName, IPAddress, MacAddress, AlarmCode, AlarmActivity, Severity, AlarmValue, "Alarm", AlarmMethod, SpecificProblem, ret)
                Else
                    ret = 0
                    trans.RollbackTransaction()
                End If
                lnq = Nothing
            Catch ex As Exception
                ret = 0
            End Try

            Return ret
        End Function

        Public Shared Function UpdateAlarmWaitingClear(ByVal ServerName As String, ByVal IPAddress As String, MacAddress As String, AlarmCode As String, ByVal Severity As String, ByVal AlarmValue As Double, ByVal AlarmActivity As String, ByVal AlarmMethod As String, ByVal SpecificProblem As String, ByVal AlarmWaitingClearID As Long) As ExecuteDataInfo
            Dim frame As StackFrame = New StackFrame(1, True)
            Dim ClassName As String = frame.GetMethod.ReflectedType.Name
            Dim FunctionName As String = frame.GetMethod.Name
            Dim LineNo As Integer = frame.GetFileLineNumber

            Dim ret As New ExecuteDataInfo
            Try
                Dim lnq As New TbAlarmWaitingClearLinqDB
                lnq.GetDataByPK(AlarmWaitingClearID, Nothing)

                lnq.ALARMQTY = lnq.ALARMQTY + 1
                lnq.LASTUPDATEDATE = DateTime.Now

                If lnq.ID > 0 Then
                    Dim trans As New TransactionDB
                    ret = lnq.UpdateData(ClassName & "." & FunctionName, trans.Trans)
                    If ret.IsSuccess = True Then
                        trans.CommitTransaction()

                        InsertAlarmLog(ServerName, IPAddress, MacAddress, AlarmCode, AlarmActivity, Severity, AlarmValue, "Alarm", AlarmMethod, SpecificProblem, AlarmWaitingClearID)
                    Else
                        trans.RollbackTransaction()
                    End If
                End If
                lnq = Nothing
            Catch ex As Exception
                ret.IsSuccess = False
                ret.ErrorMessage = "Exception : " & ex.Message & ex.StackTrace
            End Try
            Return ret
        End Function

        Public Shared Sub InsertAlarmLog(ByVal ServerName As String, ByVal HostIP As String, MacAddress As String, AlarmCode As String, ByVal AlarmActivity As String, ByVal Severity As String, ByVal CurrentValue As String, ByVal FlagAlarm As String, ByVal AlarmMethod As String, ByVal SpecificPloblem As String, ByVal AlarmWaitingClearID As Integer)
            Dim frame As StackFrame = New StackFrame(1, True)
            Dim ClassName As String = frame.GetMethod.ReflectedType.Name
            Dim FunctionName As String = frame.GetMethod.Name
            Dim LineNo As Integer = frame.GetFileLineNumber

            Try
                Dim lnq As New TbAlarmLogLinqDB
                lnq.SERVERNAME = ServerName
                lnq.HOSTIP = HostIP
                lnq.MACADDRESS = MacAddress
                lnq.ALARMACTIVITY = AlarmActivity
                lnq.ALARMCODE = AlarmCode
                lnq.SEVERITY = Severity
                lnq.ALARMVALUE = CurrentValue
                lnq.ALARMMETHOD = AlarmMethod
                lnq.FLAGALARM = FlagAlarm
                lnq.SPECIFICPROBLEM = SpecificPloblem
                lnq.ALARMWAITINGCLEARID = AlarmWaitingClearID

                Dim trans As New TransactionDB
                If lnq.InsertData(ClassName & "." & FunctionName, trans.Trans).IsSuccess = True Then
                    trans.CommitTransaction()
                Else
                    trans.RollbackTransaction()
                End If
                lnq = Nothing
            Catch ex As Exception

            End Try
        End Sub

        Public Shared Function DeletePendingAlarm(ByVal AlarmActivity As String, ByVal MacAddress As String) As ExecuteDataInfo
            Dim sql As String = "delete from TB_ACTIVITY_PENDING_ALARM where MacAddress='" & MacAddress & "' and AlarmActivity='" & AlarmActivity & "' "
            Return SqlDB.ExecuteNonQuery(sql)
        End Function

        Public Shared Function SendClearAlarm(ByVal ServerName As String, ByVal HostIP As String, MacAddress As String, AlarmCode As String, ByVal Severity As String, ByVal AlarmValue As String, ByVal AlarmMethod As String, ByVal AlarmActivity As String, ByVal ClearMessage As String, ByVal AlarmWaitingClearID As Integer) As ExecuteDataInfo
            Dim frame As StackFrame = New StackFrame(1, True)
            Dim ClassName As String = frame.GetMethod.ReflectedType.Name
            Dim FunctionName As String = frame.GetMethod.Name
            Dim LineNo As Integer = frame.GetFileLineNumber

            Dim ret As New ExecuteDataInfo
            Try
                Dim lnq As New TbAlarmWaitingClearLinqDB
                lnq.GetDataByPK(AlarmWaitingClearID, Nothing)

                lnq.FLAGALARM = "Clear"
                lnq.CLEARDATE = DateTime.Now
                lnq.ISSENDCLEAR = "N"
                lnq.CLEARMESSAGE = ClearMessage

                If lnq.ID > 0 Then
                    Dim trans As New TransactionDB
                    ret = lnq.UpdateData(ClassName & "." & FunctionName, trans.Trans)
                    If ret.IsSuccess = True Then
                        trans.CommitTransaction()

                        InsertAlarmLog(ServerName, HostIP, MacAddress, AlarmCode, AlarmActivity, Severity, AlarmValue, "Clear", AlarmMethod, ClearMessage, AlarmWaitingClearID)
                    Else
                        trans.RollbackTransaction()
                    End If
                End If
                lnq = Nothing
            Catch ex As Exception
                ret.IsSuccess = False
                ret.ErrorMessage = "Exception : " & ex.Message & ex.StackTrace
            End Try
            Return ret
        End Function
    End Class
End Namespace

