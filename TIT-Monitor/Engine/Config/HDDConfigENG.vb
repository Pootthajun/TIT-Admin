﻿Imports System.IO
Imports System.Data
Imports LinqDB.TABLE

Namespace Config
    Public Class HDDConfigENG
        Inherits ConfigENG

        Dim _ShopName As String = ""
        Dim _ServerName As String = ""
        Dim _IPAddress As String = ""
        Dim _RepeateCheck As Int16 = 0
        Dim _ConfigType As String = ""
        Dim _AlarmType As String = ""
        Dim _SeverityAlarmDT As New DataTable
        Dim _IntervalMinute As Int16 = 0
        Dim _Active As String = ""
        Dim _CreateDate As String = ""
        Dim _ConfigList As New DataTable

        Public ReadOnly Property ShopName() As String
            Get
                Return _ShopName.Trim
            End Get
        End Property
        Public ReadOnly Property ServerName() As String
            Get
                Return _ServerName.Trim
            End Get
        End Property
        Public ReadOnly Property IPAddress() As String
            Get
                Return _IPAddress.Trim
            End Get
        End Property

        Public ReadOnly Property RepeateCheck() As Int16
            Get
                Return _RepeateCheck
            End Get
        End Property
        Public ReadOnly Property ConfigType() As String
            Get
                Return _ConfigType.Trim
            End Get
        End Property
        Public ReadOnly Property AlarmType() As String
            Get
                Return _AlarmType.Trim
            End Get
        End Property
        Public ReadOnly Property SeverityAlarmDT() As DataTable
            Get
                Return _SeverityAlarmDT
            End Get
        End Property
        Public ReadOnly Property IntervalMinute() As Int16
            Get
                Return _IntervalMinute
            End Get
        End Property
        Public ReadOnly Property Active() As String
            Get
                Return _Active.Trim
            End Get
        End Property
        Public ReadOnly Property CreateDate() As String
            Get
                Return _CreateDate.Trim
            End Get
        End Property
        Public ReadOnly Property ConfigList() As DataTable
            Get
                Return _ConfigList
            End Get
        End Property


        Public Shared Function GetHDDConfigList() As DataTable
            Dim dt As New DataTable
            Try
                Dim lnq As New CfConfigDriveLinqDB

                Dim sql As String = "select cd.id as cfDriveId,CDD.id as cfDriveDetailid "
                sql += "from CF_CONFIG_DRIVE CD "
                sql += " inner join  CF_CONFIG_DRIVE_DETAIL CDD on CDD.cf_config_drive_id = CD.id "
                sql += " where CD.ActiveStatus = 'Y' "

                Dim CaseDay As Integer = DatePart(DateInterval.Weekday, DateTime.Now)
                Select Case CaseDay
                    Case 1
                        sql += " and cd.AlarmSun ='Y'"
                    Case 2
                        sql += " and cd.AlarmMon ='Y'"
                    Case 3
                        sql += " and cd.AlarmTue ='Y'"
                    Case 4
                        sql += " and cd.AlarmWed ='Y'"
                    Case 5
                        sql += " and cd.AlarmThu ='Y'"
                    Case 6
                        sql += " and cd.AlarmFri ='Y'"
                    Case 7
                        sql += " and cd.AlarmSat ='Y'"
                End Select

                dt = lnq.GetListBySql(sql, Nothing, Nothing)
                lnq = Nothing

            Catch ex As Exception
                dt = New DataTable
            End Try
            Return dt
        End Function
    End Class
End Namespace

