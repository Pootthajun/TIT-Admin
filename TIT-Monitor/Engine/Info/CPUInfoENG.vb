﻿Namespace Info
    Public Class CPUInfoENG
        Dim _PercentUsage As Double = 0
        Dim _Temperature As Double = 0
        Dim _Err As String = ""

        Public ReadOnly Property PercentUsage() As Double
            Get
                Return _PercentUsage
            End Get
        End Property
        Public ReadOnly Property Temperature() As Double
            Get
                Return _Temperature
            End Get
        End Property

        Public ReadOnly Property ErrorMessage() As String
            Get
                Return _Err
            End Get
        End Property


        'Public Function GetCPUInfoXML() As XElement
        '    Dim CPUInfo As New XElement("CPUInfo")
        '    CPUInfo.Add(New XElement("PercentUsage", _PercentUsage))
        '    Return CPUInfo
        'End Function

        Public Sub New()
            'get CPU Usage
            Dim moReturn As Management.ManagementObjectCollection
            Dim moSearch As Management.ManagementObjectSearcher
            Dim mo As Management.ManagementObject
            moSearch = New Management.ManagementObjectSearcher("Select * from Win32_Processor")
            moReturn = moSearch.Get
            For Each mo In moReturn
                _PercentUsage += Convert.ToDouble(mo("LoadPercentage"))
            Next
            moSearch.Dispose()
            moReturn.Dispose()

            'get CPU Temperature
            Try
                _Temperature = 0
                Dim _cpu_core As Integer = 0
                Dim searcher As New Management.ManagementObjectSearcher("root\WMI", "SELECT * FROM MSAcpi_ThermalZoneTemperature")
                For Each queryObj As Management.ManagementObject In searcher.Get()
                    _Temperature += CDbl(queryObj("CurrentTemperature"))
                    _cpu_core += 1
                Next
                _Temperature = _Temperature / _cpu_core
                _Temperature = (_Temperature / 10) - 273.15

            Catch err As Management.ManagementException
                'MessageBox.Show("An error occurred while querying for WMI data: " & err.Message)
                _Err = "Management.ManagementException : " & err.Message & vbNewLine & err.StackTrace
            End Try
        End Sub



    End Class
End Namespace

