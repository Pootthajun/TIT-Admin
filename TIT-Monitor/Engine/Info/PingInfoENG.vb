﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports Engine.Common
Namespace Info
    Public Class PingInfoENG
#Region "Create Ping Log"
        Public Shared Function SavePingInfo(ServerIP As String, ServerName As String, MacAddress As String, ByVal Status As Char) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Try
                Dim trans As New TransactionDB
                Dim lnq As New TbPingInfoLinqDB
                lnq.ChkDataBySERVERIP(ServerIP, trans.Trans)
                If lnq.ID = 0 Then
                    lnq.ChkDataBySERVERNAME(ServerName, trans.Trans)

                    If lnq.ID = 0 Then
                        lnq.ChkDataByMACADDRESS(MacAddress, trans.Trans)
                    End If
                End If

                lnq.SERVERIP = ServerIP
                lnq.SERVERNAME = ServerName
                lnq.MACADDRESS = MacAddress
                lnq.STATUS = Status
                lnq.PINGNEXTTIME = DateAdd(DateInterval.Minute, 3, DateTime.Now)

                If lnq.ID > 0 Then
                    ret = lnq.UpdateData("PingInfoENG.SavePingInfo", trans.Trans)
                Else
                    ret = lnq.InsertData("PingInfoENG.SavePingInfo", trans.Trans)
                End If

                If ret.IsSuccess = True Then
                    ret = CreatePingLog(ServerIP, ServerName, MacAddress, Status, trans)
                    If ret.IsSuccess = True Then
                        If UpdateAvailableData(MacAddress, ServerName, ServerIP, Status, trans) = True Then
                            trans.CommitTransaction()
                        Else
                            trans.RollbackTransaction()
                        End If
                    Else
                        trans.RollbackTransaction()
                    End If
                Else
                    trans.RollbackTransaction()
                    FunctionEng.CreateErrorLog(lnq.ErrorMessage)
                End If
                lnq = Nothing
            Catch ex As Exception
                ret.IsSuccess = False
                ret.ErrorMessage = "Exception : " & ex.Message & ex.StackTrace
                'FunctionEng.CreateErrorLog("frmNewMain", "SavePingInfo", "Exception :" & ex.Message & vbNewLine & ex.StackTrace)
            End Try
            Return ret
        End Function

        Private Shared Function UpdateAvailableData(MacAddress As String, ServerName As String, IPAddress As String, Status As Char, trans As TransactionDB) As Boolean
            Dim ret As Boolean = False
            Try
                'หา Today Available
                Dim vData As String = DateTime.Now.ToString("yyyyMMdd", New Globalization.CultureInfo("en-US"))
                Dim sql As String = "Select (sum(case p.Status when 'Y' then 1 else 0 end)*100)/count(p.id) available"
                sql += " from TB_PING_LOG P "
                sql += " where Convert(Varchar(8), P.CreatedDate,112)= '" & vData & "'"
                sql += " and p.MacAddress='" & MacAddress & "'"

                Dim TodayAvailable As Long = 0
                Dim dt As DataTable = SqlDB.ExecuteTable(sql, trans.Trans)
                If dt.Rows.Count > 0 Then
                    If Convert.IsDBNull(dt.Rows(0)("available")) = False Then
                        TodayAvailable = Convert.ToInt64(dt.Rows(0)("available"))
                    Else
                        TodayAvailable = 0
                    End If
                Else
                    TodayAvailable = 0
                End If
                dt.Dispose()

                Dim ccLnq As New CfConfigComputerListLinqDB
                ccLnq.ChkDataByMACADDRESS(MacAddress, trans.Trans)
                ccLnq.MACADDRESS = MacAddress
                ccLnq.SERVERNAME = ServerName
                ccLnq.SERVERIP = IPAddress
                ccLnq.ONLINE_STATUS = Status
                ccLnq.TODAY_AVAILABLE = TodayAvailable

                If ccLnq.ID > 0 Then
                    ret = ccLnq.UpdateData(MacAddress, trans.Trans).IsSuccess
                Else
                    ccLnq.ACTIVESTATUS = "Y"
                    ret = ccLnq.InsertData(MacAddress, trans.Trans).IsSuccess
                End If
                'Dim uSql As String = "update CF_CONFIG_COMPUTER_LIST "
                'uSql += " set today_available='" & TodayAvailable & "'"
                'uSql += ", online_status = '" & Status & "'"
                'uSql += " where MacAddress='" & MacAddress & "'"
                'ret = SqlDB.ExecuteNonQuery(uSql, trans.Trans).IsSuccess

            Catch ex As Exception
                ret = False
                FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
            End Try
            Return ret
        End Function

        Private Shared Function CreatePingLog(ByVal ip As String, ServerName As String, MacAddress As String, ByVal Status As Char, trans As TransactionDB) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Try
                'Dim trans As New TransactionDB
                Dim lnq As New LinqDB.TABLE.TbPingLogLinqDB

                lnq.SERVERIP = ip
                lnq.SERVERNAME = ServerName
                lnq.MACADDRESS = MacAddress
                lnq.STATUS = Status
                ret = lnq.InsertData("frmNewMain.CreatePing", trans.Trans)
                If ret.IsSuccess = False Then
                    FunctionEng.CreateErrorLog(lnq.ErrorMessage)
                End If
                lnq = Nothing
            Catch ex As Exception
                ret.IsSuccess = False
                ret.ErrorMessage = "Exception : " & ex.Message & ex.StackTrace
                FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
            End Try
            Return ret
        End Function
#End Region

    End Class
End Namespace

