Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_DRIVE_LOG table LinqDB.
    '[Create by  on June, 9 2016]
    Public Class TbDriveLogLinqDB
        Public sub TbDriveLogLinqDB()

        End Sub 
        ' TB_DRIVE_LOG
        Const _tableName As String = "TB_DRIVE_LOG"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _CREATEDDATE As DateTime = New DateTime(1,1,1)
        Dim _CREATEDBY As String = ""
        Dim _UPDATEDDATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATEDBY As  String  = ""
        Dim _SENDTIME As DateTime = New DateTime(1,1,1)
        Dim _SERVERNAME As String = ""
        Dim _SERVERIP As String = ""
        Dim _MACADDRESS As String = ""
        Dim _DRIVELETTER As String = ""
        Dim _VOLUMNLABEL As  String  = ""
        Dim _FREESPACEGB As Double = 0
        Dim _TOTALSIZEGB As Double = 0
        Dim _PERCENTUSAGE As Double = 0

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_CREATEDDATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATEDDATE() As DateTime
            Get
                Return _CREATEDDATE
            End Get
            Set(ByVal value As DateTime)
               _CREATEDDATE = value
            End Set
        End Property 
        <Column(Storage:="_CREATEDBY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATEDBY() As String
            Get
                Return _CREATEDBY
            End Get
            Set(ByVal value As String)
               _CREATEDBY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATEDDATE", DbType:="DateTime")>  _
        Public Property UPDATEDDATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATEDDATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATEDDATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATEDBY", DbType:="VarChar(100)")>  _
        Public Property UPDATEDBY() As  String 
            Get
                Return _UPDATEDBY
            End Get
            Set(ByVal value As  String )
               _UPDATEDBY = value
            End Set
        End Property 
        <Column(Storage:="_SENDTIME", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property SENDTIME() As DateTime
            Get
                Return _SENDTIME
            End Get
            Set(ByVal value As DateTime)
               _SENDTIME = value
            End Set
        End Property 
        <Column(Storage:="_SERVERNAME", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property SERVERNAME() As String
            Get
                Return _SERVERNAME
            End Get
            Set(ByVal value As String)
               _SERVERNAME = value
            End Set
        End Property 
        <Column(Storage:="_SERVERIP", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property SERVERIP() As String
            Get
                Return _SERVERIP
            End Get
            Set(ByVal value As String)
               _SERVERIP = value
            End Set
        End Property 
        <Column(Storage:="_MACADDRESS", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property MACADDRESS() As String
            Get
                Return _MACADDRESS
            End Get
            Set(ByVal value As String)
               _MACADDRESS = value
            End Set
        End Property 
        <Column(Storage:="_DRIVELETTER", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property DRIVELETTER() As String
            Get
                Return _DRIVELETTER
            End Get
            Set(ByVal value As String)
               _DRIVELETTER = value
            End Set
        End Property 
        <Column(Storage:="_VOLUMNLABEL", DbType:="VarChar(50)")>  _
        Public Property VOLUMNLABEL() As  String 
            Get
                Return _VOLUMNLABEL
            End Get
            Set(ByVal value As  String )
               _VOLUMNLABEL = value
            End Set
        End Property 
        <Column(Storage:="_FREESPACEGB", DbType:="Float NOT NULL ",CanBeNull:=false)>  _
        Public Property FREESPACEGB() As Double
            Get
                Return _FREESPACEGB
            End Get
            Set(ByVal value As Double)
               _FREESPACEGB = value
            End Set
        End Property 
        <Column(Storage:="_TOTALSIZEGB", DbType:="Float NOT NULL ",CanBeNull:=false)>  _
        Public Property TOTALSIZEGB() As Double
            Get
                Return _TOTALSIZEGB
            End Get
            Set(ByVal value As Double)
               _TOTALSIZEGB = value
            End Set
        End Property 
        <Column(Storage:="_PERCENTUSAGE", DbType:="Float NOT NULL ",CanBeNull:=false)>  _
        Public Property PERCENTUSAGE() As Double
            Get
                Return _PERCENTUSAGE
            End Get
            Set(ByVal value As Double)
               _PERCENTUSAGE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _CREATEDDATE = New DateTime(1,1,1)
            _CREATEDBY = ""
            _UPDATEDDATE = New DateTime(1,1,1)
            _UPDATEDBY = ""
            _SENDTIME = New DateTime(1,1,1)
            _SERVERNAME = ""
            _SERVERIP = ""
            _MACADDRESS = ""
            _DRIVELETTER = ""
            _VOLUMNLABEL = ""
            _FREESPACEGB = 0
            _TOTALSIZEGB = 0
            _PERCENTUSAGE = 0
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_DRIVE_LOG table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _CreatedBy = CreatedBy
                _CreatedDate = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_DRIVE_LOG table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATEDBY = UpdatedBy
                    _UPDATEDDATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_DRIVE_LOG table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_DRIVE_LOG table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_DRIVE_LOG by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_DRIVE_LOG by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As TbDriveLogLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_DRIVE_LOG by specified DRIVELETTER_SERVERNAME key is retrieved successfully.
        '/// <param name=cDRIVELETTER_SERVERNAME>The DRIVELETTER_SERVERNAME key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByDRIVELETTER_SERVERNAME(cDRIVELETTER As String, cSERVERNAME As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(3)  As SQLParameter
            cmdPara(0) = DB.SetText("@_DRIVELETTER", cDRIVELETTER) 
            cmdPara(1) = DB.SetText("@_SERVERNAME", cSERVERNAME) 
            Return doChkData("DRIVELETTER = @_DRIVELETTER AND SERVERNAME = @_SERVERNAME", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_DRIVE_LOG by specified DRIVELETTER_SERVERNAME key is retrieved successfully.
        '/// <param name=cDRIVELETTER_SERVERNAME>The DRIVELETTER_SERVERNAME key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByDRIVELETTER_SERVERNAME(cDRIVELETTER As String, cSERVERNAME As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(3)  As SQLParameter
            cmdPara(0) = DB.SetText("@_DRIVELETTER", cDRIVELETTER) 
            cmdPara(1) = DB.SetText("@_SERVERNAME", cSERVERNAME) 
            cmdPara(2) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("DRIVELETTER = @_DRIVELETTER AND SERVERNAME = @_SERVERNAME And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_DRIVE_LOG by specified DRIVELETTER_SERVERIP key is retrieved successfully.
        '/// <param name=cDRIVELETTER_SERVERIP>The DRIVELETTER_SERVERIP key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByDRIVELETTER_SERVERIP(cDRIVELETTER As String, cSERVERIP As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(3)  As SQLParameter
            cmdPara(0) = DB.SetText("@_DRIVELETTER", cDRIVELETTER) 
            cmdPara(1) = DB.SetText("@_SERVERIP", cSERVERIP) 
            Return doChkData("DRIVELETTER = @_DRIVELETTER AND SERVERIP = @_SERVERIP", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_DRIVE_LOG by specified DRIVELETTER_SERVERIP key is retrieved successfully.
        '/// <param name=cDRIVELETTER_SERVERIP>The DRIVELETTER_SERVERIP key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByDRIVELETTER_SERVERIP(cDRIVELETTER As String, cSERVERIP As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(3)  As SQLParameter
            cmdPara(0) = DB.SetText("@_DRIVELETTER", cDRIVELETTER) 
            cmdPara(1) = DB.SetText("@_SERVERIP", cSERVERIP) 
            cmdPara(2) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("DRIVELETTER = @_DRIVELETTER AND SERVERIP = @_SERVERIP And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_DRIVE_LOG by specified DRIVELETTER_MACADDRESS key is retrieved successfully.
        '/// <param name=cDRIVELETTER_MACADDRESS>The DRIVELETTER_MACADDRESS key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByDRIVELETTER_MACADDRESS(cDRIVELETTER As String, cMACADDRESS As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(3)  As SQLParameter
            cmdPara(0) = DB.SetText("@_DRIVELETTER", cDRIVELETTER) 
            cmdPara(1) = DB.SetText("@_MACADDRESS", cMACADDRESS) 
            Return doChkData("DRIVELETTER = @_DRIVELETTER AND MACADDRESS = @_MACADDRESS", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_DRIVE_LOG by specified DRIVELETTER_MACADDRESS key is retrieved successfully.
        '/// <param name=cDRIVELETTER_MACADDRESS>The DRIVELETTER_MACADDRESS key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByDRIVELETTER_MACADDRESS(cDRIVELETTER As String, cMACADDRESS As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(3)  As SQLParameter
            cmdPara(0) = DB.SetText("@_DRIVELETTER", cDRIVELETTER) 
            cmdPara(1) = DB.SetText("@_MACADDRESS", cMACADDRESS) 
            cmdPara(2) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("DRIVELETTER = @_DRIVELETTER AND MACADDRESS = @_MACADDRESS And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_DRIVE_LOG by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_DRIVE_LOG table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_DRIVE_LOG table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_DRIVE_LOG table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(13) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_CREATEDDATE", SqlDbType.DateTime)
            cmbParam(1).Value = _CREATEDDATE

            cmbParam(2) = New SqlParameter("@_CREATEDBY", SqlDbType.VarChar)
            cmbParam(2).Value = _CREATEDBY

            cmbParam(3) = New SqlParameter("@_UPDATEDDATE", SqlDbType.DateTime)
            If _UPDATEDDATE.Value.Year > 1 Then 
                cmbParam(3).Value = _UPDATEDDATE.Value
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_UPDATEDBY", SqlDbType.VarChar)
            If _UPDATEDBY.Trim <> "" Then 
                cmbParam(4).Value = _UPDATEDBY
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_SENDTIME", SqlDbType.DateTime)
            cmbParam(5).Value = _SENDTIME

            cmbParam(6) = New SqlParameter("@_SERVERNAME", SqlDbType.VarChar)
            cmbParam(6).Value = _SERVERNAME

            cmbParam(7) = New SqlParameter("@_SERVERIP", SqlDbType.VarChar)
            cmbParam(7).Value = _SERVERIP

            cmbParam(8) = New SqlParameter("@_MACADDRESS", SqlDbType.VarChar)
            cmbParam(8).Value = _MACADDRESS

            cmbParam(9) = New SqlParameter("@_DRIVELETTER", SqlDbType.VarChar)
            cmbParam(9).Value = _DRIVELETTER

            cmbParam(10) = New SqlParameter("@_VOLUMNLABEL", SqlDbType.VarChar)
            If _VOLUMNLABEL.Trim <> "" Then 
                cmbParam(10).Value = _VOLUMNLABEL
            Else
                cmbParam(10).Value = DBNull.value
            End If

            cmbParam(11) = New SqlParameter("@_FREESPACEGB", SqlDbType.Float)
            cmbParam(11).Value = _FREESPACEGB

            cmbParam(12) = New SqlParameter("@_TOTALSIZEGB", SqlDbType.Float)
            cmbParam(12).Value = _TOTALSIZEGB

            cmbParam(13) = New SqlParameter("@_PERCENTUSAGE", SqlDbType.Float)
            cmbParam(13).Value = _PERCENTUSAGE

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_DRIVE_LOG by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("CreatedDate")) = False Then _CreatedDate = Convert.ToDateTime(Rdr("CreatedDate"))
                        If Convert.IsDBNull(Rdr("CreatedBy")) = False Then _CreatedBy = Rdr("CreatedBy").ToString()
                        If Convert.IsDBNull(Rdr("UpdatedDate")) = False Then _UpdatedDate = Convert.ToDateTime(Rdr("UpdatedDate"))
                        If Convert.IsDBNull(Rdr("UpdatedBy")) = False Then _UpdatedBy = Rdr("UpdatedBy").ToString()
                        If Convert.IsDBNull(Rdr("SendTime")) = False Then _SendTime = Convert.ToDateTime(Rdr("SendTime"))
                        If Convert.IsDBNull(Rdr("ServerName")) = False Then _ServerName = Rdr("ServerName").ToString()
                        If Convert.IsDBNull(Rdr("ServerIP")) = False Then _ServerIP = Rdr("ServerIP").ToString()
                        If Convert.IsDBNull(Rdr("MacAddress")) = False Then _MacAddress = Rdr("MacAddress").ToString()
                        If Convert.IsDBNull(Rdr("DriveLetter")) = False Then _DriveLetter = Rdr("DriveLetter").ToString()
                        If Convert.IsDBNull(Rdr("VolumnLabel")) = False Then _VolumnLabel = Rdr("VolumnLabel").ToString()
                        If Convert.IsDBNull(Rdr("FreeSpaceGB")) = False Then _FreeSpaceGB = Convert.ToDouble(Rdr("FreeSpaceGB"))
                        If Convert.IsDBNull(Rdr("TotalSizeGB")) = False Then _TotalSizeGB = Convert.ToDouble(Rdr("TotalSizeGB"))
                        If Convert.IsDBNull(Rdr("PercentUsage")) = False Then _PercentUsage = Convert.ToDouble(Rdr("PercentUsage"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_DRIVE_LOG by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbDriveLogLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("CreatedDate")) = False Then _CreatedDate = Convert.ToDateTime(Rdr("CreatedDate"))
                        If Convert.IsDBNull(Rdr("CreatedBy")) = False Then _CreatedBy = Rdr("CreatedBy").ToString()
                        If Convert.IsDBNull(Rdr("UpdatedDate")) = False Then _UpdatedDate = Convert.ToDateTime(Rdr("UpdatedDate"))
                        If Convert.IsDBNull(Rdr("UpdatedBy")) = False Then _UpdatedBy = Rdr("UpdatedBy").ToString()
                        If Convert.IsDBNull(Rdr("SendTime")) = False Then _SendTime = Convert.ToDateTime(Rdr("SendTime"))
                        If Convert.IsDBNull(Rdr("ServerName")) = False Then _ServerName = Rdr("ServerName").ToString()
                        If Convert.IsDBNull(Rdr("ServerIP")) = False Then _ServerIP = Rdr("ServerIP").ToString()
                        If Convert.IsDBNull(Rdr("MacAddress")) = False Then _MacAddress = Rdr("MacAddress").ToString()
                        If Convert.IsDBNull(Rdr("DriveLetter")) = False Then _DriveLetter = Rdr("DriveLetter").ToString()
                        If Convert.IsDBNull(Rdr("VolumnLabel")) = False Then _VolumnLabel = Rdr("VolumnLabel").ToString()
                        If Convert.IsDBNull(Rdr("FreeSpaceGB")) = False Then _FreeSpaceGB = Convert.ToDouble(Rdr("FreeSpaceGB"))
                        If Convert.IsDBNull(Rdr("TotalSizeGB")) = False Then _TotalSizeGB = Convert.ToDouble(Rdr("TotalSizeGB"))
                        If Convert.IsDBNull(Rdr("PercentUsage")) = False Then _PercentUsage = Convert.ToDouble(Rdr("PercentUsage"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_DRIVE_LOG
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (CREATEDDATE, CREATEDBY, SENDTIME, SERVERNAME, SERVERIP, MACADDRESS, DRIVELETTER, VOLUMNLABEL, FREESPACEGB, TOTALSIZEGB, PERCENTUSAGE)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.CREATEDDATE, INSERTED.CREATEDBY, INSERTED.UPDATEDDATE, INSERTED.UPDATEDBY, INSERTED.SENDTIME, INSERTED.SERVERNAME, INSERTED.SERVERIP, INSERTED.MACADDRESS, INSERTED.DRIVELETTER, INSERTED.VOLUMNLABEL, INSERTED.FREESPACEGB, INSERTED.TOTALSIZEGB, INSERTED.PERCENTUSAGE"
                Sql += " VALUES("
                sql += "@_CREATEDDATE" & ", "
                sql += "@_CREATEDBY" & ", "
                sql += "@_SENDTIME" & ", "
                sql += "@_SERVERNAME" & ", "
                sql += "@_SERVERIP" & ", "
                sql += "@_MACADDRESS" & ", "
                sql += "@_DRIVELETTER" & ", "
                sql += "@_VOLUMNLABEL" & ", "
                sql += "@_FREESPACEGB" & ", "
                sql += "@_TOTALSIZEGB" & ", "
                sql += "@_PERCENTUSAGE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_DRIVE_LOG
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "UPDATEDDATE = " & "@_UPDATEDDATE" & ", "
                Sql += "UPDATEDBY = " & "@_UPDATEDBY" & ", "
                Sql += "SENDTIME = " & "@_SENDTIME" & ", "
                Sql += "SERVERNAME = " & "@_SERVERNAME" & ", "
                Sql += "SERVERIP = " & "@_SERVERIP" & ", "
                Sql += "MACADDRESS = " & "@_MACADDRESS" & ", "
                Sql += "DRIVELETTER = " & "@_DRIVELETTER" & ", "
                Sql += "VOLUMNLABEL = " & "@_VOLUMNLABEL" & ", "
                Sql += "FREESPACEGB = " & "@_FREESPACEGB" & ", "
                Sql += "TOTALSIZEGB = " & "@_TOTALSIZEGB" & ", "
                Sql += "PERCENTUSAGE = " & "@_PERCENTUSAGE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_DRIVE_LOG
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_DRIVE_LOG
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, SENDTIME, SERVERNAME, SERVERIP, MACADDRESS, DRIVELETTER, VOLUMNLABEL, FREESPACEGB, TOTALSIZEGB, PERCENTUSAGE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
