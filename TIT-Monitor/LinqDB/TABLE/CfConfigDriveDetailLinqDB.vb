Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for CF_CONFIG_DRIVE_DETAIL table LinqDB.
    '[Create by  on June, 9 2016]
    Public Class CfConfigDriveDetailLinqDB
        Public sub CfConfigDriveDetailLinqDB()

        End Sub 
        ' CF_CONFIG_DRIVE_DETAIL
        Const _tableName As String = "CF_CONFIG_DRIVE_DETAIL"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _CREATEDDATE As DateTime = New DateTime(1,1,1)
        Dim _CREATEDBY As String = ""
        Dim _UPDATEDDATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATEDBY As  String  = ""
        Dim _CF_CONFIG_DRIVE_ID As  System.Nullable(Of Long) 
        Dim _DRIVELETTER As String = ""
        Dim _ALARMMINORVALUE As Long = 0
        Dim _ALARMMAJORVALUE As Long = 0
        Dim _ALARMCRITICALVALUE As Long = 0
        Dim _REPEATCHECKMINOR As Long = 0
        Dim _REPEATCHECKMAJOR As Long = 0
        Dim _REPEATCHECKCRITICAL As Long = 0

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_CREATEDDATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATEDDATE() As DateTime
            Get
                Return _CREATEDDATE
            End Get
            Set(ByVal value As DateTime)
               _CREATEDDATE = value
            End Set
        End Property 
        <Column(Storage:="_CREATEDBY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATEDBY() As String
            Get
                Return _CREATEDBY
            End Get
            Set(ByVal value As String)
               _CREATEDBY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATEDDATE", DbType:="DateTime")>  _
        Public Property UPDATEDDATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATEDDATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATEDDATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATEDBY", DbType:="VarChar(100)")>  _
        Public Property UPDATEDBY() As  String 
            Get
                Return _UPDATEDBY
            End Get
            Set(ByVal value As  String )
               _UPDATEDBY = value
            End Set
        End Property 
        <Column(Storage:="_CF_CONFIG_DRIVE_ID", DbType:="BigInt")>  _
        Public Property CF_CONFIG_DRIVE_ID() As  System.Nullable(Of Long) 
            Get
                Return _CF_CONFIG_DRIVE_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _CF_CONFIG_DRIVE_ID = value
            End Set
        End Property 
        <Column(Storage:="_DRIVELETTER", DbType:="VarChar(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property DRIVELETTER() As String
            Get
                Return _DRIVELETTER
            End Get
            Set(ByVal value As String)
               _DRIVELETTER = value
            End Set
        End Property 
        <Column(Storage:="_ALARMMINORVALUE", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMMINORVALUE() As Long
            Get
                Return _ALARMMINORVALUE
            End Get
            Set(ByVal value As Long)
               _ALARMMINORVALUE = value
            End Set
        End Property 
        <Column(Storage:="_ALARMMAJORVALUE", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMMAJORVALUE() As Long
            Get
                Return _ALARMMAJORVALUE
            End Get
            Set(ByVal value As Long)
               _ALARMMAJORVALUE = value
            End Set
        End Property 
        <Column(Storage:="_ALARMCRITICALVALUE", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMCRITICALVALUE() As Long
            Get
                Return _ALARMCRITICALVALUE
            End Get
            Set(ByVal value As Long)
               _ALARMCRITICALVALUE = value
            End Set
        End Property 
        <Column(Storage:="_REPEATCHECKMINOR", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property REPEATCHECKMINOR() As Long
            Get
                Return _REPEATCHECKMINOR
            End Get
            Set(ByVal value As Long)
               _REPEATCHECKMINOR = value
            End Set
        End Property 
        <Column(Storage:="_REPEATCHECKMAJOR", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property REPEATCHECKMAJOR() As Long
            Get
                Return _REPEATCHECKMAJOR
            End Get
            Set(ByVal value As Long)
               _REPEATCHECKMAJOR = value
            End Set
        End Property 
        <Column(Storage:="_REPEATCHECKCRITICAL", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property REPEATCHECKCRITICAL() As Long
            Get
                Return _REPEATCHECKCRITICAL
            End Get
            Set(ByVal value As Long)
               _REPEATCHECKCRITICAL = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _CREATEDDATE = New DateTime(1,1,1)
            _CREATEDBY = ""
            _UPDATEDDATE = New DateTime(1,1,1)
            _UPDATEDBY = ""
            _CF_CONFIG_DRIVE_ID = Nothing
            _DRIVELETTER = ""
            _ALARMMINORVALUE = 0
            _ALARMMAJORVALUE = 0
            _ALARMCRITICALVALUE = 0
            _REPEATCHECKMINOR = 0
            _REPEATCHECKMAJOR = 0
            _REPEATCHECKCRITICAL = 0
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into CF_CONFIG_DRIVE_DETAIL table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _ID = DB.GetNextID("ID",tableName, trans)
                _CreatedBy = CreatedBy
                _CreatedDate = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to CF_CONFIG_DRIVE_DETAIL table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATEDBY = UpdatedBy
                    _UPDATEDDATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to CF_CONFIG_DRIVE_DETAIL table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from CF_CONFIG_DRIVE_DETAIL table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_DRIVE_DETAIL by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of CF_CONFIG_DRIVE_DETAIL by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As CfConfigDriveDetailLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_DRIVE_DETAIL by specified CF_CONFIG_DRIVE_ID_DRIVELETTER key is retrieved successfully.
        '/// <param name=cCF_CONFIG_DRIVE_ID_DRIVELETTER>The CF_CONFIG_DRIVE_ID_DRIVELETTER key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByCF_CONFIG_DRIVE_ID_DRIVELETTER(cCF_CONFIG_DRIVE_ID As Long, cDRIVELETTER As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(3)  As SQLParameter
            cmdPara(0) = DB.SetText("@_CF_CONFIG_DRIVE_ID", cCF_CONFIG_DRIVE_ID) 
            cmdPara(1) = DB.SetText("@_DRIVELETTER", cDRIVELETTER) 
            Return doChkData("CF_CONFIG_DRIVE_ID = @_CF_CONFIG_DRIVE_ID AND DRIVELETTER = @_DRIVELETTER", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of CF_CONFIG_DRIVE_DETAIL by specified CF_CONFIG_DRIVE_ID_DRIVELETTER key is retrieved successfully.
        '/// <param name=cCF_CONFIG_DRIVE_ID_DRIVELETTER>The CF_CONFIG_DRIVE_ID_DRIVELETTER key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByCF_CONFIG_DRIVE_ID_DRIVELETTER(cCF_CONFIG_DRIVE_ID As Long, cDRIVELETTER As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(3)  As SQLParameter
            cmdPara(0) = DB.SetText("@_CF_CONFIG_DRIVE_ID", cCF_CONFIG_DRIVE_ID) 
            cmdPara(1) = DB.SetText("@_DRIVELETTER", cDRIVELETTER) 
            cmdPara(2) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("CF_CONFIG_DRIVE_ID = @_CF_CONFIG_DRIVE_ID AND DRIVELETTER = @_DRIVELETTER And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_DRIVE_DETAIL by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into CF_CONFIG_DRIVE_DETAIL table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to CF_CONFIG_DRIVE_DETAIL table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from CF_CONFIG_DRIVE_DETAIL table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(12) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_CREATEDDATE", SqlDbType.DateTime)
            cmbParam(1).Value = _CREATEDDATE

            cmbParam(2) = New SqlParameter("@_CREATEDBY", SqlDbType.VarChar)
            cmbParam(2).Value = _CREATEDBY

            cmbParam(3) = New SqlParameter("@_UPDATEDDATE", SqlDbType.DateTime)
            If _UPDATEDDATE.Value.Year > 1 Then 
                cmbParam(3).Value = _UPDATEDDATE.Value
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_UPDATEDBY", SqlDbType.VarChar)
            If _UPDATEDBY.Trim <> "" Then 
                cmbParam(4).Value = _UPDATEDBY
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_CF_CONFIG_DRIVE_ID", SqlDbType.BigInt)
            If _CF_CONFIG_DRIVE_ID IsNot Nothing Then 
                cmbParam(5).Value = _CF_CONFIG_DRIVE_ID.Value
            Else
                cmbParam(5).Value = DBNull.value
            End IF

            cmbParam(6) = New SqlParameter("@_DRIVELETTER", SqlDbType.VarChar)
            cmbParam(6).Value = _DRIVELETTER

            cmbParam(7) = New SqlParameter("@_ALARMMINORVALUE", SqlDbType.Int)
            cmbParam(7).Value = _ALARMMINORVALUE

            cmbParam(8) = New SqlParameter("@_ALARMMAJORVALUE", SqlDbType.Int)
            cmbParam(8).Value = _ALARMMAJORVALUE

            cmbParam(9) = New SqlParameter("@_ALARMCRITICALVALUE", SqlDbType.Int)
            cmbParam(9).Value = _ALARMCRITICALVALUE

            cmbParam(10) = New SqlParameter("@_REPEATCHECKMINOR", SqlDbType.Int)
            cmbParam(10).Value = _REPEATCHECKMINOR

            cmbParam(11) = New SqlParameter("@_REPEATCHECKMAJOR", SqlDbType.Int)
            cmbParam(11).Value = _REPEATCHECKMAJOR

            cmbParam(12) = New SqlParameter("@_REPEATCHECKCRITICAL", SqlDbType.Int)
            cmbParam(12).Value = _REPEATCHECKCRITICAL

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_DRIVE_DETAIL by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("CreatedDate")) = False Then _CreatedDate = Convert.ToDateTime(Rdr("CreatedDate"))
                        If Convert.IsDBNull(Rdr("CreatedBy")) = False Then _CreatedBy = Rdr("CreatedBy").ToString()
                        If Convert.IsDBNull(Rdr("UpdatedDate")) = False Then _UpdatedDate = Convert.ToDateTime(Rdr("UpdatedDate"))
                        If Convert.IsDBNull(Rdr("UpdatedBy")) = False Then _UpdatedBy = Rdr("UpdatedBy").ToString()
                        If Convert.IsDBNull(Rdr("cf_config_drive_id")) = False Then _cf_config_drive_id = Convert.ToInt64(Rdr("cf_config_drive_id"))
                        If Convert.IsDBNull(Rdr("DriveLetter")) = False Then _DriveLetter = Rdr("DriveLetter").ToString()
                        If Convert.IsDBNull(Rdr("AlarmMinorValue")) = False Then _AlarmMinorValue = Convert.ToInt32(Rdr("AlarmMinorValue"))
                        If Convert.IsDBNull(Rdr("AlarmMajorValue")) = False Then _AlarmMajorValue = Convert.ToInt32(Rdr("AlarmMajorValue"))
                        If Convert.IsDBNull(Rdr("AlarmCriticalValue")) = False Then _AlarmCriticalValue = Convert.ToInt32(Rdr("AlarmCriticalValue"))
                        If Convert.IsDBNull(Rdr("RepeatCheckMinor")) = False Then _RepeatCheckMinor = Convert.ToInt32(Rdr("RepeatCheckMinor"))
                        If Convert.IsDBNull(Rdr("RepeatCheckMajor")) = False Then _RepeatCheckMajor = Convert.ToInt32(Rdr("RepeatCheckMajor"))
                        If Convert.IsDBNull(Rdr("RepeatCheckCritical")) = False Then _RepeatCheckCritical = Convert.ToInt32(Rdr("RepeatCheckCritical"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_DRIVE_DETAIL by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As CfConfigDriveDetailLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("CreatedDate")) = False Then _CreatedDate = Convert.ToDateTime(Rdr("CreatedDate"))
                        If Convert.IsDBNull(Rdr("CreatedBy")) = False Then _CreatedBy = Rdr("CreatedBy").ToString()
                        If Convert.IsDBNull(Rdr("UpdatedDate")) = False Then _UpdatedDate = Convert.ToDateTime(Rdr("UpdatedDate"))
                        If Convert.IsDBNull(Rdr("UpdatedBy")) = False Then _UpdatedBy = Rdr("UpdatedBy").ToString()
                        If Convert.IsDBNull(Rdr("cf_config_drive_id")) = False Then _cf_config_drive_id = Convert.ToInt64(Rdr("cf_config_drive_id"))
                        If Convert.IsDBNull(Rdr("DriveLetter")) = False Then _DriveLetter = Rdr("DriveLetter").ToString()
                        If Convert.IsDBNull(Rdr("AlarmMinorValue")) = False Then _AlarmMinorValue = Convert.ToInt32(Rdr("AlarmMinorValue"))
                        If Convert.IsDBNull(Rdr("AlarmMajorValue")) = False Then _AlarmMajorValue = Convert.ToInt32(Rdr("AlarmMajorValue"))
                        If Convert.IsDBNull(Rdr("AlarmCriticalValue")) = False Then _AlarmCriticalValue = Convert.ToInt32(Rdr("AlarmCriticalValue"))
                        If Convert.IsDBNull(Rdr("RepeatCheckMinor")) = False Then _RepeatCheckMinor = Convert.ToInt32(Rdr("RepeatCheckMinor"))
                        If Convert.IsDBNull(Rdr("RepeatCheckMajor")) = False Then _RepeatCheckMajor = Convert.ToInt32(Rdr("RepeatCheckMajor"))
                        If Convert.IsDBNull(Rdr("RepeatCheckCritical")) = False Then _RepeatCheckCritical = Convert.ToInt32(Rdr("RepeatCheckCritical"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table CF_CONFIG_DRIVE_DETAIL
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (ID, CREATEDDATE, CREATEDBY, CF_CONFIG_DRIVE_ID, DRIVELETTER, ALARMMINORVALUE, ALARMMAJORVALUE, ALARMCRITICALVALUE, REPEATCHECKMINOR, REPEATCHECKMAJOR, REPEATCHECKCRITICAL)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.CREATEDDATE, INSERTED.CREATEDBY, INSERTED.UPDATEDDATE, INSERTED.UPDATEDBY, INSERTED.CF_CONFIG_DRIVE_ID, INSERTED.DRIVELETTER, INSERTED.ALARMMINORVALUE, INSERTED.ALARMMAJORVALUE, INSERTED.ALARMCRITICALVALUE, INSERTED.REPEATCHECKMINOR, INSERTED.REPEATCHECKMAJOR, INSERTED.REPEATCHECKCRITICAL"
                Sql += " VALUES("
                sql += "@_ID" & ", "
                sql += "@_CREATEDDATE" & ", "
                sql += "@_CREATEDBY" & ", "
                sql += "@_CF_CONFIG_DRIVE_ID" & ", "
                sql += "@_DRIVELETTER" & ", "
                sql += "@_ALARMMINORVALUE" & ", "
                sql += "@_ALARMMAJORVALUE" & ", "
                sql += "@_ALARMCRITICALVALUE" & ", "
                sql += "@_REPEATCHECKMINOR" & ", "
                sql += "@_REPEATCHECKMAJOR" & ", "
                sql += "@_REPEATCHECKCRITICAL"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table CF_CONFIG_DRIVE_DETAIL
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "UPDATEDDATE = " & "@_UPDATEDDATE" & ", "
                Sql += "UPDATEDBY = " & "@_UPDATEDBY" & ", "
                Sql += "CF_CONFIG_DRIVE_ID = " & "@_CF_CONFIG_DRIVE_ID" & ", "
                Sql += "DRIVELETTER = " & "@_DRIVELETTER" & ", "
                Sql += "ALARMMINORVALUE = " & "@_ALARMMINORVALUE" & ", "
                Sql += "ALARMMAJORVALUE = " & "@_ALARMMAJORVALUE" & ", "
                Sql += "ALARMCRITICALVALUE = " & "@_ALARMCRITICALVALUE" & ", "
                Sql += "REPEATCHECKMINOR = " & "@_REPEATCHECKMINOR" & ", "
                Sql += "REPEATCHECKMAJOR = " & "@_REPEATCHECKMAJOR" & ", "
                Sql += "REPEATCHECKCRITICAL = " & "@_REPEATCHECKCRITICAL" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table CF_CONFIG_DRIVE_DETAIL
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table CF_CONFIG_DRIVE_DETAIL
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, CF_CONFIG_DRIVE_ID, DRIVELETTER, ALARMMINORVALUE, ALARMMAJORVALUE, ALARMCRITICALVALUE, REPEATCHECKMINOR, REPEATCHECKMAJOR, REPEATCHECKCRITICAL FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
