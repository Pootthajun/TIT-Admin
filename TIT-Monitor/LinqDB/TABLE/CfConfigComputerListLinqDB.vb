Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for CF_CONFIG_COMPUTER_LIST table LinqDB.
    '[Create by  on October, 30 2016]
    Public Class CfConfigComputerListLinqDB
        Public sub CfConfigComputerListLinqDB()

        End Sub 
        ' CF_CONFIG_COMPUTER_LIST
        Const _tableName As String = "CF_CONFIG_COMPUTER_LIST"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _CREATEDDATE As DateTime = New DateTime(1,1,1)
        Dim _CREATEDBY As String = ""
        Dim _UPDATEDDATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATEDBY As  String  = ""
        Dim _SERVERNAME As String = ""
        Dim _SERVERIP As String = ""
        Dim _MACADDRESS As String = ""
        Dim _SYSTEM_CODE As  String  = ""
        Dim _COM_LOCATION As  String  = ""
        Dim _ONLINE_STATUS As Char = ""
        Dim _TODAY_AVAILABLE As Double = 0
        Dim _ACTIVESTATUS As Char = ""
        Dim _MAIL_ALIAS As  String  = ""
        Dim _MAIL_SUBJECT As  String  = ""
        Dim _CURRENT_SCREEN() As Byte
        Dim _CAPTURE_SCREEN_TIME As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_CREATEDDATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATEDDATE() As DateTime
            Get
                Return _CREATEDDATE
            End Get
            Set(ByVal value As DateTime)
               _CREATEDDATE = value
            End Set
        End Property 
        <Column(Storage:="_CREATEDBY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATEDBY() As String
            Get
                Return _CREATEDBY
            End Get
            Set(ByVal value As String)
               _CREATEDBY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATEDDATE", DbType:="DateTime")>  _
        Public Property UPDATEDDATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATEDDATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATEDDATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATEDBY", DbType:="VarChar(100)")>  _
        Public Property UPDATEDBY() As  String 
            Get
                Return _UPDATEDBY
            End Get
            Set(ByVal value As  String )
               _UPDATEDBY = value
            End Set
        End Property 
        <Column(Storage:="_SERVERNAME", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property SERVERNAME() As String
            Get
                Return _SERVERNAME
            End Get
            Set(ByVal value As String)
               _SERVERNAME = value
            End Set
        End Property 
        <Column(Storage:="_SERVERIP", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property SERVERIP() As String
            Get
                Return _SERVERIP
            End Get
            Set(ByVal value As String)
               _SERVERIP = value
            End Set
        End Property 
        <Column(Storage:="_MACADDRESS", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property MACADDRESS() As String
            Get
                Return _MACADDRESS
            End Get
            Set(ByVal value As String)
               _MACADDRESS = value
            End Set
        End Property 
        <Column(Storage:="_SYSTEM_CODE", DbType:="VarChar(50)")>  _
        Public Property SYSTEM_CODE() As  String 
            Get
                Return _SYSTEM_CODE
            End Get
            Set(ByVal value As  String )
               _SYSTEM_CODE = value
            End Set
        End Property 
        <Column(Storage:="_COM_LOCATION", DbType:="VarChar(255)")>  _
        Public Property COM_LOCATION() As  String 
            Get
                Return _COM_LOCATION
            End Get
            Set(ByVal value As  String )
               _COM_LOCATION = value
            End Set
        End Property 
        <Column(Storage:="_ONLINE_STATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ONLINE_STATUS() As Char
            Get
                Return _ONLINE_STATUS
            End Get
            Set(ByVal value As Char)
               _ONLINE_STATUS = value
            End Set
        End Property 
        <Column(Storage:="_TODAY_AVAILABLE", DbType:="Float NOT NULL ",CanBeNull:=false)>  _
        Public Property TODAY_AVAILABLE() As Double
            Get
                Return _TODAY_AVAILABLE
            End Get
            Set(ByVal value As Double)
               _TODAY_AVAILABLE = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVESTATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVESTATUS() As Char
            Get
                Return _ACTIVESTATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVESTATUS = value
            End Set
        End Property 
        <Column(Storage:="_MAIL_ALIAS", DbType:="VarChar(100)")>  _
        Public Property MAIL_ALIAS() As  String 
            Get
                Return _MAIL_ALIAS
            End Get
            Set(ByVal value As  String )
               _MAIL_ALIAS = value
            End Set
        End Property 
        <Column(Storage:="_MAIL_SUBJECT", DbType:="VarChar(100)")>  _
        Public Property MAIL_SUBJECT() As  String 
            Get
                Return _MAIL_SUBJECT
            End Get
            Set(ByVal value As  String )
               _MAIL_SUBJECT = value
            End Set
        End Property 
        <Column(Storage:="_CURRENT_SCREEN", DbType:="IMAGE")>  _
        Public Property CURRENT_SCREEN() As  Byte() 
            Get
                Return _CURRENT_SCREEN
            End Get
            Set(ByVal value() As Byte)
               _CURRENT_SCREEN = value
            End Set
        End Property 
        <Column(Storage:="_CAPTURE_SCREEN_TIME", DbType:="DateTime")>  _
        Public Property CAPTURE_SCREEN_TIME() As  System.Nullable(Of DateTime) 
            Get
                Return _CAPTURE_SCREEN_TIME
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _CAPTURE_SCREEN_TIME = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _CREATEDDATE = New DateTime(1,1,1)
            _CREATEDBY = ""
            _UPDATEDDATE = New DateTime(1,1,1)
            _UPDATEDBY = ""
            _SERVERNAME = ""
            _SERVERIP = ""
            _MACADDRESS = ""
            _SYSTEM_CODE = ""
            _COM_LOCATION = ""
            _ONLINE_STATUS = ""
            _TODAY_AVAILABLE = 0
            _ACTIVESTATUS = ""
            _MAIL_ALIAS = ""
            _MAIL_SUBJECT = ""
             _CURRENT_SCREEN = Nothing
            _CAPTURE_SCREEN_TIME = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into CF_CONFIG_COMPUTER_LIST table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _CreatedBy = CreatedBy
                _CreatedDate = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to CF_CONFIG_COMPUTER_LIST table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATEDBY = UpdatedBy
                    _UPDATEDDATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to CF_CONFIG_COMPUTER_LIST table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from CF_CONFIG_COMPUTER_LIST table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_COMPUTER_LIST by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of CF_CONFIG_COMPUTER_LIST by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As CfConfigComputerListLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_COMPUTER_LIST by specified SERVERNAME key is retrieved successfully.
        '/// <param name=cSERVERNAME>The SERVERNAME key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataBySERVERNAME(cSERVERNAME As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_SERVERNAME", cSERVERNAME) 
            Return doChkData("SERVERNAME = @_SERVERNAME", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of CF_CONFIG_COMPUTER_LIST by specified SERVERNAME key is retrieved successfully.
        '/// <param name=cSERVERNAME>The SERVERNAME key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateBySERVERNAME(cSERVERNAME As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_SERVERNAME", cSERVERNAME) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("SERVERNAME = @_SERVERNAME And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_COMPUTER_LIST by specified SERVERIP key is retrieved successfully.
        '/// <param name=cSERVERIP>The SERVERIP key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataBySERVERIP(cSERVERIP As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_SERVERIP", cSERVERIP) 
            Return doChkData("SERVERIP = @_SERVERIP", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of CF_CONFIG_COMPUTER_LIST by specified SERVERIP key is retrieved successfully.
        '/// <param name=cSERVERIP>The SERVERIP key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateBySERVERIP(cSERVERIP As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_SERVERIP", cSERVERIP) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("SERVERIP = @_SERVERIP And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_COMPUTER_LIST by specified MACADDRESS key is retrieved successfully.
        '/// <param name=cMACADDRESS>The MACADDRESS key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByMACADDRESS(cMACADDRESS As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_MACADDRESS", cMACADDRESS) 
            Return doChkData("MACADDRESS = @_MACADDRESS", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of CF_CONFIG_COMPUTER_LIST by specified MACADDRESS key is retrieved successfully.
        '/// <param name=cMACADDRESS>The MACADDRESS key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByMACADDRESS(cMACADDRESS As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_MACADDRESS", cMACADDRESS) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("MACADDRESS = @_MACADDRESS And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_COMPUTER_LIST by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into CF_CONFIG_COMPUTER_LIST table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to CF_CONFIG_COMPUTER_LIST table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from CF_CONFIG_COMPUTER_LIST table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(16) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_CREATEDDATE", SqlDbType.DateTime)
            cmbParam(1).Value = _CREATEDDATE

            cmbParam(2) = New SqlParameter("@_CREATEDBY", SqlDbType.VarChar)
            cmbParam(2).Value = _CREATEDBY

            cmbParam(3) = New SqlParameter("@_UPDATEDDATE", SqlDbType.DateTime)
            If _UPDATEDDATE.Value.Year > 1 Then 
                cmbParam(3).Value = _UPDATEDDATE.Value
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_UPDATEDBY", SqlDbType.VarChar)
            If _UPDATEDBY.Trim <> "" Then 
                cmbParam(4).Value = _UPDATEDBY
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_SERVERNAME", SqlDbType.VarChar)
            cmbParam(5).Value = _SERVERNAME

            cmbParam(6) = New SqlParameter("@_SERVERIP", SqlDbType.VarChar)
            cmbParam(6).Value = _SERVERIP

            cmbParam(7) = New SqlParameter("@_MACADDRESS", SqlDbType.VarChar)
            cmbParam(7).Value = _MACADDRESS

            cmbParam(8) = New SqlParameter("@_SYSTEM_CODE", SqlDbType.VarChar)
            If _SYSTEM_CODE.Trim <> "" Then 
                cmbParam(8).Value = _SYSTEM_CODE
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_COM_LOCATION", SqlDbType.VarChar)
            If _COM_LOCATION.Trim <> "" Then 
                cmbParam(9).Value = _COM_LOCATION
            Else
                cmbParam(9).Value = DBNull.value
            End If

            cmbParam(10) = New SqlParameter("@_ONLINE_STATUS", SqlDbType.Char)
            cmbParam(10).Value = _ONLINE_STATUS

            cmbParam(11) = New SqlParameter("@_TODAY_AVAILABLE", SqlDbType.Float)
            cmbParam(11).Value = _TODAY_AVAILABLE

            cmbParam(12) = New SqlParameter("@_ACTIVESTATUS", SqlDbType.Char)
            cmbParam(12).Value = _ACTIVESTATUS

            cmbParam(13) = New SqlParameter("@_MAIL_ALIAS", SqlDbType.VarChar)
            If _MAIL_ALIAS.Trim <> "" Then 
                cmbParam(13).Value = _MAIL_ALIAS
            Else
                cmbParam(13).Value = DBNull.value
            End If

            cmbParam(14) = New SqlParameter("@_MAIL_SUBJECT", SqlDbType.VarChar)
            If _MAIL_SUBJECT.Trim <> "" Then 
                cmbParam(14).Value = _MAIL_SUBJECT
            Else
                cmbParam(14).Value = DBNull.value
            End If

            If _CURRENT_SCREEN IsNot Nothing Then 
                cmbParam(15) = New SqlParameter("@_CURRENT_SCREEN",SqlDbType.Image, _CURRENT_SCREEN.Length)
                cmbParam(15).Value = _CURRENT_SCREEN
            Else
                cmbParam(15) = New SqlParameter("@_CURRENT_SCREEN", SqlDbType.Image)
                cmbParam(15).Value = DBNull.value
            End If

            cmbParam(16) = New SqlParameter("@_CAPTURE_SCREEN_TIME", SqlDbType.DateTime)
            If _CAPTURE_SCREEN_TIME.Value.Year > 1 Then 
                cmbParam(16).Value = _CAPTURE_SCREEN_TIME.Value
            Else
                cmbParam(16).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_COMPUTER_LIST by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("CreatedDate")) = False Then _CreatedDate = Convert.ToDateTime(Rdr("CreatedDate"))
                        If Convert.IsDBNull(Rdr("CreatedBy")) = False Then _CreatedBy = Rdr("CreatedBy").ToString()
                        If Convert.IsDBNull(Rdr("UpdatedDate")) = False Then _UpdatedDate = Convert.ToDateTime(Rdr("UpdatedDate"))
                        If Convert.IsDBNull(Rdr("UpdatedBy")) = False Then _UpdatedBy = Rdr("UpdatedBy").ToString()
                        If Convert.IsDBNull(Rdr("ServerName")) = False Then _ServerName = Rdr("ServerName").ToString()
                        If Convert.IsDBNull(Rdr("ServerIP")) = False Then _ServerIP = Rdr("ServerIP").ToString()
                        If Convert.IsDBNull(Rdr("MacAddress")) = False Then _MacAddress = Rdr("MacAddress").ToString()
                        If Convert.IsDBNull(Rdr("system_code")) = False Then _system_code = Rdr("system_code").ToString()
                        If Convert.IsDBNull(Rdr("com_location")) = False Then _com_location = Rdr("com_location").ToString()
                        If Convert.IsDBNull(Rdr("online_status")) = False Then _online_status = Rdr("online_status").ToString()
                        If Convert.IsDBNull(Rdr("today_available")) = False Then _today_available = Convert.ToDouble(Rdr("today_available"))
                        If Convert.IsDBNull(Rdr("ActiveStatus")) = False Then _ActiveStatus = Rdr("ActiveStatus").ToString()
                        If Convert.IsDBNull(Rdr("mail_alias")) = False Then _mail_alias = Rdr("mail_alias").ToString()
                        If Convert.IsDBNull(Rdr("mail_subject")) = False Then _mail_subject = Rdr("mail_subject").ToString()
                        If Convert.IsDBNull(Rdr("current_screen")) = False Then _current_screen = CType(Rdr("current_screen"), Byte())
                        If Convert.IsDBNull(Rdr("capture_screen_time")) = False Then _capture_screen_time = Convert.ToDateTime(Rdr("capture_screen_time"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_COMPUTER_LIST by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As CfConfigComputerListLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("CreatedDate")) = False Then _CreatedDate = Convert.ToDateTime(Rdr("CreatedDate"))
                        If Convert.IsDBNull(Rdr("CreatedBy")) = False Then _CreatedBy = Rdr("CreatedBy").ToString()
                        If Convert.IsDBNull(Rdr("UpdatedDate")) = False Then _UpdatedDate = Convert.ToDateTime(Rdr("UpdatedDate"))
                        If Convert.IsDBNull(Rdr("UpdatedBy")) = False Then _UpdatedBy = Rdr("UpdatedBy").ToString()
                        If Convert.IsDBNull(Rdr("ServerName")) = False Then _ServerName = Rdr("ServerName").ToString()
                        If Convert.IsDBNull(Rdr("ServerIP")) = False Then _ServerIP = Rdr("ServerIP").ToString()
                        If Convert.IsDBNull(Rdr("MacAddress")) = False Then _MacAddress = Rdr("MacAddress").ToString()
                        If Convert.IsDBNull(Rdr("system_code")) = False Then _system_code = Rdr("system_code").ToString()
                        If Convert.IsDBNull(Rdr("com_location")) = False Then _com_location = Rdr("com_location").ToString()
                        If Convert.IsDBNull(Rdr("online_status")) = False Then _online_status = Rdr("online_status").ToString()
                        If Convert.IsDBNull(Rdr("today_available")) = False Then _today_available = Convert.ToDouble(Rdr("today_available"))
                        If Convert.IsDBNull(Rdr("ActiveStatus")) = False Then _ActiveStatus = Rdr("ActiveStatus").ToString()
                        If Convert.IsDBNull(Rdr("mail_alias")) = False Then _mail_alias = Rdr("mail_alias").ToString()
                        If Convert.IsDBNull(Rdr("mail_subject")) = False Then _mail_subject = Rdr("mail_subject").ToString()
                        If Convert.IsDBNull(Rdr("current_screen")) = False Then _current_screen = CType(Rdr("current_screen"), Byte())
                        If Convert.IsDBNull(Rdr("capture_screen_time")) = False Then _capture_screen_time = Convert.ToDateTime(Rdr("capture_screen_time"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table CF_CONFIG_COMPUTER_LIST
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (CREATEDDATE, CREATEDBY, SERVERNAME, SERVERIP, MACADDRESS, SYSTEM_CODE, COM_LOCATION, ONLINE_STATUS, TODAY_AVAILABLE, ACTIVESTATUS, MAIL_ALIAS, MAIL_SUBJECT, CURRENT_SCREEN, CAPTURE_SCREEN_TIME)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.CREATEDDATE, INSERTED.CREATEDBY, INSERTED.UPDATEDDATE, INSERTED.UPDATEDBY, INSERTED.SERVERNAME, INSERTED.SERVERIP, INSERTED.MACADDRESS, INSERTED.SYSTEM_CODE, INSERTED.COM_LOCATION, INSERTED.ONLINE_STATUS, INSERTED.TODAY_AVAILABLE, INSERTED.ACTIVESTATUS, INSERTED.MAIL_ALIAS, INSERTED.MAIL_SUBJECT, INSERTED.CURRENT_SCREEN, INSERTED.CAPTURE_SCREEN_TIME"
                Sql += " VALUES("
                sql += "@_CREATEDDATE" & ", "
                sql += "@_CREATEDBY" & ", "
                sql += "@_SERVERNAME" & ", "
                sql += "@_SERVERIP" & ", "
                sql += "@_MACADDRESS" & ", "
                sql += "@_SYSTEM_CODE" & ", "
                sql += "@_COM_LOCATION" & ", "
                sql += "@_ONLINE_STATUS" & ", "
                sql += "@_TODAY_AVAILABLE" & ", "
                sql += "@_ACTIVESTATUS" & ", "
                sql += "@_MAIL_ALIAS" & ", "
                sql += "@_MAIL_SUBJECT" & ", "
                sql += "@_CURRENT_SCREEN" & ", "
                sql += "@_CAPTURE_SCREEN_TIME"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table CF_CONFIG_COMPUTER_LIST
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "UPDATEDDATE = " & "@_UPDATEDDATE" & ", "
                Sql += "UPDATEDBY = " & "@_UPDATEDBY" & ", "
                Sql += "SERVERNAME = " & "@_SERVERNAME" & ", "
                Sql += "SERVERIP = " & "@_SERVERIP" & ", "
                Sql += "MACADDRESS = " & "@_MACADDRESS" & ", "
                Sql += "SYSTEM_CODE = " & "@_SYSTEM_CODE" & ", "
                Sql += "COM_LOCATION = " & "@_COM_LOCATION" & ", "
                Sql += "ONLINE_STATUS = " & "@_ONLINE_STATUS" & ", "
                Sql += "TODAY_AVAILABLE = " & "@_TODAY_AVAILABLE" & ", "
                Sql += "ACTIVESTATUS = " & "@_ACTIVESTATUS" & ", "
                Sql += "MAIL_ALIAS = " & "@_MAIL_ALIAS" & ", "
                Sql += "MAIL_SUBJECT = " & "@_MAIL_SUBJECT" & ", "
                Sql += "CURRENT_SCREEN = " & "@_CURRENT_SCREEN" & ", "
                Sql += "CAPTURE_SCREEN_TIME = " & "@_CAPTURE_SCREEN_TIME" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table CF_CONFIG_COMPUTER_LIST
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table CF_CONFIG_COMPUTER_LIST
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, SERVERNAME, SERVERIP, MACADDRESS, SYSTEM_CODE, COM_LOCATION, ONLINE_STATUS, TODAY_AVAILABLE, ACTIVESTATUS, MAIL_ALIAS, MAIL_SUBJECT, CURRENT_SCREEN, CAPTURE_SCREEN_TIME FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
