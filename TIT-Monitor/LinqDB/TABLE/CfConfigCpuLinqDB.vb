Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for CF_CONFIG_CPU table LinqDB.
    '[Create by  on June, 9 2016]
    Public Class CfConfigCpuLinqDB
        Public sub CfConfigCpuLinqDB()

        End Sub 
        ' CF_CONFIG_CPU
        Const _tableName As String = "CF_CONFIG_CPU"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _CREATEDDATE As DateTime = New DateTime(1,1,1)
        Dim _CREATEDBY As String = ""
        Dim _UPDATEDDATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATEDBY As  String  = ""
        Dim _SERVERNAME As String = ""
        Dim _SERVERIP As String = ""
        Dim _MACADDRESS As String = ""
        Dim _CHECKINTERVALMINUTE As Long = 0
        Dim _ALARMCODE As String = ""
        Dim _ALARMMINORVALUE As Long = 0
        Dim _ALARMMAJORVALUE As Long = 0
        Dim _ALARMCRITICALVALUE As Long = 0
        Dim _ACTIVESTATUS As Char = "Y"
        Dim _REPEATCHECKMINOR As Long = 0
        Dim _REPEATCHECKMAJOR As Long = 0
        Dim _REPEATCHECKCRITICAL As Long = 0
        Dim _ALARMSUN As Char = "Y"
        Dim _ALARMMON As Char = "Y"
        Dim _ALARMTUE As Char = "Y"
        Dim _ALARMWED As Char = "Y"
        Dim _ALARMTHU As Char = "Y"
        Dim _ALARMFRI As Char = "Y"
        Dim _ALARMSAT As Char = "Y"
        Dim _ALARMTIMEFROM As  String  = ""
        Dim _ALARMTIMETO As  String  = ""
        Dim _ALLDAYEVENT As Char = "Y"
        Dim _LASTCHECKTIME As DateTime = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_CREATEDDATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATEDDATE() As DateTime
            Get
                Return _CREATEDDATE
            End Get
            Set(ByVal value As DateTime)
               _CREATEDDATE = value
            End Set
        End Property 
        <Column(Storage:="_CREATEDBY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATEDBY() As String
            Get
                Return _CREATEDBY
            End Get
            Set(ByVal value As String)
               _CREATEDBY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATEDDATE", DbType:="DateTime")>  _
        Public Property UPDATEDDATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATEDDATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATEDDATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATEDBY", DbType:="VarChar(100)")>  _
        Public Property UPDATEDBY() As  String 
            Get
                Return _UPDATEDBY
            End Get
            Set(ByVal value As  String )
               _UPDATEDBY = value
            End Set
        End Property 
        <Column(Storage:="_SERVERNAME", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property SERVERNAME() As String
            Get
                Return _SERVERNAME
            End Get
            Set(ByVal value As String)
               _SERVERNAME = value
            End Set
        End Property 
        <Column(Storage:="_SERVERIP", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property SERVERIP() As String
            Get
                Return _SERVERIP
            End Get
            Set(ByVal value As String)
               _SERVERIP = value
            End Set
        End Property 
        <Column(Storage:="_MACADDRESS", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property MACADDRESS() As String
            Get
                Return _MACADDRESS
            End Get
            Set(ByVal value As String)
               _MACADDRESS = value
            End Set
        End Property 
        <Column(Storage:="_CHECKINTERVALMINUTE", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property CHECKINTERVALMINUTE() As Long
            Get
                Return _CHECKINTERVALMINUTE
            End Get
            Set(ByVal value As Long)
               _CHECKINTERVALMINUTE = value
            End Set
        End Property 
        <Column(Storage:="_ALARMCODE", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMCODE() As String
            Get
                Return _ALARMCODE
            End Get
            Set(ByVal value As String)
               _ALARMCODE = value
            End Set
        End Property 
        <Column(Storage:="_ALARMMINORVALUE", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMMINORVALUE() As Long
            Get
                Return _ALARMMINORVALUE
            End Get
            Set(ByVal value As Long)
               _ALARMMINORVALUE = value
            End Set
        End Property 
        <Column(Storage:="_ALARMMAJORVALUE", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMMAJORVALUE() As Long
            Get
                Return _ALARMMAJORVALUE
            End Get
            Set(ByVal value As Long)
               _ALARMMAJORVALUE = value
            End Set
        End Property 
        <Column(Storage:="_ALARMCRITICALVALUE", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMCRITICALVALUE() As Long
            Get
                Return _ALARMCRITICALVALUE
            End Get
            Set(ByVal value As Long)
               _ALARMCRITICALVALUE = value
            End Set
        End Property 
        <Column(Storage:="_ACTIVESTATUS", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ACTIVESTATUS() As Char
            Get
                Return _ACTIVESTATUS
            End Get
            Set(ByVal value As Char)
               _ACTIVESTATUS = value
            End Set
        End Property 
        <Column(Storage:="_REPEATCHECKMINOR", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property REPEATCHECKMINOR() As Long
            Get
                Return _REPEATCHECKMINOR
            End Get
            Set(ByVal value As Long)
               _REPEATCHECKMINOR = value
            End Set
        End Property 
        <Column(Storage:="_REPEATCHECKMAJOR", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property REPEATCHECKMAJOR() As Long
            Get
                Return _REPEATCHECKMAJOR
            End Get
            Set(ByVal value As Long)
               _REPEATCHECKMAJOR = value
            End Set
        End Property 
        <Column(Storage:="_REPEATCHECKCRITICAL", DbType:="Int NOT NULL ",CanBeNull:=false)>  _
        Public Property REPEATCHECKCRITICAL() As Long
            Get
                Return _REPEATCHECKCRITICAL
            End Get
            Set(ByVal value As Long)
               _REPEATCHECKCRITICAL = value
            End Set
        End Property 
        <Column(Storage:="_ALARMSUN", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMSUN() As Char
            Get
                Return _ALARMSUN
            End Get
            Set(ByVal value As Char)
               _ALARMSUN = value
            End Set
        End Property 
        <Column(Storage:="_ALARMMON", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMMON() As Char
            Get
                Return _ALARMMON
            End Get
            Set(ByVal value As Char)
               _ALARMMON = value
            End Set
        End Property 
        <Column(Storage:="_ALARMTUE", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMTUE() As Char
            Get
                Return _ALARMTUE
            End Get
            Set(ByVal value As Char)
               _ALARMTUE = value
            End Set
        End Property 
        <Column(Storage:="_ALARMWED", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMWED() As Char
            Get
                Return _ALARMWED
            End Get
            Set(ByVal value As Char)
               _ALARMWED = value
            End Set
        End Property 
        <Column(Storage:="_ALARMTHU", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMTHU() As Char
            Get
                Return _ALARMTHU
            End Get
            Set(ByVal value As Char)
               _ALARMTHU = value
            End Set
        End Property 
        <Column(Storage:="_ALARMFRI", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMFRI() As Char
            Get
                Return _ALARMFRI
            End Get
            Set(ByVal value As Char)
               _ALARMFRI = value
            End Set
        End Property 
        <Column(Storage:="_ALARMSAT", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMSAT() As Char
            Get
                Return _ALARMSAT
            End Get
            Set(ByVal value As Char)
               _ALARMSAT = value
            End Set
        End Property 
        <Column(Storage:="_ALARMTIMEFROM", DbType:="VarChar(5)")>  _
        Public Property ALARMTIMEFROM() As  String 
            Get
                Return _ALARMTIMEFROM
            End Get
            Set(ByVal value As  String )
               _ALARMTIMEFROM = value
            End Set
        End Property 
        <Column(Storage:="_ALARMTIMETO", DbType:="VarChar(5)")>  _
        Public Property ALARMTIMETO() As  String 
            Get
                Return _ALARMTIMETO
            End Get
            Set(ByVal value As  String )
               _ALARMTIMETO = value
            End Set
        End Property 
        <Column(Storage:="_ALLDAYEVENT", DbType:="Char(1) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALLDAYEVENT() As Char
            Get
                Return _ALLDAYEVENT
            End Get
            Set(ByVal value As Char)
               _ALLDAYEVENT = value
            End Set
        End Property 
        <Column(Storage:="_LASTCHECKTIME", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property LASTCHECKTIME() As DateTime
            Get
                Return _LASTCHECKTIME
            End Get
            Set(ByVal value As DateTime)
               _LASTCHECKTIME = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _CREATEDDATE = New DateTime(1,1,1)
            _CREATEDBY = ""
            _UPDATEDDATE = New DateTime(1,1,1)
            _UPDATEDBY = ""
            _SERVERNAME = ""
            _SERVERIP = ""
            _MACADDRESS = ""
            _CHECKINTERVALMINUTE = 0
            _ALARMCODE = ""
            _ALARMMINORVALUE = 0
            _ALARMMAJORVALUE = 0
            _ALARMCRITICALVALUE = 0
            _ACTIVESTATUS = "Y"
            _REPEATCHECKMINOR = 0
            _REPEATCHECKMAJOR = 0
            _REPEATCHECKCRITICAL = 0
            _ALARMSUN = "Y"
            _ALARMMON = "Y"
            _ALARMTUE = "Y"
            _ALARMWED = "Y"
            _ALARMTHU = "Y"
            _ALARMFRI = "Y"
            _ALARMSAT = "Y"
            _ALARMTIMEFROM = ""
            _ALARMTIMETO = ""
            _ALLDAYEVENT = "Y"
            _LASTCHECKTIME = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into CF_CONFIG_CPU table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _CreatedBy = CreatedBy
                _CreatedDate = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to CF_CONFIG_CPU table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATEDBY = UpdatedBy
                    _UPDATEDDATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to CF_CONFIG_CPU table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from CF_CONFIG_CPU table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_CPU by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of CF_CONFIG_CPU by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As CfConfigCpuLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_CPU by specified SERVERNAME key is retrieved successfully.
        '/// <param name=cSERVERNAME>The SERVERNAME key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataBySERVERNAME(cSERVERNAME As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_SERVERNAME", cSERVERNAME) 
            Return doChkData("SERVERNAME = @_SERVERNAME", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of CF_CONFIG_CPU by specified SERVERNAME key is retrieved successfully.
        '/// <param name=cSERVERNAME>The SERVERNAME key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateBySERVERNAME(cSERVERNAME As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_SERVERNAME", cSERVERNAME) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("SERVERNAME = @_SERVERNAME And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_CPU by specified SERVERIP key is retrieved successfully.
        '/// <param name=cSERVERIP>The SERVERIP key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataBySERVERIP(cSERVERIP As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_SERVERIP", cSERVERIP) 
            Return doChkData("SERVERIP = @_SERVERIP", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of CF_CONFIG_CPU by specified SERVERIP key is retrieved successfully.
        '/// <param name=cSERVERIP>The SERVERIP key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateBySERVERIP(cSERVERIP As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_SERVERIP", cSERVERIP) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("SERVERIP = @_SERVERIP And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_CPU by specified MACADDRESS key is retrieved successfully.
        '/// <param name=cMACADDRESS>The MACADDRESS key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByMACADDRESS(cMACADDRESS As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_MACADDRESS", cMACADDRESS) 
            Return doChkData("MACADDRESS = @_MACADDRESS", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of CF_CONFIG_CPU by specified MACADDRESS key is retrieved successfully.
        '/// <param name=cMACADDRESS>The MACADDRESS key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByMACADDRESS(cMACADDRESS As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_MACADDRESS", cMACADDRESS) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("MACADDRESS = @_MACADDRESS And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_CPU by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into CF_CONFIG_CPU table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to CF_CONFIG_CPU table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from CF_CONFIG_CPU table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(27) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_CREATEDDATE", SqlDbType.DateTime)
            cmbParam(1).Value = _CREATEDDATE

            cmbParam(2) = New SqlParameter("@_CREATEDBY", SqlDbType.VarChar)
            cmbParam(2).Value = _CREATEDBY

            cmbParam(3) = New SqlParameter("@_UPDATEDDATE", SqlDbType.DateTime)
            If _UPDATEDDATE.Value.Year > 1 Then 
                cmbParam(3).Value = _UPDATEDDATE.Value
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_UPDATEDBY", SqlDbType.VarChar)
            If _UPDATEDBY.Trim <> "" Then 
                cmbParam(4).Value = _UPDATEDBY
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_SERVERNAME", SqlDbType.VarChar)
            cmbParam(5).Value = _SERVERNAME

            cmbParam(6) = New SqlParameter("@_SERVERIP", SqlDbType.VarChar)
            cmbParam(6).Value = _SERVERIP

            cmbParam(7) = New SqlParameter("@_MACADDRESS", SqlDbType.VarChar)
            cmbParam(7).Value = _MACADDRESS

            cmbParam(8) = New SqlParameter("@_CHECKINTERVALMINUTE", SqlDbType.Int)
            cmbParam(8).Value = _CHECKINTERVALMINUTE

            cmbParam(9) = New SqlParameter("@_ALARMCODE", SqlDbType.VarChar)
            cmbParam(9).Value = _ALARMCODE

            cmbParam(10) = New SqlParameter("@_ALARMMINORVALUE", SqlDbType.Int)
            cmbParam(10).Value = _ALARMMINORVALUE

            cmbParam(11) = New SqlParameter("@_ALARMMAJORVALUE", SqlDbType.Int)
            cmbParam(11).Value = _ALARMMAJORVALUE

            cmbParam(12) = New SqlParameter("@_ALARMCRITICALVALUE", SqlDbType.Int)
            cmbParam(12).Value = _ALARMCRITICALVALUE

            cmbParam(13) = New SqlParameter("@_ACTIVESTATUS", SqlDbType.Char)
            cmbParam(13).Value = _ACTIVESTATUS

            cmbParam(14) = New SqlParameter("@_REPEATCHECKMINOR", SqlDbType.Int)
            cmbParam(14).Value = _REPEATCHECKMINOR

            cmbParam(15) = New SqlParameter("@_REPEATCHECKMAJOR", SqlDbType.Int)
            cmbParam(15).Value = _REPEATCHECKMAJOR

            cmbParam(16) = New SqlParameter("@_REPEATCHECKCRITICAL", SqlDbType.Int)
            cmbParam(16).Value = _REPEATCHECKCRITICAL

            cmbParam(17) = New SqlParameter("@_ALARMSUN", SqlDbType.Char)
            cmbParam(17).Value = _ALARMSUN

            cmbParam(18) = New SqlParameter("@_ALARMMON", SqlDbType.Char)
            cmbParam(18).Value = _ALARMMON

            cmbParam(19) = New SqlParameter("@_ALARMTUE", SqlDbType.Char)
            cmbParam(19).Value = _ALARMTUE

            cmbParam(20) = New SqlParameter("@_ALARMWED", SqlDbType.Char)
            cmbParam(20).Value = _ALARMWED

            cmbParam(21) = New SqlParameter("@_ALARMTHU", SqlDbType.Char)
            cmbParam(21).Value = _ALARMTHU

            cmbParam(22) = New SqlParameter("@_ALARMFRI", SqlDbType.Char)
            cmbParam(22).Value = _ALARMFRI

            cmbParam(23) = New SqlParameter("@_ALARMSAT", SqlDbType.Char)
            cmbParam(23).Value = _ALARMSAT

            cmbParam(24) = New SqlParameter("@_ALARMTIMEFROM", SqlDbType.VarChar)
            If _ALARMTIMEFROM.Trim <> "" Then 
                cmbParam(24).Value = _ALARMTIMEFROM
            Else
                cmbParam(24).Value = DBNull.value
            End If

            cmbParam(25) = New SqlParameter("@_ALARMTIMETO", SqlDbType.VarChar)
            If _ALARMTIMETO.Trim <> "" Then 
                cmbParam(25).Value = _ALARMTIMETO
            Else
                cmbParam(25).Value = DBNull.value
            End If

            cmbParam(26) = New SqlParameter("@_ALLDAYEVENT", SqlDbType.Char)
            cmbParam(26).Value = _ALLDAYEVENT

            cmbParam(27) = New SqlParameter("@_LASTCHECKTIME", SqlDbType.DateTime)
            cmbParam(27).Value = _LASTCHECKTIME

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_CPU by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("CreatedDate")) = False Then _CreatedDate = Convert.ToDateTime(Rdr("CreatedDate"))
                        If Convert.IsDBNull(Rdr("CreatedBy")) = False Then _CreatedBy = Rdr("CreatedBy").ToString()
                        If Convert.IsDBNull(Rdr("UpdatedDate")) = False Then _UpdatedDate = Convert.ToDateTime(Rdr("UpdatedDate"))
                        If Convert.IsDBNull(Rdr("UpdatedBy")) = False Then _UpdatedBy = Rdr("UpdatedBy").ToString()
                        If Convert.IsDBNull(Rdr("ServerName")) = False Then _ServerName = Rdr("ServerName").ToString()
                        If Convert.IsDBNull(Rdr("ServerIP")) = False Then _ServerIP = Rdr("ServerIP").ToString()
                        If Convert.IsDBNull(Rdr("MacAddress")) = False Then _MacAddress = Rdr("MacAddress").ToString()
                        If Convert.IsDBNull(Rdr("CheckIntervalMinute")) = False Then _CheckIntervalMinute = Convert.ToInt32(Rdr("CheckIntervalMinute"))
                        If Convert.IsDBNull(Rdr("AlarmCode")) = False Then _AlarmCode = Rdr("AlarmCode").ToString()
                        If Convert.IsDBNull(Rdr("AlarmMinorValue")) = False Then _AlarmMinorValue = Convert.ToInt32(Rdr("AlarmMinorValue"))
                        If Convert.IsDBNull(Rdr("AlarmMajorValue")) = False Then _AlarmMajorValue = Convert.ToInt32(Rdr("AlarmMajorValue"))
                        If Convert.IsDBNull(Rdr("AlarmCriticalValue")) = False Then _AlarmCriticalValue = Convert.ToInt32(Rdr("AlarmCriticalValue"))
                        If Convert.IsDBNull(Rdr("ActiveStatus")) = False Then _ActiveStatus = Rdr("ActiveStatus").ToString()
                        If Convert.IsDBNull(Rdr("RepeatCheckMinor")) = False Then _RepeatCheckMinor = Convert.ToInt32(Rdr("RepeatCheckMinor"))
                        If Convert.IsDBNull(Rdr("RepeatCheckMajor")) = False Then _RepeatCheckMajor = Convert.ToInt32(Rdr("RepeatCheckMajor"))
                        If Convert.IsDBNull(Rdr("RepeatCheckCritical")) = False Then _RepeatCheckCritical = Convert.ToInt32(Rdr("RepeatCheckCritical"))
                        If Convert.IsDBNull(Rdr("AlarmSun")) = False Then _AlarmSun = Rdr("AlarmSun").ToString()
                        If Convert.IsDBNull(Rdr("AlarmMon")) = False Then _AlarmMon = Rdr("AlarmMon").ToString()
                        If Convert.IsDBNull(Rdr("AlarmTue")) = False Then _AlarmTue = Rdr("AlarmTue").ToString()
                        If Convert.IsDBNull(Rdr("AlarmWed")) = False Then _AlarmWed = Rdr("AlarmWed").ToString()
                        If Convert.IsDBNull(Rdr("AlarmThu")) = False Then _AlarmThu = Rdr("AlarmThu").ToString()
                        If Convert.IsDBNull(Rdr("AlarmFri")) = False Then _AlarmFri = Rdr("AlarmFri").ToString()
                        If Convert.IsDBNull(Rdr("AlarmSat")) = False Then _AlarmSat = Rdr("AlarmSat").ToString()
                        If Convert.IsDBNull(Rdr("AlarmTimeFrom")) = False Then _AlarmTimeFrom = Rdr("AlarmTimeFrom").ToString()
                        If Convert.IsDBNull(Rdr("AlarmTimeTo")) = False Then _AlarmTimeTo = Rdr("AlarmTimeTo").ToString()
                        If Convert.IsDBNull(Rdr("AllDayEvent")) = False Then _AllDayEvent = Rdr("AllDayEvent").ToString()
                        If Convert.IsDBNull(Rdr("LastCheckTime")) = False Then _LastCheckTime = Convert.ToDateTime(Rdr("LastCheckTime"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of CF_CONFIG_CPU by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As CfConfigCpuLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("CreatedDate")) = False Then _CreatedDate = Convert.ToDateTime(Rdr("CreatedDate"))
                        If Convert.IsDBNull(Rdr("CreatedBy")) = False Then _CreatedBy = Rdr("CreatedBy").ToString()
                        If Convert.IsDBNull(Rdr("UpdatedDate")) = False Then _UpdatedDate = Convert.ToDateTime(Rdr("UpdatedDate"))
                        If Convert.IsDBNull(Rdr("UpdatedBy")) = False Then _UpdatedBy = Rdr("UpdatedBy").ToString()
                        If Convert.IsDBNull(Rdr("ServerName")) = False Then _ServerName = Rdr("ServerName").ToString()
                        If Convert.IsDBNull(Rdr("ServerIP")) = False Then _ServerIP = Rdr("ServerIP").ToString()
                        If Convert.IsDBNull(Rdr("MacAddress")) = False Then _MacAddress = Rdr("MacAddress").ToString()
                        If Convert.IsDBNull(Rdr("CheckIntervalMinute")) = False Then _CheckIntervalMinute = Convert.ToInt32(Rdr("CheckIntervalMinute"))
                        If Convert.IsDBNull(Rdr("AlarmCode")) = False Then _AlarmCode = Rdr("AlarmCode").ToString()
                        If Convert.IsDBNull(Rdr("AlarmMinorValue")) = False Then _AlarmMinorValue = Convert.ToInt32(Rdr("AlarmMinorValue"))
                        If Convert.IsDBNull(Rdr("AlarmMajorValue")) = False Then _AlarmMajorValue = Convert.ToInt32(Rdr("AlarmMajorValue"))
                        If Convert.IsDBNull(Rdr("AlarmCriticalValue")) = False Then _AlarmCriticalValue = Convert.ToInt32(Rdr("AlarmCriticalValue"))
                        If Convert.IsDBNull(Rdr("ActiveStatus")) = False Then _ActiveStatus = Rdr("ActiveStatus").ToString()
                        If Convert.IsDBNull(Rdr("RepeatCheckMinor")) = False Then _RepeatCheckMinor = Convert.ToInt32(Rdr("RepeatCheckMinor"))
                        If Convert.IsDBNull(Rdr("RepeatCheckMajor")) = False Then _RepeatCheckMajor = Convert.ToInt32(Rdr("RepeatCheckMajor"))
                        If Convert.IsDBNull(Rdr("RepeatCheckCritical")) = False Then _RepeatCheckCritical = Convert.ToInt32(Rdr("RepeatCheckCritical"))
                        If Convert.IsDBNull(Rdr("AlarmSun")) = False Then _AlarmSun = Rdr("AlarmSun").ToString()
                        If Convert.IsDBNull(Rdr("AlarmMon")) = False Then _AlarmMon = Rdr("AlarmMon").ToString()
                        If Convert.IsDBNull(Rdr("AlarmTue")) = False Then _AlarmTue = Rdr("AlarmTue").ToString()
                        If Convert.IsDBNull(Rdr("AlarmWed")) = False Then _AlarmWed = Rdr("AlarmWed").ToString()
                        If Convert.IsDBNull(Rdr("AlarmThu")) = False Then _AlarmThu = Rdr("AlarmThu").ToString()
                        If Convert.IsDBNull(Rdr("AlarmFri")) = False Then _AlarmFri = Rdr("AlarmFri").ToString()
                        If Convert.IsDBNull(Rdr("AlarmSat")) = False Then _AlarmSat = Rdr("AlarmSat").ToString()
                        If Convert.IsDBNull(Rdr("AlarmTimeFrom")) = False Then _AlarmTimeFrom = Rdr("AlarmTimeFrom").ToString()
                        If Convert.IsDBNull(Rdr("AlarmTimeTo")) = False Then _AlarmTimeTo = Rdr("AlarmTimeTo").ToString()
                        If Convert.IsDBNull(Rdr("AllDayEvent")) = False Then _AllDayEvent = Rdr("AllDayEvent").ToString()
                        If Convert.IsDBNull(Rdr("LastCheckTime")) = False Then _LastCheckTime = Convert.ToDateTime(Rdr("LastCheckTime"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table CF_CONFIG_CPU
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (CREATEDDATE, CREATEDBY, SERVERNAME, SERVERIP, MACADDRESS, CHECKINTERVALMINUTE, ALARMCODE, ALARMMINORVALUE, ALARMMAJORVALUE, ALARMCRITICALVALUE, ACTIVESTATUS, REPEATCHECKMINOR, REPEATCHECKMAJOR, REPEATCHECKCRITICAL, ALARMSUN, ALARMMON, ALARMTUE, ALARMWED, ALARMTHU, ALARMFRI, ALARMSAT, ALARMTIMEFROM, ALARMTIMETO, ALLDAYEVENT, LASTCHECKTIME)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.CREATEDDATE, INSERTED.CREATEDBY, INSERTED.UPDATEDDATE, INSERTED.UPDATEDBY, INSERTED.SERVERNAME, INSERTED.SERVERIP, INSERTED.MACADDRESS, INSERTED.CHECKINTERVALMINUTE, INSERTED.ALARMCODE, INSERTED.ALARMMINORVALUE, INSERTED.ALARMMAJORVALUE, INSERTED.ALARMCRITICALVALUE, INSERTED.ACTIVESTATUS, INSERTED.REPEATCHECKMINOR, INSERTED.REPEATCHECKMAJOR, INSERTED.REPEATCHECKCRITICAL, INSERTED.ALARMSUN, INSERTED.ALARMMON, INSERTED.ALARMTUE, INSERTED.ALARMWED, INSERTED.ALARMTHU, INSERTED.ALARMFRI, INSERTED.ALARMSAT, INSERTED.ALARMTIMEFROM, INSERTED.ALARMTIMETO, INSERTED.ALLDAYEVENT, INSERTED.LASTCHECKTIME"
                Sql += " VALUES("
                sql += "@_CREATEDDATE" & ", "
                sql += "@_CREATEDBY" & ", "
                sql += "@_SERVERNAME" & ", "
                sql += "@_SERVERIP" & ", "
                sql += "@_MACADDRESS" & ", "
                sql += "@_CHECKINTERVALMINUTE" & ", "
                sql += "@_ALARMCODE" & ", "
                sql += "@_ALARMMINORVALUE" & ", "
                sql += "@_ALARMMAJORVALUE" & ", "
                sql += "@_ALARMCRITICALVALUE" & ", "
                sql += "@_ACTIVESTATUS" & ", "
                sql += "@_REPEATCHECKMINOR" & ", "
                sql += "@_REPEATCHECKMAJOR" & ", "
                sql += "@_REPEATCHECKCRITICAL" & ", "
                sql += "@_ALARMSUN" & ", "
                sql += "@_ALARMMON" & ", "
                sql += "@_ALARMTUE" & ", "
                sql += "@_ALARMWED" & ", "
                sql += "@_ALARMTHU" & ", "
                sql += "@_ALARMFRI" & ", "
                sql += "@_ALARMSAT" & ", "
                sql += "@_ALARMTIMEFROM" & ", "
                sql += "@_ALARMTIMETO" & ", "
                sql += "@_ALLDAYEVENT" & ", "
                sql += "@_LASTCHECKTIME"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table CF_CONFIG_CPU
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "UPDATEDDATE = " & "@_UPDATEDDATE" & ", "
                Sql += "UPDATEDBY = " & "@_UPDATEDBY" & ", "
                Sql += "SERVERNAME = " & "@_SERVERNAME" & ", "
                Sql += "SERVERIP = " & "@_SERVERIP" & ", "
                Sql += "MACADDRESS = " & "@_MACADDRESS" & ", "
                Sql += "CHECKINTERVALMINUTE = " & "@_CHECKINTERVALMINUTE" & ", "
                Sql += "ALARMCODE = " & "@_ALARMCODE" & ", "
                Sql += "ALARMMINORVALUE = " & "@_ALARMMINORVALUE" & ", "
                Sql += "ALARMMAJORVALUE = " & "@_ALARMMAJORVALUE" & ", "
                Sql += "ALARMCRITICALVALUE = " & "@_ALARMCRITICALVALUE" & ", "
                Sql += "ACTIVESTATUS = " & "@_ACTIVESTATUS" & ", "
                Sql += "REPEATCHECKMINOR = " & "@_REPEATCHECKMINOR" & ", "
                Sql += "REPEATCHECKMAJOR = " & "@_REPEATCHECKMAJOR" & ", "
                Sql += "REPEATCHECKCRITICAL = " & "@_REPEATCHECKCRITICAL" & ", "
                Sql += "ALARMSUN = " & "@_ALARMSUN" & ", "
                Sql += "ALARMMON = " & "@_ALARMMON" & ", "
                Sql += "ALARMTUE = " & "@_ALARMTUE" & ", "
                Sql += "ALARMWED = " & "@_ALARMWED" & ", "
                Sql += "ALARMTHU = " & "@_ALARMTHU" & ", "
                Sql += "ALARMFRI = " & "@_ALARMFRI" & ", "
                Sql += "ALARMSAT = " & "@_ALARMSAT" & ", "
                Sql += "ALARMTIMEFROM = " & "@_ALARMTIMEFROM" & ", "
                Sql += "ALARMTIMETO = " & "@_ALARMTIMETO" & ", "
                Sql += "ALLDAYEVENT = " & "@_ALLDAYEVENT" & ", "
                Sql += "LASTCHECKTIME = " & "@_LASTCHECKTIME" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table CF_CONFIG_CPU
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table CF_CONFIG_CPU
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, SERVERNAME, SERVERIP, MACADDRESS, CHECKINTERVALMINUTE, ALARMCODE, ALARMMINORVALUE, ALARMMAJORVALUE, ALARMCRITICALVALUE, ACTIVESTATUS, REPEATCHECKMINOR, REPEATCHECKMAJOR, REPEATCHECKCRITICAL, ALARMSUN, ALARMMON, ALARMTUE, ALARMWED, ALARMTHU, ALARMFRI, ALARMSAT, ALARMTIMEFROM, ALARMTIMETO, ALLDAYEVENT, LASTCHECKTIME FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
