Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_ALARM_LOG table LinqDB.
    '[Create by  on June, 26 2016]
    Public Class TbAlarmLogLinqDB
        Public sub TbAlarmLogLinqDB()

        End Sub 
        ' TB_ALARM_LOG
        Const _tableName As String = "TB_ALARM_LOG"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _CREATEDDATE As DateTime = New DateTime(1,1,1)
        Dim _CREATEDBY As String = ""
        Dim _UPDATEDDATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATEDBY As  String  = ""
        Dim _SERVERNAME As String = ""
        Dim _HOSTIP As String = ""
        Dim _MACADDRESS As String = ""
        Dim _ALARMACTIVITY As String = ""
        Dim _ALARMCODE As String = "((1))"
        Dim _SEVERITY As String = ""
        Dim _ALARMVALUE As String = ""
        Dim _ALARMMETHOD As String = ""
        Dim _FLAGALARM As String = ""
        Dim _SPECIFICPROBLEM As String = ""
        Dim _ALARMWAITINGCLEARID As Long = 0

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_CREATEDDATE", DbType:="DateTime NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATEDDATE() As DateTime
            Get
                Return _CREATEDDATE
            End Get
            Set(ByVal value As DateTime)
               _CREATEDDATE = value
            End Set
        End Property 
        <Column(Storage:="_CREATEDBY", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property CREATEDBY() As String
            Get
                Return _CREATEDBY
            End Get
            Set(ByVal value As String)
               _CREATEDBY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATEDDATE", DbType:="DateTime")>  _
        Public Property UPDATEDDATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATEDDATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATEDDATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATEDBY", DbType:="VarChar(100)")>  _
        Public Property UPDATEDBY() As  String 
            Get
                Return _UPDATEDBY
            End Get
            Set(ByVal value As  String )
               _UPDATEDBY = value
            End Set
        End Property 
        <Column(Storage:="_SERVERNAME", DbType:="VarChar(100) NOT NULL ",CanBeNull:=false)>  _
        Public Property SERVERNAME() As String
            Get
                Return _SERVERNAME
            End Get
            Set(ByVal value As String)
               _SERVERNAME = value
            End Set
        End Property 
        <Column(Storage:="_HOSTIP", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property HOSTIP() As String
            Get
                Return _HOSTIP
            End Get
            Set(ByVal value As String)
               _HOSTIP = value
            End Set
        End Property 
        <Column(Storage:="_MACADDRESS", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property MACADDRESS() As String
            Get
                Return _MACADDRESS
            End Get
            Set(ByVal value As String)
               _MACADDRESS = value
            End Set
        End Property 
        <Column(Storage:="_ALARMACTIVITY", DbType:="VarChar(255) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMACTIVITY() As String
            Get
                Return _ALARMACTIVITY
            End Get
            Set(ByVal value As String)
               _ALARMACTIVITY = value
            End Set
        End Property 
        <Column(Storage:="_ALARMCODE", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMCODE() As String
            Get
                Return _ALARMCODE
            End Get
            Set(ByVal value As String)
               _ALARMCODE = value
            End Set
        End Property 
        <Column(Storage:="_SEVERITY", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property SEVERITY() As String
            Get
                Return _SEVERITY
            End Get
            Set(ByVal value As String)
               _SEVERITY = value
            End Set
        End Property 
        <Column(Storage:="_ALARMVALUE", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMVALUE() As String
            Get
                Return _ALARMVALUE
            End Get
            Set(ByVal value As String)
               _ALARMVALUE = value
            End Set
        End Property 
        <Column(Storage:="_ALARMMETHOD", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMMETHOD() As String
            Get
                Return _ALARMMETHOD
            End Get
            Set(ByVal value As String)
               _ALARMMETHOD = value
            End Set
        End Property 
        <Column(Storage:="_FLAGALARM", DbType:="VarChar(50) NOT NULL ",CanBeNull:=false)>  _
        Public Property FLAGALARM() As String
            Get
                Return _FLAGALARM
            End Get
            Set(ByVal value As String)
               _FLAGALARM = value
            End Set
        End Property 
        <Column(Storage:="_SPECIFICPROBLEM", DbType:="VarChar(500) NOT NULL ",CanBeNull:=false)>  _
        Public Property SPECIFICPROBLEM() As String
            Get
                Return _SPECIFICPROBLEM
            End Get
            Set(ByVal value As String)
               _SPECIFICPROBLEM = value
            End Set
        End Property 
        <Column(Storage:="_ALARMWAITINGCLEARID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ALARMWAITINGCLEARID() As Long
            Get
                Return _ALARMWAITINGCLEARID
            End Get
            Set(ByVal value As Long)
               _ALARMWAITINGCLEARID = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _CREATEDDATE = New DateTime(1,1,1)
            _CREATEDBY = ""
            _UPDATEDDATE = New DateTime(1,1,1)
            _UPDATEDBY = ""
            _SERVERNAME = ""
            _HOSTIP = ""
            _MACADDRESS = ""
            _ALARMACTIVITY = ""
            _ALARMCODE = "((1))"
            _SEVERITY = ""
            _ALARMVALUE = ""
            _ALARMMETHOD = ""
            _FLAGALARM = ""
            _SPECIFICPROBLEM = ""
            _ALARMWAITINGCLEARID = 0
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_ALARM_LOG table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _ID = DB.GetNextID("ID",tableName, trans)
                _CreatedBy = CreatedBy
                _CreatedDate = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_ALARM_LOG table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 
                    _UPDATEDBY = UpdatedBy
                    _UPDATEDDATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_ALARM_LOG table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_ALARM_LOG table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_ALARM_LOG by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_ALARM_LOG by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As TbAlarmLogLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_ALARM_LOG by specified SERVERNAME key is retrieved successfully.
        '/// <param name=cSERVERNAME>The SERVERNAME key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataBySERVERNAME(cSERVERNAME As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_SERVERNAME", cSERVERNAME) 
            Return doChkData("SERVERNAME = @_SERVERNAME", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_ALARM_LOG by specified SERVERNAME key is retrieved successfully.
        '/// <param name=cSERVERNAME>The SERVERNAME key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateBySERVERNAME(cSERVERNAME As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_SERVERNAME", cSERVERNAME) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("SERVERNAME = @_SERVERNAME And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_ALARM_LOG by specified HOSTIP key is retrieved successfully.
        '/// <param name=cHOSTIP>The HOSTIP key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByHOSTIP(cHOSTIP As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_HOSTIP", cHOSTIP) 
            Return doChkData("HOSTIP = @_HOSTIP", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_ALARM_LOG by specified HOSTIP key is retrieved successfully.
        '/// <param name=cHOSTIP>The HOSTIP key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByHOSTIP(cHOSTIP As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_HOSTIP", cHOSTIP) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("HOSTIP = @_HOSTIP And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_ALARM_LOG by specified MACADDRESS key is retrieved successfully.
        '/// <param name=cMACADDRESS>The MACADDRESS key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByMACADDRESS(cMACADDRESS As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_MACADDRESS", cMACADDRESS) 
            Return doChkData("MACADDRESS = @_MACADDRESS", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_ALARM_LOG by specified MACADDRESS key is retrieved successfully.
        '/// <param name=cMACADDRESS>The MACADDRESS key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByMACADDRESS(cMACADDRESS As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_MACADDRESS", cMACADDRESS) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("MACADDRESS = @_MACADDRESS And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_ALARM_LOG by specified ALARMACTIVITY key is retrieved successfully.
        '/// <param name=cALARMACTIVITY>The ALARMACTIVITY key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByALARMACTIVITY(cALARMACTIVITY As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_ALARMACTIVITY", cALARMACTIVITY) 
            Return doChkData("ALARMACTIVITY = @_ALARMACTIVITY", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_ALARM_LOG by specified ALARMACTIVITY key is retrieved successfully.
        '/// <param name=cALARMACTIVITY>The ALARMACTIVITY key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByALARMACTIVITY(cALARMACTIVITY As String, cID As Long, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_ALARMACTIVITY", cALARMACTIVITY) 
            cmdPara(1) = DB.SetBigInt("@_ID", cID) 
            Return doChkData("ALARMACTIVITY = @_ALARMACTIVITY And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_ALARM_LOG by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_ALARM_LOG table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_ALARM_LOG table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_ALARM_LOG table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(15) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_CREATEDDATE", SqlDbType.DateTime)
            cmbParam(1).Value = _CREATEDDATE

            cmbParam(2) = New SqlParameter("@_CREATEDBY", SqlDbType.VarChar)
            cmbParam(2).Value = _CREATEDBY

            cmbParam(3) = New SqlParameter("@_UPDATEDDATE", SqlDbType.DateTime)
            If _UPDATEDDATE.Value.Year > 1 Then 
                cmbParam(3).Value = _UPDATEDDATE.Value
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_UPDATEDBY", SqlDbType.VarChar)
            If _UPDATEDBY.Trim <> "" Then 
                cmbParam(4).Value = _UPDATEDBY
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_SERVERNAME", SqlDbType.VarChar)
            cmbParam(5).Value = _SERVERNAME

            cmbParam(6) = New SqlParameter("@_HOSTIP", SqlDbType.VarChar)
            cmbParam(6).Value = _HOSTIP

            cmbParam(7) = New SqlParameter("@_MACADDRESS", SqlDbType.VarChar)
            cmbParam(7).Value = _MACADDRESS

            cmbParam(8) = New SqlParameter("@_ALARMACTIVITY", SqlDbType.VarChar)
            cmbParam(8).Value = _ALARMACTIVITY

            cmbParam(9) = New SqlParameter("@_ALARMCODE", SqlDbType.VarChar)
            cmbParam(9).Value = _ALARMCODE

            cmbParam(10) = New SqlParameter("@_SEVERITY", SqlDbType.VarChar)
            cmbParam(10).Value = _SEVERITY

            cmbParam(11) = New SqlParameter("@_ALARMVALUE", SqlDbType.VarChar)
            cmbParam(11).Value = _ALARMVALUE

            cmbParam(12) = New SqlParameter("@_ALARMMETHOD", SqlDbType.VarChar)
            cmbParam(12).Value = _ALARMMETHOD

            cmbParam(13) = New SqlParameter("@_FLAGALARM", SqlDbType.VarChar)
            cmbParam(13).Value = _FLAGALARM

            cmbParam(14) = New SqlParameter("@_SPECIFICPROBLEM", SqlDbType.VarChar)
            cmbParam(14).Value = _SPECIFICPROBLEM

            cmbParam(15) = New SqlParameter("@_ALARMWAITINGCLEARID", SqlDbType.BigInt)
            cmbParam(15).Value = _ALARMWAITINGCLEARID

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_ALARM_LOG by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("CreatedDate")) = False Then _CreatedDate = Convert.ToDateTime(Rdr("CreatedDate"))
                        If Convert.IsDBNull(Rdr("CreatedBy")) = False Then _CreatedBy = Rdr("CreatedBy").ToString()
                        If Convert.IsDBNull(Rdr("UpdatedDate")) = False Then _UpdatedDate = Convert.ToDateTime(Rdr("UpdatedDate"))
                        If Convert.IsDBNull(Rdr("UpdatedBy")) = False Then _UpdatedBy = Rdr("UpdatedBy").ToString()
                        If Convert.IsDBNull(Rdr("ServerName")) = False Then _ServerName = Rdr("ServerName").ToString()
                        If Convert.IsDBNull(Rdr("HostIP")) = False Then _HostIP = Rdr("HostIP").ToString()
                        If Convert.IsDBNull(Rdr("MacAddress")) = False Then _MacAddress = Rdr("MacAddress").ToString()
                        If Convert.IsDBNull(Rdr("AlarmActivity")) = False Then _AlarmActivity = Rdr("AlarmActivity").ToString()
                        If Convert.IsDBNull(Rdr("AlarmCode")) = False Then _AlarmCode = Rdr("AlarmCode").ToString()
                        If Convert.IsDBNull(Rdr("Severity")) = False Then _Severity = Rdr("Severity").ToString()
                        If Convert.IsDBNull(Rdr("AlarmValue")) = False Then _AlarmValue = Rdr("AlarmValue").ToString()
                        If Convert.IsDBNull(Rdr("AlarmMethod")) = False Then _AlarmMethod = Rdr("AlarmMethod").ToString()
                        If Convert.IsDBNull(Rdr("FlagAlarm")) = False Then _FlagAlarm = Rdr("FlagAlarm").ToString()
                        If Convert.IsDBNull(Rdr("SpecificProblem")) = False Then _SpecificProblem = Rdr("SpecificProblem").ToString()
                        If Convert.IsDBNull(Rdr("AlarmWaitingClearID")) = False Then _AlarmWaitingClearID = Convert.ToInt64(Rdr("AlarmWaitingClearID"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_ALARM_LOG by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbAlarmLogLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("CreatedDate")) = False Then _CreatedDate = Convert.ToDateTime(Rdr("CreatedDate"))
                        If Convert.IsDBNull(Rdr("CreatedBy")) = False Then _CreatedBy = Rdr("CreatedBy").ToString()
                        If Convert.IsDBNull(Rdr("UpdatedDate")) = False Then _UpdatedDate = Convert.ToDateTime(Rdr("UpdatedDate"))
                        If Convert.IsDBNull(Rdr("UpdatedBy")) = False Then _UpdatedBy = Rdr("UpdatedBy").ToString()
                        If Convert.IsDBNull(Rdr("ServerName")) = False Then _ServerName = Rdr("ServerName").ToString()
                        If Convert.IsDBNull(Rdr("HostIP")) = False Then _HostIP = Rdr("HostIP").ToString()
                        If Convert.IsDBNull(Rdr("MacAddress")) = False Then _MacAddress = Rdr("MacAddress").ToString()
                        If Convert.IsDBNull(Rdr("AlarmActivity")) = False Then _AlarmActivity = Rdr("AlarmActivity").ToString()
                        If Convert.IsDBNull(Rdr("AlarmCode")) = False Then _AlarmCode = Rdr("AlarmCode").ToString()
                        If Convert.IsDBNull(Rdr("Severity")) = False Then _Severity = Rdr("Severity").ToString()
                        If Convert.IsDBNull(Rdr("AlarmValue")) = False Then _AlarmValue = Rdr("AlarmValue").ToString()
                        If Convert.IsDBNull(Rdr("AlarmMethod")) = False Then _AlarmMethod = Rdr("AlarmMethod").ToString()
                        If Convert.IsDBNull(Rdr("FlagAlarm")) = False Then _FlagAlarm = Rdr("FlagAlarm").ToString()
                        If Convert.IsDBNull(Rdr("SpecificProblem")) = False Then _SpecificProblem = Rdr("SpecificProblem").ToString()
                        If Convert.IsDBNull(Rdr("AlarmWaitingClearID")) = False Then _AlarmWaitingClearID = Convert.ToInt64(Rdr("AlarmWaitingClearID"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_ALARM_LOG
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (ID, CREATEDDATE, CREATEDBY, SERVERNAME, HOSTIP, MACADDRESS, ALARMACTIVITY, ALARMCODE, SEVERITY, ALARMVALUE, ALARMMETHOD, FLAGALARM, SPECIFICPROBLEM, ALARMWAITINGCLEARID)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.CREATEDDATE, INSERTED.CREATEDBY, INSERTED.UPDATEDDATE, INSERTED.UPDATEDBY, INSERTED.SERVERNAME, INSERTED.HOSTIP, INSERTED.MACADDRESS, INSERTED.ALARMACTIVITY, INSERTED.ALARMCODE, INSERTED.SEVERITY, INSERTED.ALARMVALUE, INSERTED.ALARMMETHOD, INSERTED.FLAGALARM, INSERTED.SPECIFICPROBLEM, INSERTED.ALARMWAITINGCLEARID"
                Sql += " VALUES("
                sql += "@_ID" & ", "
                sql += "@_CREATEDDATE" & ", "
                sql += "@_CREATEDBY" & ", "
                sql += "@_SERVERNAME" & ", "
                sql += "@_HOSTIP" & ", "
                sql += "@_MACADDRESS" & ", "
                sql += "@_ALARMACTIVITY" & ", "
                sql += "@_ALARMCODE" & ", "
                sql += "@_SEVERITY" & ", "
                sql += "@_ALARMVALUE" & ", "
                sql += "@_ALARMMETHOD" & ", "
                sql += "@_FLAGALARM" & ", "
                sql += "@_SPECIFICPROBLEM" & ", "
                sql += "@_ALARMWAITINGCLEARID"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_ALARM_LOG
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "UPDATEDDATE = " & "@_UPDATEDDATE" & ", "
                Sql += "UPDATEDBY = " & "@_UPDATEDBY" & ", "
                Sql += "SERVERNAME = " & "@_SERVERNAME" & ", "
                Sql += "HOSTIP = " & "@_HOSTIP" & ", "
                Sql += "MACADDRESS = " & "@_MACADDRESS" & ", "
                Sql += "ALARMACTIVITY = " & "@_ALARMACTIVITY" & ", "
                Sql += "ALARMCODE = " & "@_ALARMCODE" & ", "
                Sql += "SEVERITY = " & "@_SEVERITY" & ", "
                Sql += "ALARMVALUE = " & "@_ALARMVALUE" & ", "
                Sql += "ALARMMETHOD = " & "@_ALARMMETHOD" & ", "
                Sql += "FLAGALARM = " & "@_FLAGALARM" & ", "
                Sql += "SPECIFICPROBLEM = " & "@_SPECIFICPROBLEM" & ", "
                Sql += "ALARMWAITINGCLEARID = " & "@_ALARMWAITINGCLEARID" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_ALARM_LOG
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_ALARM_LOG
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, SERVERNAME, HOSTIP, MACADDRESS, ALARMACTIVITY, ALARMCODE, SEVERITY, ALARMVALUE, ALARMMETHOD, FLAGALARM, SPECIFICPROBLEM, ALARMWAITINGCLEARID FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
