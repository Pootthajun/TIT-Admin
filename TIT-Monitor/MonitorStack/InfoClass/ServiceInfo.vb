﻿Imports System.IO
Imports System.Xml
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Namespace InfoClass
    Public Class ServiceInfo
        Inherits WindowSystemInfo


        Public Shared Function GetServiceInfo(ByVal MacAddress As String, ServiceName As String) As DataTable
            Dim dt As New DataTable
            Dim lnq As New TbProcessInfoLinqDB
            Dim sql As String = "select id,ServerName,ServerIP,MacAddress,ServiceName,ServiceStatus "
            sql += " from TB_SERVICE_INFO "
            sql += " where MacAddress='" & MacAddress & "'"
            sql += " and ServiceName='" & ServiceName & "'"
            dt = lnq.GetListBySql(sql, Nothing, Nothing)

            Return dt
        End Function


        Public Function GetCountService(ByVal wh As String, ByVal ServiceName As String, ByVal NextTime As DateTime) As DataTable

            Dim trans As New TransactionDB
            Dim lnq As New TbActivityPendingAlarmLinqDB
            Dim dt As New DataTable

            Dim _sql As String
            _sql = "select ActivityPendingAlarmID,ServerIP,AlarmName from TB_ACTIVITY_PENDING_ALARM where ServerIP ='" & wh & "'and AlarmName = '" & ServiceName & "'"
            dt = lnq.GetListBySql(_sql, trans.Trans, Nothing)

            Return dt

        End Function

        Public Shared Function GetdtWAITING_CLEAR(ByVal MacAddress As String, ByVal ServiceName As String)
            Dim dt As New DataTable
            Dim lnq As New TbAlarmWaitingClearLinqDB

            Dim sql = "select id,HostIP ,FlagAlarm ,AlarmQty,AlarmActivity from TB_ALARM_WAITING_CLEAR where "
            sql += "MacAddress ='" & MacAddress & "' and FlagAlarm = 'Alarm' and AlarmActivity = 'Service_" & ServiceName & "'"

            dt = lnq.GetListBySql(sql, Nothing, Nothing)

            Return dt
        End Function

        Public Sub SaveAlarmLogService(ByVal _Time As DateTime, ByVal ServerName As String, ByVal HostIP As String, ByVal CurrentValue As String, ByVal SpecificProblem As String, ByVal AlarmWaitingClearID As String, ByVal MacAddress As String, ByVal ServiceName As String)
            Dim sql As String = ""
            sql = "insert into TB_ALARM_LOG (AlarmActivity,CreateDate,ServerName,HostIP,AlarmName ,CurrentValue,AlarmMethod,FlagAlarm,SpecificProblem ,AlarmWaitingClearID ,MacAddress)"
            sql += "values('Service','" & _Time & "','" & ServerName & "','" & HostIP & "','" & ServiceName & "','" & CurrentValue & "','E-mail','Clear','" & SpecificProblem & "','" & AlarmWaitingClearID & "','" & MacAddress & "')"
            SqlDB.ExecuteNonQuery(sql)
        End Sub

        Public Sub CreateServicePendingAlarm(MacAddress As String, ByVal ServiceName As String)
            MyBase.CreatePendingAlarm("Service_" & ServiceName, MacAddress, "CRITICAL")
        End Sub

        Public Function GetServicePendingAlarm(ByVal MacAddress As String, ByVal ServiceName As String) As DataTable
            Return MyBase.GetPendingAlarm("Service_" & ServiceName, MacAddress, "CRITICAL")
        End Function

        Public Sub DeleteServicePendingAlarm(ByVal MacAddress As String, ServiceName As String)
            MyBase.DeletePendingAlarm("Service_" & ServiceName, MacAddress)
        End Sub

        Public Sub ProcessServiceAlarm(ByVal awDT As DataTable, ByVal cf As CfConfigServiceLinqDB, RepeatCheck As Int16, info As TbServiceInfoLinqDB, ByVal AlarmMethod As String, DisplayName As String)
            Try
                Dim Severity As String = "CRITICAL"
                Dim dt As New DataTable
                dt = GetServicePendingAlarm(cf.MACADDRESS, info.SERVICENAME)
                If dt.Rows.Count < (RepeatCheck - 1) Then
                    CreateServicePendingAlarm(cf.MACADDRESS, info.SERVICENAME)
                Else
                    Dim AlarmMsg As String = "Alarm The Service " & DisplayName & " on " & cf.SERVERIP & " has down"
                    awDT.DefaultView.RowFilter = " AlarmActivity='Service_' + '" & info.SERVICENAME & "'"
                    If awDT.DefaultView.Count = 0 Then
                        If InsertAlarmWaitingClear(cf.SERVERNAME, cf.SERVERIP, cf.MACADDRESS, cf.ALARMCODE, Severity, 0, "Service_" & info.SERVICENAME, AlarmMethod, AlarmMsg, "ServiceInfo", "Window Service Monitoring") > 0 Then
                            DeleteServicePendingAlarm(cf.MACADDRESS, info.SERVICENAME)
                        End If
                    Else
                        If UpdateAlarmWaitingClear(cf.SERVERNAME, cf.SERVERIP, cf.MACADDRESS, cf.ALARMCODE, Severity, 0, "Service_" & info.SERVICENAME, AlarmMethod, AlarmMsg, awDT.DefaultView(0)("id")).IsSuccess = True Then
                            DeleteServicePendingAlarm(cf.MACADDRESS, info.SERVICENAME)
                        End If
                    End If
                    awDT.DefaultView.RowFilter = ""
                End If
                dt.Dispose()

            Catch ex As Exception

            End Try

        End Sub
    End Class

End Namespace

