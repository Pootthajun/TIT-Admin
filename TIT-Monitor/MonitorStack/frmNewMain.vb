﻿Imports System.Data
Imports LinqDB.TABLE
Imports LinqDB.ConnectDB
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports Engine.Common
Imports Engine.Config
Imports Engine.Info



Public Class frmNewMain
    Dim TimeNow As New DateTime
    Private Sub frmNewMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TimeNow = Now
        Label1.Text = TimeNow
    End Sub

    Private Sub tmMonitor_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmMonitor.Tick
        tmMonitor.Enabled = False
        Me.Text = "TIT Monitor Management V" & getMyVersion() & "  Last Refresh Time :" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss", New Globalization.CultureInfo("en-US"))

        Dim cThe As New System.Threading.Thread(AddressOf MonitorCPU)
        cThe.Start()
        'MonitorCPU()

        Dim rThe As New System.Threading.Thread(AddressOf MonitorRAM)
        rThe.Start()
        'MonitorRAM()

        Dim hThe As New System.Threading.Thread(AddressOf MonitorHDD)
        hThe.Start()
        'MonitorHDD()

        Dim sThe As New System.Threading.Thread(AddressOf MonitorService)
        sThe.Start()
        'MonitorService()

        Dim pThe As New System.Threading.Thread(AddressOf MonitorProcess)
        pThe.Start()
        'MonitorProcess()

        Dim piThe As New System.Threading.Thread(AddressOf MonitorPing)
        piThe.Start()
        'MonitorPing()

        'Dim alThe As New System.Threading.Thread(AddressOf MonitorIamAlive)
        'alThe.Start()
        ''MonitorIamAlive()

        tmMonitor.Enabled = True
    End Sub

    Private Sub MonitorPing()
        Try
            Dim vDateNow As DateTime = SqlDB.GetDateNowFromDB(Nothing)
            Dim vTimeNow As String = vDateNow.ToString("HH:mm")

            Dim sql As String = "select cl.ServerName, cl.ServerIP, cl.MacAddress, cl.mail_alias, cl.mail_subject, p.PingNextTime,p.Status "
            sql += " From TB_PING_INFO p"
            sql += " Left join CF_CONFIG_COMPUTER_LIST cl on p.MacAddress=cl.MacAddress "
            sql += " where cl.ServerName is not null"

            Dim dt As DataTable = SqlDB.ExecuteTable(sql)
            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim dr As DataRow = dt.Rows(i)
                    Dim MailAlias As String = "ServiceInfo"
                    Dim MailSubject As String = "PING Monitoring"
                    If Convert.IsDBNull(dr("mail_alias")) = False Then
                        MailAlias = dr("mail_alias")
                    End If
                    If Convert.IsDBNull(dr("mail_subject")) = False Then
                        MailSubject = dr("mail_subject")
                    End If


                    Dim hDt As DataTable = AlarmEng.GetAlarmWaitingClear(dr("ServerName"), "PING_" & dr("ServerName"), "Alarm")

                    Dim PingNextTime As DateTime = Convert.ToDateTime(dr("PingNextTime"))
                    If vDateNow > PingNextTime Then
                        'ถ้าเวลาปัจจุบันมากกว่า PingNextTime แสดงว่า Send Info ไม่ทำงาน ให้ทำการส่ง Alarm
                        PingInfoENG.SavePingInfo(dr("ServerIP"), dr("ServerName"), dr("MacAddress"), "N")

                        Dim AlarmMsg As String = "Alarm Network Connection On " & dr("ServerName") & " Is Disconnected"
                        hDt.DefaultView.RowFilter = " AlarmActivity='PING_" & dr("ServerName") & "'"

                        If hDt.DefaultView.Count = 0 Then
                            If AlarmEng.InsertAlarmWaitingClear(dr("ServerName"), dr("ServerIP"), dr("MacAddress"), "10801", "CRITICAL", 0, "PING_" & dr("ServerName"), "MAIL", AlarmMsg, MailAlias, MailSubject) > 0 Then
                                AlarmEng.DeletePendingAlarm("PING_" & dr("ServerName"), dr("MacAddress"))
                            End If
                        Else
                            If AlarmEng.UpdateAlarmWaitingClear(dr("ServerName"), dr("ServerIP"), dr("MacAddress"), "10801", "CRITICAL", 0, "PING_" & dr("ServerName"), "MAIL", AlarmMsg, hDt.DefaultView(0)("id")).IsSuccess = True Then
                                AlarmEng.DeletePendingAlarm("PING_" & dr("ServerName"), dr("MacAddress"))
                            End If
                        End If
                    Else
                        If dr("status") = "Y" Then
                            If hDt.Rows.Count > 0 Then
                                'Clear Alarm
                                AlarmEng.SendClearAlarm(dr("ServerName"), dr("ServerIP"), dr("MacAddress"), "10801", "CRITICAL", 0, "MAIL", "PING_" & dr("ServerName"), "Clear Network Connection on " & dr("ServerName"), hDt.Rows(0)("id"))
                            End If
                        End If
                    End If
                    'hng = Nothing
                    hDt.Dispose()
                Next
            End If
            dt.Dispose()
        Catch ex As Exception
            FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub

    'Private Sub MonitorIamAlive()
    '    Try
    '        Dim vDateNow As DateTime = SqlDB.GetDateNowFromDB(Nothing)
    '        Dim vTimeNow As String = vDateNow.ToString("HH:mm")
    '        Dim dt As New DataTable
    '        Dim eng As New Engine.Config.ConfigENG
    '        dt = eng.GetIamAliveList()
    '        If dt.Rows.Count > 0 Then
    '            Dim AlarmCode As String = "0105"   'I am Alive Alarm Code

    '            For Each dr As DataRow In dt.Rows
    '                If dr("cfg_monitor_time_start") <= vTimeNow And vTimeNow <= dr("cfg_monitor_time_end") Then
    '                    Dim hng As New InfoClass.WindowSystemInfo
    '                    Dim hDt As DataTable = hng.GetAlarmWaitingClear(dr("ServerName"), "I_AM_ALIVE_" & dr("ServerName"), "Alarm")

    '                    Dim NextAliveTime As DateTime = Convert.ToDateTime(dr("next_alive_time"))
    '                    If vDateNow > NextAliveTime Then
    '                        'ถ้าเวลาปัจจุบันมากกว่า Next Alive Time แสดงว่า FaultManagement ไม่ทำงาน ให้ทำการส่ง Alarm
    '                        'Clear Alarm
    '                        Dim AlarmMsg As String = "Alarm SendClientInfo Process on " & dr("ServerName") & " is not Alive"
    '                        hDt.DefaultView.RowFilter = " AlarmActivity='I_AM_ALIVE_' + '" & dr("ServerName") & "'"
    '                        If hDt.DefaultView.Count = 0 Then
    '                            If hng.InsertAlarmWaitingClear(dr("ServerName"), dr("HostIP"), dr("MacAddress"), AlarmCode, "CRITICAL", 0, "I_AM_ALIVE_" & dr("ServerName"), "MAIL", AlarmMsg) > 0 Then
    '                                hng.DeletePendingAlarm("I_AM_ALIVE_" & dr("ServerName"), dr("MacAddress"))
    '                            End If
    '                        Else
    '                            If hng.UpdateAlarmWaitingClear(dr("ServerName"), dr("HostIP"), dr("MacAddress"), AlarmCode, "CRITICAL", 0, "I_AM_ALIVE_" & dr("ServerName"), "MAIL", AlarmMsg, hDt.DefaultView(0)("id")).IsSuccess = True Then
    '                                hng.DeletePendingAlarm("I_AM_ALIVE_" & dr("ServerName"), dr("MacAddress"))
    '                            End If
    '                        End If
    '                    Else
    '                        If hDt.Rows.Count > 0 Then
    '                            'Clear Alarm
    '                            hng.SendClearAlarm(dr("ServerName"), dr("HostIP"), dr("MacAddress"), AlarmCode, "CRITICAL", 0, "MAIL", "I_AM_ALIVE_" & dr("ServerName"), "SendClientInfo Process on " & dr("ServerName") & " is Clear", hDt.Rows(0)("id"))
    '                        End If
    '                    End If
    '                    hng = Nothing
    '                    hDt.Dispose()
    '                End If
    '            Next
    '        End If
    '        dt.Dispose()
    '        eng = Nothing
    '    Catch ex As Exception
    '        FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
    '    End Try
    'End Sub

    Private Function getMyVersion() As String
        Dim version As System.Version = System.Reflection.Assembly.GetExecutingAssembly.GetName().Version
        Return version.Major & "." & version.Minor & "." & version.Build & "." & version.Revision
    End Function


#Region "Monitor CPU"
    Private Sub MonitorCPU()
        Try
            Dim CPUcfDt As New DataTable
            CPUcfDt = CPUConfigENG.GetCPUConfigList()
            If CPUcfDt.Rows.Count > 0 Then
                For Each CPUcfDr As DataRow In CPUcfDt.Rows
                    Dim cfLnq As New LinqDB.TABLE.CfConfigCpuLinqDB
                    cfLnq.GetDataByPK(CPUcfDr("id"), Nothing)
                    If cfLnq.ID > 0 Then
                        Dim chk As Boolean = (cfLnq.ALLDAYEVENT = "Y")
                        If chk = False Then
                            Dim CurrTime As String = DateTime.Now.ToString("HH:mm")
                            If cfLnq.ALARMTIMEFROM <= CurrTime And CurrTime <= cfLnq.ALARMTIMETO Then
                                chk = True
                            End If
                        End If

                        If chk = True Then
                            If DateAdd(DateInterval.Minute, cfLnq.CHECKINTERVALMINUTE, cfLnq.LASTCHECKTIME) < LinqDB.ConnectDB.SqlDB.GetDateNowFromDB(Nothing) Then
                                Dim CPUInfoDt As DataTable = InfoClass.CPUInfo.GetCPUInfo("MacAddress='" & cfLnq.MACADDRESS & "'")
                                If CPUInfoDt.Rows.Count > 0 Then
                                    Dim infoLnq As New LinqDB.TABLE.TbCpuInfoLinqDB
                                    infoLnq.GetDataByPK(CPUInfoDt.Rows(0)("id"), Nothing)
                                    If infoLnq.ID > 0 Then
                                        Dim awDT As New DataTable
                                        awDT = InfoClass.CPUInfo.GetdtWAITING_CLEAR(cfLnq.MACADDRESS)

                                        Dim iEng As New InfoClass.CPUInfo
                                        If infoLnq.CPUPERCENT > cfLnq.ALARMCRITICALVALUE Then
                                            Dim Desc As String = "CPU Usage on " & cfLnq.SERVERNAME & " is " & infoLnq.CPUPERCENT & " % over " & cfLnq.ALARMCRITICALVALUE & " % (CRITICAL)"
                                            iEng.ProcessCPUAlarm(awDT, cfLnq, "CRITICAL", cfLnq.ALARMCRITICALVALUE, cfLnq.REPEATCHECKCRITICAL, infoLnq, infoLnq.CPUPERCENT, "MAIL")
                                        ElseIf infoLnq.CPUPERCENT > cfLnq.ALARMMAJORVALUE Then
                                            Dim Desc As String = "CPU Usage on " & cfLnq.SERVERNAME & " is " & infoLnq.CPUPERCENT & " % over " & cfLnq.ALARMMAJORVALUE & " % (MAJOR)"
                                            iEng.ProcessCPUAlarm(awDT, cfLnq, "MAJOR", cfLnq.ALARMMAJORVALUE, cfLnq.REPEATCHECKMAJOR, infoLnq, infoLnq.CPUPERCENT, "MAIL")
                                        ElseIf infoLnq.CPUPERCENT > cfLnq.ALARMMINORVALUE Then
                                            Dim Desc As String = "CPU Usage on " & cfLnq.SERVERNAME & " is " & infoLnq.CPUPERCENT & " % over " & cfLnq.ALARMMINORVALUE & " % (MONOR)"
                                            iEng.ProcessCPUAlarm(awDT, cfLnq, "MINOR", cfLnq.ALARMMINORVALUE, cfLnq.REPEATCHECKMINOR, infoLnq, infoLnq.CPUPERCENT, "MAIL")
                                        ElseIf infoLnq.CPUPERCENT <= cfLnq.ALARMMINORVALUE Then
                                            If awDT.Rows.Count > 0 Then
                                                Dim dr As DataRow = awDT.Rows(0)
                                                Dim hw As New InfoClass.WindowSystemInfo
                                                Dim SpecificProblem As String = cfLnq.SERVERNAME & " CPU Usage " & dr("Severity") & " is Clear"
                                                hw.SendClearAlarm(cfLnq.SERVERNAME, cfLnq.SERVERIP, cfLnq.MACADDRESS, cfLnq.ALARMCODE, dr("Severity"), infoLnq.CPUPERCENT, "MAIL", "CPU", SpecificProblem, dr("id"))
                                                hw = Nothing
                                            End If
                                        End If
                                        awDT.Dispose()
                                    End If
                                End If
                                CPUInfoDt.Dispose()

                                InfoClass.WindowSystemInfo.UpdateLastCheckTime(cfLnq.TableName, cfLnq.ID)
                            End If
                        End If
                    End If
                    cfLnq = Nothing
                Next
            End If
            CPUcfDt.Dispose()
        Catch ex As Exception
            FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub
#End Region


#Region "MonitorRAM"
    Private Sub MonitorRAM()
        Try
            Dim cfDt As DataTable = RamConfigENG.GetRAMConfigList()
            If cfDt.Rows.Count > 0 Then
                For Each cfDr As DataRow In cfDt.Rows
                    Dim cfLnq As New CfConfigRamLinqDB
                    cfLnq.GetDataByPK(cfDr("id"), Nothing)
                    If cfLnq.ID > 0 Then
                        Dim chk As Boolean = (cfLnq.ALLDAYEVENT = "Y")
                        If chk = False Then
                            Dim CurrTime As String = DateTime.Now.ToString("HH:mm")
                            If cfLnq.ALARMTIMEFROM <= CurrTime And CurrTime <= cfLnq.ALARMTIMETO Then
                                chk = True
                            End If
                        End If

                        If chk = True Then
                            If DateAdd(DateInterval.Minute, cfLnq.CHECKINTERVALMINUTE, cfLnq.LASTCHECKTIME) < LinqDB.ConnectDB.SqlDB.GetDateNowFromDB(Nothing) Then
                                Dim InfoDt As DataTable = InfoClass.RAMInfo.GetRAMInfo("MacAddress='" & cfLnq.MACADDRESS & "'")
                                If InfoDt.Rows.Count > 0 Then
                                    Dim infoLnq As New TbRamInfoLinqDB
                                    infoLnq.GetDataByPK(InfoDt.Rows(0)("id"), Nothing)
                                    If infoLnq.ID > 0 Then
                                        Dim awDT As New DataTable
                                        awDT = InfoClass.RAMInfo.GetdtWAITING_CLEAR(cfLnq.MACADDRESS)

                                        Dim iEng As New InfoClass.RAMInfo
                                        If infoLnq.RAMPERCENT >= cfLnq.ALARMCRITICALVALUE Then
                                            Dim Desc As String = "RAM Usage on " & cfLnq.SERVERIP & " is " & infoLnq.RAMPERCENT & " % over " & cfLnq.ALARMCRITICALVALUE & " % (CRITICAL)"
                                            iEng.ProcessRAMAlarm(awDT, cfLnq, "CRITICAL", cfLnq.ALARMCRITICALVALUE, cfLnq.REPEATCHECKCRITICAL, infoLnq, infoLnq.RAMPERCENT, "MAIL")
                                        ElseIf infoLnq.RAMPERCENT >= cfLnq.ALARMMAJORVALUE Then
                                            Dim Desc As String = "RAM Usage on " & cfLnq.SERVERIP & " is " & infoLnq.RAMPERCENT & " % over " & cfLnq.ALARMMAJORVALUE & " % (MAJOR)"
                                            iEng.ProcessRAMAlarm(awDT, cfLnq, "MAJOR", cfLnq.ALARMMAJORVALUE, cfLnq.REPEATCHECKMAJOR, infoLnq, infoLnq.RAMPERCENT, "MAIL")
                                        ElseIf infoLnq.RAMPERCENT >= cfLnq.ALARMMINORVALUE Then
                                            Dim Desc As String = "RAM Usage on " & cfLnq.SERVERIP & " is " & infoLnq.RAMPERCENT & " % over " & cfLnq.ALARMMINORVALUE & " % (MINOR)"
                                            iEng.ProcessRAMAlarm(awDT, cfLnq, "MINOR", cfLnq.ALARMMAJORVALUE, cfLnq.REPEATCHECKMAJOR, infoLnq, infoLnq.RAMPERCENT, "MAIL")
                                        Else
                                            If awDT.Rows.Count > 0 Then
                                                Dim dr As DataRow = awDT.Rows(0)
                                                Dim hw As New InfoClass.WindowSystemInfo
                                                Dim SpecificProblem As String = cfLnq.SERVERNAME & " RAM Usage " & dr("Severity") & " is Clear"
                                                hw.SendClearAlarm(cfLnq.SERVERNAME, cfLnq.SERVERIP, cfLnq.MACADDRESS, cfLnq.ALARMCODE, dr("Severity"), infoLnq.RAMPERCENT, "MAIL", "RAM", SpecificProblem, dr("id"))
                                                hw = Nothing
                                            End If
                                        End If
                                        iEng = Nothing
                                    End If
                                    infoLnq = Nothing
                                End If
                                InfoDt.Dispose()

                                InfoClass.WindowSystemInfo.UpdateLastCheckTime(cfLnq.TableName, cfLnq.ID)
                            End If
                        End If
                    End If
                    cfLnq = Nothing
                Next
            End If
            cfDt.Dispose()
        Catch ex As Exception
            FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub
#End Region


#Region "MonitorHDD"
    Private Sub MonitorHDD()
        Try
            Dim TmpcfDt As DataTable = HDDConfigENG.GetHDDConfigList()
            If TmpcfDt.Rows.Count > 0 Then
                Dim cfDt As DataTable = TmpcfDt.DefaultView.ToTable(True, "cfDriveId").Copy
                For Each cfDr As DataRow In cfDt.Rows
                    Dim cfLnq As New CfConfigDriveLinqDB
                    cfLnq.GetDataByPK(cfDr("cfDriveId"), Nothing)
                    If cfLnq.ID > 0 Then
                        Dim chk As Boolean = (cfLnq.ALLDAYEVENT = "Y")
                        If chk = False Then
                            Dim CurrTime As String = DateTime.Now.ToString("HH:mm")
                            If cfLnq.ALARMTIMEFROM <= CurrTime And CurrTime <= cfLnq.ALARMTIMETO Then
                                chk = True
                            End If
                        End If

                        If chk = True Then
                            If DateAdd(DateInterval.Minute, cfLnq.CHECKINTERVALMINUTE, cfLnq.LASTCHECKTIME) < LinqDB.ConnectDB.SqlDB.GetDateNowFromDB(Nothing) Then
                                TmpcfDt.DefaultView.RowFilter = "cfDriveId='" & cfLnq.ID & "'"
                                For Each dr As DataRowView In TmpcfDt.DefaultView
                                    Dim cfDetailLnq As New CfConfigDriveDetailLinqDB
                                    cfDetailLnq.GetDataByPK(dr("cfDriveDetailid"), Nothing)
                                    If cfDetailLnq.ID > 0 Then
                                        Dim InfoDt As DataTable = InfoClass.HDDInfo.GetHDDInfo("MacAddress='" & cfLnq.MACADDRESS & "' and DriveLetter='" & cfDetailLnq.DRIVELETTER & "'")
                                        If InfoDt.Rows.Count > 0 Then
                                            Dim infoLnq As New TbDriveInfoLinqDB
                                            infoLnq.GetDataByPK(InfoDt.Rows(0)("id"), Nothing)
                                            If infoLnq.ID > 0 Then
                                                Dim awDT As New DataTable
                                                awDT = InfoClass.HDDInfo.GetdtWAITING_CLEAR(cfLnq.MACADDRESS, infoLnq.DRIVELETTER)

                                                Dim iEng As New InfoClass.HDDInfo
                                                If infoLnq.PERCENTUSAGE >= cfDetailLnq.ALARMCRITICALVALUE Then
                                                    Dim Desc As String = "HDD Usage on " & cfLnq.SERVERIP & " Drive " & cfDetailLnq.DRIVELETTER & " is " & infoLnq.PERCENTUSAGE & " % over " & cfDetailLnq.ALARMCRITICALVALUE & " % (CRITICAL)"
                                                    iEng.ProcessHDDAlarm(awDT, cfLnq, "CRITICAL", cfDetailLnq.ALARMCRITICALVALUE, cfDetailLnq.REPEATCHECKCRITICAL, infoLnq, infoLnq.PERCENTUSAGE, "MAIL", cfDetailLnq.DRIVELETTER)
                                                ElseIf infoLnq.PERCENTUSAGE >= cfDetailLnq.ALARMMAJORVALUE Then
                                                    Dim Desc As String = "HDD Usage on " & cfLnq.SERVERIP & " Drive " & cfDetailLnq.DRIVELETTER & " is " & infoLnq.PERCENTUSAGE & " % over " & cfDetailLnq.ALARMMAJORVALUE & " % (MAJOR)"
                                                    iEng.ProcessHDDAlarm(awDT, cfLnq, "MAJOR", cfDetailLnq.ALARMMAJORVALUE, cfDetailLnq.REPEATCHECKMAJOR, infoLnq, infoLnq.PERCENTUSAGE, "MAIL", cfDetailLnq.DRIVELETTER)
                                                ElseIf infoLnq.PERCENTUSAGE >= cfDetailLnq.ALARMMINORVALUE Then
                                                    Dim Desc As String = "HDD Usage on " & cfLnq.SERVERIP & " Drive " & cfDetailLnq.DRIVELETTER & " is " & infoLnq.PERCENTUSAGE & " % over " & cfDetailLnq.ALARMMINORVALUE & " % (MINOR)"
                                                    iEng.ProcessHDDAlarm(awDT, cfLnq, "MINOR", cfDetailLnq.ALARMMINORVALUE, cfDetailLnq.REPEATCHECKMINOR, infoLnq, infoLnq.PERCENTUSAGE, "MAIL", cfDetailLnq.DRIVELETTER)
                                                Else
                                                    If awDT.Rows.Count > 0 Then
                                                        Dim awDR As DataRow = awDT.Rows(0)
                                                        Dim hw As New InfoClass.WindowSystemInfo
                                                        Dim ClearMessage As String = cfLnq.SERVERNAME & " HDD Usage on Drive " & cfDetailLnq.DRIVELETTER & " " & awDR("Severity") & " is Clear"
                                                        hw.SendClearAlarm(cfLnq.SERVERNAME, cfLnq.SERVERIP, cfLnq.MACADDRESS, cfLnq.ALARMCODE, awDR("Severity"), infoLnq.PERCENTUSAGE, "MAIL", "Drive_" & cfDetailLnq.DRIVELETTER, ClearMessage, awDR("id"))
                                                        hw = Nothing
                                                    End If
                                                End If
                                                awDT.Dispose()
                                            End If
                                            infoLnq = Nothing
                                        End If
                                        InfoDt.Dispose()
                                    End If
                                    cfDetailLnq = Nothing
                                Next
                                TmpcfDt.DefaultView.RowFilter = ""

                                InfoClass.WindowSystemInfo.UpdateLastCheckTime(cfLnq.TableName, cfLnq.ID)
                            End If
                        End If
                    End If
                    cfLnq = Nothing
                Next
                cfDt.Dispose()
            End If
            TmpcfDt.Dispose()
        Catch ex As Exception
            FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub
#End Region
    '-------------------------------MonitorHDD


#Region "MonitorProcess"
    Private Sub MonitorProcess()
        Try
            Dim TmpCfDt As DataTable = ProcessConfigENG.GetProcessConfigList()
            If TmpCfDt.Rows.Count > 0 Then
                Dim cfDT As DataTable = TmpCfDt.DefaultView.ToTable(True, "ProcessID")
                For Each cfDr As DataRow In cfDT.Rows
                    Dim cfLnq As New CfConfigProcessLinqDB
                    cfLnq.GetDataByPK(cfDr("ProcessID"), Nothing)
                    If cfLnq.ID > 0 Then
                        Dim chk As Boolean = (cfLnq.ALLDAYEVENT = "Y")
                        If chk = False Then
                            Dim CurrTime As String = DateTime.Now.ToString("HH:mm")
                            If cfLnq.ALARMTIMEFROM <= CurrTime And CurrTime <= cfLnq.ALARMTIMETO Then
                                chk = True
                            End If
                        End If

                        If chk = True Then
                            If DateAdd(DateInterval.Minute, cfLnq.CHECKINTERVALMINUTE, cfLnq.LASTCHECKTIME) < LinqDB.ConnectDB.SqlDB.GetDateNowFromDB(Nothing) Then
                                TmpCfDt.DefaultView.RowFilter = "ProcessID='" & cfLnq.ID & "'"
                                For Each dr As DataRowView In TmpCfDt.DefaultView
                                    Dim cfDetailLnq As New CfConfigProcessDetailLinqDB
                                    cfDetailLnq.GetDataByPK(dr("ProcessDetailID"), Nothing)
                                    If cfDetailLnq.ID > 0 Then
                                        Dim InfoLnq As New TbProcessInfoLinqDB
                                        InfoLnq.ChkDataByMACADDRESS_WINDOWPROCESSNAME(cfLnq.MACADDRESS, dr("WindowProcessName"), Nothing)
                                        If InfoLnq.ID > 0 Then
                                            Dim awDT As New DataTable
                                            awDT = InfoClass.ProcessInfo.GetdtWAITING_CLEAR(cfLnq.MACADDRESS, InfoLnq.WINDOWPROCESSNAME)

                                            Dim iEng As New InfoClass.ProcessInfo
                                            If InfoLnq.PROCESSALIVE <> "Y" Then
                                                iEng.ProcessProcessAlarm(awDT, cfLnq, cfDetailLnq.REPEATCHECKQTY, InfoLnq, "MAIL", dr("DisplayName"))
                                            Else
                                                If awDT.Rows.Count > 0 Then
                                                    Dim hw As New InfoClass.WindowSystemInfo
                                                    Dim ClearMsg As String = "Windows Process " & dr("DisplayName") & " down on " & cfLnq.SERVERIP & " is Clear"
                                                    hw.SendClearAlarm(cfLnq.SERVERNAME, cfLnq.SERVERIP, cfLnq.MACADDRESS, cfLnq.ALARMCODE, "CRITICAL", 0, "MAIL", "Process_" & InfoLnq.WINDOWPROCESSNAME, ClearMsg, awDT.Rows(0)("id"))
                                                    hw = Nothing
                                                End If
                                            End If
                                            awDT.Dispose()
                                        End If
                                        InfoLnq = Nothing
                                    End If
                                    cfDetailLnq = Nothing
                                Next
                                TmpCfDt.DefaultView.RowFilter = ""

                                InfoClass.WindowSystemInfo.UpdateLastCheckTime(cfLnq.TableName, cfLnq.ID)
                            End If
                        End If
                    End If
                    cfLnq = Nothing
                Next
                cfDT.Dispose()
            End If
            TmpCfDt.Dispose()
        Catch ex As Exception
            FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub

    Private Sub MonitorService()
        Try
            Dim TmpCfDt As DataTable = ServiceConfigENG.GetServiceConfigList()
            If TmpCfDt.Rows.Count > 0 Then
                Dim cfDT As DataTable = TmpCfDt.DefaultView.ToTable(True, "ServiceID")
                If cfDT.Rows.Count > 0 Then
                    For Each cfDr As DataRow In cfDT.Rows
                        Dim cfLnq As New CfConfigServiceLinqDB
                        cfLnq.GetDataByPK(cfDr("ServiceID"), Nothing)
                        If cfLnq.ID > 0 Then
                            Dim chk As Boolean = (cfLnq.ALLDAYEVENT = "Y")
                            If chk = False Then
                                Dim CurrTime As String = DateTime.Now.ToString("HH:mm")
                                If cfLnq.ALARMTIMEFROM <= CurrTime And CurrTime <= cfLnq.ALARMTIMETO Then
                                    chk = True
                                End If
                            End If

                            If chk = True Then
                                If DateAdd(DateInterval.Minute, cfLnq.CHECKINTERVALMINUTE, cfLnq.LASTCHECKTIME) < LinqDB.ConnectDB.SqlDB.GetDateNowFromDB(Nothing) Then
                                    TmpCfDt.DefaultView.RowFilter = "ServiceID='" & cfLnq.ID & "'"
                                    For Each dr As DataRowView In TmpCfDt.DefaultView
                                        Dim cfDetailLnq As New CfConfigServiceDetailLinqDB
                                        cfDetailLnq.GetDataByPK(dr("ServiceDetailID"), Nothing)
                                        If cfDetailLnq.ID > 0 Then
                                            Dim InfoLnq As New TbServiceInfoLinqDB
                                            InfoLnq.ChkDataByMACADDRESS_SERVICENAME(cfLnq.MACADDRESS, dr("WindowServiceName"), Nothing)
                                            If InfoLnq.ID > 0 Then
                                                Dim awDT As New DataTable
                                                awDT = InfoClass.ServiceInfo.GetdtWAITING_CLEAR(cfLnq.MACADDRESS, InfoLnq.SERVICENAME)

                                                Dim iEng As New InfoClass.ServiceInfo
                                                If InfoLnq.SERVICESTATUS <> "RUNNING" Then
                                                    iEng.ProcessServiceAlarm(awDT, cfLnq, cfDetailLnq.REPEATCHECKQTY, InfoLnq, "MAIL", dr("DisplayName"))
                                                Else
                                                    If awDT.Rows.Count > 0 Then
                                                        Dim hw As New InfoClass.WindowSystemInfo
                                                        Dim ClearMsg As String = "Service " & dr("DisplayName") & " down on " & cfLnq.SERVERIP & " is Clear"
                                                        hw.SendClearAlarm(cfLnq.SERVERNAME, cfLnq.SERVERIP, cfLnq.MACADDRESS, cfLnq.ALARMCODE, "CRITICAL", 0, "MAIL", "Service_" & InfoLnq.SERVICENAME, ClearMsg, awDT.Rows(0)("id"))
                                                        hw = Nothing
                                                    End If
                                                End If
                                                awDT.Dispose()
                                            End If
                                            InfoLnq = Nothing
                                        End If
                                        cfDetailLnq = Nothing
                                    Next
                                    TmpCfDt.DefaultView.RowFilter = ""

                                    InfoClass.WindowSystemInfo.UpdateLastCheckTime(cfLnq.TableName, cfLnq.ID)
                                End If
                            End If
                        End If
                        cfLnq = Nothing
                    Next
                End If
                cfDt.Dispose()
            End If
            TmpCfDt.Dispose()
        Catch ex As Exception
            FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub
#End Region

    ''-------------------------------MonitorFileSize
    'Private Sub MonitorFileSize()
    '    Try
    '        Dim TmpCfDt As DataTable = FileConfigENG.GetFileSizeConfigList
    '        If TmpCfDt.Rows.Count > 0 Then
    '            Dim cfDT As DataTable = TmpCfDt.DefaultView.ToTable(True, "FileSizeID")
    '            For Each cfDr As DataRow In cfDT.Rows
    '                Dim cfLnq As New CfConfigFilesizeLinqDB
    '                cfLnq.GetDataByPK(cfDr("FileSizeID"), Nothing)
    '                If cfLnq.ID > 0 Then
    '                    Dim chk As Boolean = (cfLnq.ALLDAYEVENT = "Y")
    '                    If chk = False Then
    '                        Dim CurrTime As String = DateTime.Now.ToString("HH:mm")
    '                        If cfLnq.ALARMTIMEFROM <= CurrTime And CurrTime <= cfLnq.ALARMTIMETO Then
    '                            chk = True
    '                        End If
    '                    End If

    '                    If chk = True Then
    '                        If DateAdd(DateInterval.Minute, cfLnq.CHECKINTERVALMINUTE, cfLnq.LASTCHECKTIME) < LinqDB.ConnectDB.SqlDB.GetDateNowFromDB(Nothing) Then
    '                            TmpCfDt.DefaultView.RowFilter = "FileSizeID='" & cfLnq.ID & "'"
    '                            For Each dr As DataRowView In TmpCfDt.DefaultView
    '                                Dim cfDetailLnq As New CfConfigFilesizeDetailLinqDB
    '                                cfDetailLnq.GetDataByPK(dr("FileSizeDetailID"), Nothing)
    '                                If cfDetailLnq.ID > 0 Then
    '                                    Dim InfoDt As DataTable = InfoClass.FileSizeInfo.GetFileSizeInfo(cfLnq.MACADDRESS, cfDetailLnq.FILENAME)
    '                                    If InfoDt.Rows.Count > 0 Then
    '                                        Dim InfoLnq As New TbFilesizeInfoLinqDB
    '                                        InfoLnq.GetDataByPK(InfoDt.Rows(0)("id"), Nothing)
    '                                        If InfoLnq.ID > 0 Then
    '                                            Dim awDT As New DataTable
    '                                            awDT = InfoClass.FileSizeInfo.GetdtWAITING_CLEAR(cfLnq.MACADDRESS, InfoLnq.FILENAME)

    '                                            Dim iEng As New InfoClass.FileSizeInfo
    '                                            If InfoLnq.FILESIZEGB >= cfDetailLnq.FILESIZECRITICAL Then
    '                                                Dim Desc As String = "File " & InfoLnq.FILENAME & " on " & cfLnq.SERVERIP & " size " & InfoLnq.FILESIZEGB & " is over than " & cfDetailLnq.FILESIZECRITICAL & "  (CRITICAL)"
    '                                                iEng.ProcessFileSizeAlarm(awDT, cfLnq, "CRITICAL", cfDetailLnq.FILESIZECRITICAL, cfDetailLnq.REPEATCHECKCRITICAL, InfoLnq, InfoLnq.FILESIZEGB, "Mail", cfDetailLnq.FILENAME)
    '                                            ElseIf InfoLnq.FILESIZEGB >= cfDetailLnq.FILESIZEMAJOR Then
    '                                                Dim Desc As String = "File " & InfoLnq.FILENAME & " on " & cfLnq.SERVERIP & " size " & InfoLnq.FILESIZEGB & " is over than " & cfDetailLnq.FILESIZEMAJOR & "  (MAJOR)"
    '                                                iEng.ProcessFileSizeAlarm(awDT, cfLnq, "MAJOR", cfDetailLnq.FILESIZEMAJOR, cfDetailLnq.REPEATCHECKMAJOR, InfoLnq, InfoLnq.FILESIZEGB, "Mail", cfDetailLnq.FILENAME)
    '                                            ElseIf InfoLnq.FILESIZEGB >= cfDetailLnq.FILESIZEMINOR Then
    '                                                Dim Desc As String = "File " & InfoLnq.FILENAME & " on " & cfLnq.SERVERIP & " size " & InfoLnq.FILESIZEGB & " is over than " & cfDetailLnq.FILESIZEMINOR & "  (MINOR)"
    '                                                iEng.ProcessFileSizeAlarm(awDT, cfLnq, "MINOR", cfDetailLnq.FILESIZEMINOR, cfDetailLnq.REPEATCHECKMINOR, InfoLnq, InfoLnq.FILESIZEGB, "Mail", cfDetailLnq.FILENAME)
    '                                            Else
    '                                                If awDT.Rows.Count > 0 Then
    '                                                    Dim awDR As DataRow = awDT.Rows(0)
    '                                                    Dim hw As New InfoClass.HardwareInfo
    '                                                    Dim ClearMessage As String = " File Size Alarm " & InfoLnq.FILENAME & " on " & cfLnq.SERVERNAME & " " & awDR("Severity") & " is Clear"
    '                                                    hw.SendClearAlarm(cfLnq.SERVERNAME, cfLnq.SERVERIP, cfLnq.MACADDRESS, awDR("Severity"), InfoLnq.FILESIZEGB, "Mail", "FileSize_" & cfDetailLnq.FILENAME, ClearMessage, awDR("id"))
    '                                                    hw = Nothing
    '                                                End If
    '                                            End If
    '                                            awDT.Dispose()
    '                                        End If
    '                                        InfoLnq = Nothing
    '                                    End If
    '                                    InfoDt.Dispose()
    '                                End If
    '                                cfDetailLnq = Nothing
    '                            Next
    '                            TmpCfDt.DefaultView.RowFilter = ""

    '                            InfoClass.HardwareInfo.UpdateLastCheckTime(cfLnq.TableName, cfLnq.ID)
    '                        End If
    '                    End If
    '                End If
    '                cfLnq = Nothing
    '            Next
    '            cfDT.Dispose()
    '        End If
    '        TmpCfDt.Dispose()
    '    Catch ex As Exception
    '        FunctionEng.CreateErrorLog("frmNewMain", "MonitorFileSize", "Exception :" & ex.Message & vbNewLine & ex.StackTrace)
    '    End Try
    'End Sub

    'Private Sub MonitorFileLost()
    '    Try
    '        Dim TmpCfDt As DataTable = FileConfigENG.GetFileLostConfigList
    '        If TmpCfDt.Rows.Count > 0 Then
    '            Dim cfDT As DataTable = TmpCfDt.DefaultView.ToTable(True, "FileLostID")
    '            For Each cfDr As DataRow In cfDT.Rows
    '                Dim cfLnq As New CfConfigFilelostLinqDB
    '                cfLnq.GetDataByPK(cfDr("FileLostID"), Nothing)
    '                If cfLnq.ID > 0 Then
    '                    Dim chk As Boolean = (cfLnq.ALLDAYEVENT = "Y")
    '                    If chk = False Then
    '                        Dim CurrTime As String = DateTime.Now.ToString("HH:mm")
    '                        If cfLnq.ALARMTIMEFROM <= CurrTime And CurrTime <= cfLnq.ALARMTIMETO Then
    '                            chk = True
    '                        End If
    '                    End If

    '                    If chk = True Then
    '                        If DateAdd(DateInterval.Minute, cfLnq.CHECKINTERVALMINUTE, cfLnq.LASTCHECKTIME) < LinqDB.ConnectDB.SqlDB.GetDateNowFromDB(Nothing) Then
    '                            TmpCfDt.DefaultView.RowFilter = "FileLostID='" & cfLnq.ID & "'"
    '                            For Each dr As DataRowView In TmpCfDt.DefaultView
    '                                Dim cfDetailLnq As New CfConfigFilelostDetailLinqDB
    '                                cfDetailLnq.GetDataByPK(dr("FileLostDetailID"), Nothing)
    '                                If cfDetailLnq.ID > 0 Then
    '                                    Dim InfoDt As DataTable = InfoClass.FileLostInfo.GetFileLostInfo(cfLnq.MACADDRESS, cfDetailLnq.FILENAME)
    '                                    If InfoDt.Rows.Count > 0 Then
    '                                        Dim InfoLnq As New TbFilelostInfoLinqDB
    '                                        InfoLnq.GetDataByPK(InfoDt.Rows(0)("id"), Nothing)
    '                                        If InfoLnq.ID > 0 Then
    '                                            Dim awDT As New DataTable
    '                                            awDT = InfoClass.FileLostInfo.GetdtWAITING_CLEAR(cfLnq.MACADDRESS, InfoLnq.FILENAME)

    '                                            Dim iEng As New InfoClass.FileLostInfo
    '                                            If InfoLnq.FILELOSTSTATUS <> "Y" Then
    '                                                iEng.ProcessFileLostAlarm(awDT, cfLnq, cfLnq.REPEATECHECKQTY, InfoLnq, "Mail")
    '                                            Else
    '                                                If awDT.Rows.Count > 0 Then
    '                                                    Dim hw As New InfoClass.HardwareInfo
    '                                                    Dim ClearMsg As String = "Alarm File Config Lost " & InfoLnq.FILENAME & " down on " & cfLnq.SERVERIP & " is Clear"
    '                                                    hw.SendClearAlarm(cfLnq.SERVERNAME, cfLnq.SERVERIP, cfLnq.MACADDRESS, "CRITICAL", 0, "Mail", "FileLost_" & InfoLnq.FILENAME, ClearMsg, awDT.Rows(0)("id"))
    '                                                    hw = Nothing
    '                                                End If
    '                                            End If
    '                                            awDT.Dispose()
    '                                        End If
    '                                        InfoLnq = Nothing
    '                                    End If
    '                                    InfoDt.Dispose()
    '                                End If
    '                                cfDetailLnq = Nothing
    '                            Next
    '                            TmpCfDt.DefaultView.RowFilter = ""

    '                            InfoClass.HardwareInfo.UpdateLastCheckTime(cfLnq.TableName, cfLnq.ID)
    '                        End If
    '                    End If
    '                End If
    '                cfLnq = Nothing
    '            Next
    '            cfDT.Dispose()
    '        End If
    '        TmpCfDt.Dispose()
    '    Catch ex As Exception
    '        FunctionEng.CreateErrorLog("frmNewMain", "MonitorFileLost", "Exception :" & ex.Message & vbNewLine & ex.StackTrace)
    '    End Try
    'End Sub

    ''-------------------------------MonitorFileSize





    Private Sub tmSendAlarm_Tick(sender As Object, e As EventArgs) Handles tmSendAlarm.Tick
        tmSendAlarm.Enabled = False
        SendMailAlarm()
        SendClearAlarm()

        tmSendAlarm.Enabled = True
    End Sub


#Region "Send Alarm "
    Private Sub SendMailAlarm()
        Try
            Dim dt As New DataTable
            Dim lnq As New TbAlarmWaitingClearLinqDB
            Dim sql As String

            'คิวรี่เพื่อหาจำนวน Alarm ที่เกิดขึ้นทั้งหมด (อาจมีกรณีที่ Alarm เดียวกันแต่อยู่คนละกลุ่มก็ได้)
            sql = " select aw.id, aw.HostIP,aw.MacAddress,aw.ServerName,aw.AlarmActivity,aw.AlarmCode,aw.Severity, " & vbNewLine
            sql += " isnull(ccl.com_location,aw.ServerName) com_name, mma.alarm_problem,mma.sms_message, " & vbNewLine
            sql += " isnull(aw.mail_alias, ccl.mail_alias) mail_alias, isnull(aw.mail_subject,aw.mail_subject) mail_subject " & vbNewLine
            sql += " from TB_ALARM_WAITING_CLEAR aw " & vbNewLine
            sql += " inner join MS_MASTER_MONITORING_ALARM mma On aw.AlarmCode=mma.alarm_code" & vbNewLine
            sql += " inner join CF_CONFIG_COMPUTER_LIST ccl On ccl.MacAddress=aw.MacAddress" & vbNewLine
            sql += " where aw.IsSendAlarm='N' and aw.FlagAlarm='Alarm' " & vbNewLine
            sql += " order by aw.AlarmCode " & vbNewLine

            dt = lnq.GetListBySql(sql, Nothing, Nothing)
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    Dim IP As String = dr("HostIP").ToString
                    Dim MacAddress As String = dr("MacAddress")
                    Dim ServerName As String = dr("ServerName")
                    Dim AlarmActivity As String = dr("AlarmActivity")
                    Dim AlarmSeverity As String = dr("severity")
                    Dim AlarmCode As String = dr("AlarmCode")
                    Dim SendMessage As String = dr("sms_message")
                    Dim LocationName As String = dr("com_name")

                    SendMessage = SendMessage.Replace("<Shop Short Name>", LocationName)

                    'หา Email สำหรับใช้ส่ง Email สำหรับแต่ละ Alarm
                    sql = "select  distinct am.e_mail " & vbNewLine
                    sql += " from TB_ALARM_GROUP g  " & vbNewLine
                    sql += " inner join TB_ALARM_GROUP_COMPUTER agc on g.id=agc.tb_alarm_group_id " & vbNewLine
                    sql += " inner join TB_ALARM_GROUP_MONITORING agm on g.id=agm.tb_alarm_group_id " & vbNewLine
                    sql += " inner join MS_MASTER_MONITORING_ALARM mma On mma.id=agm.ms_master_monitoring_alarm_id " & vbNewLine
                    sql += " inner join TB_ALARM_GROUP_EMAIL am on am.tb_alarm_group_id=agc.tb_alarm_group_id " & vbNewLine
                    sql += " where agc.MacAddress=@_MAC_ADDRESS and mma.alarm_code=@_ALARM_CODE " & vbNewLine
                    sql += " and g.ActiveStatus='Y' "
                    Dim mlP(2) As SqlParameter
                    mlP(0) = SqlDB.SetText("@_MAC_ADDRESS", MacAddress)
                    mlP(1) = SqlDB.SetText("@_ALARM_CODE", AlarmCode)

                    Dim mlDt As DataTable = SqlDB.ExecuteTable(sql, mlP)
                    If mlDt.Rows.Count > 0 Then
                        Dim Mail As New MailMessage
                        For Each mlDr As DataRow In mlDt.Rows
                            Mail.To.Add(mlDr("e_mail"))
                        Next

                        Dim MailContent As String = " <b>Alarm " & AlarmSeverity & " !! </b> <br>  " & SendMessage & " </br>"
                        MailContent += " <br /> on " & ServerName & "(" & IP & ")"
                        MailContent += " <br /><b>Please,Check your data.</b> </br>"

                        SendMail(Mail, MailContent, dr("mail_subject"), dr("mail_alias"))

                        UpdateSendAarm(dr("id"))
                    End If
                    mlDt.Dispose()
                Next
            End If
            dt.Dispose()
        Catch ex As Exception
            FunctionEng.CreateErrorLog("2. Exception :" & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub


    Private Sub UpdateSendAarm(vAlarmID As String)
        Try
            Dim sql As String = "update TB_ALARM_WAITING_CLEAR"
            sql += " set IsSendAlarm='Y'"
            sql += " where id = '" & vAlarmID & "'"

            Dim trans As New TransactionDB
            Dim lnq As New TbAlarmWaitingClearLinqDB
            If lnq.UpdateBySql(sql, trans.Trans, Nothing).IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
            lnq = Nothing
        Catch ex As Exception
            FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub

    Private Sub UpdateSendClearAlarm(vAlarmID As String)
        Try
            Dim sql As String = "update TB_ALARM_WAITING_CLEAR"
            sql += " set IsSendClear='Y'"
            sql += " where id = '" & vAlarmID & "'"

            Dim trans As New TransactionDB
            Dim lnq As New TbAlarmWaitingClearLinqDB
            If lnq.UpdateBySql(sql, trans.Trans, Nothing).IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
            lnq = Nothing
        Catch ex As Exception
            FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub



    Private Sub SendClearAlarm()
        Try
            Dim dt As New DataTable
            Dim lnq As New TbAlarmWaitingClearLinqDB
            Dim sql As String

            sql = " select aw.id, aw.HostIP,aw.MacAddress,aw.ServerName,aw.AlarmActivity,aw.AlarmCode,aw.Severity, " & vbNewLine
            sql += " aw.ClearMessage, isnull(aw.mail_alias, ccl.mail_alias) mail_alias, isnull(aw.mail_subject,aw.mail_subject) mail_subject " & vbNewLine
            sql += " from TB_ALARM_WAITING_CLEAR aw " & vbNewLine
            sql += " inner join CF_CONFIG_COMPUTER_LIST ccl On ccl.MacAddress=aw.MacAddress" & vbNewLine
            sql += " where aw.IsSendAlarm='Y' and aw.IsSendClear='N' and aw.FlagAlarm='Clear' " & vbNewLine
            sql += " order by aw.AlarmCode " & vbNewLine

            dt = lnq.GetListBySql(sql, Nothing, Nothing)
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    Dim IP As String = dr("HostIP").ToString
                    Dim ServerName As String = dr("ServerName")
                    Dim AlarmActivity As String = dr("AlarmActivity")
                    Dim AlarmSeverity As String = dr("severity")
                    Dim AlarmCode As String = dr("AlarmCode")
                    Dim ClearMessage As String = dr("ClearMessage")

                    'หา Email สำหรับใช้ส่ง Email สำหรับแต่ละ Alarm
                    sql = "select  distinct am.e_mail " & vbNewLine
                    sql += " from TB_ALARM_GROUP_COMPUTER agc " & vbNewLine
                    sql += " inner join TB_ALARM_GROUP_MONITORING agm on agm.tb_alarm_group_id=agc.tb_alarm_group_id " & vbNewLine
                    sql += " inner join MS_MASTER_MONITORING_ALARM mma On mma.id=agm.ms_master_monitoring_alarm_id " & vbNewLine
                    sql += " inner join TB_ALARM_GROUP_EMAIL am on am.tb_alarm_group_id=agc.tb_alarm_group_id " & vbNewLine
                    sql += " inner join TB_ALARM_GROUP ag on ag.id=am.tb_alarm_group_id" & vbNewLine
                    sql += " where agc.MacAddress=@_MAC_ADDRESS and mma.alarm_code=@_ALARM_CODE " & vbNewLine
                    sql += " and ag.ActiveStatus='Y' "
                    Dim mlP(2) As SqlParameter
                    mlP(0) = SqlDB.SetText("@_MAC_ADDRESS", dr("MacAddress"))
                    mlP(1) = SqlDB.SetText("@_ALARM_CODE", dr("AlarmCode"))

                    Dim mlDt As DataTable = SqlDB.ExecuteTable(sql, mlP)
                    If mlDt.Rows.Count > 0 Then
                        Dim Mail As New MailMessage
                        For Each mlDr As DataRow In mlDt.Rows
                            Mail.To.Add(mlDr("e_mail"))
                        Next

                        SendMail(Mail, ClearMessage, dr("mail_subject"), dr("mail_alias"))
                    End If
                    mlDt.Dispose()

                    UpdateSendClearAlarm(Convert.ToInt64(dr("id")))
                Next
            End If
            dt.Dispose()
        Catch ex As Exception
            Engine.Common.FunctionEng.CreateErrorLog("2. " & ex.Message & vbNewLine & ex.StackTrace)
        End Try

    End Sub


    Private Sub SendMail(Mail As MailMessage, MailContent As String, MailSubject As String, MailAlias As String)
        'ส่ง Email Alarm 
        Dim ini As New MonitorStack.Org.Mentalis.Files.IniReader(Application.StartupPath & "\Config.ini")
        ini.Section = "E-mail"

        Dim MailServer As String = ini.ReadString("MailServer")
        Dim Mailfrom As String = ini.ReadString("MailAccount")
        Dim MailPass As String = ini.ReadString("MailPassword")
        Dim MailPort As String = ini.ReadString("MailPort")
        Dim MailSSL As Boolean = ini.ReadBoolean("MailSSL")
        ini = Nothing

        Mail.Subject = MailSubject
        Mail.From = New MailAddress(Mailfrom, MailAlias)
        Mail.IsBodyHtml = True
        Mail.Body = MailContent 'Message Here
        Mail.BodyEncoding = System.Text.Encoding.UTF8

        Dim SMTP As New SmtpClient(MailServer)
        SMTP.Credentials = New System.Net.NetworkCredential(Mailfrom, MailPass) '<-- ชื่อเมลที่เราจะใช้ส่ง และรหัส 
        SMTP.EnableSsl = MailSSL
        SMTP.Port = MailPort
        SMTP.Send(Mail)
    End Sub

#End Region

    Private Sub tmTime_Tick(sender As Object, e As EventArgs) Handles tmTime.Tick
        Label1.Text = DateTime.Now
    End Sub

    Private Sub frmNewMain_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        If Me.WindowState = FormWindowState.Minimized Then
            Me.Hide()
            NotifyIcon1.Visible = True
            NotifyIcon1.Text = "Physical Monitor Management V" & getMyVersion()
        Else
            NotifyIcon1.Visible = False
        End If
    End Sub

    Private Sub frmNewMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Me.Text = "TIT Monitor Management V" & getMyVersion()
        tmMonitor.Start()
        tmSendAlarm.Start()
        Me.WindowState = FormWindowState.Minimized

        MonitorPing()
    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        Me.Show()
        Me.WindowState = FormWindowState.Normal
        NotifyIcon1.Visible = False
    End Sub
End Class