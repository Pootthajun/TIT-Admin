﻿'Imports System.Web
Imports System.Web.Services
'Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports Engine.Info.PingInfoENG
'Imports Engine.Common
'Imports System.Net.Mail

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class ApplicationWebservice
    Inherits System.Web.Services.WebService

    '<WebMethod()> _
    'Public Function HelloWorld() As String
    '    Return "Hello World"
    'End Function


    <WebMethod()>
    Public Function GetAlarmByServerName(ByVal ServerName As String) As DataTable
        Dim sql As String = "select * from TB_ALARM_WAITING_CLEAR where ServerName='" & ServerName & "' and FlagAlarm = 'Alarm' order by UpdatedDate desc"
        Dim dt As New DataTable
        dt = SqlDB.ExecuteTable(sql)
        dt.TableName = "GetAlarmByServerName"

        Return dt
    End Function

#Region "Send All Info"
    <WebMethod()>
    Public Function SendAllInfoToDC(ServerName As String, ServerIP As String, MacAddress As String, CPUPercentage As Double, CPUTemp As Double, RAMPercentage As Double, DriveInfo As DataTable, ServiceDT As DataTable, ProcessDT As DataTable) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            SendCPUInfoToDC(ServerName, ServerIP, CPUPercentage, CPUTemp, MacAddress)
            SendRAMInfoToDC(ServerName, ServerIP, RAMPercentage, MacAddress)
            SendDriveInfoToDC(ServerName, ServerIP, DriveInfo, MacAddress)
            SendServiceInfoToDC(ServerName, ServerIP, ServiceDT, MacAddress)
            SendProcessInfoToDC(ServerName, ServerIP, ProcessDT, MacAddress)
        Catch ex As Exception
            ret = New ExecuteDataInfo
            ret.IsSuccess = False
            ret.ErrorMessage = ex.Message & vbNewLine & ex.StackTrace
        End Try

        Return ret
    End Function
#End Region

#Region "CPU Info"

    <WebMethod()>
    Public Function SendCPUInfoToDC(ServerName As String, ServerIP As String, CPUPercentage As Double, CPUTemp As Double, ByVal MacAddress As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            Dim lnq As New TbCpuInfoLinqDB
            lnq.ChkDataByMACADDRESS(MacAddress, Nothing)

            If lnq.ID = 0 Then
                lnq.ChkDataBySERVERNAME(ServerName, Nothing)
            End If

            lnq.SENDTIME = DateTime.Now
            lnq.CPUPERCENT = CPUPercentage
            lnq.CPUTEMPERATURE = CPUTemp
            lnq.SERVERIP = ServerIP
            lnq.SERVERNAME = ServerName
            lnq.MACADDRESS = MacAddress

            Dim trans As New TransactionDB
            If lnq.ID > 0 Then
                ret = lnq.UpdateData("SendCPUInfoToDC", trans.Trans)
            Else
                ret = lnq.InsertData("SendCPUInfoToDC", trans.Trans)
            End If

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
                SendCPULog(ServerName, ServerIP, CPUPercentage, CPUTemp, MacAddress)
            Else
                trans.RollbackTransaction()
            End If

            SavePingInfo(ServerIP, ServerName, MacAddress, "Y")

            lnq = Nothing
        Catch ex As Exception

        End Try
        Return ret
    End Function
    Private Sub SendCPULog(ServerName As String, ServerIP As String, CPUPercentage As Double, CPUTemp As Double, ByVal MacAddress As String)
        Dim ret As New ExecuteDataInfo
        Try
            Dim lnq As New LinqDB.TABLE.TbCpuLogLinqDB
            Dim trans As New TransactionDB

            lnq.SENDTIME = DateTime.Now
            lnq.CPUPERCENT = CPUPercentage
            lnq.CPUTEMPERATURE = CPUTemp
            lnq.SERVERIP = ServerIP
            lnq.SERVERNAME = ServerName
            lnq.MACADDRESS = MacAddress

            ret = lnq.InsertData("SendCPULog", trans.Trans)

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If

            lnq = Nothing
        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "RAM Info"
    <WebMethod()>
    Public Function SendRAMInfoToDC(ServerName As String, ServerIP As String, RAMPercentage As Double, ByVal MacAddress As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            Dim lnq As New LinqDB.TABLE.TbRamInfoLinqDB
            lnq.ChkDataByMACADDRESS(MacAddress, Nothing)
            If lnq.ID = 0 Then
                lnq.ChkDataBySERVERNAME(ServerName, Nothing)
            End If
            lnq.SENDTIME = DateTime.Now
            lnq.RAMPERCENT = RAMPercentage
            lnq.SERVERIP = ServerIP
            lnq.SERVERNAME = ServerName
            lnq.MACADDRESS = MacAddress

            Dim trans As New TransactionDB
            If lnq.ID > 0 Then
                ret = lnq.UpdateData("SendRAMInfoToDC", trans.Trans)
            Else

                ret = lnq.InsertData("SendRAMInfoToDC", trans.Trans)
            End If
            If ret.IsSuccess = True Then
                trans.CommitTransaction()
                SendRAMLog(ServerName, ServerIP, RAMPercentage, MacAddress)
            Else
                trans.RollbackTransaction()
            End If

            lnq = Nothing

            SavePingInfo(ServerIP, ServerName, MacAddress, "Y")
        Catch ex As Exception

        End Try
        Return ret
    End Function
    Public Sub SendRAMLog(ServerName As String, ServerIP As String, RAMPercentage As Double, ByVal MacAddress As String)
        Dim ret As New ExecuteDataInfo
        Try
            Dim lnq As New LinqDB.TABLE.TbRamLogLinqDB
            Dim trans As New TransactionDB
            lnq.SENDTIME = DateTime.Now
            lnq.RAMPERCENT = RAMPercentage
            lnq.SERVERIP = ServerIP
            lnq.SERVERNAME = ServerName
            lnq.MACADDRESS = MacAddress

            ret = lnq.InsertData("SendRAMLog", trans.Trans)

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If

            lnq = Nothing
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Drive Info"
    <WebMethod()>
    Public Function SendDriveInfoToDC(ServerName As String, ServerIP As String, DriveInfo As DataTable, ByVal MacAddress As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            Dim trans As New TransactionDB
            If DriveInfo.Rows.Count > 0 Then
                For Each dr As DataRow In DriveInfo.Rows
                    Dim lnq As New LinqDB.TABLE.TbDriveInfoLinqDB
                    lnq.ChkDataByDRIVELETTER_MACADDRESS(dr("DriveLetter"), MacAddress, trans.Trans)
                    If lnq.ID = 0 Then
                        lnq.ChkDataByDRIVELETTER_SERVERNAME(dr("DriveLetter"), ServerName, trans.Trans)
                    End If
                    lnq.SENDTIME = DateTime.Now
                    lnq.VOLUMNLABEL = dr("VolumnLabel")
                    lnq.FREESPACEGB = Convert.ToDouble(dr("FreeSpaceGB"))
                    lnq.TOTALSIZEGB = Convert.ToDouble(dr("TotalSizeGB"))
                    lnq.PERCENTUSAGE = Convert.ToDouble(dr("PercentUsage"))
                    lnq.SERVERIP = ServerIP
                    lnq.SERVERNAME = ServerName
                    lnq.MACADDRESS = MacAddress
                    If lnq.ID > 0 Then
                        ret = lnq.UpdateData("SendDriveInfoToDC", trans.Trans)
                    Else

                        lnq.DRIVELETTER = dr("DriveLetter")
                        ret = lnq.InsertData("SendDriveInfoToDC", trans.Trans)
                    End If

                    If ret.IsSuccess = False Then
                        Exit For
                    End If

                    lnq = Nothing
                Next
            End If
            If ret.IsSuccess = True Then
                trans.CommitTransaction()
                SendDriveLog(ServerName, ServerIP, DriveInfo, MacAddress)
            Else
                trans.RollbackTransaction()
            End If

            SavePingInfo(ServerIP, ServerName, MacAddress, "Y")
        Catch ex As Exception

        End Try
        Return ret
    End Function
    Public Sub SendDriveLog(ServerName As String, ServerIP As String, DriveInfo As DataTable, ByVal MacAddress As String)
        Dim ret As New ExecuteDataInfo
        Try
            Dim trans As New TransactionDB
            If DriveInfo.Rows.Count > 0 Then
                For Each dr As DataRow In DriveInfo.Rows
                    Dim lnq As New LinqDB.TABLE.TbDriveLogLinqDB

                    lnq.SENDTIME = DateTime.Now
                    lnq.VOLUMNLABEL = dr("VolumnLabel")
                    lnq.FREESPACEGB = Convert.ToDouble(dr("FreeSpaceGB"))
                    lnq.TOTALSIZEGB = Convert.ToDouble(dr("TotalSizeGB"))
                    lnq.PERCENTUSAGE = Convert.ToDouble(dr("PercentUsage"))
                    lnq.SERVERIP = ServerIP
                    lnq.SERVERNAME = ServerName
                    lnq.MACADDRESS = MacAddress
                    lnq.DRIVELETTER = dr("DriveLetter")
                    ret = lnq.InsertData("SendDriveLog", trans.Trans)

                    If ret.IsSuccess = False Then
                        Exit For
                    End If

                    lnq = Nothing
                Next
            End If

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Service Info"

    <WebMethod()>
    Public Function SendServiceInfoToDC(ServerName As String, ServerIP As String, ServiceDT As DataTable, ByVal MacAddress As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            If ServiceDT.Rows.Count > 0 Then
                For Each dr As DataRow In ServiceDT.Rows
                    Dim trans As New TransactionDB
                    Dim lnq As New LinqDB.TABLE.TbServiceInfoLinqDB

                    lnq.ChkDataByMACADDRESS_SERVICENAME(MacAddress, dr("ServiceName"), Nothing)

                    If lnq.ID = 0 Then
                        lnq.ChkDataBySERVERNAME_SERVICENAME(ServerName, dr("ServiceName"), Nothing)
                    End If

                    lnq.SENDTIME = DateTime.Now
                    lnq.SERVICETYPE = dr("ServiceType")
                    lnq.SERVICESTATUS = dr("ServiceStatus")
                    lnq.SERVICENAME = dr("ServiceName")
                    lnq.SERVERIP = ServerIP
                    lnq.SERVERNAME = ServerName
                    lnq.MACADDRESS = MacAddress
                    If lnq.ID > 0 Then
                        ret = lnq.UpdateData("SendServiceInfoToDC", trans.Trans)
                    Else
                        ret = lnq.InsertData("SendServiceInfoToDC", trans.Trans)
                    End If

                    If ret.IsSuccess = True Then
                        trans.CommitTransaction()
                    Else
                        trans.RollbackTransaction()
                    End If
                    lnq = Nothing
                Next

                SendServiceLog(ServerName, ServerIP, ServiceDT, MacAddress)
            End If

            SavePingInfo(ServerIP, ServerName, MacAddress, "Y")
        Catch ex As Exception
            ret.IsSuccess = False
            ret.ErrorMessage = "Exception : " & ex.Message & ex.StackTrace
        End Try
        Return ret
    End Function
    Public Sub SendServiceLog(ServerName As String, ServerIP As String, ServiceDT As DataTable, ByVal MacAddress As String)
        Dim ret As New ExecuteDataInfo

        Try
            If ServiceDT.Rows.Count > 0 Then
                For Each dr As DataRow In ServiceDT.Rows
                    Dim trans As New TransactionDB
                    Dim lnq As New LinqDB.TABLE.TbServiceLogLinqDB

                    lnq.SENDTIME = DateTime.Now
                    lnq.SERVICETYPE = dr("ServiceType")
                    lnq.SERVICESTATUS = dr("ServiceStatus")
                    lnq.SERVICENAME = dr("ServiceName")
                    lnq.SERVERIP = ServerIP
                    lnq.SERVERNAME = ServerName
                    lnq.MACADDRESS = MacAddress
                    ret = lnq.InsertData("SendServiceLog", trans.Trans)

                    If ret.IsSuccess = True Then
                        trans.CommitTransaction()
                    Else
                        trans.RollbackTransaction()
                    End If
                    lnq = Nothing
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Window Process Info"
    <WebMethod()>
    Public Function SendProcessInfoToDC(ServerName As String, ServerIP As String, ProcessDT As DataTable, ByVal MacAddress As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            Dim trans As New TransactionDB
            If ProcessDT.Rows.Count > 0 Then
                For Each dr As DataRow In ProcessDT.Rows
                    Dim lnq As New LinqDB.TABLE.TbProcessInfoLinqDB
                    lnq.ChkDataByMACADDRESS_WINDOWPROCESSNAME(MacAddress, dr("WindowProcessName"), Nothing)
                    If lnq.ID = 0 Then
                        lnq.ChkDataBySERVERNAME_WINDOWPROCESSNAME(ServerName, dr("WindowProcessName"), Nothing)
                    End If
                    lnq.SENDTIME = DateTime.Now
                    lnq.WINDOWPROCESSNAME = dr("WindowProcessName")
                    lnq.PROCESSALIVE = dr("ProcessAlive")
                    lnq.SERVERIP = ServerIP
                    lnq.SERVERNAME = ServerName
                    lnq.MACADDRESS = MacAddress
                    If lnq.ID > 0 Then
                        ret = lnq.UpdateData("SendProcessInfoToDC", trans.Trans)
                    Else
                        ret = lnq.InsertData("SendProcessInfoToDC", trans.Trans)
                    End If
                    If ret.IsSuccess = False Then
                        Exit For
                    End If

                    lnq = Nothing
                Next
            End If

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
                SendProcessLog(ServerName, ServerIP, ProcessDT, MacAddress)
            Else
                trans.RollbackTransaction()
            End If

            SavePingInfo(ServerIP, ServerName, MacAddress, "Y")
        Catch ex As Exception

        End Try
        Return ret
    End Function
    Private Sub SendProcessLog(ServerName As String, ServerIP As String, ProcessDT As DataTable, ByVal MacAddress As String)
        Dim ret As New ExecuteDataInfo
        Try
            Dim trans As New TransactionDB
            If ProcessDT.Rows.Count > 0 Then
                For Each dr As DataRow In ProcessDT.Rows
                    Dim lnq As New LinqDB.TABLE.TbProcessLogLinqDB

                    lnq.SENDTIME = DateTime.Now
                    lnq.WINDOWPROCESSNAME = dr("WindowProcessName")
                    lnq.PROCESSALIVE = dr("ProcessAlive")
                    lnq.SERVERIP = ServerIP
                    lnq.SERVERNAME = ServerName
                    lnq.MACADDRESS = MacAddress

                    ret = lnq.InsertData("SendProcessLog", trans.Trans)

                    If ret.IsSuccess = False Then
                        Exit For
                    End If

                    lnq = Nothing
                Next
            End If

            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
        Catch ex As Exception

        End Try
    End Sub

#End Region



#Region "Create Monitor Port"
    <WebMethod()>
    Public Function GetConfigPortList() As DataTable
        Dim dt As New DataTable
        Try
            Dim sql As String = " select cf.id PortID, cfd.id PortDetailID "
            sql += " from CF_CONFIG_PORT cf "
            sql += " inner join CF_CONFIG_PORT_DETAIL cfd on cf.id=cfd.cf_config_port_id"
            sql += " where cf.ActiveStatus='Y' "
            Dim CaseDay As Integer = DatePart(DateInterval.Weekday, DateTime.Now)
            Select Case CaseDay
                Case 1
                    sql += " and cf.AlarmSun ='Y'"
                Case 2
                    sql += " and cf.AlarmMon ='Y'"
                Case 3
                    sql += " and cf.AlarmTue ='Y'"
                Case 4
                    sql += " and cf.AlarmWed ='Y'"
                Case 5
                    sql += " and cf.AlarmThu ='Y'"
                Case 6
                    sql += " and cf.AlarmFri ='Y'"
                Case 7
                    sql += " and cf.AlarmSat ='Y'"
            End Select

            dt = SqlDB.ExecuteTable(sql)
        Catch ex As Exception

        End Try
        dt.TableName = "GetConfigPortList"
        Return dt
    End Function

    <WebMethod()>
    Public Function GetCfConfigPortLinqDB(cfPortID As Long) As LinqDB.TABLE.CfConfigPortLinqDB
        Dim ret As New LinqDB.TABLE.CfConfigPortLinqDB
        ret.GetDataByPK(cfPortID, Nothing)
        Return ret
    End Function

    <WebMethod()>
    Public Function GetCfConfigPortDetailLinqDB(cfPortDetailID As Long) As LinqDB.TABLE.CfConfigPortDetailLinqDB
        Dim ret As New LinqDB.TABLE.CfConfigPortDetailLinqDB
        ret.GetDataByPK(cfPortDetailID, Nothing)
        Return ret
    End Function
#End Region

#Region "Monitor Port Alarm"

    <WebMethod()>
    Public Function GetPortAlarmWaitingClear(ByVal MacAddress As String, ByVal PortNumber As String) As DataTable
        Dim dt As New DataTable
        Try
            Dim lnq As New LinqDB.TABLE.TbAlarmWaitingClearLinqDB

            Dim sql = "select id,AlarmActivity from TB_ALARM_WAITING_CLEAR where "
            sql += "MacAddress =@_MACADDRESS and FlagAlarm = 'Alarm' and AlarmActivity = @_PORTNUMBER"
            Dim parm(2) As SqlParameter
            parm(0) = SqlDB.SetText("@_MACADDRESS", MacAddress)
            parm(1) = SqlDB.SetText("@_PORTNUMBER", "Port_" & PortNumber)

            dt = lnq.GetListBySql(sql, Nothing, parm)
            lnq = Nothing
        Catch ex As Exception
            dt = New DataTable
        End Try

        dt.TableName = "GetPortAlarmWaitingClear"
        Return dt
    End Function

    <WebMethod()>
    Public Sub ProcessPortAlarm(ByVal awDT As DataTable, ByVal cf As LinqDB.TABLE.CfConfigPortLinqDB, PortNumber As Integer, RepeatCheck As Int16, ByVal AlarmMethod As String)
        Try
            Dim Severity As String = "CRITICAL"
            Dim dt As New DataTable
            dt = GetPendingAlarm("Port", cf.MACADDRESS, Severity)
            If dt.Rows.Count < (RepeatCheck - 1) Then
                CreatePendingAlarm("Port", cf.MACADDRESS, Severity)
            Else
                Dim AlarmMsg As String = "PORT " & PortNumber & " on " & cf.SERVERIP & " is not connect"
                awDT.DefaultView.RowFilter = " AlarmActivity='Port_' + '" & PortNumber & "'"
                If awDT.DefaultView.Count = 0 Then
                    If InsertAlarmWaitingClear(cf.SERVERNAME, cf.SERVERIP, cf.MACADDRESS, Severity, 0, "Port_" & PortNumber, AlarmMethod, AlarmMsg) > 0 Then
                        DeletePendingAlarm("Port", cf.MACADDRESS)
                    End If
                Else
                    Dim re As ExecuteDataInfo = UpdateAlarmWaitingClear(cf.SERVERNAME, cf.SERVERIP, cf.MACADDRESS, Severity, 0, "Port_" & PortNumber, AlarmMethod, AlarmMsg, awDT.DefaultView(0)("id"))
                    If re.IsSuccess = True Then
                        DeletePendingAlarm("Port", cf.MACADDRESS)
                    End If
                End If
                awDT.DefaultView.RowFilter = ""
            End If
            dt.Dispose()

        Catch ex As Exception

        End Try

    End Sub

    Private Function GetPendingAlarm(AlarmActivity As String, ByVal MacAddress As String, ByVal Severity As String) As DataTable
        Dim dt As New DataTable
        Dim sql As String = "select id from TB_ACTIVITY_PENDING_ALARM "
        sql += " where AlarmActivity=@_AlarmActivity and MacAddress=@_MacAddress and Severity=@_Severity"
        Dim parm(3) As SqlParameter
        parm(0) = SqlDB.SetText("@_AlarmActivity", AlarmActivity)
        parm(1) = SqlDB.SetText("@_MacAddress", MacAddress)
        parm(2) = SqlDB.SetText("@_Severity", Severity)

        dt = SqlDB.ExecuteTable(sql, parm)
        Return dt
    End Function

    Private Sub CreatePendingAlarm(AlarmActivity As String, ByVal MacAddress As String, ByVal Severity As String)
        Try
            Dim lnq As New LinqDB.TABLE.TbActivityPendingAlarmLinqDB
            lnq.ALARMACTIVITY = AlarmActivity
            lnq.MACADDRESS = MacAddress
            lnq.SEVERITY = Severity

            Dim trans As New TransactionDB
            If lnq.InsertData("ApplicationWebservice", trans.Trans).IsSuccess Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
            lnq = Nothing
        Catch ex As Exception

        End Try
    End Sub

    <WebMethod()>
    Public Function InsertAlarmWaitingClear(ByVal ServerName As String, ByVal IPAddress As String, MacAddress As String, ByVal Severity As String, ByVal AlarmValue As Double, ByVal AlarmActivity As String, ByVal AlarmMethod As String, ByVal SpecificProblem As String) As Long
        Dim ret As Long = 0
        Try
            Dim lnq As New LinqDB.TABLE.TbAlarmWaitingClearLinqDB
            lnq.SERVERNAME = ServerName
            lnq.HOSTIP = IPAddress
            lnq.MACADDRESS = MacAddress
            lnq.ALARMACTIVITY = AlarmActivity
            lnq.SEVERITY = Severity
            lnq.ALARMVALUE = AlarmValue
            lnq.ALARMMETHOD = AlarmMethod
            lnq.FLAGALARM = "Alarm"
            lnq.SPECIFICPROBLEM = SpecificProblem
            lnq.ALARMQTY = 1
            lnq.ISSENDALARM = "N"
            lnq.ISSENDCLEAR = "Z"

            Dim trans As New TransactionDB

            If lnq.InsertData("ApplicationWebservice", trans.Trans).IsSuccess = True Then
                trans.CommitTransaction()
                ret = lnq.ID
                InsertAlarmLog(ServerName, IPAddress, MacAddress, AlarmActivity, Severity, AlarmValue, "Alarm", AlarmMethod, SpecificProblem, ret)
            Else
                ret = 0
                trans.RollbackTransaction()
            End If
            lnq = Nothing
        Catch ex As Exception
            ret = 0
        End Try

        Return ret
    End Function

    Private Sub InsertAlarmLog(ByVal ServerName As String, ByVal HostIP As String, MacAddress As String, ByVal AlarmActivity As String, ByVal Severity As String, ByVal CurrentValue As String, ByVal FlagAlarm As String, ByVal AlarmMethod As String, ByVal SpecificPloblem As String, ByVal AlarmWaitingClearID As Integer)
        Try
            Dim lnq As New LinqDB.TABLE.TbAlarmLogLinqDB
            lnq.SERVERNAME = ServerName
            lnq.HOSTIP = HostIP
            lnq.MACADDRESS = MacAddress
            lnq.ALARMACTIVITY = AlarmActivity
            lnq.SEVERITY = Severity
            lnq.ALARMVALUE = CurrentValue
            lnq.ALARMMETHOD = AlarmMethod
            lnq.FLAGALARM = FlagAlarm
            lnq.SPECIFICPROBLEM = SpecificPloblem
            lnq.ALARMWAITINGCLEARID = AlarmWaitingClearID

            Dim trans As New TransactionDB
            If lnq.InsertData("ApplicationWebservice", trans.Trans).IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
            lnq = Nothing
        Catch ex As Exception

        End Try

    End Sub

    <WebMethod()>
    Public Function UpdateAlarmWaitingClear(ByVal ServerName As String, ByVal IPAddress As String, MacAddress As String, ByVal Severity As String, ByVal AlarmValue As Double, ByVal AlarmActivity As String, ByVal AlarmMethod As String, ByVal SpecificProblem As String, ByVal AlarmWaitingClearID As Long) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            Dim lnq As New LinqDB.TABLE.TbAlarmWaitingClearLinqDB
            lnq.GetDataByPK(AlarmWaitingClearID, Nothing)

            lnq.ALARMQTY = lnq.ALARMQTY + 1
            lnq.LASTUPDATEDATE = DateTime.Now

            If lnq.ID > 0 Then
                Dim trans As New TransactionDB
                ret = lnq.UpdateData("ApplicationService_UpdateAlarmWaitingClear", trans.Trans)
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()

                    InsertAlarmLog(ServerName, IPAddress, MacAddress, AlarmActivity, Severity, AlarmValue, "Alarm", AlarmMethod, SpecificProblem, AlarmWaitingClearID)
                Else
                    trans.RollbackTransaction()
                End If
            End If
            lnq = Nothing
        Catch ex As Exception
            ret.IsSuccess = False
            ret.ErrorMessage = "Exception : " & ex.Message & ex.StackTrace
        End Try
        Return ret
    End Function

    <WebMethod()>
    Public Sub DeletePendingAlarm(ByVal AlarmActivity As String, ByVal MacAddress As String)
        Dim sql As String = "delete from TB_ACTIVITY_PENDING_ALARM where MacAddress='" & MacAddress & "' and AlarmActivity='" & AlarmActivity & "' "
        SqlDB.ExecuteNonQuery(sql)
    End Sub

    <WebMethod()>
    Public Function SendClearAlarm(ByVal ServerName As String, ByVal HostIP As String, MacAddress As String, ByVal Severity As String, ByVal AlarmValue As String, ByVal AlarmMethod As String, ByVal AlarmActivity As String, ByVal ClearMessage As String, ByVal AlarmWaitingClearID As Integer) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            Dim lnq As New LinqDB.TABLE.TbAlarmWaitingClearLinqDB
            lnq.GetDataByPK(AlarmWaitingClearID, Nothing)

            lnq.FLAGALARM = "Clear"
            lnq.CLEARDATE = DateTime.Now
            lnq.ISSENDCLEAR = "N"
            lnq.CLEARMESSAGE = ClearMessage

            If lnq.ID > 0 Then
                Dim trans As New TransactionDB
                ret = lnq.UpdateData("ApplicationWebservice.SendClearAlarm", trans.Trans)
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()

                    InsertAlarmLog(ServerName, HostIP, MacAddress, AlarmActivity, Severity, AlarmValue, "Clear", AlarmMethod, ClearMessage, AlarmWaitingClearID)
                Else
                    trans.RollbackTransaction()
                End If
            End If
            lnq = Nothing
        Catch ex As Exception
            ret.IsSuccess = False
            ret.ErrorMessage = "Exception : " & ex.Message & ex.StackTrace
        End Try

        Return ret
    End Function
    <WebMethod()>
    Public Function UpdateLastCheckTime(TbName As String, cfID As Long) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            Dim sql As String = "update " & TbName
            sql += " set LastCheckTime=getdate()"
            sql += " where id=@_ID"
            Dim parm(1) As SqlParameter
            parm(0) = SqlDB.SetBigInt("@_ID", cfID)

            ret = SqlDB.ExecuteNonQuery(sql, parm)
        Catch ex As Exception
            ret.IsSuccess = False
            ret.ErrorMessage = "Exception : " & ex.Message & ex.StackTrace
        End Try
        Return ret
    End Function
#End Region

#Region "Master Checklist"
    <WebMethod()>
    Public Function GetWindowsServiceCheckList(MacAddress As String) As DataTable
        Dim ret As New DataTable
        Try
            Dim lnq As New LinqDB.TABLE.CfConfigServiceLinqDB
            Dim sql As String = "select ms.WindowServiceName, cf.ActiveStatus "
            sql += " from CF_CONFIG_SERVICE cf"
            sql += " inner join CF_CONFIG_SERVICE_DETAIL cfd on cf.id=cfd.cf_config_service_id"
            sql += " inner join MS_MASTER_SERVICE_CHECKLIST ms on ms.id=cfd.ms_master_service_checklist_id"
            sql += " where ms.ActiveStatus='Y' and cf.ActiveStatus='Y'"
            sql += " and cf.MacAddress=@_MAC_ADDRESS"
            sql += " order by ms.WindowServiceName"

            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_MAC_ADDRESS", MacAddress)

            ret = lnq.GetListBySql(sql, Nothing, p)
            lnq = Nothing
        Catch ex As Exception

        End Try

        ret.TableName = "GetWindowsServiceCheckList"
        Return ret
    End Function

    <WebMethod()>
    Public Function GetWindowsProcessCheckList(MacAddress As String) As DataTable
        Dim ret As New DataTable
        Try
            Dim sql As String = "select ms.WindowProcessName, p.HeartbeatFile, pd.AutoRestart, p.ProcessExeFile "
            sql += " from MS_MASTER_PROCESS_CHECKLIST ms "
            sql += " inner join CF_CONFIG_PROCESS_DETAIL pd on pd.ms_master_process_checklist_id=ms.id "
            sql += " inner join CF_CONFIG_PROCESS p on pd.cf_config_process_id=p.id "
            sql += " where p.ActiveStatus='Y' and ms.ActiveStatus='Y'"
            sql += " and p.MacAddress=@_MAC_ADDRESS"
            sql += " order by ms.WindowProcessName"

            Dim lnq As New LinqDB.TABLE.CfConfigProcessLinqDB
            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_MAC_ADDRESS", MacAddress)

            ret = lnq.GetListBySql(sql, Nothing, p)
            lnq = Nothing
        Catch ex As Exception

        End Try

        ret.TableName = "GetWindowsProcessCheckList"
        Return ret
    End Function
#End Region

#Region "Send Alarm from Other App"
    <WebMethod()>
    Public Function SendAlarmOtherApp(MacAddress As String, alDt As DataTable, IsProblem As Boolean, LocationName As String, MailAlias As String, MailSubject As String) As String
        Dim ret As String = "false"
        Try
            Dim re As ExecuteDataInfo = Engine.Common.OtherAppEng.SendAlarmOtherApp(MacAddress, alDt, IsProblem, LocationName, MailAlias, MailSubject)
            If re.IsSuccess = True Then
                ret = "true"
            Else
                ret = "false|" & re.ErrorMessage
            End If

        Catch ex As Exception
            ret = "false|Exception " & ex.Message & vbNewLine & ex.StackTrace
        End Try

        Return ret
    End Function

    <WebMethod()>
    Public Function SendDTAlarmOtherApp(MacAddress As String, alDt As DataTable, dt As DataTable, LocationName As String, MailAlias As String) As String
        Dim ret As String = "false"
        Try
            Dim re As New ExecuteDataInfo '= Engine.Common.OtherAppEng.SendAlarmOtherApp(MacAddress, alDt, IsProblem, LocationName, MailAlias, MailSubject)
            For Each dr As DataRow In dt.Rows
                alDt.DefaultView.RowFilter = "alarm_problem='" & dr("alarm_problem") & "'"
                If alDt.DefaultView.Count > 0 Then
                    re = Engine.Common.OtherAppEng.SendAlarmOtherApp(MacAddress, alDt.DefaultView.ToTable.Copy, (dr("is_alarm") = "Y"), LocationName, "Alarm Left & Lug " & dr("alarm_problem"), MailAlias)
                    If re.IsSuccess = False Then
                        Exit For
                    End If
                End If
                alDt.DefaultView.RowFilter = ""
            Next

            If re.IsSuccess = True Then
                ret = "true"
            Else
                ret = "false|" & re.ErrorMessage
            End If

        Catch ex As Exception
            ret = "false|Exception " & ex.Message & vbNewLine & ex.StackTrace
        End Try

        Return ret
    End Function
#End Region

#Region "Master Group Alarm"
    <WebMethod()>
    Public Function SendMasterAlarmGroup(SystemCode As String, UserName As String, GroupCode As String, GroupName As String, GroupStatus As String, DT_COMPUTER As DataTable, DT_KIOSK As DataTable, DT_EMAIL As DataTable) As String
        Dim re As String = "false"
        Dim trans As New TransactionDB
        Try
            '=== TB_ALARM_GROUP
            Dim lnqAlGroup As New TbAlarmGroupLinqDB
            lnqAlGroup.ChkDataByALARM_GROUP_CODE_SYSTEM_CODE(GroupCode, SystemCode, trans.Trans)
            With lnqAlGroup
                .SYSTEM_CODE = SystemCode
                .ALARM_GROUP_CODE = GroupCode
                .ALARM_GROUP_NAME = GroupName
                .ACTIVESTATUS = GroupStatus  'Y/N
            End With
            Dim ret As ExecuteDataInfo
            If lnqAlGroup.ID > 0 Then
                ret = lnqAlGroup.UpdateData(UserName, trans.Trans)
            Else
                ret = lnqAlGroup.InsertData(UserName, trans.Trans)
            End If

            If ret.IsSuccess = False Then
                trans.RollbackTransaction()
                Return ret.ErrorMessage
            End If


            '=== TB_ALARM_GROUP_MONITORING
            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_GROUP_ID", lnqAlGroup.ID)
            ret = SqlDB.ExecuteNonQuery("DELETE FROM TB_ALARM_GROUP_MONITORING WHERE TB_ALARM_GROUP_ID=@_GROUP_ID", p)

            If ret.IsSuccess = True AndAlso DT_KIOSK.Rows.Count > 0 Then
                For i As Integer = 0 To DT_KIOSK.Rows.Count - 1
                    With DT_KIOSK
                        'Dim alarm_id As String = .Rows(i)("ID").ToString
                        Dim msMonLnq As New MsMasterMonitoringAlarmLinqDB
                        msMonLnq.ChkDataByALARM_CODE_SYSTEM_CODE(.Rows(i)("ALARM_CODE").ToString(), SystemCode, trans.Trans)
                        If msMonLnq.ID > 0 Then
                            Dim lnqAlGroupMonitoring = New TbAlarmGroupMonitoringLinqDB

                            With lnqAlGroupMonitoring
                                .TB_ALARM_GROUP_ID = lnqAlGroup.ID
                                .MS_MASTER_MONITORING_ALARM_ID = msMonLnq.ID
                            End With

                            Dim d_ret As New ExecuteDataInfo
                            d_ret = lnqAlGroupMonitoring.InsertData(UserName, trans.Trans)

                            If d_ret.IsSuccess = False Then
                                trans.RollbackTransaction()
                                Return d_ret.ErrorMessage
                            End If

                            lnqAlGroupMonitoring = Nothing
                        Else
                            trans.RollbackTransaction()
                            Return msMonLnq.ErrorMessage
                        End If
                    End With
                Next
            End If


            '===TB_ALARM_GROUP_COMPUTER
            Dim p_c(1) As SqlParameter
            p_c(0) = SqlDB.SetText("@_GROUP_ID", lnqAlGroup.ID)
            ret = SqlDB.ExecuteNonQuery("DELETE FROM TB_ALARM_GROUP_COMPUTER WHERE TB_ALARM_GROUP_ID=@_GROUP_ID", p_c)

            If ret.IsSuccess = True AndAlso DT_COMPUTER.Rows.Count > 0 Then
                For i As Integer = 0 To DT_COMPUTER.Rows.Count - 1
                    With DT_COMPUTER
                        Dim mac_address As String = .Rows(i)("MACADDRESS").ToString
                        Dim lnqAlGroupComputer = New TbAlarmGroupComputerLinqDB

                        With lnqAlGroupComputer
                            .TB_ALARM_GROUP_ID = lnqAlGroup.ID
                            .MACADDRESS = mac_address
                        End With

                        Dim d_ret As New ExecuteDataInfo
                        d_ret = lnqAlGroupComputer.InsertData(UserName, trans.Trans)

                        If d_ret.IsSuccess = False Then
                            trans.RollbackTransaction()
                            Return d_ret.ErrorMessage
                        End If

                        lnqAlGroupComputer = Nothing
                    End With
                Next
            End If

            '===TB_ALARM_GROUP_EMAIL
            Dim p_e(1) As SqlParameter
            p_e(0) = SqlDB.SetText("@_GROUP_ID", lnqAlGroup.ID)
            ret = SqlDB.ExecuteNonQuery("DELETE FROM TB_ALARM_GROUP_EMAIL WHERE TB_ALARM_GROUP_ID=@_GROUP_ID", p_e)

            If ret.IsSuccess = True AndAlso DT_EMAIL.Rows.Count > 0 Then
                For i As Integer = 0 To DT_EMAIL.Rows.Count - 1
                    With DT_EMAIL
                        Dim e_mail As String = .Rows(i)("E_MAIL").ToString
                        Dim lnqAlGroupEmail = New TbAlarmGroupEmailLinqDB

                        With lnqAlGroupEmail
                            .TB_ALARM_GROUP_ID = lnqAlGroup.ID
                            .E_MAIL = e_mail
                        End With

                        Dim d_ret As New ExecuteDataInfo
                        d_ret = lnqAlGroupEmail.InsertData(UserName, trans.Trans)

                        If d_ret.IsSuccess = False Then
                            trans.RollbackTransaction()
                            Return d_ret.ErrorMessage
                        End If

                        lnqAlGroupEmail = Nothing
                    End With
                Next
            End If

            trans.CommitTransaction()
            lnqAlGroup = Nothing
            re = "true"
        Catch ex As Exception
            re = "Exception " & ex.Message & " " & ex.StackTrace
        End Try

        Return re
    End Function
#End Region

#Region "Master Config Computer List"
    <WebMethod()>
    Public Function CheckConfigComputerList(ServerName As String, ServerIP As String, MacAddress As String) As String
        Dim ret As String = "false"
        Try
            Dim lnq As New CfConfigComputerListLinqDB
            lnq.ChkDataBySERVERNAME(ServerName, Nothing)

            If lnq.ID = 0 Then
                lnq.ChkDataBySERVERIP(ServerIP, Nothing)

                If lnq.ID = 0 Then
                    lnq.ChkDataByMACADDRESS(MacAddress, Nothing)
                End If
            End If

            If lnq.ID > 0 Then
                ret = "true|" & lnq.SYSTEM_CODE & "|" & lnq.ACTIVESTATUS
            Else
                ret = "false|No data found"
            End If
            lnq = Nothing
        Catch ex As Exception
            ret = "false|Exception" & ex.Message & vbNewLine & ex.StackTrace
        End Try
        Return ret
    End Function

    <WebMethod()>
    Public Function SaveConfigComputerList(UserName As String, ServerName As String, ServerIP As String, MacAddress As String, SystemCode As String, ActiveStatus As String, MailAlias As String, MailSubject As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try

#Region "Validate"
            If ServerName.Trim = "" Then
                ret.IsSuccess = False
                ret.ErrorMessage = "Please input ServerName"

                Return ret
            End If
            If ServerIP.Trim = "" Then
                ret.IsSuccess = False
                ret.ErrorMessage = "Please input ServerIP"

                Return ret
            End If
            If MacAddress.Trim = "" Then
                ret.IsSuccess = False
                ret.ErrorMessage = "Please input MacAddress"

                Return ret
            End If
            If SystemCode.Trim = "" Then
                ret.IsSuccess = False
                ret.ErrorMessage = "Please input SystemCode"

                Return ret
            End If
            If ActiveStatus.Trim = "" Then
                ret.IsSuccess = False
                ret.ErrorMessage = "Please input ActiveStatus"

                Return ret
            End If
            If MailAlias.Trim = "" Then
                ret.IsSuccess = False
                ret.ErrorMessage = "Please input MailAlias"

                Return ret
            End If
            If MailSubject.Trim = "" Then
                ret.IsSuccess = False
                ret.ErrorMessage = "Please input MailSubject"

                Return ret
            End If
#End Region

            Dim lnq As New CfConfigComputerListLinqDB
            lnq.ChkDataBySERVERNAME(ServerName, Nothing)

            If lnq.ID = 0 Then
                lnq.ChkDataBySERVERIP(ServerIP, Nothing)

                If lnq.ID = 0 Then
                    lnq.ChkDataByMACADDRESS(MacAddress, Nothing)
                End If
            End If

            lnq.SERVERNAME = ServerName
            lnq.SERVERIP = ServerIP
            lnq.MACADDRESS = MacAddress
            lnq.SYSTEM_CODE = SystemCode
            lnq.ONLINE_STATUS = "N"
            lnq.TODAY_AVAILABLE = 0
            lnq.ACTIVESTATUS = ActiveStatus
            lnq.MAIL_ALIAS = MailAlias
            lnq.MAIL_SUBJECT = MailSubject

            Dim trans As New TransactionDB
            If lnq.ID > 0 Then
                ret = lnq.UpdateData(UserName, trans.Trans)
            Else
                ret = lnq.InsertData(UserName, trans.Trans)
            End If
            lnq = Nothing

            If ret.IsSuccess = True Then
                trans.CommitTransaction()

                SavePingInfo(ServerIP, ServerName, MacAddress, "N")
            Else
                trans.RollbackTransaction()
            End If

        Catch ex As Exception
            ret.IsSuccess = False
            ret.ErrorMessage = "false|Exception" & ex.Message & vbNewLine & ex.StackTrace
        End Try

        Return ret
    End Function

    <WebMethod()>
    Public Function SaveCaptureScreen(ServerName As String, ServerIP As String, MacAddress As String, Screen As Byte()) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Try
            Dim lnq As New CfConfigComputerListLinqDB
            lnq.ChkDataBySERVERNAME(ServerName, Nothing)

            If lnq.ID = 0 Then
                lnq.ChkDataBySERVERIP(ServerIP, Nothing)

                If lnq.ID = 0 Then
                    lnq.ChkDataByMACADDRESS(MacAddress, Nothing)
                End If
            End If

            If lnq.ID > 0 Then
                Dim trans As New TransactionDB

                lnq.CURRENT_SCREEN = Screen
                lnq.CAPTURE_SCREEN_TIME = DateTime.Now
                ret = lnq.UpdateData(lnq.UPDATEDBY, trans.Trans)

                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                Else
                    trans.RollbackTransaction()
                End If
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = "No computer data"
            End If
            lnq = Nothing
        Catch ex As Exception
            ret.IsSuccess = False
            ret.ErrorMessage = "false|Exception" & ex.Message & vbNewLine & ex.StackTrace
        End Try
        Return ret
    End Function

#End Region

    <WebMethod()>
    Public Function CheckDbConnect() As Boolean
        Return SqlDB.ChkConnection()
    End Function

End Class