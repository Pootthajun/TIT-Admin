﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports Engine.Info.PingInfoENG

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
'<System.Web.Script.Services.ScriptService()>
<WebService(Namespace:="http://tempuri.org/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class MonitorInfoWebService
    Inherits System.Web.Services.WebService

    '<WebMethod()>
    'Public Function HelloWorld() As String
    '    Return "Hello World"
    'End Function


    <WebMethod()>
    Public Function GetMonitorComputerInfo(MacAddress As String) As DataTable
        Return Engine.Common.OtherAppEng.GetMonitorComputerInfo(MacAddress)
    End Function

    <WebMethod()>
    Public Function CheckDbConnect() As Boolean
        Return SqlDB.ChkConnection()
    End Function

End Class