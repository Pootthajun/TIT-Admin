﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data
Partial Class frmDisplayMonitorScreen
    Inherits System.Web.UI.Page

    Private Sub btnDisplay_Click(sender As Object, e As EventArgs) Handles btnDisplay.Click
        Try
            imgScreenImage.Attributes.Remove("src")
            Dim lnq As New CfConfigComputerListLinqDB
            lnq.GetDataByPK(cmbMacAddress.SelectedValue, Nothing)
            If lnq.ID > 0 Then
                Dim ByteImage As Byte() = lnq.CURRENT_SCREEN 'CType(Session("ProductImage"), Byte())
                Dim base64String As String = Convert.ToBase64String(ByteImage, 0, ByteImage.Length)

                imgScreenImage.Attributes.Remove("src")
                imgScreenImage.Attributes.Add("src", "data:image/jpeg;base64," & base64String)

                If lnq.CAPTURE_SCREEN_TIME IsNot Nothing Then
                    lblCatpureTime.Text = lnq.CAPTURE_SCREEN_TIME.Value.ToString("dd/MM/yyyy HH:mm:ss.fff", New Globalization.CultureInfo("th-TH"))
                End If
            End If
            lnq = Nothing

            'Dim lnq As New CfConfigComputerListLinqDB
            'lnq.ChkDataByMACADDRESS(txtMacAddress.Text.Trim, Nothing)
            'If lnq.ID > 0 Then
            '    Dim ByteImage As Byte() = lnq.CURRENT_SCREEN 'CType(Session("ProductImage"), Byte())
            '    Dim base64String As String = Convert.ToBase64String(ByteImage, 0, ByteImage.Length)

            '    imgScreenImage.Attributes.Remove("src")
            '    imgScreenImage.Attributes.Add("src", "data:image/jpeg;base64," & base64String)

            '    If lnq.CAPTURE_SCREEN_TIME IsNot Nothing Then
            '        lblCatpureTime.Text = lnq.CAPTURE_SCREEN_TIME.Value.ToString("dd/MM/yyyy HH:mm:ss.fff", New Globalization.CultureInfo("th-TH"))
            '    End If
            'End If
            'lnq = Nothing
        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmDisplayMonitorScreen_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Engine.Common.FunctionEng.SetComputerList(cmbMacAddress)
        End If
    End Sub
End Class
