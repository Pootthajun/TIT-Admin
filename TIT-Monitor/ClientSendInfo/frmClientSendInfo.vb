﻿Imports System.Data
Imports Engine.Common
Imports System.IO
Imports System.DirectoryServices
Imports System.Management
Imports System.ServiceProcess

Public Class frmClientSendInfo
    Dim ComputerName As String = Environment.MachineName
    Dim IPAddress As String '= GetIPAddress()
    Dim MacAddress As String '= FunctionEng.GetMACAddress


#Region "Monitor Zone"
    Private Sub SendCPUInfo(ws As TITMonitorWebservice.ApplicationWebservice)
        Try
            Dim inf As New Engine.Info.CPUInfoENG()
            Try
                'สำหรับ Bypass SSL กรณีเรียก WebService ผ่าน https://
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                  Function(se As Object, cert As System.Security.Cryptography.X509Certificates.X509Certificate,
                  chain As System.Security.Cryptography.X509Certificates.X509Chain,
                  sslerror As System.Net.Security.SslPolicyErrors) True

                ws.SendCPUInfoToDC(ComputerName, IPAddress, inf.PercentUsage, inf.Temperature, MacAddress)
                'If inf.ErrorMessage.Trim <> "" Then
                '    Engine.Common.FunctionEng.CreateErrorLog(Me.Name, "SendCPUInfo", inf.ErrorMessage)
                'End If
            Catch ex As Exception
                Engine.Common.FunctionEng.CreateErrorLog(ex.Message & vbNewLine & ex.StackTrace)
            End Try
            inf = Nothing
        Catch ex As Exception
            Engine.Common.FunctionEng.CreateErrorLog(ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub
    Private Sub SendRamInfo(ws As TITMonitorWebservice.ApplicationWebservice)
        Try
            Dim inf As New Engine.Info.RamInfoENG
            Try
                'สำหรับ Bypass SSL กรณีเรียก WebService ผ่าน https://
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                  Function(se As Object, cert As System.Security.Cryptography.X509Certificates.X509Certificate,
                  chain As System.Security.Cryptography.X509Certificates.X509Chain,
                  sslerror As System.Net.Security.SslPolicyErrors) True

                ws.SendRAMInfoToDC(ComputerName, IPAddress, inf.PercentUsageGB, MacAddress)
            Catch ex As Exception
                Engine.Common.FunctionEng.CreateErrorLog(ex.Message & vbNewLine & ex.StackTrace)
            End Try
            inf = Nothing
        Catch ex As Exception
            Engine.Common.FunctionEng.CreateErrorLog(ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub
    Private Sub SendDriveInfo(ws As TITMonitorWebservice.ApplicationWebservice)
        Try
            Dim inf As New Engine.Info.DriveInfoENG
            Try
                'สำหรับ Bypass SSL กรณีเรียก WebService ผ่าน https://
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                  Function(se As Object, cert As System.Security.Cryptography.X509Certificates.X509Certificate,
                  chain As System.Security.Cryptography.X509Certificates.X509Chain,
                  sslerror As System.Net.Security.SslPolicyErrors) True

                ws.SendDriveInfoToDC(ComputerName, IPAddress, inf.GetDriveInfoToDT, MacAddress)
            Catch ex As Exception
                FunctionEng.CreateErrorLog(ex.Message & vbNewLine & ex.StackTrace)
            End Try
            inf = Nothing
        Catch ex As Exception
            FunctionEng.CreateErrorLog(ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub
    Private Sub SendServiceInfo(ws As TITMonitorWebservice.ApplicationWebservice)
        Dim ServiceDt As DataTable = GetServiceDt(ws)

        Try
            ws.SendServiceInfoToDC(ComputerName, IPAddress, ServiceDt, MacAddress)
        Catch ex As Exception
            FunctionEng.CreateErrorLog("1. Exception : " & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub

    Private Function GetServiceDt(ws As TITMonitorWebservice.ApplicationWebservice) As DataTable
        Dim ServiceDt As New DataTable
        ServiceDt.Columns.Add("ServiceName")
        ServiceDt.Columns.Add("ServiceType")
        ServiceDt.Columns.Add("ServiceStatus")

        Try
            'สำหรับ Bypass SSL กรณีเรียก WebService ผ่าน https://
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                  Function(se As Object, cert As System.Security.Cryptography.X509Certificates.X509Certificate,
                  chain As System.Security.Cryptography.X509Certificates.X509Chain,
                  sslerror As System.Net.Security.SslPolicyErrors) True



            Dim dt As New DataTable
            dt = ws.GetWindowsServiceCheckList(MacAddress)
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    If dr("ActiveStatus").ToString = "Y" Then

                        Dim ServiceInfo As New Engine.Info.WindowsServiceInfoENG(dr("WindowServiceName"))
                        Dim sdr As DataRow = ServiceDt.NewRow
                        sdr("ServiceName") = dr("WindowServiceName")

                        If ServiceInfo.ServiceType = "" Then
                            sdr("ServiceType") = "Not_Type"
                        Else
                            sdr("ServiceType") = ServiceInfo.ServiceType
                        End If

                        Select Case ServiceInfo.ServiceStatus
                            Case "1"
                                sdr("ServiceStatus") = "STOPPED"
                            Case "2"
                                sdr("ServiceStatus") = "START_PENDING"
                            Case "3"
                                sdr("ServiceStatus") = "STOP_PENDING"
                            Case "4"
                                sdr("ServiceStatus") = "RUNNING"
                            Case ""
                                sdr("ServiceStatus") = "Not_Service"
                            Case Else
                                sdr("ServiceStatus") = "Service"
                        End Select
                        ServiceDt.Rows.Add(sdr)
                    End If
                Next
            End If
            dt.Dispose()
        Catch ex As Exception
            ServiceDt = New DataTable
        End Try
        ServiceDt.TableName = "ServiceDt"

        Return ServiceDt
    End Function

    Private Sub SendProcessInfo(ws As TITMonitorWebservice.ApplicationWebservice)
        Dim ProcessDt As DataTable = GetProcessDt(ws)
        Try
            ProcessDt.TableName = "ProcessDt"
            ws.SendProcessInfoToDC(ComputerName, IPAddress, ProcessDt, MacAddress)
        Catch ex As Exception
            FunctionEng.CreateErrorLog("1. Exception : " & ex.Message & vbNewLine & ex.StackTrace)
        End Try

        ProcessDt.Dispose()
    End Sub

    Private Function GetProcessDt(ws As TITMonitorWebservice.ApplicationWebservice) As DataTable
        Dim ProcessDt As New DataTable
        ProcessDt.Columns.Add("WindowProcessName")
        ProcessDt.Columns.Add("ProcessAlive")
        Try
            'สำหรับ Bypass SSL กรณีเรียก WebService ผ่าน https://
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                  Function(se As Object, cert As System.Security.Cryptography.X509Certificates.X509Certificate,
                  chain As System.Security.Cryptography.X509Certificates.X509Chain,
                  sslerror As System.Net.Security.SslPolicyErrors) True

            Dim dt As New DataTable
            dt = ws.GetWindowsProcessCheckList(MacAddress)
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    Dim pDr As DataRow = ProcessDt.NewRow
                    pDr("WindowProcessName") = dr("WindowProcessName")

                    Dim prc() As Process = Process.GetProcessesByName(dr("WindowProcessName"))
                    If prc.Length = 0 Then
                        'Process ไม่อยู่แล้ว
                        pDr("ProcessAlive") = "N"
                    Else
                        'Process ยังคงอยู่
                        pDr("ProcessAlive") = "Y"
                    End If
                    ProcessDt.Rows.Add(pDr)

                    'ตรวจสอบเวลา Heartbeat
                    If dr("AutoRestart") = "Y" Then
                        If Convert.IsDBNull(dr("HeartbeatFile")) = False Then
                            If File.Exists(dr("HeartbeatFile")) = True Then
                                Try
                                    Dim vTime As DateTime = Convert.ToDateTime(File.ReadAllText(dr("HeartbeatFile")))
                                    If vTime.AddMinutes(5) < DateTime.Now Then
                                        Dim ExeFile As String = dr("ProcessExeFile")
                                        If File.Exists(ExeFile) = True Then
                                            For Each p As Process In prc
                                                p.Kill()
                                            Next

                                            Process.Start(ExeFile)
                                        End If
                                    End If
                                Catch ex As Exception

                                End Try
                            End If
                        End If
                    End If
                Next
            End If
            dt.Dispose()
        Catch ex As Exception
            ProcessDt = New DataTable
        End Try
        ProcessDt.TableName = "ProcessDt"

        Return ProcessDt
    End Function

    Private Sub SendAllInfo(FaultWebserviceURL As String)
        Try
            Me.Text = "Client Send Physical Info V" & getMyVersion() & "  Last Refresh Time :" & DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss", New Globalization.CultureInfo("en-US"))

            If IPAddress.Trim <> "" And MacAddress.Trim <> "" Then
                'สำหรับ Bypass SSL กรณีเรียก WebService ผ่าน https://
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                  Function(se As Object, cert As System.Security.Cryptography.X509Certificates.X509Certificate,
                  chain As System.Security.Cryptography.X509Certificates.X509Chain,
                  sslerror As System.Net.Security.SslPolicyErrors) True

                Dim ws As New TITMonitorWebservice.ApplicationWebservice
                ws.Timeout = 20000
                ws.Url = FaultWebserviceURL

                Dim cpuInf As New Engine.Info.CPUInfoENG
                Dim ramInf As New Engine.Info.RamInfoENG
                Dim driveInf As New Engine.Info.DriveInfoENG

                Dim ret As TITMonitorWebservice.ExecuteDataInfo = ws.SendAllInfoToDC(ComputerName, IPAddress, MacAddress, cpuInf.PercentUsage, cpuInf.Temperature, ramInf.PercentUsageGB, driveInf.GetDriveInfoToDT, GetServiceDt(ws), GetProcessDt(ws))

                ws = Nothing
            Else
                FunctionEng.CreateErrorLog("Cannot get Network Information")
            End If
        Catch ex As Exception
            FunctionEng.CreateErrorLog(ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub


    Public Function GetWebApplicationList() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("SiteName")
        dt.Columns.Add("WebApplicationName")
        dt.Columns.Add("ApplicationPool")
        dt.Columns.Add("ApplicationStatus")
        Try
            Dim mgr As New Microsoft.Web.Administration.ServerManager()
            For Each WebSite As Microsoft.Web.Administration.Site In mgr.Sites
                For Each WebApp As Microsoft.Web.Administration.Application In WebSite.Applications
                    If Replace(WebApp.Path, "/", "") <> "" Then
                        Dim applState
                        applState = mgr.ApplicationPools(WebApp.ApplicationPoolName).State

                        Dim dr As DataRow = dt.NewRow
                        dr("SiteName") = WebSite.Name
                        dr("WebApplicationName") = Replace(WebApp.Path, "/", "")
                        dr("ApplicationPool") = Replace(WebApp.ApplicationPoolName, "/", "")
                        dr("ApplicationStatus") = applState
                        dt.Rows.Add(dr)
                    End If
                Next
            Next
        Catch ex As Exception
            FunctionEng.CreateErrorLog(ex.Message & vbNewLine & ex.StackTrace)
        End Try
        dt.TableName = "GetWebApplicationList"
        Return dt
    End Function

    'Dim ImaAliveInterval As Integer = 0
    'Dim ImaAliveMonitorTime As DateTime = DateTime.Now
    'Private Sub SendAliveInfo()
    '    Try
    '        If DateAdd(DateInterval.Minute, ImaAliveInterval, ImaAliveMonitorTime) < DateTime.Now Then
    '            Dim ini As New Org.Mentalis.Files.IniReader(Application.StartupPath & "\Config.ini")
    '            ini.Section = "ConfigAlive"

    '            Dim cfgIntervelMinute As Integer = IIf(ini.ReadString("IntervalMinute") = "", 0, ini.ReadString("IntervalMinute"))
    '            Dim AliveStartTime As String = ini.ReadString("StartTime")
    '            Dim AliveEndTime As String = ini.ReadString("EndTime")
    '            Dim vDateNow As DateTime = DateTime.Now
    '            Dim vTimeNow As String = vDateNow.ToString("HH:mm")
    '            'If vTimeNow >= AliveStartTime And vTimeNow <= AliveEndTime Then
    '            ini.Section = "Setting"
    '            Try
    '                'สำหรับ Bypass SSL กรณีเรียก WebService ผ่าน https://
    '                System.Net.ServicePointManager.ServerCertificateValidationCallback =
    '              Function(se As Object, cert As System.Security.Cryptography.X509Certificates.X509Certificate,
    '              chain As System.Security.Cryptography.X509Certificates.X509Chain,
    '              sslerror As System.Net.Security.SslPolicyErrors) True

    '                Dim ws As New TITMonitorWebservice.ApplicationWebservice
    '                ws.Url = ini.ReadString("FaultWebserviceURL")
    '                ws.SendImAlive(ComputerName, IPAddress, MacAddress, cfgIntervelMinute, AliveStartTime, AliveEndTime, vDateNow)
    '                ws = Nothing
    '            Catch ex As Exception

    '            End Try
    '            'End If
    '            ini = Nothing
    '            ImaAliveInterval = cfgIntervelMinute
    '            ImaAliveMonitorTime = DateTime.Now
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub
#End Region

#Region "Config Zone"

    Private Sub frmMain_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize

        If Me.WindowState = FormWindowState.Minimized Then
            Me.Hide()
            NotifyIcon1.Visible = True
            NotifyIcon1.Text = "Client Send Physical Info V" & getMyVersion()
        Else
            NotifyIcon1.Visible = False
        End If
    End Sub

    Private Function getMyVersion() As String
        Dim version As System.Version = System.Reflection.Assembly.GetExecutingAssembly.GetName().Version
        Return version.Major & "." & version.Minor & "." & version.Build & "." & version.Revision
    End Function

    Private Sub frmMain_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            SetNetworkInfo()

            Dim ini As New Org.Mentalis.Files.IniReader(Application.StartupPath & "\Config.ini")
            ini.Section = "Setting"

            SendAllInfo(ini.ReadString("FaultWebserviceURL"))
            ini = Nothing

            tmCheckStartService_Tick(Nothing, Nothing)
            tmCheckStartService.Interval = (3600000) * 3    '3 ชั่วโมง
            tmCheckStartService.Enabled = True

            Me.Text = "Client Send Physical Info V" & getMyVersion()
            tmSendInfo.Start()
            Me.WindowState = FormWindowState.Minimized

        Catch ex As Exception
            FunctionEng.CreateErrorLog(ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub

    Private Sub SetNetworkInfo()
        Dim ini As New Org.Mentalis.Files.IniReader(Application.StartupPath & "\Config.ini")
        ini.Section = "Setting"
        Dim NetworkAdapter As String = ini.ReadString("NetworkAdapter")

        Dim mc As New ManagementClass("Win32_NetworkAdapterConfiguration")
        Dim moc As ManagementObjectCollection = mc.GetInstances()
        For Each mo As ManagementObject In moc
            If CStr(mo("Description")).Trim = NetworkAdapter Then
                IPAddress = mo("IPAddress")(0)
                MacAddress = mo("MacAddress").ToString().Replace(":", "-")
                Exit For
            End If
            mo.Dispose()
        Next
        ini = Nothing
    End Sub

#End Region

    Dim SendInfoTime As DateTime = DateTime.Now
    Dim SendInfoIntervalMin As Integer = 2
    Private Sub tmSendInfo_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmSendInfo.Tick
        tmSendInfo.Enabled = False

        If DateAdd(DateInterval.Minute, SendInfoIntervalMin, SendInfoTime) < DateTime.Now Then
            Dim ini As New Org.Mentalis.Files.IniReader(Application.StartupPath & "\Config.ini")
            ini.Section = "Setting"
            SendAllInfo(ini.ReadString("FaultWebserviceURL"))

            SendInfoTime = DateTime.Now
            SendInfoIntervalMin = ini.ReadString("SendInfoIntervalMin")
            ini = Nothing
        End If

        tmSendInfo.Enabled = True
    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        Me.Show()
        Me.WindowState = FormWindowState.Normal
        NotifyIcon1.Visible = False
    End Sub



    Dim CaptureTime As DateTime = DateTime.Now
    Dim CaptureIntervalSec As Int16 = 5
    Private Sub TimerCaptureScreen_Tick(sender As Object, e As EventArgs) Handles TimerCaptureScreen.Tick
        TimerCaptureScreen.Enabled = False

        If DateAdd(DateInterval.Second, CaptureIntervalSec, CaptureTime) < DateTime.Now Then
            Dim ini As New Org.Mentalis.Files.IniReader(Application.StartupPath & "\Config.ini")
            ini.Section = "Setting"

            Dim ws As New TITMonitorWebservice.ApplicationWebservice
            ws.Timeout = 20000
            ws.Url = ini.ReadString("FaultWebserviceURL")
            CaptureScreen(ws)

            CaptureTime = DateTime.Now
            CaptureIntervalSec = ini.ReadString("CapScreenIntervalSec")

            ws.Dispose()
            ini = Nothing
        End If
        TimerCaptureScreen.Enabled = True
    End Sub


    Private Sub CaptureScreen(ws As TITMonitorWebservice.ApplicationWebservice)
        Try
            Dim gr As Graphics
            Dim bm As New Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, Drawing.Imaging.PixelFormat.Format32bppArgb)
            gr = Graphics.FromImage(bm)
            gr.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y, 0, 0, New Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height), CopyPixelOperation.SourceCopy)

            Dim img As Image = ResizeImage(bm, New Size(800, 600))
            Dim bitmapBytes As Byte()
            Using stream As New System.IO.MemoryStream
                img.Save(stream, Imaging.ImageFormat.Jpeg)
                bitmapBytes = stream.ToArray
            End Using

            Dim ret As TITMonitorWebservice.ExecuteDataInfo = ws.SaveCaptureScreen(ComputerName, IPAddress, MacAddress, bitmapBytes)
            If ret.IsSuccess = False Then
                FunctionEng.CreateErrorLog(ret.ErrorMessage)
            End If

            bm.Dispose()
            gr.Dispose()
        Catch ex As Exception
            'FunctionEng.CreateErrorLog(ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub

    Private Function ResizeImage(ByVal OldImage As Image, ByVal NewSize As Size) As Image
        Dim newWidth As Integer = NewSize.Width
        Dim newHeight As Integer = NewSize.Height
        Dim newImage As Image = New Bitmap(newWidth, newHeight)

        Using graphicsHandle As Graphics = Graphics.FromImage(newImage)
            Try
                graphicsHandle.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                graphicsHandle.DrawImage(OldImage, 0, 0, newWidth, newHeight)
            Catch ex As AccessViolationException
                FunctionEng.CreateErrorLog("AccessViolationException : " & ex.Message & vbNewLine & ex.StackTrace)
            Catch ex As Exception
                FunctionEng.CreateErrorLog("Exception : " & ex.Message & vbNewLine & ex.StackTrace)
            End Try
        End Using
        Return newImage
    End Function

    Private Sub tmCheckStartService_Tick(sender As Object, e As EventArgs) Handles tmCheckStartService.Tick

        'สำหรับ Bypass SSL กรณีเรียก WebService ผ่าน https://
        System.Net.ServicePointManager.ServerCertificateValidationCallback =
                  Function(se As Object, cert As System.Security.Cryptography.X509Certificates.X509Certificate,
                  chain As System.Security.Cryptography.X509Certificates.X509Chain,
                  sslerror As System.Net.Security.SslPolicyErrors) True

        Dim ini As New Org.Mentalis.Files.IniReader(Application.StartupPath & "\Config.ini")
        ini.Section = "Setting"

        Dim ws As New TITMonitorWebservice.ApplicationWebservice
        ws.Timeout = 20000
        ws.Url = ini.ReadString("FaultWebserviceURL")
        Dim sDt As DataTable = GetServiceDt(ws)

        If sDt.Rows.Count > 0 Then
            For Each sDr As DataRow In sDt.Rows
                Dim s As New ServiceController(sDr("ServiceName"))
                If s.Status = ServiceControllerStatus.Stopped Or s.Status = ServiceControllerStatus.StopPending Then
                    s.Start()
                End If
                s.Dispose()
            Next
        End If

        ws = Nothing
        ini = Nothing

    End Sub

    'Private Sub tmCheckAlive_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmCheckAlive.Tick
    '    Try
    '        tmCheckAlive.Enabled = False
    '        Dim nThread As New System.Threading.Thread(AddressOf SendAliveInfo)
    '        nThread.Start()
    '        'SendAliveInfo()

    '        tmCheckAlive.Enabled = True
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Private Shared Function GetIPAddress() As String
    '    Dim IP As String = ""
    '    Dim oAddr As System.Net.IPAddress
    '    With System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName())
    '        If .AddressList.Length > 1 Then
    '            'ถ้าเครื่องมีมากกว่า 1 IP
    '            Dim ini As New Org.Mentalis.Files.IniReader(Application.StartupPath & "\Config.ini")
    '            ini.Section = "Setting"
    '            IP = ini.ReadString("IPAddress")
    '            ini = Nothing
    '        Else
    '            oAddr = New System.Net.IPAddress(.AddressList(0).Address)
    '            IP = oAddr.ToString
    '        End If
    '    End With

    '    Return IP
    'End Function
End Class