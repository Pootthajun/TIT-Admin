﻿Imports System.Net
Imports System.Net.Sockets
Imports System.Net.NetworkInformation
Imports System.Net.NetworkInformation.Ping
Imports System.Timers
Imports System.Windows.Forms
Imports MonitorNetworkWindowService.Org.Mentalis.Files

Public Class wsMonitorNetwork
    Dim tmMonitorPort As System.Timers.Timer
    Dim tmMonitorPing As System.Timers.Timer

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.

        tmMonitorPort = New System.Timers.Timer
        tmMonitorPort.Interval = 60000    'ทำทุก 1 นาที = 60000
        AddHandler tmMonitorPort.Elapsed, AddressOf tmMonitorPort_Tick
        tmMonitorPort.Start()
        tmMonitorPort.Enabled = True



        'tmMonitorPing = New System.Timers.Timer
        'tmMonitorPing.Interval = 60000    'ทำทุก 1 นาที = 60000
        'AddHandler tmMonitorPing.Elapsed, AddressOf tmMonitorPing_Tick
        'tmMonitorPing.Start()
        'tmMonitorPing.Enabled = True
    End Sub

    Protected Sub tmMonitorPort_Tick(sender As Object, e As ElapsedEventArgs)
        tmMonitorPort.Enabled = False
        Try
            'สำหรับ Bypass SSL กรณีเรียก WebService ผ่าน https://
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                  Function(se As Object, cert As System.Security.Cryptography.X509Certificates.X509Certificate,
                  chain As System.Security.Cryptography.X509Certificates.X509Chain,
                  sslerror As System.Net.Security.SslPolicyErrors) True

            Dim ini As New IniReader(Application.StartupPath & "\Config.ini")
            ini.Section = "Setting"
            Dim ws As New MonitorWebAppService.ApplicationWebservice
            ws.Url = ini.ReadString("MonitorWebserviceURL")

            MonitorPort(ws)
            ws.Dispose()
            ini = Nothing
        Catch ex As Exception
            Engine.Common.FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
        End Try
        tmMonitorPort.Enabled = True
    End Sub

    'Protected Sub tmMonitorPing_Tick(sender As Object, e As ElapsedEventArgs)
    '    tmMonitorPing.Enabled = False
    '    Try
    '        'สำหรับ Bypass SSL กรณีเรียก WebService ผ่าน https://
    '        System.Net.ServicePointManager.ServerCertificateValidationCallback =
    '              Function(se As Object, cert As System.Security.Cryptography.X509Certificates.X509Certificate,
    '              chain As System.Security.Cryptography.X509Certificates.X509Chain,
    '              sslerror As System.Net.Security.SslPolicyErrors) True

    '        Dim ini As New IniReader(Application.StartupPath & "\Config.ini")
    '        ini.Section = "Setting"
    '        Dim ws As New MonitorWebAppService.ApplicationWebservice
    '        ws.Url = ini.ReadString("MonitorWebserviceURL")
    '        MonitorPingIP(ws)
    '        ws.Dispose()
    '        ini = Nothing
    '    Catch ex As Exception
    '        Engine.Common.FunctionEng.CreateErrorLog("frmNewMain", "tmMonitorPing_Tick", "Exception :" & ex.Message & vbNewLine & ex.StackTrace)
    '    End Try
    '    tmMonitorPing.Enabled = True
    'End Sub

#Region "Monitor Ping"
    'Public Shared Sub MonitorPingIP(ws As MonitorWebAppService.ApplicationWebservice)
    '    Dim ret As Boolean = False
    '    Try
    '        Dim dt As DataTable = ws.GetConfigKioskList
    '        For Each row As DataRow In dt.Rows
    '            'Dim ko_id As Long = Convert.ToInt64(row("ko_id"))
    '            Dim ip As String = row("ServerIP")
    '            Dim ServerName As String = row("ServerName")
    '            Dim MacAddress As String = row("MacAddress")
    '            Try
    '                If My.Computer.Network.Ping(ip, 1000) Then
    '                    ret = ws.SavePingInfo(ip, ServerName, MacAddress, "Y")
    '                    If ret = False Then
    '                        For i As Integer = 0 To 5
    '                            ret = ws.SavePingInfo(ip, ServerName, MacAddress, "Y")
    '                            If ret = True Then
    '                                Exit For
    '                            End If
    '                        Next
    '                    End If
    '                Else
    '                    ret = ws.SavePingInfo(ip, ServerName, MacAddress, "N")
    '                    If ret = False Then
    '                        For i As Integer = 0 To 5
    '                            ret = ws.SavePingInfo(ip, ServerName, MacAddress, "N")
    '                            If ret = True Then
    '                                Exit For
    '                            End If
    '                        Next
    '                    End If
    '                End If
    '            Catch ex As Exception
    '                Engine.Common.FunctionEng.CreateErrorLog("frmNewMain", "PingToServer", "1. Exception :" & ex.Message & vbNewLine & ex.StackTrace)
    '            End Try
    '        Next
    '    Catch ex As Exception
    '        Engine.Common.FunctionEng.CreateErrorLog("frmNewMain", "PingToServer", "2. Exception :" & ex.Message & vbNewLine & ex.StackTrace)
    '    End Try
    'End Sub

#End Region

#Region "Monitor Port"
    Public Shared Sub MonitorPort(ws As MonitorWebAppService.ApplicationWebservice)
        Try
            Dim TmpCfDT As DataTable = ws.GetConfigPortList
            If TmpCfDT.Rows.Count > 0 Then
                Dim cfDT As DataTable = TmpCfDT.DefaultView.ToTable(True, "PortID")
                For Each cfDr As DataRow In cfDT.Rows
                    Dim cfLnq As MonitorWebAppService.CfConfigPortLinqDB = ws.GetCfConfigPortLinqDB(cfDr("PortID"))
                    If cfLnq.ID > 0 Then
                        Dim chk As Boolean = (cfLnq.ALLDAYEVENT = "Y")
                        If chk = False Then
                            Dim CurrTime As String = DateTime.Now.ToString("HH:mm")
                            If cfLnq.ALARMTIMEFROM <= CurrTime And CurrTime <= cfLnq.ALARMTIMETO Then
                                chk = True
                            End If
                        End If

                        If chk = True Then
                            If DateAdd(DateInterval.Minute, 1, cfLnq.LASTCHECKTIME) < DateTime.Now.Date Then
                                TmpCfDT.DefaultView.RowFilter = "PortID='" & cfLnq.ID & "'"
                                For Each dr As DataRowView In TmpCfDT.DefaultView
                                    Dim cfDetailLnq As MonitorWebAppService.CfConfigPortDetailLinqDB = ws.GetCfConfigPortDetailLinqDB(Convert.ToInt64(dr("PortDetailID")))

                                    If cfDetailLnq.ID > 0 Then
                                        Dim awDT As DataTable = ws.GetPortAlarmWaitingClear(cfLnq.MACADDRESS, cfDetailLnq.PORTNUMBER)

                                        If TelnetPort(cfLnq.SERVERIP, cfDetailLnq.PORTNUMBER) = False Then
                                            'Dim iEng As New InfoClass.PortInfo
                                            'iEng.ProcessPortAlarm(awDT, cfLnq, cfDetailLnq.PORTNUMBER, 1, "Mail")
                                            ws.ProcessPortAlarm(awDT, cfLnq, cfDetailLnq.PORTNUMBER, 1, "Mail")
                                        Else
                                            If awDT.Rows.Count > 0 Then
                                                Dim ClearMsg As String = "Alarm PORT No " & cfDetailLnq.PORTNUMBER & " not connect  on " & cfLnq.SERVERIP & " is clear"
                                                ws.SendClearAlarm(cfLnq.SERVERNAME, cfLnq.SERVERIP, cfLnq.MACADDRESS, "CRITICAL", 0, "Mail", "Port" & cfDetailLnq.PORTNUMBER, ClearMsg, awDT.Rows(0)("id"))

                                                'Dim hw As New InfoClass.HardwareInfo
                                                'hw.SendClearAlarm(cfLnq.SERVERNAME, cfLnq.SERVERIP, cfLnq.MACADDRESS, "CRITICAL", 0, "Mail", "Port" & cfDetailLnq.PORTNUMBER, ClearMsg, awDT.Rows(0)("id"))
                                                'hw = Nothing
                                            End If
                                        End If
                                        awDT.Dispose()
                                    End If
                                    cfDetailLnq = Nothing
                                Next
                                TmpCfDT.DefaultView.RowFilter = ""

                                'InfoClass.HardwareInfo.UpdateLastCheckTime(cfLnq.TableName, cfLnq.ID)
                                ws.UpdateLastCheckTime("CF_CONFIG_PORT", cfLnq.ID)
                            End If
                        End If
                    End If
                    cfLnq = Nothing
                Next
                cfDT.Dispose()
            End If
            TmpCfDT.Dispose()
        Catch ex As Exception
            Engine.Common.FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub
    Private Shared Function TelnetPort(ByVal PIPAddress As String, ByVal PPort As String) As Boolean
        Dim ret As Boolean = False
        Dim remoteIPAddress As IPAddress
        Dim ep As IPEndPoint
        Dim tnSocket As Socket

        ' Get the IP Address and the Port and create an IPEndpoint (ep)
        Try
            remoteIPAddress = IPAddress.Parse(PIPAddress.Trim)
            ep = New IPEndPoint(remoteIPAddress, CType(PPort.Trim, Integer))
            ' Set the socket up (type etc)
            tnSocket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
            Try
                ' Connect
                tnSocket.Connect(ep)
                If tnSocket.Connected = True Then
                    ret = True
                End If
            Catch oEX As SocketException
                ' error
                ' You will need to do error cleanup here e.g killing the socket
                ' and exiting the procedure.

                Engine.Common.FunctionEng.CreateErrorLog("SocketException :" & oEX.Message & vbNewLine & oEX.StackTrace)

                Return False
            End Try
        Catch ex As Exception
            Engine.Common.FunctionEng.CreateErrorLog("Exception :" & ex.Message & vbNewLine & ex.StackTrace)
            Return False
        End Try


        ' Cleanup
        remoteIPAddress = Nothing
        ep = Nothing
        tnSocket = Nothing

        Return ret
    End Function
#End Region

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

End Class
