﻿Imports System
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports LinqDB.TABLE
Imports LinqDB.ConnectDB


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class TITWebService
    Inherits System.Web.Services.WebService

    '<WebMethod()> _
    'Public Function HelloWorld() As String
    '    Return "Hello World"
    'End Function

    <WebMethod()>
    Public Function LoginTIT(vUserName As String, vPassword As String, SystemCode As String, ModuleName As String, ClientIP As String, ClientBrowser As String, BrowserVersion As String, ServerURL As String) As LoginReturnData
        Dim ret As New LoginReturnData
        Try
            Dim sql As String = "select ua.id, ua.passwd, ua.active_status  "
            sql += " From MS_USER_ACCOUNT ua "
            sql += " inner join MS_USER_SYSTEM us on ua.id=us.ms_user_account_id "
            sql += " inner join MS_SYSTEM s on s.id=us.ms_system_id "
            sql += " where s.system_code=@_SYSTEM_CODE "
            sql += " and ua.username=@_USERNAME "

            Dim p(2) As SqlParameter
            p(0) = SqlDB.SetText("@_SYSTEM_CODE", SystemCode)
            p(1) = SqlDB.SetText("@_USERNAME", vUserName)

            Dim dt As DataTable = SqlDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then
                If dt.Rows(0)("active_status") = "Y" Then
                    Dim pwd As String = SqlDB.EnCripPwd(vPassword)
                    If dt.Rows(0)("passwd") = pwd Then
                        Dim lnq As New MsUserAccountLinqDB
                        lnq.GetDataByPK(Convert.ToInt64(dt.Rows(0)("id")), Nothing)
                        If lnq.ID > 0 Then
                            ret = InsertLoginHistory(lnq, SystemCode, ModuleName, ClientIP, ClientBrowser, BrowserVersion, ServerURL)
                        End If
                        lnq = Nothing
                    Else
                        ret.LoginStatus = False
                        ret.ErrorMessage = "Invalid Password"
                    End If
                Else
                    ret.LoginStatus = False
                    ret.ErrorMessage = "User is Inactive"
                End If
            Else
                ret.LoginStatus = False
                ret.ErrorMessage = "User is not Authorize for " & SystemCode & " System"
            End If
            dt.Dispose()

        Catch ex As Exception
            ret.LoginStatus = False
            ret.ErrorMessage = "LoginTIT Exception : " & ex.Message & vbNewLine & ex.StackTrace
        End Try
        Return ret
    End Function


    Private Function InsertLoginHistory(uData As MsUserAccountLinqDB, SystemCode As String, ModuleName As String, ClientIP As String, ClientBrowser As String, BrowserVersion As String, ServerURL As String) As LoginReturnData
        Dim ret As New LoginReturnData
        Try
            Dim lnq As New TbLoginHistoryLinqDB
            lnq.TOKEN = Guid.NewGuid.ToString
            lnq.USERNAME = uData.USERNAME
            lnq.FIRST_NAME = uData.FIRST_NAME
            lnq.LAST_NAME = uData.LAST_NAME
            lnq.COMPANY_NAME = uData.COMPANY_NAME
            lnq.SYS_CODE = SystemCode & "." & ModuleName
            lnq.LOGON_TIME = DateTime.Now
            lnq.CLIENT_IP = ClientIP
            lnq.CLIENT_BROWSER = "Browser :" + ClientBrowser + " Version :" & BrowserVersion
            lnq.SERVER_URL = IIf(ServerURL.Trim <> "", ServerURL, HttpContext.Current.Request.Url.AbsoluteUri)
            lnq.WEBSERVICE_URL = HttpContext.Current.Request.Url.AbsoluteUri

            Dim trans As New TransactionDB
            Dim re As ExecuteDataInfo = lnq.InsertData(uData.USERNAME, trans.Trans)
            If re.IsSuccess = True Then
                trans.CommitTransaction()
                ret.LoginStatus = True
                ret.Token = lnq.TOKEN
                ret.LoginUsername = uData.USERNAME
                ret.LoginFirstName = uData.FIRST_NAME
                ret.LoginLastName = uData.LAST_NAME
                ret.LoginCompanyName = uData.COMPANY_NAME
                ret.ForceChangePwd = uData.FORCE_CHANGE_PWD
            Else
                trans.RollbackTransaction()
                ret.LoginStatus = False
                ret.ErrorMessage = lnq.ErrorMessage
            End If
        Catch ex As Exception
            ret.LoginStatus = False
            ret.ErrorMessage = "InsertLoginHistory Exception : " & ex.Message & vbNewLine & ex.StackTrace
        End Try
        Return ret
    End Function

    <WebMethod()>
    Public Function ChangePassword(UserName As String, OldPassword As String, NewPassword As String) As ExecuteDataInfo
        Dim ret As ExecuteDataInfo
        Try
            Dim lnq As New MsUserAccountLinqDB
            lnq.ChkDataByUSERNAME(UserName, Nothing)
            If lnq.ID > 0 Then
                If lnq.PASSWD = SqlDB.EnCripPwd(OldPassword) Then
                    lnq.PASSWD = SqlDB.EnCripPwd(NewPassword)
                    lnq.FORCE_CHANGE_PWD = "N"

                    Dim trans As New TransactionDB
                    ret = lnq.UpdateData(UserName, trans.Trans)
                    If ret.IsSuccess = True Then
                        trans.CommitTransaction()
                    Else
                        trans.RollbackTransaction()
                    End If
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = "Invalid Old Password"
                End If
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = "Invalid Username"
            End If
            lnq = Nothing
        Catch ex As Exception
            ret = New ExecuteDataInfo
            ret.IsSuccess = False
            ret.ErrorMessage = "Exception : " & ex.Message & vbNewLine & ex.StackTrace
        End Try

        Return ret
    End Function

    <WebMethod()>
    Public Function ResetUserPassword(UserName As String, NewPassword As String) As ExecuteDataInfo
        Dim ret As ExecuteDataInfo
        Try
            Dim lnq As New MsUserAccountLinqDB
            lnq.ChkDataByUSERNAME(UserName, Nothing)
            If lnq.ID > 0 Then
                lnq.PASSWD = SqlDB.EnCripPwd(NewPassword)
                lnq.FORCE_CHANGE_PWD = "Y"

                Dim trans As New TransactionDB
                ret = lnq.UpdateData(UserName, trans.Trans)
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                Else
                    trans.RollbackTransaction()
                End If
            Else
                ret = New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = "Invalid Username"
            End If
            lnq = Nothing

        Catch ex As Exception
            ret = New ExecuteDataInfo
            ret.IsSuccess = False
            ret.ErrorMessage = "Exception : " & ex.Message & vbNewLine & ex.StackTrace
        End Try

        Return ret
    End Function

    <WebMethod()>
    Public Function GetListUser(AccountNo As String, FirstName As String, LastName As String, CompanyName As String, UserName As String, ActiveStatus As String, SysCode As String) As DataTable
        Dim ret As New DataTable
        Dim wh As String = " 1=1 "

        Dim p(7) As SqlParameter
        If AccountNo.Trim <> "" Then
            wh += " and u.account_no like '%' + @_ACCOUNT_NO + '%'"
            p(0) = SqlDB.SetText("@_ACCOUNT_NO", AccountNo)
        End If
        If FirstName.Trim <> "" Then
            wh += " and u.first_name like '%' + @_FIRST_NAME + '%'"
            p(1) = SqlDB.SetText("@_FIRST_NAME", FirstName)
        End If
        If LastName.Trim <> "" Then
            wh += " and u.last_name like '%' + @_LAST_NAME + '%'"
            p(2) = SqlDB.SetText("@_LAST_NAME", LastName)
        End If
        If CompanyName.Trim <> "" Then
            wh += " and u.company_name like '%' + @_COMPANY_NAME + '%'"
            p(3) = SqlDB.SetText("@_COMPANY_NAME", CompanyName)
        End If
        If UserName.Trim <> "" Then
            wh += " and u.username like '%' + @_USERNAME + '%'"
            p(4) = SqlDB.SetText("@_USERNAME", UserName)
        End If
        If ActiveStatus.Trim <> "" Then
            wh += " and u.active_status = @_ACTIVE_STATUS"
            p(5) = SqlDB.SetText("@_ACTIVE_STATUS", ActiveStatus)
        End If

        wh += " and s.system_code=@_SYS_CODE"
        p(6) = SqlDB.SetText("@_SYS_CODE", SysCode)

        Dim sql As String = "select u.* "
        sql += " from ms_user_account u"
        sql += " inner join ms_user_system us on u.id=us.ms_user_account_id "
        sql += " inner join ms_system s on s.id=us.ms_system_id "
        sql += " where " & wh

        ret = SqlDB.ExecuteTable(sql, Nothing, p)
        ret.TableName = "GetListUser"
        Return ret
    End Function

    <WebMethod()>
    Public Function InsertUserAccount(FirstName As String, LastName As String, CompanyName As String, Email As String, MobileNo As String, UserName As String, PassWD As String, ActiveStatus As String, SysCode As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo

        If FirstName.Trim = "" Then
            ret.IsSuccess = False
            ret.ErrorMessage = "FirstName is require"
            Return ret
        End If

        If LastName.Trim = "" Then
            ret.IsSuccess = False
            ret.ErrorMessage = "LastName is require"
            Return ret
        End If

        If CompanyName.Trim = "" Then
            ret.IsSuccess = False
            ret.ErrorMessage = "CompanyName is require"
            Return ret
        End If
        If UserName.Trim = "" Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Username is require"
            Return ret
        End If
        If PassWD.Trim = "" Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Password is require"
        End If

        If ActiveStatus.Trim = "" Then
            ret.IsSuccess = False
            ret.ErrorMessage = "ActiveStatus is require"
            Return ret
        End If

        If SysCode.Trim = "" Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Syscode is require"
        End If

        Dim AccountNo As String = DateTime.Now.ToString("yyyyMMddHHmmssfff")
        ret = SaveUserAccount(0, AccountNo, FirstName, LastName, CompanyName, Email, MobileNo, UserName, PassWD, True, ActiveStatus)
        If ret.IsSuccess = True Then
            Dim sLnq As New MsSystemLinqDB
            sLnq.ChkDataBySYSTEM_CODE(SysCode, Nothing)
            If sLnq.ID > 0 Then
                Dim lnq As New MsUserSystemLinqDB
                lnq.MS_USER_ACCOUNT_ID = ret.NewID
                lnq.MS_SYSTEM_ID = sLnq.ID

                Dim trans As New TransactionDB
                ret = lnq.InsertData(UserName, trans.Trans)
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                    ret.NewID = lnq.MS_USER_ACCOUNT_ID
                Else
                    trans.RollbackTransaction()
                End If
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = "Invalid Syscode"
            End If
        End If

        Return ret
    End Function

    <WebMethod()>
    Public Function EditUserAccount(UserID As Long, FirstName As String, LastName As String, CompanyName As String, Email As String, MobileNo As String, ActiveStatus As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        If UserID <= 0 Then
            ret.IsSuccess = False
            ret.ErrorMessage = "UserID is require"
            Return ret
        End If

        If FirstName.Trim = "" Then
            ret.IsSuccess = False
            ret.ErrorMessage = "FirstName is require"
            Return ret
        End If

        If LastName.Trim = "" Then
            ret.IsSuccess = False
            ret.ErrorMessage = "LastName is require"
            Return ret
        End If

        If CompanyName.Trim = "" Then
            ret.IsSuccess = False
            ret.ErrorMessage = "CompanyName is require"
            Return ret
        End If

        If ActiveStatus.Trim = "" Then
            ret.IsSuccess = False
            ret.ErrorMessage = "ActiveStatus is require"
            Return ret
        End If

        Dim lnq As New MsUserAccountLinqDB
        lnq.GetDataByPK(UserID, Nothing)
        If lnq.ID > 0 Then
            Dim ForceChangePwd As Boolean = False
            ret = SaveUserAccount(lnq.ID, lnq.ACCOUNT_NO, FirstName, LastName, CompanyName, Email, MobileNo, lnq.USERNAME, SqlDB.DeCripPwd(lnq.PASSWD), ForceChangePwd, ActiveStatus)
        Else
            ret.IsSuccess = False
            ret.ErrorMessage = "Invalid UserID"
        End If
        lnq = Nothing

        Return ret
    End Function

    Private Function SaveUserAccount(UserID As Long, AccountNo As String, FirstName As String, LastName As String, CompanyName As String, Email As String, MobileNo As String, UserName As String, Passwd As String, ForceChangePwd As Boolean, ActiveStatus As String) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        Dim lnq As New MsUserAccountLinqDB

        If lnq.ChkDuplicateByUSERNAME(UserName, UserID, Nothing) = True Then
            ret.IsSuccess = False
            ret.ErrorMessage = "Duplicate Username"
            Return ret
        End If

        lnq.GetDataByPK(UserID, Nothing)
        lnq.ACCOUNT_NO = AccountNo
        lnq.FIRST_NAME = FirstName
        lnq.LAST_NAME = LastName
        lnq.COMPANY_NAME = CompanyName
        lnq.EMAIL = Email
        lnq.MOBILE_NO = MobileNo
        lnq.USERNAME = UserName
        lnq.PASSWD = SqlDB.EnCripPwd(Passwd)
        lnq.FORCE_CHANGE_PWD = IIf(ForceChangePwd = True, "Y", "N")
        lnq.ACTIVE_STATUS = ActiveStatus

        Dim trans As New TransactionDB
        If lnq.ID > 0 Then
            ret = lnq.UpdateData(UserName, trans.Trans)
        Else
            ret = lnq.InsertData(UserName, trans.Trans)
        End If

        If ret.IsSuccess = True Then
            trans.CommitTransaction()
            ret.NewID = lnq.ID
        Else
            trans.RollbackTransaction()
        End If
        lnq = Nothing

        Return ret
    End Function

    <WebMethod()>
    Public Function DeleteUserAccount(UserID As Long) As ExecuteDataInfo
        Dim ret As New ExecuteDataInfo
        If UserID <= 0 Then
            ret.IsSuccess = False
            ret.ErrorMessage = "UserID is require"
            Return ret
        End If

        Dim trans As New TransactionDB

        Dim sql As String = "delete from MS_USER_SYSTEM where ms_user_account_id=@_USER_ID"
        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetBigInt("@_USER_ID", UserID)

        ret = SqlDB.ExecuteNonQuery(sql, trans.Trans, p)
        If ret.IsSuccess = True Then
            Dim lnq As New MsUserAccountLinqDB
            ret = lnq.DeleteByPK(UserID, trans.Trans)
            If ret.IsSuccess = True Then
                trans.CommitTransaction()
            Else
                trans.RollbackTransaction()
            End If
            lnq = Nothing
        Else
            trans.RollbackTransaction()
        End If

        Return ret
    End Function

    <WebMethod()>
    Public Function CheckDuplicateUserAccount(UserName As String, UserID As Long) As Boolean
        Dim ret As Boolean = False
        Dim lnq As New MsUserAccountLinqDB
        ret = lnq.ChkDuplicateByUSERNAME(UserName, UserID, Nothing)
        lnq = Nothing

        Return ret
    End Function
End Class