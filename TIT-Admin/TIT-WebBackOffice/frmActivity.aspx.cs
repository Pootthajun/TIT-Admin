﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TIT_WebBackoffice
{
    public partial class frmActivity : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            btnCreate.Visible = false;
            if (btnCreate.Visible == false)
            {
                pnlCreate.Visible = true;
                pnlCustomerList.Visible = false;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlCreate.Visible = false;
            if (pnlCreate.Visible == false)
            {
                pnlCustomerList.Visible = true;
                //Clear();
                btnCreate.Visible = true;

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            pnlCreate.Visible = false;
            if (pnlCreate.Visible == false)
            {
                pnlCustomerList.Visible = true;
                //Clear();
                btnCreate.Visible = true;

            }
        }
    }
}