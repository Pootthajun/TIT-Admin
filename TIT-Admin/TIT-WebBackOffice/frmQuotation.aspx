﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmQuotation.aspx.cs" Inherits="TIT_WebBackoffice.frmQuotation" %>

<%@ Register Src="~/UCTablePrice.ascx" TagPrefix="uc1" TagName="UCTablePrice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Quotations | TiT</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <section class="page-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="frmHome.aspx">Home</a></li>
                            <li class="active">Quotation</li>

                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>Quotation</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <asp:Panel ID="pnlCustomerList" runat="server" Visible="True">
                <section class="panel">
                    <div class="form-group dst-bottom">
                        <div class="panel panel-default dst-bottom">
                            <div class="panel-heading">
                                <h4 class="panel-title bold"><i class="fa fa-search dst-icon bold"></i>ค้นหาใบเสนอราคา
                                </h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-md-2">
                                        <label>เลขที่ใบเสนอราคา</label>
                                        <input type="text" value="" class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <label>วันที่เสนอราคา</label>
                                        <div class="input-daterange input-group" data-plugin-datepicker="">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                                <asp:TextBox ID="txtDateSearch" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label>กำหนดยืนราคา</label>
                                        <input type="text" value="" class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <label>รหัสลูกค้า</label>
                                        <input type="text" value="" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>ชื่อโครงการ</label>
                                        <asp:DropDownList ID="ddlProjectNameSearch" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group lbl-control">
                                    <div class="col-md-12">
                                        <input type="button" value="Search.." class="btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-heading">
                        <h3 class="panel-bottom">Quotation List</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                                <thead>
                                    <tr>
                                        <th width="12%" class="tbl-th">เลขที่ใบเสนอราคา</th>
                                        <th width="12%" class="tbl-th">วันที่เสนอราคา</th>
                                        <th width="12%" class="tbl-th">กำหนดยืนราคา</th>
                                        <th width="12%" class="tbl-th">รหัสลูกค้า</th>
                                        <th class="tbl-th">ชื่อโครงการ</th>
                                        <th width="12%" class="tbl-th">เครื่องมือ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptQuotation" runat="server" OnItemDataBound="rptQuotation_ItemDataBound" >
                                    <ItemTemplate>
                                    <tr>
                                        <td align="center" class="actions th">QUO1703005(R2)
                                            <asp:Label ID="lblQUO_ID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "QUO_ID") %>'></asp:Label>
                                  
                                        </td>
                                        <td align="center" class="actions th">7-Mar-2017</td>
                                        <td align="center" class="actions th">30 วัน</td>
                                        <td align="center" class="actions th">BSC1702001</td>
                                        <td align="center" class="actions th">ATTA-Network System โรงพยาบาลรามาธิบดี</td>
                                        <td class="actions th" align="center">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-navicon text-blue"></i>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <%--<li><a href="#" class="on-default edit-row"><i class="fa fa-search distance"></i>ดูรายละเอียด</a></li>--%>
                                                    <li><a href="#" class="on-default edit-row"><i class="fa fa-pencil-square-o distance"></i>แก้ไข</a></li>
                                                    <li><a href="#" class="on-default remove-row"><i class="fa fa-trash-o distance"></i>ลบ</a></li>
                                                    <li><a href="#" class="on-default print-row"><i class="fa fa-print distance"></i>พิมพ์</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </asp:Panel>

            <asp:Button ID="btnCreate" runat="server" CssClass="btn btn-primary" Text="CREATE" OnClick="btnCreate_Click" />

            <asp:Panel ID="pnlCreate" runat="server" Visible="False">
                <h4 class="panel-bottom">Creat New Quotation</h4>
                <div class="panel-body">
                    <!--ข้อมูลลูกค้า-->
                    <div class="form-group dst-bottom">
                        <div class="panel panel-default dst-bottom">
                            <div class="panel-heading">
                                <h4 class="panel-title"><i class="fa fa-usd dst-icon"></i>ข้อมูลใบเสนอราคา</h4>
                            </div>
                            <div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <asp:Label ID="Label2" runat="server" Text="กำหนดยืนราคา"></asp:Label>
                                            <div class="input-daterange input-group" data-plugin-datepicker="">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <asp:TextBox ID="TextBox2" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <%--<asp:Label ID="lblCusID" runat="server" Text="0" Visible="false"></asp:Label>
                                        <div class="col-md-3">
                                            <asp:Label ID="lblCusName" runat="server" Text="Quotation Name"></asp:Label>
                                            <asp:TextBox ID="txtCusName" runat="server" CssClass="form-control" MaxLength="150" Text="Quotation:ใบเสนอราคา" Enabled="false"></asp:TextBox>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="lblCusCode" runat="server" Text="Quotation No."></asp:Label>
                                            <asp:TextBox ID="txtCusCode" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>--%>
                                        <div class="col-md-3">
                                            <asp:Label ID="Label1" runat="server" Text="รหัสลูกค้า"></asp:Label>
                                            <asp:DropDownList ID="DropDownList1" runat="server" class="form-control populate">
                                                <asp:ListItem Text="กรุณาเลือก..."></asp:ListItem>
                                                <asp:ListItem Text="AAA"></asp:ListItem>
                                                <asp:ListItem Text="BBB"></asp:ListItem>
                                                <asp:ListItem Text="CCC"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <%--<div class="col-md-3">
                                            <asp:Label ID="Label3" runat="server" Text="Tax ID"></asp:Label>
                                            <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>--%>
                                    </div>
                                    <%--<div class="form-group">
                                        <div class="col-md-3">
                                            <asp:Label ID="Label2" runat="server" Text="กำหนดยืนราคา"></asp:Label>
                                            <div class="input-daterange input-group" data-plugin-datepicker="">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <asp:TextBox ID="TextBox2" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="Label4" runat="server" Text="กำหนดยืนราคา"></asp:Label>
                                            <asp:TextBox ID="TextBox4" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>--%>
                                    <div class="form-group">
                                        <uc1:UCTablePrice ID="TablePrice" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group" align="center">
                        <asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="CANCEL" CssClass="btn btn-danger" OnClick="btnCancel_Click" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </form>
</asp:Content>
