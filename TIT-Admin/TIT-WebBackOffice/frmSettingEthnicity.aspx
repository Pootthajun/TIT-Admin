﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmSettingEthnicity.aspx.cs" Inherits="TIT_WebBackoffice.frmSettingEthnicity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Ethnicity | TiT</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="upPnl" runat="server">
            <ContentTemplate>
                <section class="page-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="breadcrumb">
                                    <li><a href="#">Master</a></li>
                                    <li class="active">Ethnicity</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h1>Ethnicity</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="container">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-bottom">Ethnicity List</h4>
                        </div>

                        <div class="panel-body">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped mb-none panel-bottom">
                                    <thead>
                                        <tr>
                                            <%--<th class="tbl-th" width="30%">Position Code</th>--%>
                                            <th class="tbl-th">สัญชาติ</th>
                                            <%--<th class="tbl-th">Department</th>--%>
                                            <th class="tbl-th" width="12%">เครื่องมือ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptEthnicity" runat="server" OnItemDataBound="rptEthnicity_ItemDataBound" OnItemCommand="rptEthnicity_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="actions th">
                                                         <asp:Label ID="lblID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ETH_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblEthnicityName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ETH_NAME") %>'></asp:Label></td>
                                                    <td class="actions th" align="center">
                                                        <asp:LinkButton ID="edit" runat="server" Text=" Edit" CssClass="fa fa-pencil-square-o" CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ETH_ID") %>'></asp:LinkButton>
                                                        <asp:LinkButton ID="delete" runat="server" Text=" Delete" CssClass="fa fa-trash-o" CommandName="delete" OnClientClick='javascript:return confirm("Are you sure you want to delete?")' 
                                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ETH_ID") %>'></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>

                    <asp:Panel ID="pnlEthnicity" runat="server">
                        <div class="panel panel-default dst-bottom">
                            <div class="panel-heading">
                                <h4 class="panel-title"><i class="fa fa-group distance"></i>เพิ่ม / แก้ไขสัญชาติ
                                </h4>
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                    <asp:Label ID="lblEthnicityID" runat="server" Text="0" Visible="false"></asp:Label>
                                    <%--<div class="col-md-3">
                                        <label>Position Code :</label>
                                        <asp:TextBox ID="txtPositionCode" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>--%>
                                    <div class="col-md-5">
                                        <label>สัญชาติ :</label>
                                        <asp:TextBox ID="txtEthnicityName" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <%--<div class="col-md-4">
                                        <label>Position Name :</label>
                                        <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>--%>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" align="center">
                                        <asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnCancel" runat="server" Text="CANCEL" CssClass="btn btn-danger" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</asp:Content>
