﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmSettingGender : System.Web.UI.Page
    {
        backofficeBL BL = new backofficeBL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Gender();
                //BL.Bind_BL_Department(ddlDepartment);
            }
        }

        public void Bind_Gender()
        {
            string sql = "select GENDER_ID, GENDER_NAME from TB_GENDER";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            rptGender.DataSource = dt;
            rptGender.DataBind();
        }
        protected void rptGender_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblGenderName = (Label)e.Item.FindControl("lblGenderName");
            //Label lblPositionName = (Label)e.Item.FindControl("lblPositionName");
            //Label lblDepartment = (Label)e.Item.FindControl("lblDepartment");

            lblGenderName.Text = dr["GENDER_NAME"].ToString();
            //lblPositionName.Text = dr["POSITION_Name"].ToString();
            //lblDepartment.Text = dr["DEPARTMENT_NAME"].ToString();
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                //Insert/Update Data 
                TransactionDB trans = new TransactionDB();
                TbGenderLinqDB genderLnq = new TbGenderLinqDB();

                genderLnq.GetDataByPK(Convert.ToInt16(lblGenderID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();

                genderLnq.GENDER_NAME = txtGenderName.Text;
                //PositionLnq.POSITION_NAME = txtPositionName.Text;
                //PositionLnq.DEPARTMENT_ID = Convert.ToInt64(ddlDepartment.SelectedValue);

                if (genderLnq.GENDER_ID == 0)
                {
                    ret = genderLnq.InsertData("Username", trans.Trans);
                }
                else
                {
                    ret = genderLnq.UpdateData("Username", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblGenderID.Text = genderLnq.GENDER_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
            Clear();
            Bind_Gender();
        }

        private void Clear()
        {
            txtGenderName.Text = "";
            //txtPositionName.Text = "";
            Bind_Gender();
        }


        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtGenderName.Text))
            {
                alertmsg("กรุณาระบุเพศ");
                ret = false;
            }

            return ret;
        }

        protected void rptGender_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblGenderName = (Label)e.Item.FindControl("lblGenderName");
            Label lblID = (Label)e.Item.FindControl("lblID");

            if (e.CommandName == "edit")
            {
                lblGenderID.Text = lblID.Text;
                txtGenderName.Text = lblGenderName.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbGenderLinqDB genderLnq = new TbGenderLinqDB();

                ExecuteDataInfo ret = genderLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_Gender();
            }
        }
    }
}