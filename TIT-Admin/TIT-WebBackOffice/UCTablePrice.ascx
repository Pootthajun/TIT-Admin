﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCTablePrice.ascx.cs" Inherits="TIT_WebBackoffice.UCTablePrice" %>
<div class="col-md-12">
    <label class="bold">ราคาสินค้า/บริการ</label>
    <div class="col-md-12">
        <asp:Panel ID="addContact" runat="server" Visible="true">
            <asp:Label ID="lblCP_ID" runat="server" Text="0" Visible="false"></asp:Label>
            <table class="table table-padding table-bordered table-striped mb-none" id="contactzone">
                <thead>
                    <tr>
                        <th width="7%" class="tbl-th">No.<br />
                            ลำดับที่</th>
                        <th class="tbl-th">Description<br />
                            รายละเอียด</th>
                        <th width="10%" class="tbl-th">Qty<br />
                            จำนวน</th>
                        <th width="10%" class="tbl-th">Unit<br />
                            หน่วย</th>
                        <th width="15%" class="tbl-th">Unit Price<br />
                            ราคาต่อหน่วย</th>
                        <th width="15%" class="tbl-th">Line Total<br />
                            รวมเป็นจำนวนเงิน</th>
                        <th class="tbl-th" width="8%">เครื่องมือ</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rpt_Contact" runat="server" OnItemDataBound="rpt_Contact_ItemDataBound" OnItemCommand="rpt_Contact_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtNo" CssClass="form-control" runat="server" class="form-control"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDescription" CssClass="form-control" runat="server" class="form-control"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQty" CssClass="form-control" runat="server" class="form-control"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtUnit" CssClass="form-control" runat="server" class="form-control"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtUnitPrice" CssClass="form-control" runat="server" class="form-control"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLineTotal" CssClass="form-control" runat="server" class="form-control"></asp:TextBox>
                                </td>
                                <td class="actions th">
                                    <asp:LinkButton ID="delete" CommandName="delete" runat="server" Text=" delete" CssClass="fa fa-trash-o"></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary btn-float" Text="เพิ่ม" OnClick="btnAdd_Click" />
        </asp:Panel>
    </div>
</div>
