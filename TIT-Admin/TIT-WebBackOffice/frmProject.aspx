﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmProject.aspx.cs" Inherits="TIT_WebBackoffice.frmProject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Projects | TiT</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <section class="page-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="frmHome.aspx">Home</a></li>
                            <li class="active">Projects</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>Projects</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <asp:Panel ID="pnlCustomerList" runat="server" Visible="True">
                <div class="form-group dst-bottom">
                    <div class="panel panel-default dst-bottom">
                        <div class="panel-heading">
                            <h4 class="panel-title bold"><i class="fa fa-search dst-icon bold"></i>ค้นหารายชื่อโครงการ
                            </h4>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label>รหัสโครงการ</label>
                                    <input type="text" value="" class="form-control">
                                </div>
                                <div class="col-md-5">
                                    <label>ชื่อโครงการ</label>
                                    <input type="text" value="" class="form-control">
                                </div>
                                <div class="col-md-5">
                                    <label>ชื่อลูกค้า</label>
                                    <input type="text" value="" class="form-control">
                                </div>
                            </div>
                            <div class="form-group lbl-control">
                                <div class="col-md-12">
                                    <input type="button" value="Search.." class="btn btn-primary" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <section class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-bottom">Projects List</h2>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <!--ตารางแสดงผลลูกค้า-->
                            <div class="col-md-12">
                                <table id="tbCustomer" class="table table-bordered table-striped mb-none">
                                    <thead>
                                        <tr class="bg-gray">
                                            <th class="tbl-th" width="10%">รหัสโครงการ</th>
                                            <th class="tbl-th">ชื่อโครงการ</th>
                                            <th class="tbl-th">ชื่อลูกค้า</th>
                                            <th class="tbl-th" width="10%">วันเริ่มต้นโครงการ</th>
                                            <th class="tbl-th" width="10%">วันสิ้นสุดโครงการ</th>
                                            <th class="tbl-th" width="8%">เครื่องมือ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%--<asp:Repeater ID="rptProjectList" runat="server" OnItemDataBound="rptProjectList_ItemDataBound">
                                            <ItemTemplate>--%>
                                        <tr>
                                            <td align="center" class="actions th">TEST</td>
                                            <td align="center" class="actions th">TEST</td>
                                            <td align="center" class="actions th">TEST</td>
                                            <td align="center" class="actions th">01-01-2017</td>
                                            <td align="center" class="actions th">31-12-2017</td>
                                            <td class="actions th" align="center">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-navicon text-blue"></i>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#"><i class="fa fa-search distance"></i>ดูรายละเอียด</a></li>
                                                        <li><a href="#"><i class="fa fa-pencil-square-o distance"></i>แก้ไข</a></li>
                                                        <li><a href="#"><i class="fa fa-trash-o distance"></i>ลบ</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <%--</ItemTemplate>
                                        </asp:Repeater>--%>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </asp:Panel>
            <asp:Button ID="btnCreate" runat="server" CssClass="btn btn-primary" Text="CREATE" OnClick="btnCreate_Click" />

            <asp:Panel ID="pnlCreate" runat="server" Visible="False">
                <%--<h4 class="panel-bottom">Creat New Project</h4>--%>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#popular" data-toggle="tab"><i class="fa fa-list-alt distance"></i>Information</a>
                                    </li>
                                    <li>
                                        <a href="#recent" data-toggle="tab"><i class="fa fa-th-list distance"></i>Project Plan</a>
                                    </li>
                                    <li>
                                        <a href="#aa" data-toggle="tab"><i class="fa fa-th-list distance"></i>Responsible Person</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div id="popular" class="tab-pane active">
                                        <div class="form-group">
                                            <asp:Label ID="lblCusID" runat="server" Text="0" Visible="false"></asp:Label>
                                            <div class="col-md-3">
                                                <asp:Label ID="lblCusCode" runat="server" Text="รหัสโครงการ"></asp:Label>
                                                <asp:TextBox ID="txtCusCode" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-md-9">
                                                <asp:Label ID="lblCusName" runat="server" Text="ชื่อโครงการ"></asp:Label>
                                                <asp:TextBox ID="txtCusName" runat="server" CssClass="form-control" MaxLength="150"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <asp:Label ID="lblCus_Type" runat="server" Text="ชื่อลูกค้า"></asp:Label>
                                                <select class="form-control input-sm mb-md">
                                                    <option>Ana</option>
                                                    <option>Lexi</option>
                                                    <option>Vince</option>
                                                    <option>Adam</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>ผู้จัดการโครงการ</label>
                                                <select class="form-control input-sm mb-md">
                                                    <option>Ana</option>
                                                    <option>Lexi</option>
                                                    <option>Vince</option>
                                                    <option>Adam</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label>วันเริ่มโครงการ</label>
                                                <div class="input-daterange input-group" data-plugin-datepicker="">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <asp:TextBox ID="OF_Brith" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>วันสิ้นสุดโครงการ</label>
                                                <div class="input-daterange input-group" data-plugin-datepicker="">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </span>
                                                        <asp:TextBox ID="txtDate" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="form-group" align="center">
                                                    <asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="btnSave_Click" />
                                                    <asp:Button ID="btnCancel" runat="server" Text="CANCEL" CssClass="btn btn-danger" OnClick="btnCancel_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="recent" class="tab-pane">
                                        <div class="row">
                                            <!--ตารางแสดงผลลูกค้า-->
                                            <div class="col-md-12">
                                                <asp:Button ID="Button3" runat="server" CssClass="btn btn-primary" Text="Add" OnClick="Button3_Click" />

                                                <table class="table table-bordered table-striped mb-none">
                                                    <thead>
                                                        <tr class="bg-gray">
                                                            <%--<th class="tbl-th" width="10%">รหัสโครงการ</th>--%>
                                                            <th class="tbl-th th">ชื่อกิจกรรม</th>
                                                            <%--<th class="tbl-th">ชื่อลูกค้า</th>--%>
                                                            <th class="tbl-th" width="12%">วันเริ่มต้นกิจกรรม</th>
                                                            <th class="tbl-th" width="12%">วันสิ้นสุดกิจกรรม</th>
                                                            <th class="tbl-th" width="10%">จำนวนวัน</th>
                                                            <th class="tbl-th" width="12%">เครื่องมือ</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <%--<asp:Repeater ID="rptProjectList" runat="server" OnItemDataBound="rptProjectList_ItemDataBound">
                                            <ItemTemplate>--%>
                                                        <tr>
                                                            <%--<td align="center" class="actions th">TEST</td>--%>
                                                            <td align="center" class="actions th">TEST</td>
                                                            <td align="center" class="actions th">01-01-2017</td>
                                                            <td align="center" class="actions th">31-12-2017</td>
                                                            <td align="center" class="actions th">TEST</td>
                                                            <td class="actions th" align="center">
                                                                <a href="#"><i class="fa fa-plus distance"></i></a>
                                                                <a href="#"><i class="fa fa-pencil-square-o distance"></i></a>
                                                                <a href="#"><i class="fa fa-trash-o distance"></i></a>
                                                            </td>
                                                        </tr>
                                                        <%--</ItemTemplate>
                                        </asp:Repeater>--%>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <asp:Panel ID="Panel1" runat="server" Visible="False">
                                            <h4 class="panel-bottom">Creat New Activity</h4>
                                            <div class="panel-body">
                                                <!--ข้อมูลลูกค้า-->
                                                <div class="form-group dst-bottom">
                                                    <div class="panel panel-default dst-bottom">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title"><i class="fa fa-building dst-icon"></i>ข้อมูลโครงการ</h4>
                                                        </div>
                                                        <div>
                                                            <div class="panel-body">
                                                                <div class="form-group">
                                                                    <asp:Label ID="Label1" runat="server" Text="0" Visible="false"></asp:Label>
                                                                    <div class="col-md-6">
                                                                        <asp:Label ID="Label2" runat="server" Text="ชื่อกิจกรรม"></asp:Label>
                                                                        <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <asp:Label ID="Label3" runat="server" Text="ชื่อโครงการ"></asp:Label>
                                                                        <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control" MaxLength="150"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-md-3">
                                                                        <label>วันเริ่มต้นกิจกรรม</label>
                                                                        <div class="input-daterange input-group" data-plugin-datepicker="">
                                                                            <span class="input-group-addon">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </span>
                                                                            <asp:TextBox ID="TextBox4" CssClass="form-control" runat="server"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>วันสิ้นสุดกิจกรรม</label>
                                                                        <div class="input-daterange input-group" data-plugin-datepicker="">
                                                                            <span class="input-group-addon">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </span>
                                                                            <asp:TextBox ID="TextBox5" CssClass="form-control" runat="server"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group" align="center">
                                                                            <asp:Button ID="Button1" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="Button1_Click" />
                                                                            <asp:Button ID="Button2" runat="server" Text="CANCEL" CssClass="btn btn-danger" OnClick="Button2_Click" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div id="aa" class="tab-pane">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <table class="table table-padding table-bordered table-striped mb-none" id="edu">
                                                    <thead>
                                                        <tr>
                                                            <th class="tbl-th">ฝ่ายงาน</th>
                                                            <th class="tbl-th">พนักงาน</th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <label>ออกแบบดาต้าเบส</label>
                                                            </td>
                                                            <td>
                                                                <select class="form-control input-sm mb-md">
                                                                    <option>Ana</option>
                                                                    <option>Lexi</option>
                                                                    <option>Vince</option>
                                                                    <option>Adam</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>ออกแบบหน้าเว็บ</label>
                                                            </td>
                                                            <td>
                                                                <select class="form-control input-sm mb-md">
                                                                    <option>Ana</option>
                                                                    <option>Lexi</option>
                                                                    <option>Vince</option>
                                                                    <option>Adam</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>Programing</label>
                                                            </td>
                                                            <td>
                                                                <select class="form-control input-sm mb-md">
                                                                    <option>Ana</option>
                                                                    <option>Lexi</option>
                                                                    <option>Vince</option>
                                                                    <option>Adam</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>ดูแลระบบ</label>
                                                            </td>
                                                            <td>
                                                                <select class="form-control input-sm mb-md">
                                                                    <option>Ana</option>
                                                                    <option>Lexi</option>
                                                                    <option>Vince</option>
                                                                    <option>Adam</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>ออกแบบดาต้าเบส</label>
                                                            </td>
                                                            <td>
                                                                <select class="form-control input-sm mb-md">
                                                                    <option>Ana</option>
                                                                    <option>Lexi</option>
                                                                    <option>Vince</option>
                                                                    <option>Adam</option>
                                                                    <option>Coco</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div style="text-align: center;" class="col-md-12">
                                                    <asp:Button ID="PMsave" CssClass="btn btn-primary" runat="server" Text="SAVE" />
                                                    <asp:Button ID="btnOFcancel" CssClass="btn btn-danger" runat="server" Text="CANCEL" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </form>
</asp:Content>
