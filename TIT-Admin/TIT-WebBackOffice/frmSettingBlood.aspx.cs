﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmSettingBlood : System.Web.UI.Page
    {
        backofficeBL BL = new backofficeBL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Blood();
                //BL.Bind_BL_Department(ddlDepartment);
            }
        }

        public void Bind_Blood()
        {
            string sql = "select BLOOD_ID, BLOOD_NAME from TB_OF_BLOOD";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            rptBlood.DataSource = dt;
            rptBlood.DataBind();
        }
        protected void rptBlood_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblBloodName = (Label)e.Item.FindControl("lblBloodName");
            Label lblID = (Label)e.Item.FindControl("lblID");

            lblBloodName.Text = dr["BLOOD_NAME"].ToString();
            lblID.Text = dr["BLOOD_ID"].ToString();

        }

        protected void rptBlood_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblBloodName = (Label)e.Item.FindControl("lblBloodName");
            Label lblID = (Label)e.Item.FindControl("lblID");

            if (e.CommandName == "edit") {
                lblBloodID.Text = lblID.Text;
                txtBloodName .Text = lblBloodName.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbOfBloodLinqDB bloodLnq = new TbOfBloodLinqDB();

                ExecuteDataInfo ret = bloodLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_Blood();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                //Insert/Update Data 
                TransactionDB trans = new TransactionDB();
                TbOfBloodLinqDB bloodLnq = new TbOfBloodLinqDB();

                bloodLnq.GetDataByPK(Convert.ToInt16(lblBloodID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();

                bloodLnq.BLOOD_NAME = txtBloodName.Text;

                if (bloodLnq.BLOOD_ID == 0)
                {
                    ret = bloodLnq.InsertData("Username", trans.Trans);
                }
                else
                {
                    ret = bloodLnq.UpdateData("Username", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblBloodID.Text = bloodLnq.BLOOD_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
            Clear();
            Bind_Blood();
        }

        private void Clear()
        {
            txtBloodName.Text = "";
            Bind_Blood();
        }


        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtBloodName.Text))
            {
                alertmsg("กรุณาระบุกรุ๊ปเลือด");
                ret = false;
            }
            return ret;
        }

        protected void edit_Click(object sender, EventArgs e)
        {
                if (txtBloodName.Text != "" )
                {
                    TransactionDB trans = new TransactionDB();
                    TbOfBloodLinqDB bloodLnq = new TbOfBloodLinqDB();
                //string sql = "UPDATE TB_OF_BLOOD SET BLOOD_NAME=@BLOOD_NAME WHERE BLOOD_ID = @BLOOD_ID";
                ExecuteDataInfo ret = bloodLnq.UpdateData("UPDATE TB_OF_BLOOD SET BLOOD_NAME=@BLOOD_NAME WHERE BLOOD_ID = @BLOOD_ID", trans.Trans);
            }
                else
                {
                    alertmsg("Please Select Record to Update");
                }
            Bind_Blood();

            //    if (txtPositionCode.Text != "" && txtPositionName.Text != "")
            //    {
            //        //string sql = "UPDATE TB_CUS_TYPE SET CUS_Type_Code = , CUS_Type_Name= WHERE CUS_Type_ID =";

            //    }
            //    else
            //    {
            //        alertmsg("Please Select Record to Update");
            //    }
        }

        protected void delete_Click(object sender, EventArgs e)
        {
            TransactionDB trans = new TransactionDB();
            TbOfBloodLinqDB bloodLnq = new TbOfBloodLinqDB();
            //ExecuteDataInfo ret = bloodLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
            ExecuteDataInfo ret = bloodLnq.DeleteByPK(20, trans.Trans);
            //bloodLnq.GetDataByPK(Convert.ToInt16(lblBloodID.Text), trans.Trans);
            if (ret.IsSuccess == true)
            {
                trans.CommitTransaction();
            }
            else
            {
                trans.RollbackTransaction();
            }
            Bind_Blood();
        }

       
    }
}