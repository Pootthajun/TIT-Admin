﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmCustomers : System.Web.UI.Page
    {
        backofficeBL BL = new backofficeBL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BL.Bind_BL_Province(Cus_ProvinceList);
                BL.Bind_BL_CustomerType(ddlCusType);
                BL.Bind_BL_CustomerType(ddlCusTypeSearch);
                BL.Bind_BL_Province(Brn_ProvinceList);
                BindData();
                //BindList();
            }
        }
        public DataTable AllData
        {
            get
            {
                try
                {
                    return (DataTable)Session["ProjectPage"];
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            set { Session["ProjectPage"] = value; }
        }

        //ddl Customer       
        protected void Cus_ProvinceList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cus_ProvinceList.SelectedIndex > 0)
            {
                BL.Bind_BL_Amphur(Cus_AmphurList, Cus_ProvinceList.SelectedValue);
                Cus_AmphurList_SelectedIndexChanged(null, null);
            }
            else
            {
                BL.Bind_BL_Amphur(Cus_AmphurList, Cus_ProvinceList.SelectedValue);
                BL.Bind_BL_Tumbol(Cus_TumbolList, Cus_AmphurList.SelectedValue);
            }
        }
        protected void Cus_AmphurList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BL.Bind_BL_Tumbol(Cus_TumbolList, Cus_AmphurList.SelectedValue);
        }

        //ddl Branch
        protected void Brn_ProvinceList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Brn_ProvinceList.SelectedIndex > 0)
            {
                BL.Bind_BL_Amphur(Brn_AmphurList, Brn_ProvinceList.SelectedValue);
                Brn_AmphurList_SelectedIndexChanged(null, null);
            }
            else
            {
                BL.Bind_BL_Amphur(Brn_AmphurList, Brn_ProvinceList.SelectedValue);
                BL.Bind_BL_Tumbol(Brn_TumbolList, Brn_AmphurList.SelectedValue);
            }
        }
        protected void Brn_AmphurList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BL.Bind_BL_Tumbol(Brn_TumbolList, Brn_AmphurList.SelectedValue);
        }
        
        protected void BindData()
        { 
                string sql = "select c.CUS_ID, c.CUS_Code, c.CUS_Name, c.CUS_TaxID, c.CUS_Type_ID, ct.CUS_Type_Name";
                sql += " from TB_CUSTOMER as c ";
                sql += " inner join TB_CUS_TYPE as ct on c.CUS_Type_ID = ct.CUS_Type_ID";
            if (txtSearch_CusCode.Text != "")
            {
                sql += " WHERE c.CUS_Code LIKE '%" + txtSearch_CusCode.Text + "%' ";
            }
            if (txtSearch_CusName.Text != "")
            {
                sql += " WHERE c.CUS_Name LIKE '%" + txtSearch_CusName.Text + "%' ";
            }
            if (txtSearch_CusTaxID.Text != "")
            {
                sql += " WHERE c.CUS_TaxID LIKE '%" + txtSearch_CusTaxID.Text + "%' ";
            }
            if (ddlCusTypeSearch.SelectedValue != "")
            {
                sql += " WHERE c.CUS_Type_ID LIKE '%" + ddlCusTypeSearch.Text + "%' ";
            }
            sql += " ORDER BY c.CUS_ID DESC";
                DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
                rptList.DataSource = dt;
                rptList.DataBind();
        }

        //protected void BindData_Search()
        //{
        //    string sql_search = "select c.CUS_ID, c.CUS_Code, c.CUS_Name, c.CUS_TaxID, c.CUS_Type_ID, ct.CUS_Type_Name";
        //    sql_search += " from TB_CUSTOMER as c ";
        //    sql_search += " Left join TB_CUS_TYPE as ct on c.CUS_Type_ID = ct.CUS_Type_ID";
        //    sql_search += " WHERE c.CUS_Code LIKE '%" + txtSearch_CusCode.Text + "%' AND c.CUS_Name LIKE '%" + txtSearch_CusName.Text + "%' AND c.CUS_TaxID LIKE '%" + txtSearch_CusTaxID.Text + "%' AND c.CUS_Type_ID LIKE '%" + ddlCusTypeSearch.Text + "%' ";
        //    sql_search += " ORDER BY CUS_ID DESC";
        //    DataTable ds = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql_search);
        //    rptList.DataSource = ds;
        //    rptList.DataBind();
        //}

        protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }
            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblCUS_ID = (Label)e.Item.FindControl("lblCUS_ID");
            Label lblCUS_Code = (Label)e.Item.FindControl("lblCUS_Code");
            Label lblCUS_Name = (Label)e.Item.FindControl("lblCUS_Name");
            Label lblCUS_TaxID = (Label)e.Item.FindControl("lblCUS_TaxID");
            Label lblCUS_Type_Name = (Label)e.Item.FindControl("lblCUS_Type_Name");
            Label lblCUS_Type_ID = (Label)e.Item.FindControl("lblCUS_Type_ID");

            lblCUS_ID.Text = dr["CUS_ID"].ToString();
            lblCUS_Code.Text = dr["CUS_Code"].ToString();
            lblCUS_Name.Text = dr["CUS_Name"].ToString();
            lblCUS_TaxID.Text = dr["CUS_TaxID"].ToString();
            lblCUS_Type_Name.Text = dr["CUS_Type_Name"].ToString();
            lblCUS_Type_ID.Text = dr["CUS_Type_ID"].ToString();
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblCUS_ID = (Label)e.Item.FindControl("lblCUS_ID");
            Label lblCUS_Code = (Label)e.Item.FindControl("lblCUS_Code");
            Label lblCUS_Name = (Label)e.Item.FindControl("lblCUS_Name");
            Label lblCUS_Type_ID = (Label)e.Item.FindControl("lblCUS_Type_ID");
            Label lblCUS_TaxID = (Label)e.Item.FindControl("lblCUS_TaxID");
           
            if (e.CommandName == "edit")
            {
                string sqlCus = "select c.CUS_ID, CUS_Code, CUS_Name, CUS_TaxID, c.CUS_Type_ID, ct.CUS_Type_Name, CUS_Address, PROVINCE_ID, AMPHUR_ID, TUMBOL_ID, Postal_Code, Customer_Detail";
                sqlCus += " from TB_CUSTOMER as c ";
                sqlCus += " Left join TB_CUS_TYPE as ct on c.CUS_Type_ID = ct.CUS_Type_ID";
                sqlCus += " Where c.CUS_ID = " + lblCUS_ID.Text + " ";
                sqlCus += " ORDER BY CUS_ID DESC";
                DataTable de = LinqDB.ConnectDB.SqlDB.ExecuteTable(sqlCus);
                if (de.Rows.Count > 0)
                {
                    lblCusID.Text = de.Rows[0]["CUS_ID"].ToString();
                    txtCusCode.Text = de.Rows[0]["CUS_Code"].ToString();
                    txtCusName.Text = de.Rows[0]["CUS_Name"].ToString();
                    ddlCusType.SelectedValue = de.Rows[0]["CUS_Type_ID"].ToString();
                    txtTaxID.Text = de.Rows[0]["CUS_TaxID"].ToString();
                    txtCusAddress.Text = de.Rows[0]["CUS_Address"].ToString();
                    Cus_ProvinceList.SelectedValue = de.Rows[0]["PROVINCE_ID"].ToString();
                    Cus_ProvinceList_SelectedIndexChanged(null, null);
                    Cus_AmphurList.SelectedValue = de.Rows[0]["AMPHUR_ID"].ToString();
                    Cus_AmphurList_SelectedIndexChanged(null, null);
                    Cus_TumbolList.SelectedValue = de.Rows[0]["TUMBOL_ID"].ToString();
                    txtPostalCode.Text = de.Rows[0]["Postal_Code"].ToString();
                    txtDetail.InnerText = de.Rows[0]["Customer_Detail"].ToString();

                    contactperson.BindData_ContactPerson(lblCUS_ID.Text);
                }

                string sqlBrn = "select CUS_ID, BRN_ID, BRN_Name, BRN_TaxID, BRN_Address, PROVINCE_ID, AMPHUR_ID, TUMBOL_ID, Postal_Code";
                sqlBrn += " from TB_BRANCH ";
                sqlBrn += " Where CUS_ID = " + lblCUS_ID.Text + " ";
                sqlBrn += " ORDER BY CUS_ID DESC";
                DataTable ds = LinqDB.ConnectDB.SqlDB.ExecuteTable(sqlBrn);
                if (ds.Rows.Count > 0)
                {
                    lblBranchID.Text = ds.Rows[0]["BRN_ID"].ToString();
                    txtBranch.Text = ds.Rows[0]["BRN_Name"].ToString();
                    txtTaxIDBrn.Text = ds.Rows[0]["BRN_TaxID"].ToString();
                    txtAddressBrn.Text = ds.Rows[0]["BRN_Address"].ToString();
                    Brn_ProvinceList.SelectedValue = ds.Rows[0]["PROVINCE_ID"].ToString();
                    Brn_ProvinceList_SelectedIndexChanged(null, null);
                    Brn_AmphurList.SelectedValue = ds.Rows[0]["AMPHUR_ID"].ToString();
                    Brn_AmphurList_SelectedIndexChanged(null, null);
                    Brn_TumbolList.SelectedValue = ds.Rows[0]["TUMBOL_ID"].ToString();
                    txtPostalCode2.Text = ds.Rows[0]["Postal_Code"].ToString();

                    ContactPersonBrn.BindData_ContactPersonBrn(lblCUS_ID.Text, lblBranchID.Text);
                }

            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                  
                SqlDB.ExecuteNonQuery("DELETE FROM TB_CUS_CONTACT WHERE CP_ID IN(SELECT q.CP_ID FROM TB_CUS_CONTACT q INNER JOIN TB_CONTACT_PERSON u on(u.CP_ID = q.CP_ID) WHERE u.CP_ID = q.CP_ID AND u.CUS_ID = " + lblCUS_ID.Text + ")", trans.Trans);
                    TbContactPersonLinqDB CPLnq = new TbContactPersonLinqDB();
                    ExecuteDataInfo retCP = CPLnq.DeleteByPK(Convert.ToInt16(lblCUS_ID.Text), trans.Trans);
                if (retCP.IsSuccess == true)
                {;
                    TbBranchLinqDB brnLnq = new TbBranchLinqDB();
                    ExecuteDataInfo retBrn = brnLnq.DeleteByPK(Convert.ToInt16(lblCUS_ID.Text), trans.Trans);
                    if (retBrn.IsSuccess == true)
                    {
                        TbCustomerLinqDB CusLnq = new TbCustomerLinqDB();
                        ExecuteDataInfo retCus = CusLnq.DeleteByPK(Convert.ToInt16(lblCUS_ID.Text), trans.Trans);
                        if (retCus.IsSuccess == true)
                        {
                            trans.CommitTransaction();
                        }
                    }
                }
                else
                {
                    trans.RollbackTransaction();
                }
                BindData();
            }
        }
        //private void BindList()
        //{
        //    string sql = "select c.CUS_ID, CUS_Code, CUS_Name, CUS_TaxID, c.CUS_Type_ID, ct.CUS_Type_Name";
        //    sql += " from TB_CUSTOMER as c ";
        //    sql += " inner join TB_CUS_TYPE as ct on c.CUS_Type_ID = ct.CUS_Type_ID";
        //    DataTable DT = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
        //    rptList.DataSource = DT;
        //    rptList.DataBind();
        //    AllData = DT;
        //    Pager.SesssionSourceName = "ProjectPage";
        //    Pager.RenderLayout();
        //}

        //protected void Pager_PageChanging(UCPageNavigation Sender)
        //{
        //    Pager.TheRepeater = rptList;
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if(validate() == true)
            {
                
                TransactionDB trans = new TransactionDB();
                TbCustomerLinqDB CusLnq = new TbCustomerLinqDB();
                TbBranchLinqDB brnLnq = new TbBranchLinqDB();
                CusLnq.GetDataByPK(Convert.ToInt16(lblCusID.Text), trans.Trans);
                CusLnq.CUS_CODE = txtCusCode.Text;
                CusLnq.CUS_NAME = txtCusName.Text;
                CusLnq.CUS_TAXID = txtTaxID.Text;
                CusLnq.CUS_TYPE_ID = Convert.ToInt64(ddlCusType.SelectedValue);
                CusLnq.CUS_ADDRESS = txtCusAddress.Text;
                CusLnq.PROVINCE_ID = Convert.ToInt64(Cus_ProvinceList.SelectedValue).ToString();
                CusLnq.AMPHUR_ID = Convert.ToInt64(Cus_AmphurList.SelectedValue).ToString();
                CusLnq.TUMBOL_ID = Convert.ToInt64(Cus_TumbolList.SelectedValue).ToString();
                CusLnq.POSTAL_CODE = txtPostalCode.Text;
                CusLnq.CUSTOMER_DETAIL = txtDetail.InnerText;
                CusLnq.CUS_CREATE_YEAR = Convert.ToString(DateTime.Now.Year);
                CusLnq.CUS_CREATE_MONTH = Convert.ToString(DateTime.Now.Month);
               
                ExecuteDataInfo ret = new ExecuteDataInfo();
                if (CusLnq.CUS_ID == 0)
                {
                    CusLnq.CUS_RUNNING_NO = getNewCustomerRunningNo(DateTime.Now.Year, DateTime.Now.Month, (int) CusLnq.CUS_TYPE_ID).ToString();
                    ret = CusLnq.InsertData("Username", trans.Trans);
                }
                else
                {
                    ret = CusLnq.UpdateData("Username", trans.Trans);
                }

                if (ret.IsSuccess == true)
                {
                    lblCusID.Text = CusLnq.CUS_ID.ToString();
                    ret = Save_UCContactPerson(trans, 0);

                    if (ret.IsSuccess == true)
                    {

                        ret = SaveBranch(trans);

                        if (ret.IsSuccess == true)
                            trans.CommitTransaction();
                        else
                            trans.RollbackTransaction();
                    }
                    else {
                        trans.RollbackTransaction();
                    }
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Clear();
                BindData();
                pnlCreate.Visible = true;
                btnCreate.Visible = false;
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            } 
        }

        private ExecuteDataInfo Save_UCContactPerson(TransactionDB trans, long BRN_ID)
        {
            //contactperson.Get_Current_Data_H_Detail();
            DataTable _dt_CUS_Contact_H = contactperson.dtCustContact();
            //DataTable _dt_CUS_Contact_BRN = ContactPersonBrn.dtCustContact();
            ExecuteDataInfo ret = new ExecuteDataInfo();
            
            //----------------------------------------------------------
            for (int i = 0; i <= _dt_CUS_Contact_H.Rows.Count - 1; i++)
            {
                TbContactPersonLinqDB CPLnq = new TbContactPersonLinqDB();
                CPLnq.GetDataByPK(Convert.ToInt16(_dt_CUS_Contact_H.Rows[i]["contact_person_id"].ToString()), trans.Trans);
                CPLnq.CUS_ID = Convert.ToInt64(lblCusID.Text);
                CPLnq.TI_ID = Convert.ToInt64(_dt_CUS_Contact_H.Rows[i]["title_id"].ToString());
                CPLnq.CP_NAME = _dt_CUS_Contact_H.Rows[i]["person_first_name"].ToString();
                CPLnq.CP_SURNAME = _dt_CUS_Contact_H.Rows[i]["person_surname"].ToString();
                CPLnq.POSITION_ID = Convert.ToInt64(_dt_CUS_Contact_H.Rows[i]["position_id"].ToString());
                CPLnq.DEPARTMENT_ID = Convert.ToInt64(_dt_CUS_Contact_H.Rows[i]["department_id"].ToString());
                CPLnq.BRN_ID = null;

                if (CPLnq.CP_ID == 0)
                {
                    ret = CPLnq.InsertData("Username", trans.Trans);
                }
                else
                {
                    ret = CPLnq.UpdateData("Username", trans.Trans);
                }


                if (ret.IsSuccess == true)
                {
                    if (BRN_ID == 0)  //ผู้ติดต่อของลูกค้า
                    {
                        SqlDB.ExecuteNonQuery("Delete TB_CUS_CONTACT  WHERE CP_ID =" + CPLnq.CP_ID + "", trans.Trans);

                        DataTable contactList = (DataTable)_dt_CUS_Contact_H.Rows[i]["contact_list"];
                        for (int j = 0; j <= contactList.Rows.Count - 1; j++)
                        {
                            TbCusContactLinqDB CusContLnq = new TbCusContactLinqDB();
                            CusContLnq.CP_ID = CPLnq.CP_ID;
                            CusContLnq.CT_TYPE_ID = Convert.ToInt64(contactList.Rows[j]["CT_TYPE_ID"].ToString());
                            CusContLnq.CONTACT_DETAIL = contactList.Rows[j]["Contact_Detail"].ToString();

                            if (CusContLnq.CUS_CONTACT_ID == 0) 
                            {
                                ret = CusContLnq.InsertData("Username", trans.Trans);
                                //ret = SaveBranch(trans);
                            }
                            else
                            {
                                ret = CusContLnq.UpdateData("Username", trans.Trans);
                               // ret = SaveBranch(trans);
                            }
                            if (ret.IsSuccess == false)
                            {
                                break;
                            }
                        }
                    }

                    //if (BRN_ID == 0)
                    //{


                    //}
                    //else
                    //{
                    //    //for (int iB = 0; iB <= _dt_CUS_Contact_BRN.Rows.Count - 1; iB++)
                    //    //{
                    //    //    CPLnq.GetDataByPK(Convert.ToInt16(_dt_CUS_Contact_BRN.Rows[iB]["contact_person_id"].ToString()), trans.Trans);
                    //    //    CPLnq.CUS_ID = Convert.ToInt64(lblCusID.Text);
                    //    //    CPLnq.TI_ID = Convert.ToInt64(_dt_CUS_Contact_BRN.Rows[iB]["title_id"].ToString());
                    //    //    CPLnq.CP_NAME = _dt_CUS_Contact_BRN.Rows[iB]["person_first_name"].ToString();
                    //    //    CPLnq.CP_SURNAME = _dt_CUS_Contact_BRN.Rows[iB]["person_surname"].ToString();
                    //    //    CPLnq.POSITION_ID = Convert.ToInt64(_dt_CUS_Contact_BRN.Rows[iB]["position_id"].ToString());
                    //    //    CPLnq.DEPARTMENT_ID = Convert.ToInt64(_dt_CUS_Contact_BRN.Rows[iB]["department_id"].ToString());
                    //    //    CPLnq.BRN_ID = Convert.ToInt64(lblBranchID.Text);
                    //    //}
                    //}


                    //else //ผู้ติดต่อของสาขา
                    //{
                    //    SqlDB.ExecuteNonQuery("Delete TB_CUS_CONTACT  WHERE CP_ID =" + CPLnq.CP_ID + "", trans.Trans);

                    //    DataTable contactList = (DataTable)_dt_CUS_Contact_BRN.Rows[i]["contact_list"];
                    //    for (int j = 0; j <= contactList.Rows.Count - 1; j++)
                    //    {
                    //        TbCusContactLinqDB CusContLnq = new TbCusContactLinqDB();

                    //        CusContLnq.CP_ID = CPLnq.CP_ID;
                    //        CusContLnq.CT_TYPE_ID = Convert.ToInt64(contactList.Rows[j]["CT_TYPE_ID"].ToString());
                    //        CusContLnq.CONTACT_DETAIL = contactList.Rows[j]["Contact_Detail"].ToString();

                    //        if (CusContLnq.CUS_CONTACT_ID == 0)
                    //        {
                    //            ret = CusContLnq.InsertData("Username", trans.Trans);
                    //            ret = SaveBranch(trans);
                    //        }
                    //        else
                    //        {
                    //            ret = CusContLnq.UpdateData("Username", trans.Trans);
                    //            ret = SaveBranch(trans);
                    //        }
                    //        if (ret.IsSuccess == false)
                    //        {
                    //            break;
                    //        }
                    //    }
                    //}

                    if (ret.IsSuccess == false)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }


            return ret;
        }

        private ExecuteDataInfo SaveBranch(TransactionDB trans)
        {
            ExecuteDataInfo ret = new ExecuteDataInfo();





            TbBranchLinqDB brnLnq = new TbBranchLinqDB();

            brnLnq.GetDataByPK(Convert.ToUInt16(lblBranchID.Text), trans.Trans);
          
            if (txtBranch.Text == "" && txtAddressBrn.Text == "")
            {
                lblBranchID.Text = brnLnq.BRN_ID.ToString();
                //ret = Save_UCContactPerson(trans);
            }
            else
            {
                brnLnq.CUS_ID = Convert.ToInt64(lblCusID.Text);
                brnLnq.BRN_NAME = txtBranch.Text;
                brnLnq.BRN_TAXID = txtTaxIDBrn.Text;
                brnLnq.BRN_ADDRESS = txtAddressBrn.Text;
                brnLnq.PROVINCE_ID = Convert.ToInt64(Brn_ProvinceList.SelectedValue).ToString();
                brnLnq.AMPHUR_ID = Convert.ToInt64(Brn_AmphurList.SelectedValue).ToString();
                brnLnq.TUMBOL_ID = Convert.ToInt64(Brn_TumbolList.SelectedValue).ToString();
                brnLnq.POSTAL_CODE = txtPostalCode2.Text;

                if (brnLnq.BRN_ID == 0)
                {
                    ret = brnLnq.InsertData("Username", trans.Trans);

                    if (ret.IsSuccess == true)
                    {
                        lblBranchID.Text = brnLnq.BRN_ID.ToString();
                        //ret = Save_UCContactPerson(trans, 0);
                        //ret = Save_UCContactPersonBranch(trans);
                        DataTable _dt_CUS_Contact_BRN = ContactPersonBrn.dtCustContact();
                        for (int i = 0; i <= _dt_CUS_Contact_BRN.Rows.Count - 1; i++)
                        {
                            TbContactPersonLinqDB CPLnq = new TbContactPersonLinqDB();
                            CPLnq.GetDataByPK(Convert.ToInt16(_dt_CUS_Contact_BRN.Rows[i]["contact_person_id"].ToString()), trans.Trans);
                            CPLnq.CUS_ID = Convert.ToInt64(lblCusID.Text);
                            CPLnq.TI_ID = Convert.ToInt64(_dt_CUS_Contact_BRN.Rows[i]["title_id"].ToString());
                            CPLnq.CP_NAME = _dt_CUS_Contact_BRN.Rows[i]["person_first_name"].ToString();
                            CPLnq.CP_SURNAME = _dt_CUS_Contact_BRN.Rows[i]["person_surname"].ToString();
                            CPLnq.POSITION_ID = Convert.ToInt64(_dt_CUS_Contact_BRN.Rows[i]["position_id"].ToString());
                            CPLnq.DEPARTMENT_ID = Convert.ToInt64(_dt_CUS_Contact_BRN.Rows[i]["department_id"].ToString());
                            CPLnq.BRN_ID = Convert.ToInt64(lblBranchID.Text);

                            if (CPLnq.CP_ID == 0)
                            {
                                ret = CPLnq.InsertData("Username", trans.Trans);
                            }
                            else
                            {
                                ret = CPLnq.UpdateData("Username", trans.Trans);
                            }


                            if (ret.IsSuccess == true)
                            {

                                SqlDB.ExecuteNonQuery("Delete TB_CUS_CONTACT  WHERE CP_ID =" + CPLnq.CP_ID + "", trans.Trans);

                                DataTable contactList = (DataTable)_dt_CUS_Contact_BRN.Rows[i]["contact_list"];
                                for (int j = 0; j <= contactList.Rows.Count - 1; j++)
                                {
                                    TbCusContactLinqDB CusContLnq = new TbCusContactLinqDB();

                                    CusContLnq.CP_ID = CPLnq.CP_ID;
                                    CusContLnq.CT_TYPE_ID = Convert.ToInt64(contactList.Rows[j]["CT_TYPE_ID"].ToString());
                                    CusContLnq.CONTACT_DETAIL = contactList.Rows[j]["Contact_Detail"].ToString();

                                    if (CusContLnq.CUS_CONTACT_ID == 0)
                                    {
                                        ret = CusContLnq.InsertData("Username", trans.Trans);
                                    }
                                    else
                                    {
                                        ret = CusContLnq.UpdateData("Username", trans.Trans);
                                    }
                                    if (ret.IsSuccess == false)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                        }
                    else
                    {
                        trans.RollbackTransaction();
                    }
                }
                else
                {
                    ret = brnLnq.UpdateData("Username", trans.Trans);
                    if (ret.IsSuccess == true)
                    {
                        lblBranchID.Text = brnLnq.BRN_ID.ToString();
                        //ret = Save_UCContactPerson(trans, brnLnq.BRN_ID);
                        //ret = Save_UCContactPersonBranch(trans);
                        //SqlDB.ExecuteNonQuery("Delete TB_CUS_CONTACT  WHERE CP_ID IN(SELECT q.CP_ID FROM TB_CUS_CONTACT q INNER JOIN TB_CONTACT_PERSON u on(u.CP_ID = q.CP_ID) WHERE u.CP_ID = q.CP_ID AND u.CUS_ID = " + lblCusID.Text + " AND BRN_ID =" + lblBranchID.Text + ") )", trans.Trans);
                        SqlDB.ExecuteNonQuery("Delete TB_CONTACT_PERSON  WHERE BRN_ID =" + lblBranchID.Text + "", trans.Trans);
                        DataTable _dt_CUS_Contact_BRN = ContactPersonBrn.dtCustContact();
                        for (int i = 0; i <= _dt_CUS_Contact_BRN.Rows.Count - 1; i++)
                        {
                            TbContactPersonLinqDB CPLnq = new TbContactPersonLinqDB();
                            CPLnq.GetDataByPK(Convert.ToInt16(_dt_CUS_Contact_BRN.Rows[i]["contact_person_id"].ToString()), trans.Trans);
                            CPLnq.CUS_ID = Convert.ToInt64(lblCusID.Text);
                            CPLnq.TI_ID = Convert.ToInt64(_dt_CUS_Contact_BRN.Rows[i]["title_id"].ToString());
                            CPLnq.CP_NAME = _dt_CUS_Contact_BRN.Rows[i]["person_first_name"].ToString();
                            CPLnq.CP_SURNAME = _dt_CUS_Contact_BRN.Rows[i]["person_surname"].ToString();
                            CPLnq.POSITION_ID = Convert.ToInt64(_dt_CUS_Contact_BRN.Rows[i]["position_id"].ToString());
                            CPLnq.DEPARTMENT_ID = Convert.ToInt64(_dt_CUS_Contact_BRN.Rows[i]["department_id"].ToString());
                            CPLnq.BRN_ID = Convert.ToInt64(lblBranchID.Text);

                            if (CPLnq.CP_ID == 0)
                            {
                                ret = CPLnq.InsertData("Username", trans.Trans);
                            }
                            else
                            {
                                ret = CPLnq.UpdateData("Username", trans.Trans);
                            }


                            if (ret.IsSuccess == true)
                            {

                                SqlDB.ExecuteNonQuery("Delete TB_CUS_CONTACT  WHERE CP_ID =" + CPLnq.CP_ID + "", trans.Trans);

                                DataTable contactList = (DataTable)_dt_CUS_Contact_BRN.Rows[i]["contact_list"];
                                for (int j = 0; j <= contactList.Rows.Count - 1; j++)
                                {
                                    TbCusContactLinqDB CusContLnq = new TbCusContactLinqDB();

                                    CusContLnq.CP_ID = CPLnq.CP_ID;
                                    CusContLnq.CT_TYPE_ID = Convert.ToInt64(contactList.Rows[j]["CT_TYPE_ID"].ToString());
                                    CusContLnq.CONTACT_DETAIL = contactList.Rows[j]["Contact_Detail"].ToString();

                                    if (CusContLnq.CUS_CONTACT_ID == 0)
                                    {
                                        ret = CusContLnq.InsertData("Username", trans.Trans);
                                    }
                                    else
                                    {
                                        ret = CusContLnq.UpdateData("Username", trans.Trans);
                                    }
                                    if (ret.IsSuccess == false)
                                    {
                                        break;
                                    }
                                }
                            }

                        }
                    }
                }
            }
            return ret;
        }



        private ExecuteDataInfo Save_UCContactPersonBranch(TransactionDB trans)
        {
            ContactPersonBrn.Get_Current_Data_H_Detail();
            DataTable _dt_CUS_Contact_H = ContactPersonBrn.dt_CUS_Contact_H;

            ExecuteDataInfo ret = new ExecuteDataInfo();
            for (int i = 0; i <= _dt_CUS_Contact_H.Rows.Count - 1; i++)
            {
                TbContactPersonLinqDB CPBrnLnq = new TbContactPersonLinqDB();

                CPBrnLnq.CUS_ID = Convert.ToInt64(lblCusID.Text);
                CPBrnLnq.TI_ID = Convert.ToInt64(_dt_CUS_Contact_H.Rows[i]["TI_ID"].ToString());
                CPBrnLnq.CP_NAME = _dt_CUS_Contact_H.Rows[i]["CP_NAME"].ToString();
                CPBrnLnq.CP_SURNAME = _dt_CUS_Contact_H.Rows[i]["CP_SURNAME"].ToString();
                CPBrnLnq.POSITION_ID = Convert.ToInt64(_dt_CUS_Contact_H.Rows[i]["POSITION_ID"].ToString());
                CPBrnLnq.DEPARTMENT_ID = Convert.ToInt64(_dt_CUS_Contact_H.Rows[i]["DEPARTMENT_ID"].ToString());
                CPBrnLnq.BRN_ID = Convert.ToInt64(lblBranchID.Text);
                long _CP_ID = Convert.ToInt64(_dt_CUS_Contact_H.Rows[i]["CP_ID"].ToString());
                if (CPBrnLnq.CP_ID == 0)
                {
                    ret = CPBrnLnq.InsertData("Username", trans.Trans);

                    if (ret.IsSuccess == true)
                    {
                        lblContPerson_ID.Text = CPBrnLnq.CP_ID.ToString();

                        DataTable Tem_dt_CUS_Contact_Detail = ContactPersonBrn.dt_CUS_Contact_Detail;
                        Tem_dt_CUS_Contact_Detail.DefaultView.RowFilter = "CP_ID = '" + _CP_ID + "'";
                        DataTable _dt_CUS_Contact_Detail = Tem_dt_CUS_Contact_Detail.DefaultView.ToTable();

                        for (int j = 0; j <= _dt_CUS_Contact_Detail.Rows.Count - 1; j++)
                        {
                            TbCusContactLinqDB CusContBrnLnq = new TbCusContactLinqDB();
                            CusContBrnLnq.CP_ID = Convert.ToInt64(lblContPerson_ID.Text);
                            CusContBrnLnq.CT_TYPE_ID = Convert.ToInt64(_dt_CUS_Contact_Detail.Rows[j]["CT_TYPE_ID"].ToString());
                            CusContBrnLnq.CONTACT_DETAIL = _dt_CUS_Contact_Detail.Rows[j]["Contact_Detail"].ToString();

                            if (CusContBrnLnq.CUS_CONTACT_ID == 0)
                            {
                                ret = CusContBrnLnq.InsertData("Username", trans.Trans);
                            }
                            else
                            {
                                ret = CusContBrnLnq.UpdateData("Username", trans.Trans);
                            }
                        }
                    }
                    else
                    {
                        trans.RollbackTransaction();
                    }
                }
                else
                {
                    ret = CPBrnLnq.UpdateData("Username", trans.Trans);
                }
            }

            if (ret.IsSuccess == true)
            {
                trans.CommitTransaction();
            }
            else
            {
                trans.RollbackTransaction();
            }
            return ret;
        }

        public void Clear()
        {
            txtCusCode.Text = "";
            txtCusName.Text = "";
            txtTaxID.Text = "";
            txtDetail.InnerText = "";
            BL.Bind_BL_Province(Cus_ProvinceList);
            BL.Bind_BL_CustomerType(ddlCusType);
            BL.Bind_BL_CustomerType(ddlCusTypeSearch);
            BL.Bind_BL_Province(Brn_ProvinceList);
            txtCusAddress.Text = "";
            Cus_ProvinceList_SelectedIndexChanged(null, null);
            txtPostalCode.Text = "";
            txtBranch.Text = "";
            txtTaxIDBrn.Text = "";
            txtAddressBrn.Text = "";
            txtPostalCode2.Text = "";
            Brn_ProvinceList_SelectedIndexChanged(null, null);
            lblCusID.Text = "0";
            lblBranchID.Text = "0";
            lblContPerson_ID.Text = "0";
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            btnCreate.Visible = false;
            if (btnCreate.Visible == false)
            {
                pnlCreate.Visible = true;
                pnlCustomerList.Visible = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlCreate.Visible = false;
            if (pnlCreate.Visible == false)
            {
                pnlCustomerList.Visible = true;
                Clear();
                btnCreate.Visible = true;
                
            }
        }
        
        //running no
        public int getNewCustomerRunningNo(int Create_Year, int Create_Month, int CUS_Type_ID)
        {
            string sql = "SELECT ISNULL(MAX(CUS_Running_No),0)+1\n";
            sql += " FROM TB_CUSTOMER\n";
            sql += " WHERE CUS_Create_Year=" + Create_Year + "\n";
            sql += " AND CUS_Create_Month=" + Create_Month + "\n";
            sql += " AND CUS_Type_ID=" + CUS_Type_ID + "\n";
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            return (int)dt.Rows[0][0];
        }

        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtCusCode.Text))
            {
                alertmsg("กรุณาระบุรหัสลูกค้า");
                ret = false;
            }
            if (string.IsNullOrEmpty(txtCusName.Text))
            {
                alertmsg("กรุณาระบุชื่อลูกค้า");
                ret = false;
            }
            if (string.IsNullOrEmpty(ddlCusType.SelectedValue))
            {
                alertmsg("กรุณาเลือกประเภทลูกค้า");
                ret = false;
            }
            if (string.IsNullOrEmpty(txtTaxID.Text))
            {
                alertmsg("กรุณาระบุเลขประจำตัวผู้เสียภาษี");
                ret = false;
            }
            if (string.IsNullOrEmpty(txtCusAddress.Text))
            {
                alertmsg("กรุณาระบุที่อยู่ลูกค้า");
                ret = false;
            }
            if (string.IsNullOrEmpty(Cus_ProvinceList.Text))
            {
                alertmsg("กรุณาเลือกจังหวัด");
                ret = false;
            }
            if (string.IsNullOrEmpty(Cus_AmphurList.Text))
            {
                alertmsg("กรุณาเลือกอำเภอ");
                ret = false;
            }
            if (string.IsNullOrEmpty(Cus_TumbolList.Text))
            {
                alertmsg("กรุณาเลือกตำบล");
                ret = false;
            }
            if (string.IsNullOrEmpty(Cus_ProvinceList.Text))
            {
                alertmsg("กรุณาเลือกจังหวัด");
                ret = false;
            }
            if (string.IsNullOrEmpty(txtPostalCode.Text))
            {
                alertmsg("กรุณาระบุรหัสไปรษณีย์");
                ret = false;
            }
            if (string.IsNullOrEmpty(txtDetail.InnerText))
            {
                alertmsg("กรุณาระบุรายละเอียดเพิ่มเติม");
                ret = false;
            }
            if (string.IsNullOrEmpty(txtBranch.Text))
            {
                alertmsg("กรุณาระบุชื่อสาขา");
                ret = false;
            }
            if (string.IsNullOrEmpty(txtTaxIDBrn.Text))
            {
                alertmsg("กรุณาระบุเลขประจำตัวผู้เสียภาษี");
                ret = false;
            }
            if (string.IsNullOrEmpty(txtAddressBrn.Text))
            {
                alertmsg("กรุณาระบุที่อยู่ของสาขา");
                ret = false;
            }
            if (string.IsNullOrEmpty(Brn_ProvinceList.Text))
            {
                alertmsg("กรุณาเลือกจังหวัดของสาขา");
                ret = false;
            }
            if (string.IsNullOrEmpty(Brn_AmphurList.Text))
            {
                alertmsg("กรุณาเลือกอำเภอของสาขา");
                ret = false;
            }
            if (string.IsNullOrEmpty(Brn_TumbolList.Text))
            {
                alertmsg("กรุณาเลือกตำบลของสาขา");
                ret = false;
            }

            return ret;
        }

        protected void edit_Click(object sender, EventArgs e)
        {
            btnCreate.Visible = false;
            if (btnCreate.Visible == false)
            {
                pnlCreate.Visible = true;
                pnlCustomerList.Visible = false;
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            BindData();
        }
    }
}