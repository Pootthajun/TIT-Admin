﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TIT_WebBackoffice
{
    public partial class UCPageNavigation : System.Web.UI.UserControl
    {
        backofficeBL BL = new backofficeBL();
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public event PageChangedEventHandler PageChanged;
        public delegate void PageChangedEventHandler(UCPageNavigation Sender);
        public event PageChangingEventHandler PageChanging;
        public delegate void PageChangingEventHandler(UCPageNavigation Sender);


        public Repeater TheRepeater;
        //public int MaximunPageCount
        //{
        //    get { return Convert.ToInt32(btnFirst.Attributes["MaximunPageCount"]); }
        //    set { btnFirst.Attributes["MaximunPageCount"] = value.ToString(); }
        //}

        //public int PageSize
        //{
        //    //-------------1
        //    get { return Convert.ToInt32(btnFirst.Attributes["PageSize"]); }
        //    set { btnFirst.Attributes["PageSize"] = value.ToString(); }
        //}

        //public string SesssionSourceName
        //{
        //    //-------------2
        //    get { return (btnFirst.Attributes["SourceName"]); }
        //    set { btnFirst.Attributes["SourceName"] = value; }
        //}

        //public DataTable Datasource
        //{
        //    //-------------3
        //    get
        //    {
        //        if (string.IsNullOrEmpty(SesssionSourceName))
        //            return null;
        //        return (DataTable)Session["SesssionSourceName"];
        //    }
        //}

        //public int PageCount
        //{
        //    //-------------4
        //    get
        //    {
        //        if ((Datasource == null))
        //            return 0;
        //        DataTable Source = Datasource.Copy();
        //        int count = Source.Rows.Count;
        //        return Math.Ceiling(count / PageSize);
        //        //return PageSize;
        //    }
        //}

        //public int CurrentPage
        //{
        //    //-------------4
        //    get { return Convert.ToInt32(btnFirst.Attributes["CurrentPage"]); }
        //    set
        //    {
        //        if (value > PageCount)
        //        {
        //            value = PageCount;
        //        }
        //        if (value <= 0)
        //        {
        //            value = 0;
        //        }
        //        btnFirst.Attributes["CurrentPage"] = value.ToString();
        //        RenderLayout();


        //    }
        //}

        //protected void rptPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        //{
        //    if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
        //        return;
        //    Button btnPage = (Button)e.Item.FindControl("btnPage");
        //    CurrentPage = Convert.ToInt32(btnPage.Text);

        //}

        //protected void rptPage_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
        //        return;
        //    DataRowView dr = (DataRowView)e.Item.DataItem;

        //    Button btnPage = (Button)e.Item.FindControl("btnPage");
        //    btnPage.Text = dr["P"].ToString();
        //    //btnPage.Text = (Button)(e.Item.DataItem("P"));
        //    ///////////////////////////////////////

        //    if (Convert.ToInt32(dr["P"].ToString()) == CurrentPage)
        //    {
        //        btnPage.CssClass = "btn current-Page";
        //    }
        //    else
        //    {
        //        btnPage.CssClass = "btn";
        //    }
        //}


        //public void RenderLayout()
        //{
        //    if (PageChanging != null)
        //    {
        //        PageChanging(this);
        //    }

        //    //----------------- Set Pager---------------------
        //    if ((Datasource == null) || Datasource.Rows.Count == 0)
        //    {
        //        this.Visible = false;
        //        if ((TheRepeater != null))
        //        {
        //            TheRepeater.DataSource = null;
        //            TheRepeater.DataBind();
        //        }
        //        return;
        //    }

        //    DataTable Source = Datasource.Copy();

        //    int TotalRecord = Source.Rows.Count;
        //    if (CurrentPage == 0)
        //    {
        //        CurrentPage = 1;
        //        return;
        //    }
        //    else if (CurrentPage > PageCount)
        //    {
        //        CurrentPage = PageCount;
        //        return;
        //    }

        //    int StartIndex = (CurrentPage - 1) * PageSize;
        //    int EndIndex = StartIndex + PageSize - 1;
        //    if (EndIndex > TotalRecord - 1 | TotalRecord == 0)
        //        EndIndex = TotalRecord - 1;
        //    //-------------- Set Display Record-----------

        //    DataTable NewSource = Source.Copy();
        //    NewSource.Rows.Clear();

        //    if (StartIndex < 0)
        //        StartIndex = 0;
        //    for (int i = StartIndex; i <= EndIndex; i++)
        //    {
        //        DataRow DR = NewSource.NewRow();
        //        DR.ItemArray = Source.Rows[i].ItemArray;
        //        NewSource.Rows.Add(DR);
        //    }

        //    //-----------Render Page Number------------
        //    int TotalGroup = Math.Ceiling(PageCount / MaximunPageCount);
        //    int ThisGroup = Math.Ceiling(CurrentPage / MaximunPageCount);

        //    StartIndex = ((ThisGroup - 1) * MaximunPageCount) + 1;
        //    EndIndex = StartIndex + MaximunPageCount - 1;
        //    if (EndIndex > PageCount)
        //        EndIndex = PageCount;
        //    DataTable DT = new DataTable();
        //    DT.Columns.Add("P");
        //    for (int i = StartIndex; i <= EndIndex; i++)
        //    {
        //        DataRow DR = DT.NewRow();
        //        DR["P"] = i;
        //        DT.Rows.Add(DR);
        //    }
        //    rptPage.DataSource = DT;
        //    rptPage.DataBind();

        //    //-----------End Page Number------------

        //    //-------- Page Number Visible----------
        //    //btnNext.Visible = CurrentPage < PageCount
        //    //btnBack.Visible = CurrentPage > 1
        //    //btnFirst.Visible = CurrentPage <> 1
        //    //btnLast.Visible = CurrentPage <> PageCount
        //    //------- End Page Number Visible--------

        //    if ((TheRepeater == null))
        //        return;
        //    TheRepeater.DataSource = NewSource;
        //    TheRepeater.DataBind();

        //    if (PageChanged != null)
        //    {
        //        PageChanged(this);
        //    }

        //    this.Visible = PageCount > 1;

        //}

        //#region "Page Change"
        //// First Page
        //protected void btnFirst_Click(object sender, System.EventArgs e)
        //{
        //    CurrentPage = 1;
        //}

        //// Next
        //protected void btnNext_Click(object sender, System.EventArgs e)
        //{
        //    CurrentPage += 1;
        //}

        //// Back
        //protected void btnBack_Click(object sender, System.EventArgs e)
        //{
        //    CurrentPage -= 1;
        //}

        //// last Page
        //protected void btnLast_Click(object sender, System.EventArgs e)
        //{
        //    CurrentPage = PageCount;
        //}

        //#endregion
    }
}