﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmSettingGender.aspx.cs" Inherits="TIT_WebBackoffice.frmSettingGender" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Gender | TiT</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="upPnl" runat="server">
            <ContentTemplate>
                <section class="page-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="breadcrumb">
                                    <li><a href="#">Master</a></li>
                                    <li class="active">Gender</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h1>Gender</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="container">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-bottom">Gender List</h4>
                        </div>

                        <div class="panel-body">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped mb-none panel-bottom">
                                    <thead>
                                        <tr>
                                            <%--<th class="tbl-th" width="30%">Position Code</th>--%>
                                            <th class="tbl-th">เพศ</th>
                                            <%--<th class="tbl-th">Department</th>--%>
                                            <th class="tbl-th" width="12%">เครื่องมือ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptGender" runat="server" OnItemDataBound="rptGender_ItemDataBound" OnItemCommand="rptGender_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="actions th">
                                                        <asp:Label ID="lblID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "GENDER_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblGenderName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "GENDER_NAME") %>'></asp:Label></td>
                                                    <td class="actions th" align="center">
                                                        <asp:LinkButton ID="edit" runat="server" Text=" Edit" CssClass="fa fa-pencil-square-o" CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GENDER_ID") %>'></asp:LinkButton>
                                                        <asp:LinkButton ID="delete" runat="server" Text=" Delete" CssClass="fa fa-trash-o" CommandName="delete" OnClientClick='javascript:return confirm("Are you sure you want to delete?")' 
                                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GENDER_ID") %>'></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>

                    <asp:Panel ID="pnlGender" runat="server">
                        <div class="panel panel-default dst-bottom">
                            <div class="panel-heading">
                                <h4 class="panel-title"><i class="fa fa-group distance"></i>เพิ่ม / แก้ไขเพศ
                                </h4>
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                    <asp:Label ID="lblGenderID" runat="server" Text="0" Visible="false"></asp:Label>
                                    </div>
                                    <div class="col-md-5">
                                    <label>เพศ :</label>
                                        <asp:TextBox ID="txtGenderName" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" align="center">
                                        <asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnCancel" runat="server" Text="CANCEL" CssClass="btn btn-danger" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</asp:Content>
