﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmSettingPosition : System.Web.UI.Page
    {
        backofficeBL BL = new backofficeBL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Position();
                BL.Bind_BL_Department(ddlDepartment);
            }
        }

        public void Bind_Position()
        {
            string sql = "select p.POSITION_ID, p.POSITION_CODE, POSITION_Name, p.DEPARTMENT_ID, DEPARTMENT_NAME from TB_POSITION as p";
            sql += " inner join TB_DEPARTMENT as d on p.DEPARTMENT_ID = d.DEPARTMENT_ID";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            rptPosition.DataSource = dt;
            rptPosition.DataBind();
        }
        protected void rptPosition_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblPositionCode = (Label)e.Item.FindControl("lblPositionCode");
            Label lblPositionName = (Label)e.Item.FindControl("lblPositionName");
            Label lblDepartment = (Label)e.Item.FindControl("lblDepartment");

            lblPositionCode.Text = dr["POSITION_CODE"].ToString();
            lblPositionName.Text = dr["POSITION_Name"].ToString();
            lblDepartment.Text = dr["DEPARTMENT_NAME"].ToString();
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                //Insert/Update Data 
                TransactionDB trans = new TransactionDB();
                TbPositionLinqDB PositionLnq = new TbPositionLinqDB();

                PositionLnq.GetDataByPK(Convert.ToInt16(lblPositionID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();

                PositionLnq.POSITION_CODE = txtPositionCode.Text;
                PositionLnq.POSITION_NAME = txtPositionName.Text;
                PositionLnq.DEPARTMENT_ID = Convert.ToInt64(ddlDepartment.SelectedValue);

                if (PositionLnq.POSITION_ID == 0)
                {
                    ret = PositionLnq.InsertData("Username", trans.Trans);
                }
                else
                {
                    ret = PositionLnq.UpdateData("Username", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblPositionID.Text = PositionLnq.POSITION_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
            Clear();
            Bind_Position();
        }

        private void Clear()
        {
            txtPositionCode.Text = "";
            txtPositionName.Text = "";
            ddlDepartment.SelectedValue = "";
            Bind_Position();
        }


        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtPositionCode.Text))
            {
                alertmsg("กรุณาระบุรหัสตำแหน่ง");
                ret = false;
            }
            if (string.IsNullOrEmpty(txtPositionName.Text))
            {
                alertmsg("กรุณาระบุตำแหน่ง");
                ret = false;
            }
            if (string.IsNullOrEmpty(ddlDepartment.SelectedValue))
            {
                alertmsg("กรุณาเลือกแผนก");
                ret = false;
            }
            return ret;
        }

        protected void rptPosition_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblPositionCode = (Label)e.Item.FindControl("lblPositionCode");
            Label lblPositionName = (Label)e.Item.FindControl("lblPositionName");
            Label lblDepartment = (Label)e.Item.FindControl("lblDepartment");
            Label lblDepartmentID = (Label)e.Item.FindControl("lblDepartmentID");
            Label lblID = (Label)e.Item.FindControl("lblID");

            if (e.CommandName == "edit")
            {
                lblPositionID.Text = lblID.Text;
                txtPositionName.Text = lblPositionName.Text;
                txtPositionCode.Text = lblPositionCode.Text;
                ddlDepartment.SelectedValue = lblDepartmentID.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbPositionLinqDB PositionLnq = new TbPositionLinqDB();

                ExecuteDataInfo ret = PositionLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_Position();
            }
        }
    }
}