﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmBackOffice.aspx.cs" Inherits="TIT_WebBackoffice.frmBackOffice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Back Office | TiT</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="page-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="frmHome.aspx">Home</a></li>
                        <li class="active">Back Office</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1>Back Office</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="short">Project</h2>
                    <hr class="visible-sm visible-xs tall" />

                    <div class="panel-body">
                        <div class="row text-center">
                            <div class="col-md-6">
                                <div class="circular-bar">
                                    <div class="circular-bar-chart" data-percent="85" data-plugin-options='{ "barColor": "#0088CC", "delay": 300 }'>
                                        <strong><a href="frmProject.aspx">Design</a></strong>
                                        <label><span class="percent">85</span>%</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="circular-bar">
                                    <div class="circular-bar-chart" data-percent="60" data-plugin-options='{ "barColor": "#000000", "delay": 600 }'>
                                        <strong><a href="frmProject.aspx">HTML</a></strong>
                                        <label><span class="percent">60</span>%</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <h2 class="short">Officer</h2>

                    <div class="progress-bars">
                        <div class="progress-label">
                            <span><a href="frmStaffProject.aspx">Project Manager</a></span>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-appear-progress-animation="40%">
                                <span class="progress-bar-tooltip">40%</span>
                            </div>
                        </div>
                        <div class="progress-label">
                            <span><a href="frmStaffProject.aspx">SA</a></span>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-appear-progress-animation="10%" data-appear-animation-delay="10%">
                                <span class="progress-bar-tooltip">10%</span>
                            </div>
                        </div>
                        <div class="progress-label">
                            <span><a href="frmStaffProject.aspx">Programer</a></span>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-appear-progress-animation="40%" data-appear-animation-delay="40%">
                                <span class="progress-bar-tooltip">40%</span>
                            </div>
                        </div>
                        <div class="progress-label">
                            <span><a href="frmStaffProject.aspx">System Engeneer</a></span>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" data-appear-progress-animation="10%" data-appear-animation-delay="10%">
                                <span class="progress-bar-tooltip">10%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="tall" />
    <div class="container">
        <div class="row">
            <h2 class="short">PO</h2>
            <div class="col-md-12">
                <div class="blog-posts">

                    <section class="timeline">
                        <div class="timeline-body">
                            <div class="timeline-date">
                                <h3>November 2016</h3>
                            </div>

                            <article class="timeline-box left post post-medium">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-image">
                                            
                                                <div>
                                                    <div class="img-thumbnail">
                                                        <a href="frmPurchaseOrder.aspx" class="thumb-info team">
                                                            <img class="img-responsive" src="img/blog/blog-image-1.jpg" alt="" />
                                                        </a>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-content">
                                            <h4><a href="frmProject.aspx">Cocomax</a></h4>
                                            <p>
                                                รหัสลูกค้า : CM      Cocomax
                                               TexID : 1390448590962
                                                55 สุขุมวิท กรุงเทพ
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-meta">
                                            <span><i class="fa fa-calendar"></i> November 5, 2016 </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-meta">
                                            <span>Invoice <i class="fa fa-tag"></i><a href="frmInvoice.aspx"> วางบิลครั้งที่ 1 </span>
                                            <span><i class="fa fa-tag"></i><a href="frmInvoice.aspx"> วางบิลครั้งที่ 2 </span>
                                            <span><i class="fa fa-tag"></i><a href="frmInvoice.aspx"> วางบิลครั้งที่ 3 </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="frmPurchaseOrder.aspx" class="btn btn-xs btn-primary pull-right">Read more...</a>
                                    </div>
                                </div>
                            </article>

                            <div class="timeline-date">
                                <h3>January 2017</h3>
                            </div>
                            <article class="timeline-box left post post-medium">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-image">
                                            
                                                <div>
                                                    <div class="img-thumbnail">
                                                        <a href="frmPurchaseOrder.aspx" class="thumb-info team">
                                                            <img class="img-responsive" src="img/blog/blog-image-1.jpg" alt="" />
                                                        </a>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-content">
                                            <h4><a href="frmProject.aspx">PTT</a></h4>
                                            <p>
                                                รหัสลูกค้า : PTT              PTT
                                               TexID : 6793348370582
                                                ชลบุรี
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-meta">
                                            <span><i class="fa fa-calendar"></i> January 5, 2017 </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-meta">
                                            <span>Invoice <i class="fa fa-tag"></i><a href="frmInvoice.aspx"> วางบิลครั้งที่ 1 </span>
                                            <span><i class="fa fa-tag"></i><a href="frmInvoice.aspx"> วางบิลครั้งที่ 2 </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="frmPurchaseOrder.aspx" class="btn btn-xs btn-primary pull-right">Read more...</a>
                                    </div>
                                </div>
                            </article>
                            <article class="timeline-box right post post-medium">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-image">
                                            
                                                <div>
                                                    <div class="img-thumbnail">
                                                        <a href="frmPurchaseOrder.aspx" class="thumb-info team">
                                                            <img class="img-responsive" src="img/blog/blog-image-1.jpg" alt="" />
                                                        </a>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-content">
                                            <h4><a href="frmProject.aspx">Starbucks</a></h4>
                                            <p>
                                                รหัสลูกค้า : SB              Starbucks
                                               TexID : 1593348590582
                                                55 ลาดพร้าว กรุงเทพ
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-meta">
                                            <span><i class="fa fa-calendar"></i> January 30, 2017 </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-meta">
                                            <span>Invoice <i class="fa fa-tag"></i><a href="frmInvoice.aspx"> วางบิลครั้งที่ 1 </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="frmPurchaseOrder.aspx" class="btn btn-xs btn-primary pull-right">Read more...</a>
                                    </div>
                                </div>
                            </article>
                            <div class="timeline-date">
                                <h3><a href="#">Load More...</a></h3>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
