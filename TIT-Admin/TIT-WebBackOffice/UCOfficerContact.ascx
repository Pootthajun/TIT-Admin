﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCOfficerContact.ascx.cs" Inherits="TIT_WebBackoffice.UCContact" %>

<div class="col-md-12">
    <asp:Label runat="server" Text="ข้อมูลติดต่อ"></asp:Label>
    <asp:Panel ID="addContact" runat="server" Visible="true">
        <asp:Label ID="lblOF_ID" runat="server" Text="0" Visible="false"></asp:Label>
        <table class="table table-padding table-bordered table-striped mb-none" id="contactzone">
            <thead>
                <tr>
                    <th class="tbl-th" width="22%">ประเภท</th>
                    <th class="tbl-th">รายละเอียด</th>
                    <th class="tbl-th" width="15%">เครื่องมือ</th>
                </tr>

            </thead>
            <tbody>
                <asp:Repeater ID="rptContact" runat="server" OnItemDataBound="rptContact_ItemDataBound" OnItemCommand="rptContact_ItemCommand">
                    <ItemTemplate>
                        <tr>
                            <td>

                                <asp:DropDownList ID="ddlConnectionList" CssClass="form-control" runat="server"></asp:DropDownList>

                            </td>
                            <td>
                                <asp:TextBox ID="txtContact" CssClass="form-control" runat="server" class="form-control"></asp:TextBox>
                                <asp:Label ID="lblOFContactID" runat="server" Text="0" Visible="false"></asp:Label>
                            </td>
                            <td class="actions th">
                                <asp:LinkButton ID="delete" CommandName="delete" runat="server" Text=" delete" CssClass="fa fa-trash-o" OnClick="delete_Click"></asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary btn-float btn-sm" Text="เพิ่ม" OnClick="btnAdd_Click" />
    </asp:Panel>
</div>
