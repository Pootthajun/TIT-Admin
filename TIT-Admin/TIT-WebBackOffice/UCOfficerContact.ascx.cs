﻿using System;
using System.Data;
using System.Web.UI.WebControls;

namespace TIT_WebBackoffice
{
    public partial class UCContact : System.Web.UI.UserControl
    {
        backofficeBL BL = new backofficeBL();
        DataTable dt = new DataTable();
        public DataTable ContactDT
        {
            //get { return GetContactDT(); }
            set
            {
                dt = value;
                SetContact(dt);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BindData()
        {
            string sql = "select c.CONTACT_ID, ct.CT_TYPE_ID, c.CONTACT, c.CT_TYPE_ID";
            sql += " from TB_OF_CONTACT as c";
            sql += " inner join TB_CT_TYPE as ct on c.CT_TYPE_ID = ct.CT_TYPE_ID";
            sql += " where c.OF_ID =" + CT_ID;
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);

            rptContact.DataSource = dt;
            rptContact.DataBind();
        }
        public string CT_ID
        {
            get { return (lblOF_ID.Attributes["OF_ID"]); }
            set { lblOF_ID.Attributes["OF_ID"] = value; }
        }
        public string OF_Contact_ID
        {
            get { return (lblOF_ID.Attributes["CONTACT_ID"]); }
            set { lblOF_ID.Attributes["CONTACT_ID"] = value; }
        }

        protected void rptContact_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            DropDownList ddlConnectionList = (DropDownList)e.Item.FindControl("ddlConnectionList");
            TextBox txtContact = (TextBox)e.Item.FindControl("txtContact");
            Label lblOFContactID = (Label)e.Item.FindControl("lblOFContactID");
            LinkButton delete = (LinkButton)e.Item.FindControl("delete");

            BL.Bind_BL_CtType(ddlConnectionList);

            lblOFContactID.Attributes["index"] = (e.Item.ItemIndex).ToString();
            if (!string.IsNullOrEmpty(dr["CT_TYPE_ID"].ToString()))
            {

                ddlConnectionList.SelectedValue = dr["CT_TYPE_ID"].ToString();
            }


            txtContact.Text = dr["CONTACT"].ToString();
            lblOFContactID.Text = dr["CONTACT_ID"].ToString();

            delete.CommandArgument = (e.Item.ItemIndex).ToString();
        }

        protected void delete_Click(object sender, EventArgs e)
        {

        }

        protected void rptContact_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            LinkButton delete = (LinkButton)e.Item.FindControl("delete");

            switch (e.CommandName)
            {
                case "delete":
                    DataTable dt = Current_Data();
                    dt.Rows.RemoveAt(Convert.ToInt32(delete.CommandArgument));

                    rptContact.DataSource = dt;
                    rptContact.DataBind();

                    break;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //Clear();
            DataTable dt = Current_Data();
            // อ่าน repeater
            dt.Rows.Add();
            rptContact.DataSource = dt;
            rptContact.DataBind();
        }
        protected DataTable Current_Data()
        {
            string sql = "select c.CONTACT_ID, ct.CT_TYPE_ID, c.CONTACT, c.CT_TYPE_ID";
            sql += " from TB_OF_CONTACT as c";
            sql += " inner join TB_CT_TYPE as ct on c.CT_TYPE_ID = ct.CT_TYPE_ID";
            sql += " where 0=1";
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            DataRow DR;
            dt.Columns.Add("index");
            foreach (RepeaterItem item in rptContact.Items)
            {
                DropDownList ddlConnectionList = (DropDownList)item.FindControl("ddlConnectionList");
                TextBox txtContact = (TextBox)item.FindControl("txtContact");
                Label lblOFContactID = (Label)item.FindControl("lblOFContactID");

                DR = dt.NewRow();
                DR["index"] = lblOFContactID.Attributes["index"];

                if (lblOFContactID.Text == "")
                {
                    DR["CONTACT_ID"] = 0;
                }
                else
                {
                    DR["CONTACT_ID"] = lblOFContactID.Text;
                }
                if (ddlConnectionList.SelectedValue != "")
                {
                    DR["CT_TYPE_ID"] = ddlConnectionList.SelectedValue;
                }
                DR["CONTACT"] = txtContact.Text;
                dt.Rows.Add(DR);

            }

            return dt;
        }

        private void SetContact(DataTable dt)
        {
     
        }
        public void Set_rptCantact(DataTable dt)
        {
            rptContact.DataSource = dt;
            rptContact.DataBind();
        }

        public DataTable GetContactDT(Repeater rpt)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CONTACT_ID");
            dt.Columns.Add("OF_ID");
            dt.Columns.Add("CT_TYPE_ID");
            dt.Columns.Add("CONTACT");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptContact.Items.Count - 1; i++)
            {
                DropDownList ddlConnectionList = (DropDownList)rptContact.FindControl("ddlConnectionList");
                TextBox txtContact = (TextBox)rptContact.FindControl("txtContact");

                dr = dt.NewRow();
                dr["CONTACT_ID"] = dt.Rows.Count + 1;
                dr["TYPE_NAME"] = ddlConnectionList.SelectedItem.Text;
                dr["CT_TYPE_ID"] = ddlConnectionList.SelectedItem;
                dr["CONTACT"] = txtContact.Text;
                dt.Rows.Add(dr);

            }

            return dt;
        }
        public DataTable Get_ContactDT_Detail(int Con_id)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CONTACT_ID");
            dt.Columns.Add("OF_ID");
            dt.Columns.Add("CT_TYPE_ID");
            dt.Columns.Add("CONTACT");

            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptContact.Items.Count - 1; i++)
            {
                DropDownList ddlConnectionList = (DropDownList)rptContact.Items[i].FindControl("ddlConnectionList");
                TextBox txtContact = (TextBox)rptContact.Items[i].FindControl("txtContact");

                dr = dt.NewRow();
                dr["CONTACT_ID"] = i + 1;
                dr["OF_ID"] = Con_id;
                dr["TYPE_NAME"] = ddlConnectionList.SelectedItem.Text;
                dr["CT_TYPE_ID"] = ddlConnectionList.SelectedItem;
                dr["CONTACT"] = txtContact.Text;
                dt.Rows.Add(dr);

            }

            return dt;
        }

        public UCContact()
        {
            
        }
    }
}