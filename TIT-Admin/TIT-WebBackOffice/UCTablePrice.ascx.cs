﻿using System;
using System.Data;
using System.Web.UI.WebControls;

namespace TIT_WebBackoffice
{
    public partial class UCTablePrice : System.Web.UI.UserControl
    {
        backofficeBL BL = new backofficeBL();

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void BindData_Contact()
        {
            string sql = "select  c.CP_ID,c.CUS_Contact_ID, c.CT_TYPE_ID, Contact_Detail, ct.TYPE_NAME ";
            sql += " from TB_CUS_CONTACT as c ";
            sql += " inner join TB_CT_TYPE as ct on c.CT_TYPE_ID = ct.CT_TYPE_ID";

            //sql += " where c.CP_ID =" + CP_ID;
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);

            rpt_Contact.DataSource = dt;
            rpt_Contact.DataBind();
        }

        protected void rpt_Contact_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            DropDownList ddlContType = (DropDownList)e.Item.FindControl("ddlContType");
            TextBox txtContact = (TextBox)e.Item.FindControl("txtContact");
            Label lblCUS_Contact_ID = (Label)e.Item.FindControl("lblCUS_Contact_ID");
            Label lblCP_ID = (Label)e.Item.FindControl("lblCP_ID");
            LinkButton delete = (LinkButton)e.Item.FindControl("delete");

            BL.Bind_BL_CtType(ddlContType);

            lblCUS_Contact_ID.Attributes["index"] = (e.Item.ItemIndex).ToString();
            if (!string.IsNullOrEmpty(dr["CT_TYPE_ID"].ToString()))
            {

                ddlContType.SelectedValue = dr["CT_TYPE_ID"].ToString();
            }


            txtContact.Text = dr["Contact_Detail"].ToString();
            lblCUS_Contact_ID.Text = dr["CUS_Contact_ID"].ToString();
            lblCP_ID.Text = dr["CP_ID"].ToString();
            delete.CommandArgument = (e.Item.ItemIndex).ToString();
        }

        protected void rpt_Contact_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            LinkButton delete = (LinkButton)e.Item.FindControl("delete");

            switch (e.CommandName)
            {
                case "delete":
                    DataTable dt = Current_Data();
                    dt.Rows.RemoveAt(Convert.ToInt32(delete.CommandArgument));

                    rpt_Contact.DataSource = dt;
                    rpt_Contact.DataBind();

                    break;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //Clear();
            DataTable dt = Current_Data();
            // อ่าน repeater
            dt.Rows.Add();
            rpt_Contact.DataSource = dt;
            rpt_Contact.DataBind();
        }
        protected DataTable Current_Data()
        {
            string sql = "select c.CP_ID,c.CUS_Contact_ID, c.CT_TYPE_ID, Contact_Detail, ct.TYPE_NAME";
            sql += " from TB_CUS_CONTACT as c ";
            sql += " inner join TB_CT_TYPE as ct on c.CT_TYPE_ID = ct.CT_TYPE_ID";

            sql += " where 0=1 ";
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            DataRow DR;
            dt.Columns.Add("index");
            foreach (RepeaterItem item in rpt_Contact.Items)
            {
                DropDownList ddlContType = (DropDownList)item.FindControl("ddlContType");
                TextBox txtContact = (TextBox)item.FindControl("txtContact");
                Label lblCUS_Contact_ID = (Label)item.FindControl("lblCUS_Contact_ID");

                DR = dt.NewRow();
                DR["index"] = lblCUS_Contact_ID.Attributes["index"];

                if (lblCUS_Contact_ID.Text == "")
                {
                    DR["CUS_Contact_ID"] = 0;
                }
                else
                {
                    DR["CUS_Contact_ID"] = lblCUS_Contact_ID.Text;
                }
                if (ddlContType.SelectedValue != "")
                {
                    DR["CT_TYPE_ID"] = ddlContType.SelectedValue;
                }
                DR["Contact_Detail"] = txtContact.Text;
                dt.Rows.Add(DR);

            }

            return dt;
        }
    }
}