﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmSettingDepartment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Department();
            }
        }

        public void Bind_Department()
        {
            string sql = "select DEPARTMENT_ID,DEPARTMENT_NAME from TB_DEPARTMENT";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            rptDepartment.DataSource = dt;
            rptDepartment.DataBind();
        }
        protected void rptDepartment_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblDepartmentName = (Label)e.Item.FindControl("lblDepartmentName");
            Label lblID = (Label)e.Item.FindControl("lblID");

            lblDepartmentName.Text = dr["DEPARTMENT_NAME"].ToString();
            lblID.Text = dr["DEPARTMENT_ID"].ToString();
        }

        protected void rptDepartment_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblDepartmentName = (Label)e.Item.FindControl("lblDepartmentName");
            Label lblID = (Label)e.Item.FindControl("lblID");

            if (e.CommandName == "edit")
            {
                lblDepartmentID.Text = lblID.Text;
                txtDepartmentName.Text = lblDepartmentName.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbDepartmentLinqDB DepartmentLnq = new TbDepartmentLinqDB();

                ExecuteDataInfo ret = DepartmentLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_Department();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                //Insert/Update Data 
                TransactionDB trans = new TransactionDB();
                TbDepartmentLinqDB departLnq = new TbDepartmentLinqDB();

                departLnq.GetDataByPK(Convert.ToInt16(lblDepartmentID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();

                //CusTypeLnq.CUS_TYPE_CODE = txtCustomerTypeCode.Text;
                departLnq.DEPARTMENT_NAME = txtDepartmentName.Text;

                if (departLnq.DEPARTMENT_ID == 0)
                {
                    ret = departLnq.InsertData("Username", trans.Trans);
                }
                else
                {
                    ret = departLnq.UpdateData("Username", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblDepartmentID.Text = departLnq.DEPARTMENT_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
            Clear();
            Bind_Department();
        }

        private void Clear()
        {
            //txtCustomerTypeCode.Text = "";
            txtDepartmentName.Text = "";
        }


        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtDepartmentName.Text))
            {
                alertmsg("กรุณาระบุแผนก");
                ret = false;
            }
            return ret;
        }

    }
}