﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmSettingCusType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_CustomerType();
            }
        }

        public void Bind_CustomerType()
        {
            string sql = "select CUS_Type_ID, CUS_Type_Code, CUS_Type_Name from TB_CUS_TYPE";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            rptCustomerType.DataSource = dt;
            rptCustomerType.DataBind();
        }
        protected void rptCustomerType_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblCustomerTypeCode = (Label)e.Item.FindControl("lblCustomerTypeCode");
            Label lblCustomerTypeName = (Label)e.Item.FindControl("lblCustomerTypeName");
            Label lblID = (Label)e.Item.FindControl("lblID");

            lblCustomerTypeCode.Text = dr["CUS_Type_Code"].ToString();
            lblCustomerTypeName.Text = dr["CUS_Type_Name"].ToString();
            lblID.Text = dr["CUS_Type_ID"].ToString();
        }

        protected void rptCustomerType_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblCustomerTypeCode = (Label)e.Item.FindControl("lblCustomerTypeCode");
            Label lblCustomerTypeName = (Label)e.Item.FindControl("lblCustomerTypeName");
            Label lblID = (Label)e.Item.FindControl("lblID");
            if (e.CommandName == "edit")
            {
                lblCusTypeID.Text = lblID.Text;
                txtCustomerTypeCode.Text = lblCustomerTypeCode.Text;
                txtCustomerTypeName.Text = lblCustomerTypeName.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbCusTypeLinqDB CusTypeLnq = new TbCusTypeLinqDB();

                ExecuteDataInfo ret = CusTypeLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_CustomerType();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                //Insert/Update Data 
                TransactionDB trans = new TransactionDB();
                TbCusTypeLinqDB CusTypeLnq = new TbCusTypeLinqDB();

                CusTypeLnq.GetDataByPK(Convert.ToInt16(lblCusTypeID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();

                CusTypeLnq.CUS_TYPE_CODE = txtCustomerTypeCode.Text;
                CusTypeLnq.CUS_TYPE_NAME = txtCustomerTypeName.Text;

                if (CusTypeLnq.CUS_TYPE_ID == 0)
                {
                    ret = CusTypeLnq.InsertData("Username", trans.Trans);
                }
                else
                {
                    ret = CusTypeLnq.UpdateData("Username", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblCusTypeID.Text = CusTypeLnq.CUS_TYPE_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
            Clear();
            Bind_CustomerType();
        }

        private void Clear()
        {
            txtCustomerTypeCode.Text = "";
            txtCustomerTypeName.Text = "";
        }
        

        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtCustomerTypeCode.Text))
            {
                alertmsg("กรุณาระบุรหัสประเภทลูกค้า");
                ret = false;
            }
            if (string.IsNullOrEmpty(txtCustomerTypeName.Text))
            {
                alertmsg("กรุณาระบุชื่อประเภทลูกค้า");
                ret = false;
            }
            return ret;
        }
    }
}