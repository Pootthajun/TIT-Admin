﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;


namespace TIT_WebBackoffice
{
    public partial class frmSettingMaritalStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind_Data();
        }

        public void Bind_Data()
        {
            string sql = "select MR_ID,MR_STATUS from TB_MARITAL_STATUS";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            rptMaritalStatus.DataSource = dt;
            rptMaritalStatus.DataBind();
        }

        protected void rptMaritalStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblMaritalStatus = (Label)e.Item.FindControl("lblMaritalStatus");

            lblMaritalStatus.Text = dr["MR_STATUS"].ToString();
        }

        protected void btnMSedit_Click(object sender, EventArgs e)
        {
            
        }

        protected void btnMSdelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnMSSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                TransactionDB trans = new TransactionDB();
                TbMaritalStatusLinqDB MaritalLnq = new TbMaritalStatusLinqDB();

                MaritalLnq.GetDataByPK(Convert.ToInt16(lblMSID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();
                MaritalLnq.MR_STATUS = txtMS.Text;
                if (MaritalLnq.MR_ID == 0)
                {
                    ret = MaritalLnq.InsertData("User", trans.Trans);
                }
                else
                {
                    ret = MaritalLnq.UpdateData("User", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblMSID.Text = MaritalLnq.MR_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
            Clear();
            Bind_Data();
        }
        private void Clear()
        {
            txtMS.Text = "";
            Bind_Data();
        }
        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtMS.Text))
            {
                alertmsg("กรุณาระบุสถานภาพสมรส");
                ret = false;
            }
            return ret;
        }

        protected void rptMaritalStatus_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblMaritalStatus = (Label)e.Item.FindControl("lblMaritalStatus");
            Label lblID = (Label)e.Item.FindControl("lblID");

            if (e.CommandName == "edit")
            {
                lblMSID.Text = lblID.Text;
                txtMS.Text = lblMaritalStatus.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbMaritalStatusLinqDB msLnq = new TbMaritalStatusLinqDB();

                ExecuteDataInfo ret = msLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_Data();
            }
        }
    }
}