﻿<%@ Page Title="" Language="C#" UnobtrusiveValidationMode="none" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmWeeklyProgress.aspx.cs" Inherits="TIT_WebBackoffice.frmWeeklyProgress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Weekly Progress | TiT</title>
    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.js"></script>
    <style>
        .lbl-control {
            text-align: right;
        }

        .dst-bottom {
            margin-bottom: 12px;
            /*ระยะห่างระหว่างตารางกับช่อง search*/
        }

        .panel-bottom {
            margin-bottom: 0px;
            /*ระยะห่างส่วนล่างของ panel */
        }

        .btn-padding {
            padding-right: 20px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="upPnl" runat="server">
            <ContentTemplate>
                <section class="page-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="breadcrumb">
                                    <li><a href="frmHome.aspx">Home</a></li>
                                    <li class="active">Weekly Progress</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h1>Weekly Progress</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="container">
                    <!--หัวตาราง ส่วนการค้นหา-->
                    <asp:Panel ID="pnlCustomerList" runat="server" Visible="True">
                        <div class="form-group dst-bottom">
                            <div class="panel panel-default dst-bottom">
                                <div class="panel-heading">
                                    <h4 class="panel-title bold"><i class="fa fa-search dst-icon bold"></i>ค้นหา Weekly Progress
                                    </h4>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <%--<label>ชื่อพนักงาน</label>--%>
                                            <asp:DropDownList ID="ddlSearch_OFName" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-3">
                                            <%--<label>ชื่อโครงการ</label>--%>
                                            <asp:DropDownList ID="ddlSearch_Project" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <div class="form-group lbl-control">     
                                            <asp:Button ID="Search" runat="server" CssClass="btn btn-primary" Text="Search.."/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <section class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-bottom">Weekly Progress</h2>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <!--ตารางแสดงWeekly Progress-->
                                    <div class="col-md-12">
                                        <table id="tbWorkProgress" class="table table-bordered table-striped mb-none">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th class="tbl-th" width="15%">Officer</th>
                                                    <th class="tbl-th" width="15%">Project</th>
                                                    <th class="tbl-th">Description</th>
                                                    <th class="tbl-th" width="8%">Plan Start</th>
                                                    <th class="tbl-th" width="8%">Plan End</th>
                                                    <th class="tbl-th" width="10%">Actual Start</th>
                                                    <th class="tbl-th" width="9%">Actual End</th>
                                                    <th class="tbl-th" width="8%">Status</th>
                                                    <th class="tbl-th" width="8%">Tool</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="WorkProgressList" runat="server" OnItemDataBound="WorkProgressList_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                               <asp:Label ID="lblWP_ID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "WP_ID") %>'></asp:Label>
                                                               <asp:Label ID="lblOfficer_Name" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OF_ID") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                               <asp:Label ID="lblProject_name" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PROJECT_ID") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                               <asp:Label ID="lbldescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DESCRIPTION") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                               <asp:Label ID="lblPlanStart" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PLAN_START") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                               <asp:Label ID="lblPlanFinish" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PLAN_FINISH") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                               <asp:Label ID="lblActualStart" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ACTUAL_START") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                               <asp:Label ID="lblActualFinish" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ACTUAL_FINISH") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                               <asp:Label ID="lblStatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "STATUS") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <a href="#collapsefive" class="on-default edit-row" data-toggle="collapse" data-parent="#accordion" aria-expanded="true"><i class="fa fa-pencil-square-o distance-icon"></i></a>
                                                                <a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </asp:Panel>
                    <div class="panel-body">
                    <button class="btn btn-primary" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="true" aria-hidden="False">CREATE</button>
                    </div>
                    <!--create-->
                            <div id="collapsefive" class="accordion-body collapse" aria-expanded="true">
                                <section class="panel panel-default panel-bottom" id="w2">
                                    <div class="panel-heading">
                                        <h4 class="panel-bottom">Create/Edit Work Progress</h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="row">
                                                 <div class="col-md-2">
                                                     <asp:Label ID="lblOfficerID" runat="server" Text="0" Visible="false"></asp:Label>
                                                     <asp:Label runat="server" Text="Officer"></asp:Label>
                                                     <asp:DropDownList ID="ddlOfficer" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <div class="col-md-2">
                                                     <asp:Label ID="lblWpID" runat="server" Text="0" Visible="false"></asp:Label>
                                                     <asp:Label runat="server" Text="Project"></asp:Label>
                                                     <asp:DropDownList ID="ddlProject" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <div class="col-md-2">
                                                    <asp:Label runat="server" Text="Site"></asp:Label>
                                                    <asp:TextBox ID="txtWorkSite" CssClass="form-control" runat="server"></asp:TextBox>
                                                    <asp:Label ID="lblWorksite" runat="server" Text="0" Visible="false"></asp:Label>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:Label runat="server" Text="Description"></asp:Label>
                                                    <asp:TextBox ID="txtDescription" Rows="3" CssClass="form-control" runat="server"></asp:TextBox>
                                                    <asp:Label ID="lblDescription" runat="server" Text="0" Visible="false"></asp:Label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <asp:Label runat="server" Text="Plan range"></asp:Label>
									                <div class="input-daterange input-group" data-plugin-datepicker="">
								                        <span class="input-group-addon">
									                        <i class="fa fa-calendar"></i>
										                </span>
                                                        <asp:TextBox ID="txtPlandStart" CssClass="form-control" runat="server"></asp:TextBox>
										                <span class="input-group-addon">to</span>
										                <asp:TextBox ID="txtPlandEnd" CssClass="form-control" runat="server"></asp:TextBox>
									                </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:Label runat="server" Text="Actual range"></asp:Label>
                                                    <div class="input-daterange input-group" data-plugin-datepicker="">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </span>
                                                        <asp:TextBox ID="txtActualStart" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <span class="input-group-addon">to</span>
                                                        <asp:TextBox ID="txtActualEnd" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <asp:Label runat="server" Text="Status"></asp:Label>
                                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <div class="col-md-3">
                                                    <asp:Label runat="server" Text="Assigned by"></asp:Label>
                                                    <asp:TextBox ID="txtAssignedBy" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:Label runat="server" Text="Detail of Progress"></asp:Label>
                                                    <asp:TextBox ID="txtDetails" Rows="3" CssClass="form-control" runat="server"></asp:TextBox>
                                                    <asp:Label ID="lblDetails" runat="server" Text="0" Visible="false"></asp:Label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <asp:Label runat="server" Text="Issue"></asp:Label>
                                                    <asp:TextBox ID="txtIssue" Rows="3" CssClass="form-control" runat="server"></asp:TextBox>
                                                    <asp:Label ID="lblIssue" runat="server" Text="0" Visible="false"></asp:Label>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:Label runat="server" Text="แนวทางการแก้ไข"></asp:Label>
                                                    <asp:TextBox ID="txtProcess" Rows="3" CssClass="form-control" runat="server"></asp:TextBox>
                                                    <asp:Label ID="lblProcess" runat="server" Text="0" Visible="false"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <asp:Label runat="server" Text="ผลการแก้ไข"></asp:Label>
                                                    <asp:TextBox ID="txtResult" Rows="3" CssClass="form-control" runat="server"></asp:TextBox>
                                                    <asp:Label ID="lblResult" runat="server" Text="0" Visible="false"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12" style="text-align: center;">
                                                    <%--<asp:Button ID="wp_save" class="btn btn-primary" runat="server" Text="Save" OnClick="wp_save_Click" />--%>
                                                    <asp:Button ID="WP_save" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="WP_save_Click" />
                                                    <a id="wp_cancle" class="btn btn-danger" href="javascript:__doPostBack('ctl00$ContentPlaceHolder1$btnaddadmin','')">CANCLE</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                    
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
</asp:Content>

