﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmSettingReligion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind_Data();
        }
        public void Bind_Data()
        {
            string sql = "select RELIGION_ID,RELIGION_NAME from TB_RELIGION";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            rptReligion.DataSource = dt;
            rptReligion.DataBind();
        }

        protected void rptReligion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblReligion = (Label)e.Item.FindControl("lblReligion");

            lblReligion.Text = dr["RELIGION_NAME"].ToString();
        }

        protected void rptReligion_ItemCommand1(object source, RepeaterCommandEventArgs e)
        {
            Label lblReligion = (Label)e.Item.FindControl("lblReligion");
            Label lblID = (Label)e.Item.FindControl("lblID");

            if (e.CommandName == "edit")
            {
                lblReligionID.Text = lblID.Text;
                txtReligion.Text = lblReligion.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbReligionLinqDB ReligionlLnq = new TbReligionLinqDB();

                ExecuteDataInfo ret = ReligionlLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_Data();
            }
        }

        protected void btnReligionSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                TransactionDB trans = new TransactionDB();
                TbReligionLinqDB ReligionlLnq = new TbReligionLinqDB();

                ReligionlLnq.GetDataByPK(Convert.ToInt16(lblReligionID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();
                ReligionlLnq.RELIGION_NAME = txtReligion.Text;
                if (ReligionlLnq.RELIGION_ID == 0)
                {
                    ret = ReligionlLnq.InsertData("User", trans.Trans);
                }
                else
                {
                    ret = ReligionlLnq.UpdateData("User", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblReligionID.Text = ReligionlLnq.RELIGION_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
            Clear();
            Bind_Data();
        }
        private void Clear()
        {
            txtReligion.Text = "";
            Bind_Data();
        }

        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtReligion.Text))
            {
                alertmsg("กรุณาระบุเชื้อชาติ");
                ret = false;
            }
            return ret;
        }
    }
}