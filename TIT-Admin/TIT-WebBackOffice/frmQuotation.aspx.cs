﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmQuotation : System.Web.UI.Page
    {
        backofficeBL BL = new backofficeBL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BL.Bind_BL_Project(ddlProjectNameSearch);
            }
        }
        public DataTable AllData
        {
            get
            {
                try
                {
                    return (DataTable)Session["ProjectPage"];
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            set { Session["ProjectPage"] = value; }
        }

        //protected void BindData()
        //{
        //    string sql = "select c.CUS_ID, CUS_Code, CUS_Name, CUS_TaxID, c.CUS_Type_ID, ct.CUS_Type_Name";
        //    sql += " from TB_QUOTATION as q ";
        //    sql += " inner join TB_PROJECT as pj on q.CUS_Type_ID = pj.CUS_Type_ID";
        //    DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
        //    rptQuotation.DataSource = dt;
        //    rptQuotation.DataBind();
        //}

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            btnCreate.Visible = false;
            if (btnCreate.Visible == false)
            {
                pnlCreate.Visible = true;
                pnlCustomerList.Visible = false;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlCreate.Visible = false;
            if (pnlCreate.Visible == false)
            {
                pnlCustomerList.Visible = true;
                //Clear();
                btnCreate.Visible = true;

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            pnlCreate.Visible = false;
            if (pnlCreate.Visible == false)
            {
                pnlCustomerList.Visible = true;
                //Clear();
                btnCreate.Visible = true;

            }
        }

        protected void rptQuotation_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }
    }
}