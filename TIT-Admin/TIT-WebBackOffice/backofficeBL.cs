﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using LinqDB.ConnectDB;
using LinqDB.TABLE;

namespace TIT_WebBackoffice
{
    public class backofficeBL
    {
        //ddl จังหวัด
        public void Bind_BL_Province(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select PROVINCE_ID,PROVINCE_NAME from TB_PROVINCE");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือกจังหวัด...", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["PROVINCE_NAME"].ToString(), dt.Rows [i]["PROVINCE_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }

        //ddl อำเภอ
        public void Bind_BL_Amphur(DropDownList ddl,String Province_ID)
        {
            SqlParameter[] pm = new SqlParameter[1];
            pm[0] = SqlDB.SetText("@_provinceID", Province_ID);
            DataTable dt = SqlDB.ExecuteTable("Select AMPHUR_ID,AMPHUR_NAME from TB_AMPHUR where PROVINCE_ID = @_provinceID", pm);

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือกอำเภอ...", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["AMPHUR_NAME"].ToString(), dt.Rows[i]["AMPHUR_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }

        //ddl ตำบล
        public void Bind_BL_Tumbol(DropDownList ddl, String Amphur_ID)
        {
            SqlParameter[] pm = new SqlParameter[1];
            pm[0] = SqlDB.SetText("@_amphurID", Amphur_ID);
            DataTable dt = SqlDB.ExecuteTable("Select TUMBOL_ID,TUMBOL_NAME from TB_TUMBOL where AMPHUR_ID = @_amphurID", pm);

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือกตำบล...", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["TUMBOL_NAME"].ToString(), dt.Rows[i]["TUMBOL_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }

        //คำนำหน้าชื่อภาษาไทย
        public void Bind_BL_ThTitle(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("select TI_NAME_TH, TI_ID from TB_TITLE");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือก..."));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["TI_NAME_TH"].ToString(), dt.Rows[i]["TI_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }

        public void Bind_BL_EnTitle(DropDownList ddl, int TI_ID)
        {
            SqlParameter[] pm = new SqlParameter[1];
        pm[0] = SqlDB.SetBigInt("@_TiID", TI_ID);
            DataTable dt = SqlDB.ExecuteTable("select TI_NAME_EN, TI_ID from TB_TITLE where TI_ID = @_TiID", pm);

        ddl.Items.Clear();
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["TI_NAME_EN"].ToString(), dt.Rows[i]["TI_ID"].ToString());
        ddl.Items.Add(Item);
            }
    ddl.SelectedIndex = 0;
        }

        //ประเภทการติดต่อ
        public void Bind_BL_CtType(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select CT_TYPE_ID, TYPE_NAME from TB_CT_TYPE");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือก",""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["TYPE_NAME"].ToString(), dt.Rows[i]["CT_TYPE_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }

        //ประเภทลูกค้า
        public void Bind_BL_CustomerType(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select CUS_Type_ID, CUS_Type_Name from TB_CUS_TYPE");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือกประเภทลูกค้า...", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["CUS_Type_Name"].ToString(), dt.Rows[i]["CUS_Type_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //แผนก
        public void Bind_BL_Department(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select DEPARTMENT_ID,DEPARTMENT_NAME from TB_DEPARTMENT");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือกแผนก...", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["DEPARTMENT_NAME"].ToString(), dt.Rows[i]["DEPARTMENT_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //ตำแหน่ง
        public void Bind_BL_Position(DropDownList ddl, int Department_ID)
        {
            SqlParameter[] pm = new SqlParameter[1];
            pm[0] = SqlDB.SetBigInt("@_DepartmentID", Department_ID);
            DataTable dt = SqlDB.ExecuteTable("select POSITION_ID,POSITION_CODE,POSITION_NAME where DEPARTMENT_ID = @_DepartmentID", pm);

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือกตำแหน่ง...", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["POSITION_NAME"].ToString(), dt.Rows[i]["POSITION_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //ประเภทความสามารถพิเศษ
        public void Bind_BL_SKillType(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select SKILL_TYPE_ID, SKILL_TYPE from TB_SKILL_TYPE");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือกประเภทความสามารถพิเศษ...", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["SKILL_TYPE"].ToString(), dt.Rows[i]["SKILL_TYPE_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //ความสามารถพิเศษ
        public void Bind_BL_MsSkill(DropDownList ddl, int SKILL_TYPE_ID)
        {
            SqlParameter[] pm = new SqlParameter[1];
            pm[0] = SqlDB.SetBigInt("@_SKILL_TYPE_ID", SKILL_TYPE_ID);
            DataTable dt = SqlDB.ExecuteTable("select MS_ID,MS_NAME where SKILL_TYPE_ID = @_SKILL_TYPE_ID", pm);

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือกความสามารถพิเศษ...", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["MS_NAME"].ToString(), dt.Rows[i]["MS_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //โครงการ
        public void Bind_BL_Project(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select PROJECT_ID, PROJ_CODE from TB_PROJECT");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือกโครงการ...", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["PROJ_CODE"].ToString(), dt.Rows[i]["PROJECT_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //officer
        public void Bind_BL_Officer(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select OF_ID, OF_NAME from TB_OFFICER");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือกพนักงาน...", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["OF_NAME"].ToString(), dt.Rows[i]["OF_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }

        internal void Bind_BL_EnTitle()
        {
            throw new NotImplementedException();
        }

        internal void Bind_BL_ThTitle(DropDownList dropDownthTI, object selectedValue)
        {
            throw new NotImplementedException();
        }

        internal void Bind_BL_Tumbol(object ddlPosition, object selectedValue)
        {
            throw new NotImplementedException();
        }
    }
}