﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;


namespace TIT_WebBackoffice
{
    public partial class frmSettingSkill : System.Web.UI.Page
    {
        backofficeBL BL = new backofficeBL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Data();
                BL.Bind_BL_SKillType(ddlSkillTypeList);
                //Bind_SkillType(ddlSkillTypeList);
            }
        }
        public void Bind_Data()
        {
            string sql = "select ms.SKILL_TYPE_ID,ms.MS_ID,ms.MS_NAME,st.SKILL_TYPE";
            sql += " from TB_MS_SKILL as ms";
            sql += " inner join TB_SKILL_TYPE as st on st.SKILL_TYPE_ID = ms.SKILL_TYPE_ID";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            rptSkills.DataSource = dt;
            rptSkills.DataBind();
        }

        protected void rptSkills_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblSkillType = (Label)e.Item.FindControl("lblSkillType");
            Label lblSkills = (Label)e.Item.FindControl("lblSkills");

            lblSkillType.Text = dr["SKILL_TYPE"].ToString();
            lblSkills.Text = dr["MS_NAME"].ToString();
        }

        protected void btnSkillsSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                TransactionDB trans = new TransactionDB();
                TbMsSkillLinqDB SkilllLnq = new TbMsSkillLinqDB();

                SkilllLnq.GetDataByPK(Convert.ToInt16(lblSkillID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();
                SkilllLnq.SKILL_TYPE_ID = Convert.ToInt64(ddlSkillTypeList.SelectedValue);
                SkilllLnq.MS_NAME = txtSkills.Text;
                if (SkilllLnq.MS_ID == 0)
                {
                    ret = SkilllLnq.InsertData("User", trans.Trans);
                }
                else
                {
                    ret = SkilllLnq.UpdateData("User", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblSkillID.Text = SkilllLnq.MS_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
                Clear();
                Bind_Data();
        }
        private void Clear()
        {
            txtSkills.Text = "";
            ddlSkillTypeList.SelectedValue = "";
            Bind_Data();
        }
        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtSkills.Text))
            {
                alertmsg("กรุณาระบุความสามารถพิเศษ");
                ret = false;
            }
            if (string.IsNullOrEmpty(ddlSkillTypeList.SelectedValue))
            {
                alertmsg("กรุณาเลือกประเภทความสามารถพิเศษ");
                ret = false;
            }
            return ret;
        }

        protected void rptSkills_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblSkills = (Label)e.Item.FindControl("lblSkills");
            Label lblSkillType = (Label)e.Item.FindControl("lblSkillType");
            Label lblID = (Label)e.Item.FindControl("lblID");
            Label lblSkillTypeID = (Label)e.Item.FindControl("lblSkillTypeID");

            if (e.CommandName == "edit")
            {
                lblSkillID.Text = lblID.Text;
                txtSkills.Text = lblSkills.Text;
                ddlSkillTypeList.SelectedValue = lblSkillTypeID.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbMsSkillLinqDB SkilllLnq = new TbMsSkillLinqDB();

                ExecuteDataInfo ret = SkilllLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_Data();
            }
        }
       
    }
}