﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmSettingSkillType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Data();
            }
        }
        public void Bind_Data()
        {
            string sql = "select SKILL_TYPE_ID, SKILL_TYPE from TB_SKILL_TYPE";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            rptSkillType.DataSource = dt;
            rptSkillType.DataBind();
        }

        protected void rptSkillType_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblSkillType = (Label)e.Item.FindControl("lblSkillType");

            lblSkillType.Text = dr["SKILL_TYPE"].ToString();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                //Insert/Update Data 
                TransactionDB trans = new TransactionDB();
                TbSkillTypeLinqDB SkillTypeLnq = new TbSkillTypeLinqDB();

                SkillTypeLnq.GetDataByPK(Convert.ToInt16(lblSkillTypeID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();

                SkillTypeLnq.SKILL_TYPE = txtSkillType.Text;

                if (SkillTypeLnq.SKILL_TYPE_ID == 0)
                {
                    ret = SkillTypeLnq.InsertData("Username", trans.Trans);
                }
                else
                {
                    ret = SkillTypeLnq.UpdateData("Username", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblSkillTypeID.Text = SkillTypeLnq.SKILL_TYPE_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
            Clear();
            Bind_Data();
        }

        private void Clear()
        {
            txtSkillType.Text = "";
            Bind_Data();
        }

        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtSkillType.Text))
            {
                alertmsg("กรุณาระบุประเภทของความสามารถพิเศษ");
                ret = false;
            }                                                                                                                          
            return ret;
        }
        
        protected void rptSkillType_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblSkillType = (Label)e.Item.FindControl("lblSkillType");
            Label lblID = (Label)e.Item.FindControl("lblID");

            if (e.CommandName == "edit")
            {
                lblSkillTypeID.Text = lblID.Text;
                txtSkillType.Text = lblSkillType.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbSkillTypeLinqDB SkillTypeLnq = new TbSkillTypeLinqDB();

                ExecuteDataInfo ret = SkillTypeLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_Data();
            }
        }
    }
}