﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCCustomerContact.ascx.cs" Inherits="TIT_WebBackoffice.UCCustomerContact" %>

<div class="col-md-12">
    <asp:Panel ID="addContact" runat="server" Visible="true">
        <asp:Label ID="lblCP_ID" runat="server" Text="0" Visible="false"></asp:Label>
        <table class="table table-padding table-bordered table-striped mb-none" id="contactzone">
            <thead>
                <tr>
                    <th class="tbl-th" width="22%">ประเภท</th>
                    <th class="tbl-th">รายละเอียด</th>
                    <th class="tbl-th" width="15%">เครื่องมือ</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rpt_Contact" runat="server" OnItemDataBound="rpt_Contact_ItemDataBound" OnItemCommand="rpt_Contact_ItemCommand">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:DropDownList ID="ddlContType" CssClass="form-control" runat="server"></asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="txtContact" CssClass="form-control" runat="server" class="form-control"></asp:TextBox>
                                <asp:Label ID="lblCUS_Contact_ID" runat="server" Text="0" Visible="false"></asp:Label>
                                 <asp:Label ID="lblCP_ID" runat="server" Text="0" Visible="false"></asp:Label>
                            </td>
                            <td class="actions th">
                                <asp:LinkButton ID="delete" CommandName="delete" runat="server" Text=" delete" CssClass="fa fa-trash-o"></asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary btn-float" Text="เพิ่ม" OnClick="btnAdd_Click" />
    </asp:Panel>
</div>
