﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmSettingEthnicity : System.Web.UI.Page
    {
        backofficeBL BL = new backofficeBL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Ethnicity();
                //BL.Bind_BL_Department(ddlDepartment);
            }
        }

        public void Bind_Ethnicity()
        {
            string sql = "select ETH_ID, ETH_NAME from TB_ETHNICITY";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            rptEthnicity.DataSource = dt;
            rptEthnicity.DataBind();
        }
        protected void rptEthnicity_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblEthnicityName = (Label)e.Item.FindControl("lblEthnicityName");
            //Label lblPositionName = (Label)e.Item.FindControl("lblPositionName");
            //Label lblDepartment = (Label)e.Item.FindControl("lblDepartment");

            lblEthnicityName.Text = dr["ETH_NAME"].ToString();
            //lblPositionName.Text = dr["POSITION_Name"].ToString();
            //lblDepartment.Text = dr["DEPARTMENT_NAME"].ToString();
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                //Insert/Update Data 
                TransactionDB trans = new TransactionDB();
                TbEthnicityLinqDB ethLnq = new TbEthnicityLinqDB();

                ethLnq.GetDataByPK(Convert.ToInt16(lblEthnicityID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();

                ethLnq.ETH_NAME = txtEthnicityName.Text;
                //PositionLnq.POSITION_NAME = txtPositionName.Text;
                //PositionLnq.DEPARTMENT_ID = Convert.ToInt64(ddlDepartment.SelectedValue);

                if (ethLnq.ETH_ID == 0)
                {
                    ret = ethLnq.InsertData("Username", trans.Trans);
                }
                else
                {
                    ret = ethLnq.UpdateData("Username", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblEthnicityID.Text = ethLnq.ETH_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
            Clear();
            Bind_Ethnicity();
        }

        private void Clear()
        {
            txtEthnicityName.Text = "";
            //txtPositionName.Text = "";
            Bind_Ethnicity();
        }


        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtEthnicityName.Text))
            {
                alertmsg("กรุณาระบุสัญชาติ");
                ret = false;
            }

            return ret;
        }

        protected void rptEthnicity_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblEthnicityName = (Label)e.Item.FindControl("lblEthnicityName");
            Label lblID = (Label)e.Item.FindControl("lblID");

            if (e.CommandName == "edit")
            {
                lblEthnicityID.Text = lblID.Text;
                txtEthnicityName.Text = lblEthnicityName.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbEthnicityLinqDB EthnicityLnq = new TbEthnicityLinqDB();

                ExecuteDataInfo ret = EthnicityLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_Ethnicity();
            }
        }
    }
}