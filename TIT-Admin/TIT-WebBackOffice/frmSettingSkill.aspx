﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmSettingSkill.aspx.cs" Inherits="TIT_WebBackoffice.frmSettingSkill" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Skill | TiT</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <section class="page-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="breadcrumb">
                                    <li><a href="#">Master</a></li>
                                    <li class="active">Skill</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h1>Skill</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="container">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-bottom">Skills List</h4>
                        </div>

                        <div class="panel-body">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped mb-none panel-bottom">
                                    <thead>
                                        <tr>
                                            <th class="tbl-th">ประเภท</th>
                                            <th class="tbl-th">ความสามารถพิเศษ</th>
                                            <th class="tbl-th" width="12%">เครื่องมือ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptSkills" runat="server" OnItemDataBound="rptSkills_ItemDataBound" OnItemCommand="rptSkills_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="actions th">
                                                        <asp:Label ID="lblID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MS_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblSkillTypeID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SKILL_TYPE_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblSkillType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SKILL_TYPE") %>'></asp:Label>
                                                    </td>
                                                    <td class="actions th">
                                                        <asp:Label ID="lblSkills" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MS_NAME") %>'></asp:Label>
                                                    </td>
                                                    <td class="actions th" align="center">
                                                        <asp:LinkButton ID="btnSkillsedit" runat="server" Text=" Edit" CssClass="fa fa-pencil-square-o" CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "MS_ID") %>'></asp:LinkButton>
                                                        <asp:LinkButton ID="btnSkillsdelete" runat="server" Text=" Delete" CssClass="fa fa-trash-o" CommandName="delete" OnClientClick='javascript:return confirm("Are you sure you want to delete?")' 
                                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "MS_ID") %>'></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>

                    <asp:Panel ID="pnlPosition" runat="server">
                        <div class="panel panel-default dst-bottom">
                            <div class="panel-heading">
                                <h4 class="panel-title"><i class="fa fa-group distance"></i>เพิ่ม / แก้ไขความสามารถพิเศษ
                                </h4>
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label>ประเภทความสามารถพิเศษ :</label>
                                        <asp:DropDownList ID="ddlSkillTypeList" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <asp:Label ID="lblSkillID" runat="server" Text="0" Visible="false"></asp:Label>
                                    <div class="col-md-4">
                                        <label>ความสามารถพิเศษ :</label>
                                        <asp:TextBox ID="txtSkills" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" align="center">
                                        <asp:Button ID="btnSkillsSave" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="btnSkillsSave_Click" />
                                        <asp:Button ID="btnSkillsCancel" runat="server" Text="CANCEL" CssClass="btn btn-danger" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>

    </form>
</asp:Content>
