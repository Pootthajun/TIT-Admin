﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmStaffProject.aspx.cs" Inherits="TIT_WebBackoffice.frmStaffProject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Staff Project | TiT</title>
    <style>
        .dst-bottom {
            margin-bottom: 12px;
            /*ระยะห่างระหว่างตารางกับช่อง search*/
        }

        .lbl-control {
            text-align: right;
        }

        .panel-bottom {
            margin-bottom: 0px;
            /*ระยะห่างส่วนล่างของ panel */
        }

        .btn-padding {
            padding-right: 20px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="page-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="frmHome.aspx">Home</a></li>
                        <li class="active">Staff-Project</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1>Staff-Project</h1>
                </div>
            </div>
        </div>
    </section>

    <%--<div class="container">
        <section class="panel panel-default panel-bottom">
            <div class="panel-heading">
                <h1 class="panel-bottom">Staff-Project</h1>
            </div>
            <div class="panel-body">
                <div class="row dst-bottom">
                    <div class="col-md-2">
                        <select class="form-control" data-plugin-multiselect id="ms_example1">
                            <option value="OF_ID">Officer</option>
                            <option value="PROJECT_ID">Project</option>
                        </select>
                    </div>

                    <div class="search">
                        <form id="searchForm" action="page-search-results.html" method="get">
                            <div class="col-md-4">
                                <div class="input-group input-search">
                                    <input type="text" class="form-control" name="q" id="q" placeholder="Enter Keywords...">
                                    <span class="input-group-btn">
                                        <button id="of_search" class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                    <thead>
                        <tr>
                            <th class="tbl-th">Customer</th>
                            <th class="tbl-th">Projet</th>
                            <th class="tbl-th">Officer</th>
                            <th class="tbl-th">Project Start</th>
                            <th class="tbl-th">เครื่องมือ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td class="actions th">
                                <a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a>
                                <a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a>
                                <a href="#Edit" class="on-default edit-row" data-toggle="collapse" data-parent="#accordion" aria-expanded="true"><i class="fa fa-pencil distance-icon"></i></a>
                                <a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </div>--%>

    <div class="container">
        <div class="form-group">
            <div class="col-md-4">
                <div class="team-item thumbnail">
                    <a href="frmStaff.aspx" class="thumb-info team">
                        <img class="img-responsive" alt="" src="img/team/team-1.jpg" />
                        <span class="thumb-info-title">
                            <span class="thumb-info-inner">John Doe</span>
                            <span class="thumb-info-type">Project Manager</span>
                        </span>
                    </a>
                    <span class="thumb-info-caption">
                        <li><a rel="tag" href="frmProject.aspx">Cocomax</a></li>
                    </span>
                    <span class="thumb-info-social-icons">
						<a data-tooltip="" data-placement="bottom" data-original-title="Facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i><span>Facebook</span></a>
						<a data-tooltip="" data-placement="bottom" data-original-title="Twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
						<a data-tooltip="" data-placement="bottom" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
					</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="team-item thumbnail">
                    <a href="frmStaff.aspx"" class="thumb-info team">
                        <img class="img-responsive" alt="" src="img/team/team-1.jpg" />
                        <span class="thumb-info-title">
                            <span class="thumb-info-inner">Jane Doe</span>
                            <span class="thumb-info-type">Project Manager</span>
                        </span>
                    </a>
                    <span class="thumb-info-caption">
                        <li><a rel="tag" href="frmProject.aspx">Starbucks</a></li>
                        <li><a rel="tag" href="frmProject.aspx">PTT</a></li>
                    </span>
                    <span class="thumb-info-social-icons">
						<a data-tooltip="" data-placement="bottom" data-original-title="Facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i><span>Facebook</span></a>
						<a data-tooltip="" data-placement="bottom" data-original-title="Twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
						<a data-tooltip="" data-placement="bottom" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
					</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="team-item thumbnail">
                    <a href="frmStaff.aspx" class="thumb-info team">
                        <img class="img-responsive" alt="" src="img/team/team-1.jpg" />
                        <span class="thumb-info-title">
                            <span class="thumb-info-inner">Ana</span>
                            <span class="thumb-info-type">SA</span>
                        </span>
                    </a>
                    <span class="thumb-info-caption">
                        <li><a rel="tag" href="frmProject.aspx">Cocomax</a></li>
                        <li><a rel="tag" href="frmProject.aspx">Starbucks</a></li>
                        <li><a rel="tag" href="frmProject.aspx">PTT</a></li>
                    </span>
                    <span class="thumb-info-social-icons">
						<a data-tooltip="" data-placement="bottom" data-original-title="Facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i><span>Facebook</span></a>
						<a data-tooltip="" data-placement="bottom" data-original-title="Twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
						<a data-tooltip="" data-placement="bottom" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
					</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4">
                <div class="team-item thumbnail">
                    <a href="frmStaff.aspx" class="thumb-info team">
                        <img class="img-responsive" alt="" src="img/team/team-1.jpg" />
                        <span class="thumb-info-title">
                            <span class="thumb-info-inner">Lexi</span>
                            <span class="thumb-info-type">Programer</span>
                        </span>
                    </a>
                    <span class="thumb-info-caption">
                        <li><a rel="tag" href="frmProject.aspx">Cocomax</a></li>
                        <li><a rel="tag" href="frmProject.aspx">PTT</a></li>
                    </span>
                    <span class="thumb-info-social-icons">
						<a data-tooltip="" data-placement="bottom" data-original-title="Facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i><span>Facebook</span></a>
						<a data-tooltip="" data-placement="bottom" data-original-title="Twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
						<a data-tooltip="" data-placement="bottom" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
					</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="team-item thumbnail">
                    <a href="frmStaff.aspx" class="thumb-info team">
                        <img class="img-responsive" alt="" src="img/team/team-1.jpg" />
                        <span class="thumb-info-title">
                            <span class="thumb-info-inner">Vince</span>
                            <span class="thumb-info-type">Programer</span>
                        </span>
                    </a>
                    <span class="thumb-info-caption">
                        <li><a rel="tag" href="frmProject.aspx">Starbucks</a></li>
                        <li><a rel="tag" href="frmProject.aspx">PTT</a></li>
                    </span>
                    <span class="thumb-info-social-icons">
						<a data-tooltip="" data-placement="bottom" data-original-title="Facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i><span>Facebook</span></a>
						<a data-tooltip="" data-placement="bottom" data-original-title="Twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
						<a data-tooltip="" data-placement="bottom" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
					</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="team-item thumbnail">
                    <a href="frmStaff.aspx" class="thumb-info team">
                        <img class="img-responsive" alt="" src="img/team/team-1.jpg" />
                        <span class="thumb-info-title">
                            <span class="thumb-info-inner">Adam</span>
                            <span class="thumb-info-type">System Engeneer</span>
                        </span>
                    </a>
                    <span class="thumb-info-caption">
                        <li>
                        <a rel="tag" href="frmProject.aspx">Cocomax</a></li>
                        <li>
                        <a rel="tag" href="frmProject.aspx">Starbucks</a></li>
                        <li>
                        <a rel="tag" href="frmProject.aspx">PTT</a></li>
                    </span>
                    <span class="thumb-info-social-icons">
						<a data-tooltip="" data-placement="bottom" data-original-title="Facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i><span>Facebook</span></a>
						<a data-tooltip="" data-placement="bottom" data-original-title="Twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
						<a data-tooltip="" data-placement="bottom" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
					</span>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
