﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmWeeklyProgress : System.Web.UI.Page
    {
        backofficeBL BL = new backofficeBL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BL.Bind_BL_Project(ddlProject);
                BL.Bind_BL_Officer(ddlOfficer);
                BL.Bind_BL_Project(ddlSearch_Project);
                BL.Bind_BL_Officer(ddlSearch_OFName);
                Bind_Status(ddlStatus);
                BindData();
                //Bind_Project(ProjectName);
            }
        }

        protected void BindData()
        {
            String sql = "select w.WP_ID, w.OF_ID, o.OF_NAME, w.PROJECT_ID, p.PROJ_NAME, w.DESCRIPTION, w.PLAN_START, w.PLAN_FINISH, w.ACTUAL_START, w.ACTUAL_FINISH, w.STATUS";
            sql += "from TB_WORK_PROGRESS as w";
            sql += "inner join TB_PROJECT as p on w.PROJECT_ID = p.PROJECT_ID";
            sql += "inner join TB_OFFICER as o on w.OF_ID = o.OF_ID";
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            WorkProgressList.DataSource = dt;
            WorkProgressList.DataBind();
        }

        public void Bind_Status(DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือก...", ""));
            ddl.Items.Add(new ListItem("Complete", ""));
            ddl.Items.Add(new ListItem("Inprogress", ""));
            ddl.SelectedIndex = 0;
        }

        protected void WorkProgressList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblWP_ID = (Label)e.Item.FindControl("lblWP_ID");
            Label lblOfficer_Name = (Label)e.Item.FindControl("lblOfficer_Name");
            Label lblProject_Name = (Label)e.Item.FindControl("lblProject_Name");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblPlanStart = (Label)e.Item.FindControl("lblPlanStart");
            Label lblPlanFinish = (Label)e.Item.FindControl("lblPlanFinish");
            Label lblActualStart = (Label)e.Item.FindControl("lblActualStart");
            Label lblActualFinish = (Label)e.Item.FindControl("lblActualFinish");
            Label lblStatus = (Label)e.Item.FindControl("lblStatus");

            lblWP_ID.Text = dr["WP_ID"].ToString();
            lblOfficer_Name.Text = dr["OF_ID"].ToString();
            lblProject_Name.Text = dr["PROJ_NAME"].ToString();
            lblDescription.Text = dr["DESCRIPTION"].ToString();
            lblPlanStart.Text = dr["PLAN_START"].ToString();
            lblPlanFinish.Text = dr["PLAN_FINISH"].ToString();
            lblActualStart.Text = dr["ACTUAL_START"].ToString();
            lblActualFinish.Text = dr["ACTUAL_FINISH"].ToString();
            lblStatus.Text = dr["STATUS"].ToString();
        }
        //        protected void OfficerList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //        {
        //            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
        //            {
        //                return;
        //            }

        //            DataRowView dr = (DataRowView)e.Item.DataItem;
        //;
        //            Label lblProject_Name = (Label)e.Item.FindControl("lblProject_Name");
        //            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
        //            Label lblPStart = (Label)e.Item.FindControl("lblPStart");
        //            Label lblPEnd = (Label)e.Item.FindControl("lblPEnd");

        //            lblProject_Name.Text = dr["PROJ_NAME"].ToString();
        //            lblDescription.Text = dr["DESCRIPTION"].ToString();
        //            lblPStart.Text = dr["ACTUAL_START"].ToString();
        //            lblPEnd.Text = dr["ACTUAL_FINISH"].ToString();
        //        }
        //Project
        //public void Bind_Project(DropDownList ddl)
        //{
        //    DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select PROJECT_ID, PROJ_NAME from TB_PROJECT");

        //    ddl.Items.Clear();
        //    ddl.Items.Add(new ListItem("กรุณาเลือก...", ""));
        //    for (int i = 0; i <= dt.Rows.Count - 1; i++)
        //    {
        //        ListItem Item = new ListItem(dt.Rows[i]["PROJ_Name"].ToString(), dt.Rows[i]["PROJ_Name"].ToString());
        //        ddl.Items.Add(Item);
        //    }
        //    ddl.SelectedIndex = 0;
        //}


        protected void WP_save_Click(object sender, EventArgs e)
        {
            TransactionDB trans = new TransactionDB();
            TbWorkProgressLinqDB WorkprogressLnq = new TbWorkProgressLinqDB();

            WorkprogressLnq.GetDataByPK(Convert.ToInt16(lblWpID.Text), trans.Trans);

            WorkprogressLnq.PROJECT_ID = Convert.ToInt64(ddlProject.SelectedValue);
            WorkprogressLnq.SITE = txtWorkSite.Text;
            WorkprogressLnq.DESCRIPTION = txtDescription.Text;
            WorkprogressLnq.PLAN_START = Convert.ToDateTime(txtPlandStart.Text);
            WorkprogressLnq.PLAN_FINISH = Convert.ToDateTime(txtPlandEnd.Text);
            WorkprogressLnq.ACTUAL_START = Convert.ToDateTime(txtActualStart.Text);
            WorkprogressLnq.ACTUAL_FINISH = Convert.ToDateTime(txtActualEnd.Text);
            WorkprogressLnq.STATUS = ddlStatus.SelectedValue;
            WorkprogressLnq.ASSIGNED_BY = txtAssignedBy.Text;
            WorkprogressLnq.DETAIL_OF_PROGRESS = txtDetails.Text;
            WorkprogressLnq.ISSUE = txtIssue.Text;
            WorkprogressLnq.PROCESS = txtProcess.Text;
            WorkprogressLnq.RESULT = txtResult.Text;

            ExecuteDataInfo ret = new ExecuteDataInfo();
            if (WorkprogressLnq.WP_ID == 0)
            {
                ret = WorkprogressLnq.InsertData("User", trans.Trans);
            }
            else
            {
                ret = WorkprogressLnq.UpdateData("User", trans.Trans);
            }
            if (ret.IsSuccess == true)
            {
                //lblWorksite.Text = WorkprogressLnq.WP_ID.ToString();
                //lblPlandStart.Text = WorkprogressLnq.WP_ID.ToString();
                //lblDescription.Text = WorkprogressLnq.WP_ID.ToString();
                //lblDetails.Text = WorkprogressLnq.WP_ID.ToString();
                //lblIssue.Text = WorkprogressLnq.WP_ID.ToString();
                //lblProcess.Text = WorkprogressLnq.WP_ID.ToString();
                //lblResult.Text = WorkprogressLnq.WP_ID.ToString();
                lblWpID.Text = WorkprogressLnq.WP_ID.ToString();
                trans.CommitTransaction();
            }
            else
            {
                trans.RollbackTransaction();
            }
            //Clear();
            //BindData();
        }

     
    }
}