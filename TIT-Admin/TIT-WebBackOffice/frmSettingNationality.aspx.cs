﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmSettingNationality : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind_Data();
        }

        public void Bind_Data()
        {
            string sql = "select NAT_ID,NAT_NAME from TB_NATIONALITY";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            rptNationality.DataSource = dt;
            rptNationality.DataBind();
        }

        protected void rptNationality_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblNationality = (Label)e.Item.FindControl("lblNationality");

            lblNationality.Text = dr["NAT_NAME"].ToString();
        }

        protected void btnNatSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                TransactionDB trans = new TransactionDB();
                TbNationalityLinqDB NationalLnq = new TbNationalityLinqDB();

                NationalLnq.GetDataByPK(Convert.ToInt16(lblNationalityID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();
                NationalLnq.NAT_NAME = txtNat.Text;
                if (NationalLnq.NAT_ID == 0)
                {
                    ret = NationalLnq.InsertData("User", trans.Trans);
                }
                else
                {
                    ret = NationalLnq.UpdateData("User", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblNationalityID.Text = NationalLnq.NAT_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
            Clear();
            Bind_Data();
        }
        private void Clear()
        {
            txtNat.Text = "";
            Bind_Data();
        }
        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtNat.Text))
            {
                alertmsg("กรุณาระบุสัญชาติ");
                ret = false;
            }
            return ret;
        }

        protected void rptNationality_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblNationality = (Label)e.Item.FindControl("lblNationality");
            Label lblID = (Label)e.Item.FindControl("lblID");

            if (e.CommandName == "edit")
            {
                lblNationalityID.Text = lblID.Text;
                txtNat.Text = lblNationality.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbNationalityLinqDB NationalLnq = new TbNationalityLinqDB();

                ExecuteDataInfo ret = NationalLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_Data();
            }
        }
    }
}