﻿using System;
using System.Web.UI.WebControls;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class UCCustomerContact : System.Web.UI.UserControl
    {
        backofficeBL BL = new backofficeBL();
        DataTable dt = new DataTable();
        public DataTable ContactDT
        {
            get { return GetContactDT(rpt_Contact); }
            set
            {
                dt = value;
                rpt_Contact.DataSource = dt;
                rpt_Contact.DataBind();
                //SetContact(dt);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //CP_ID = "1";
                BindData_Contact(null);
            }
        }

        
        public string CP_ID
        {
            get { return (lblCP_ID.Attributes["CP_ID"]); }
            set { lblCP_ID.Attributes["CP_ID"] = value; }
        }

        public string CUS_Contact_ID
        {
            get { return (lblCP_ID.Attributes["CUS_Contact_ID"]); }
            set { lblCP_ID.Attributes["CUS_Contact_ID"] = value; }
        }

        public DataTable  DT_CUS_Contact_ID
        {
            get { return ((DataTable)ViewState["CUS_Contact_DataTable"]); }
            set { ViewState ["CUS_Contact_DataTable"] = value; }
        }
        public string test_1 = "";

        public void BindData_Contact(String CP_ID)
        {
            string sql = "select DISTINCT c.CP_ID,c.CUS_Contact_ID, c.CT_TYPE_ID, Contact_Detail";
            sql += " from TB_CUS_CONTACT as c ";
            sql += " where c.CP_ID =" + CP_ID;
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);

            rpt_Contact.DataSource = dt;
            rpt_Contact.DataBind();
        }

        protected void rpt_Contact_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            DropDownList ddlContType = (DropDownList)e.Item.FindControl("ddlContType");
            TextBox txtContact = (TextBox)e.Item.FindControl("txtContact");
            Label lblCUS_Contact_ID = (Label)e.Item.FindControl("lblCUS_Contact_ID");
            Label lblCP_ID = (Label)e.Item.FindControl("lblCP_ID");
            LinkButton delete = (LinkButton)e.Item.FindControl("delete");

            BL.Bind_BL_CtType(ddlContType);

            lblCUS_Contact_ID.Attributes["index"] = (e.Item.ItemIndex).ToString();
            if (!string.IsNullOrEmpty(dr["CT_TYPE_ID"].ToString()))
            {

                ddlContType.SelectedValue = dr["CT_TYPE_ID"].ToString();
            }


            txtContact.Text = dr["Contact_Detail"].ToString();
            lblCUS_Contact_ID.Text = dr["CUS_Contact_ID"].ToString();
            lblCP_ID.Text = dr["CP_ID"].ToString();
            delete.CommandArgument = (e.Item.ItemIndex).ToString();
        }

        protected void rpt_Contact_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            LinkButton delete = (LinkButton)e.Item.FindControl("delete");

            switch (e.CommandName)
            {
                case "delete":
                    DataTable dt = Current_Data();
                    dt.Rows.RemoveAt(Convert.ToInt32(delete.CommandArgument));

                    rpt_Contact.DataSource = dt;
                    rpt_Contact.DataBind();

                    break;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //Clear();
            DataTable dt = Current_Data();
            // อ่าน repeater
            dt.Rows.Add();
            rpt_Contact.DataSource = dt;
            rpt_Contact.DataBind();
        }

        //protected void delete_Click(object sender, EventArgs e)
        //{
            
        //}

        protected DataTable Current_Data()
        {
            string sql = "select c.CP_ID,c.CUS_Contact_ID, c.CT_TYPE_ID, Contact_Detail, ct.TYPE_NAME";
            sql += " from TB_CUS_CONTACT as c ";
            sql += " inner join TB_CT_TYPE as ct on c.CT_TYPE_ID = ct.CT_TYPE_ID";

            sql += " where 0=1 ";
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            DataRow DR;
            dt.Columns.Add("index");
            foreach (RepeaterItem item in rpt_Contact.Items)
            {
                DropDownList ddlContType = (DropDownList)item.FindControl("ddlContType");
                TextBox txtContact = (TextBox)item.FindControl("txtContact");
                Label lblCUS_Contact_ID = (Label)item.FindControl("lblCUS_Contact_ID");

                DR = dt.NewRow();
                DR["index"] = lblCUS_Contact_ID.Attributes["index"];

                if (lblCUS_Contact_ID.Text == "")
                {
                    DR["CUS_Contact_ID"] = 0;
                }
                else
                {
                    DR["CUS_Contact_ID"] = lblCUS_Contact_ID.Text;
                }
                if (ddlContType.SelectedValue != "")
                {
                    DR["CT_TYPE_ID"] = ddlContType.SelectedValue;
                }
                DR["Contact_Detail"] = txtContact.Text;
                dt.Rows.Add(DR);

            }
            
            return dt;
        }

        public void Set_Rpt_Customers(DataTable dt)
        {
            rpt_Contact.DataSource = dt;
            rpt_Contact.DataBind();
        }

        public DataTable dt_test;

        public  DataTable GetContactDT(Repeater rpt)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CUS_Contact_ID");
            dt.Columns.Add("CT_TYPE_ID");
            dt.Columns.Add("Contact_Detail");
            dt.Columns.Add("TYPE_NAME");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rpt_Contact.Items.Count - 1; i++)
            {
                DropDownList ddlCountry = (DropDownList)rpt_Contact.Items[i].FindControl("ddlContType");
                TextBox txtContact = (TextBox)rpt_Contact.Items[i].FindControl("txtContact");

                dr = dt.NewRow();
                dr["CUS_Contact_ID"] = dt.Rows.Count + 1;
                dr["CT_TYPE_ID"] = ddlCountry.SelectedValue;
                dr["Contact_Detail"] = txtContact.Text;
                dr["TYPE_NAME"] = ddlCountry.SelectedItem.Text;
                dt.Rows.Add(dr);

            }

            return dt;
        }



        public DataTable Get_ContactDT()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CUS_Contact_ID");
            dt.Columns.Add("CT_TYPE_ID");
            dt.Columns.Add("Contact_Detail");
            dt.Columns.Add("TYPE_NAME");

            DataRow dr = default(DataRow);
            for (int i = 0; i <= rpt_Contact.Items.Count - 1; i++)
            {
                DropDownList ddlCountry = (DropDownList)rpt_Contact.Items[i].FindControl("ddlContType");
                TextBox txtContact = (TextBox)rpt_Contact.Items[i].FindControl("txtContact");
                 
                dr = dt.NewRow();
                dr["CUS_Contact_ID"] = i + 1;
                dr["CT_TYPE_ID"] = ddlCountry.SelectedValue;
                dr["Contact_Detail"] = txtContact.Text;
                dr["TYPE_NAME"] = ddlCountry.SelectedItem.Text;
                dt.Rows.Add(dr);

            }

            //foreach (Repeater item in rpt_Contact.Items)
            //{
            //    TextBox txtContact = (TextBox)item.FindControl("txtContact");
            //    DropDownList ddlCountry = (DropDownList)item.FindControl("ddlContType");
            //    dr = dt.NewRow();
            //    dr["CT_TYPE_ID"] = ddlCountry.SelectedValue;
            //    dr["Contact_Detail"] = txtContact.Text;
            //    dt.Rows.Add(dr);
            //}



            return dt;
        }


        public DataTable Get_ContactDT_Detail(int Con_id)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CUS_Contact_ID");
            dt.Columns.Add("CP_ID");
            dt.Columns.Add("CT_TYPE_ID");
            dt.Columns.Add("Contact_Detail");
            dt.Columns.Add("TYPE_NAME");

            DataRow dr = default(DataRow);
            for (int i = 0; i <= rpt_Contact.Items.Count - 1; i++)
            {
                DropDownList ddlCountry = (DropDownList)rpt_Contact.Items[i].FindControl("ddlContType");
                TextBox txtContact = (TextBox)rpt_Contact.Items[i].FindControl("txtContact");

                dr = dt.NewRow();
                dr["CUS_Contact_ID"] = i+1;
                dr["CP_ID"] = Con_id;
                dr["CT_TYPE_ID"] = ddlCountry.SelectedValue;
                dr["Contact_Detail"] = txtContact.Text;
                dr["TYPE_NAME"] = ddlCountry.SelectedItem.Text;
                dt.Rows.Add(dr);

            }

            return dt;
        }
        public UCCustomerContact()
        {

        }
    }
}