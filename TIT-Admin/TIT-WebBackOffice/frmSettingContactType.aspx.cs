﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmSettingContactType : System.Web.UI.Page
    {
        backofficeBL BL = new backofficeBL();
        protected void Page_Load(object sender, EventArgs e)
        {
                Bind_ContactType();
                //BL.Bind_BL_Department(ddlDepartment);
        }

        public void Bind_ContactType()
        {
            string sql = "select CT_TYPE_ID, TYPE_NAME from TB_CT_TYPE";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);

            rptContactType.DataSource = dt;

            rptContactType.DataBind();
        }
        protected void rptContactType_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblContactTypeName = (Label)e.Item.FindControl("lblContactTypeName");
            Label lblID = (Label)e.Item.FindControl("lblID");

            lblContactTypeName.Text = dr["TYPE_NAME"].ToString();
            lblID.Text = dr["CT_TYPE_ID"].ToString();
        }

        protected void rptContactType_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblContactTypeName = (Label)e.Item.FindControl("lblContactTypeName");
            Label lblID = (Label)e.Item.FindControl("lblID");
            if (e.CommandName == "edit")
            {
                lblContactTypeID.Text = lblID.Text;
                txtContactTypeName.Text = lblContactTypeName.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbCtTypeLinqDB CtTypeLnq = new TbCtTypeLinqDB();

                ExecuteDataInfo ret = CtTypeLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_ContactType(); 
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                //Insert/Update Data 
                TransactionDB trans = new TransactionDB();
                TbCtTypeLinqDB ContTypeLnq = new TbCtTypeLinqDB();

                ContTypeLnq.GetDataByPK(Convert.ToInt16(lblContactTypeID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();

                ContTypeLnq.TYPE_NAME = txtContactTypeName.Text;

                if (ContTypeLnq.CT_TYPE_ID == 0)
                {
                    ret = ContTypeLnq.InsertData("Username", trans.Trans);
                }
                else
                {
                    ret = ContTypeLnq.UpdateData("Username", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblContactTypeID.Text = ContTypeLnq.CT_TYPE_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
            Clear();
            Bind_ContactType();
        }

        private void Clear()
        {
           txtContactTypeName.Text = "";
            Bind_ContactType();
        }


        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtContactTypeName.Text))
            {
                alertmsg("กรุณาระบุประเภทของการติดต่อ");
                ret = false;
            }
            return ret;
        }

        protected void edit_Click(object sender, EventArgs e)
        {
            //if (txtPositionCode.Text != "" && txtPositionName.Text != "")
            //{
            //    //string sql = "UPDATE TB_CUS_TYPE SET CUS_Type_Code = , CUS_Type_Name= WHERE CUS_Type_ID =";

            //}
            //else
            //{
            //    alertmsg("Please Select Record to Update");
            //}
        }

        protected void delete_Click(object sender, EventArgs e)
        {        
            //TransactionDB trans = new TransactionDB();
            //TbCusTypeLinqDB CusTypeLnq = new TbCusTypeLinqDB();
            //ExecuteDataInfo ret = CusTypeLnq.DeleteByPK(20, trans.Trans);

            //if (ret.IsSuccess == true)
            //{
            //    trans.CommitTransaction();
            //}
            //else
            //{
            //    trans.RollbackTransaction();
            //}
            //Bind_CustomerType();
        }

    }
}