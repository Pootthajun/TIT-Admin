﻿<%@ Page Title="" Language="C#" UnobtrusiveValidationMode="none" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmStaff.aspx.cs" Inherits="TIT_WebBackoffice.frmStaff" %>

<%@ Register Src="~/UCOfficerContact.ascx" TagPrefix="uc1" TagName="UCContact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Staff | TiT</title>
    <style>
        .dst-bottom {
            margin-bottom: 12px;
            /*ระยะห่างระหว่างตารางกับช่อง search*/
        }

        .panel-bottom {
            margin-bottom: 0px;
            /*ระยะห่างส่วนล่างของ panel */
        }

        .tbl-th {
            text-align: center;
            /*อักษรหัวตารางอยู่ตรงกลาง*/
        }

        .table-padding {
            margin-bottom: 0px;
        }

        .btn-float {
            float: right;
        }

        .lbl-control {
            text-align: right;
        }

        .btn-padding {
            padding-right: 20px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <section class="page-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="frmHome.aspx">Home</a></li>
                            <li class="active">Officer</li>

                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>Officer</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <section class="panel panel-default panel-bottom">
                <div class="panel-heading">
                    <h1 class="panel-bottom">Officer List</h1>
                </div>
                <div class="panel-body">
                    <div class="row dst-bottom">
                        <div class="col-md-2">
                            <select class="form-control" data-plugin-multiselect="" id="Searchlist">
                                <option value="OF_ID">รหัสพนักงาน</option>
                                <option value="OF_Name">ชื่อพนักงาน</option>
                                <option value="OF_Type_ID">ตำแหน่ง</option>
                            </select>
                        </div>

                        <div class="search">
                            <div class="col-md-4">
                                <div class="input-group input-search">
                                    <input type="text" class="form-control" name="q" id="q" placeholder="Enter Keywords..." />
                                    <span class="input-group-btn">
                                        <button id="of_search" class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <table id="tbCustomer" class="table table-bordered table-striped mb-none">
                            <thead>
                                <tr class="bg-gray">
                                    <th class="tbl-th" width="7%">รหัสพนักงาน</th>
                                    <th class="tbl-th" width="25%">ชื่อ</th>
                                    <th class="tbl-th" width="20%">ตำแหน่ง</th>
                                    <th class="tbl-th" width="12%">เครื่องมือ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="OfficerList" runat="server" OnItemDataBound="OfficerList_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblOfficer_ID" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOfficer_Name" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOfficer_Position" runat="server"></asp:Label>

                                            </td>
                                            <td>
                                                <a href="#collapsefive_edit" data-toggle="collapse" data-parent="#accordion" aria-expanded="true"><i class="fa fa-pencil-square-o distance-icon"></i></a>
                                                <a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>

        </div>

        <!--Hide adn seek-->
        <div class=" container ">
            <div class="panel-body">
                <h4 class="panel-title">
                    <asp:Button ID="btnCreate" runat="server" CssClass="btn btn-primary" Text="CREATE" OnClick="btnCreate_Click" />
                </h4>
            </div>
            <!--ข้อมูลส่วนตัว-->
            <panel id="pnlCreate" runat="server" visible="False">
                        <div class="form-group dst-bottom">
                            <div class="panel panel-default dst-bottom">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-user dst-icon"></i>ข้อมูลพนักงาน</h4>
                                </div>
                                <div class="feature-box secundary">
                                    <div class="panel-body">
                                        <!--Tab 1-->
                                        <!--ส่วนกรอกข้อมูล-->
                                        <!--รูปภาพ-->
                                        <div class="form-group">
                                            <asp:Label ID="lblOffficerID" runat="server" Text="0" Visible="false"></asp:Label>
                                            <div class="owl-item active" style="width: 250px; margin-right: 0px; float: right">
                                                <div class="thumbnail">
                                                      <asp:Image runat="server" id="image" style="width:150px; height :180px;"/>
                                                     <br />                
                                                  <asp:FileUpload runat="server" ID="fileupload" />
                                                 <asp:Button ID="btnCanclePic" runat="server" Text="Cancle" CssClass="btn btn-danger btn-float btn-sm" onclick="btnCancelPic_Click" />
                                                     <asp:Button ID="btnAddPic" CssClass="btn btn-primary btn-float btn-sm" runat="server" Text="upload" onclick="btnAddPic_Click"  />
                                              
                                                </div>
                                            </div>
                                        </div>
                                        <%--<div class="form-group">
                                            <asp:Label ID="lblOffficerID" runat="server" Text="0" Visible="false"></asp:Label>
                                            <label class="col-md-2 lbl-control ">Photo Upload :</label>
                                            <div class="col-md-4">
                                                <asp:FileUpload ID="Uploadpic" runat="server" />
                                            </div>
                                        </div>--%>
                                        <!--คำนำหน้า ชื่อ นามสกุล ชื่อเล่น TH -->
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <asp:UpdatePanel ID="Panel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="คำนำหน้า"></asp:Label>
                                                        <asp:DropDownList ID="DropDownthTI" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownthTI_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="col-md-4">
                                                <asp:Label runat="server" Text="ชื่อ"></asp:Label>
                                                <asp:TextBox ID="of_th_name" runat="server" CssClass="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequieredName" runat="server" ControlToValidate="of_th_name" Text="กรุณากรอกชื่อ" ForeColor="Red" SetFocusOnError="True" ErrorMessage="กรุณากรอกชื่อ"></asp:RequiredFieldValidator>--%>
                                                <%--<asp:ValidationSummary ID="nameError" runat="server" ShowMessageBox="true" ShowSummary="false" />--%>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:Label runat="server" Text="นามสกุล"></asp:Label>
                                                <asp:TextBox ID="of_th_surname" runat="server" CssClass="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredSurname" runat="server" ControlToValidate="of_th_surname" Text="กรุณากรอกนามสกุล" ForeColor="Red" SetFocusOnError="True" ErrorMessage="กรุณากรอกนามสกุล"></asp:RequiredFieldValidator>--%>
                                            </div>
                                            <div class="col-md-2 ">
                                                <asp:Label runat="server" Text="ชื่อเล่น"></asp:Label>
                                                <asp:TextBox ID="of_th_nickname" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!--คำนำหน้า ชื่อ นามสกุล ชื่อเล่น ENG -->
                                        <div class="form-group">
                                            <div class="col-md-3 spinner">
                                                <asp:Label runat="server" Text="Title"></asp:Label>
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlEnTitle" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownthTI_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label runat="server" Text="Name"></asp:Label>
                                                <asp:TextBox ID="of_en_name" runat="server" class="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="Requiredname2" runat="server" ControlToValidate="of_en_surname" Text="Please enter your first name" ForeColor="Red" SetFocusOnError="True" ErrorMessage="Please enter your first name"></asp:RequiredFieldValidator>--%>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:Label runat="server" Text="Surname"></asp:Label>
                                                <asp:TextBox ID="of_en_surname" runat="server" class="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="Requiredsurname2" runat="server" ControlToValidate="of_en_surname" Text="Please enter your surname" ForeColor="Red" SetFocusOnError="True" ErrorMessage="Please enter your surname"></asp:RequiredFieldValidator>--%>
                                            </div>
                                            <div class="col-md-2 ">
                                                <asp:Label runat="server" Text="Nickname"></asp:Label>
                                                <asp:TextBox ID="of_en_nickname" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!--ตำแหน่อง เงินเดือน -->
                                        <div class="form-group">
                                            <div id="Department" class="col-md-3">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="แผนก"></asp:Label>
                                                        <asp:DropDownList ID="ddlDepartment" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div id="jobtype" class="col-md-3">
                                                <asp:UpdatePanel ID="pnlJoblist" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="ตำแหน่ง"></asp:Label>
                                                        <asp:DropDownList ID="JobList" CssClass="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:Label runat="server" Text="เงินเดือน"></asp:Label>
                                                <asp:TextBox ID="of_salary" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:UpdatePanel ID="pnlOfficerstatus" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="สถานะ"></asp:Label>
                                                        <asp:DropDownList ID="OfficerStatus" CssClass="form-control" runat="server">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                        <!--ข้อมูลส่วนตัว-->
                                        <div class="form-group">
                                            <div id="sex" class="col-md-2">
                                                <asp:UpdatePanel ID="pnlSexlist" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="เพศ"></asp:Label>
                                                        <asp:DropDownList ID="SexList" CssClass="form-control" runat="server">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div id="blood" class="col-md-2">
                                                <asp:UpdatePanel ID="pnlBloodtype" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="กรุ๊ปเลือด"></asp:Label>
                                                        <asp:DropDownList ID="BloodtypeList" CssClass="form-control" runat="server">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div id="nationality" class="col-md-2">
                                                <asp:UpdatePanel ID="pnlNationalityList" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="สัญชาติ"></asp:Label>
                                                        <asp:DropDownList ID="NationalityList" CssClass="form-control" runat="server">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div id="ethnicity" class="col-md-2">
                                                <asp:UpdatePanel ID="pnlEthnicity" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="เชื้อชาติ"></asp:Label>
                                                        <asp:DropDownList ID="EthnicityList" CssClass="form-control" runat="server">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div id="religion" class="col-md-2">
                                                <asp:UpdatePanel ID="pnlReligionlist" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="ศาสนา"></asp:Label>
                                                        <asp:DropDownList ID="ReligionList" CssClass="form-control" runat="server">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:Label runat="server" Text="วันเกิด"></asp:Label>
                                                <div class="input-daterange input-group" data-plugin-datepicker="">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </span>
                                                        <asp:TextBox ID="OF_Brith" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <asp:Label runat="server" Text="รหัสประจำตัวประชาชน"></asp:Label>
                                                <asp:TextBox runat="server" CssClass="form-control" ID="of_idc" MaxLength="13" MinLength="13"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="Requiredidc" runat="server" ControlToValidate="of_idc" Text="กรุณากรอกรหัสประจำตัวประชาชน" ForeColor="Red" SetFocusOnError="True" ErrorMessage="กรุณากรอกรหัสประจำตัวประชาชน"></asp:RequiredFieldValidator>--%>
                                            </div>
                                            <div id="marital" class="col-md-2">
                                                <asp:UpdatePanel ID="pnlMaritallist" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="สถานภาพสมรส"></asp:Label>
                                                        <asp:DropDownList ID="MaritalList" CssClass="form-control" runat="server">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div id="military" class="col-md-3">
                                                <asp:UpdatePanel ID="pnlMilitarylist" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="สถานภาพทางทหาร"></asp:Label>
                                                        <asp:DropDownList ID="MilitaryList" CssClass="form-control" runat="server">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:Label runat="server" Text="น้ำหนัก"></asp:Label>
                                                <asp:TextBox ID="weight" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:Label runat="server" Text="ส่วนสูง"></asp:Label>
                                                <asp:TextBox ID="height" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--ข้อมูลที่อยู่-->
                        <div class="form-group dst-bottom">
                            <div class="panel panel-default dst-bottom">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-home dst-icon"></i>ข้อมูลที่อยู่</h4>
                                </div>
                                <div class="feature-box secundary">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-11">
                                                    <asp:Label runat="server" Text="ที่อยู่ตามทะเบียนบ้าน"></asp:Label>
                                                    <asp:TextBox ID="Homeaddress" CssClass="form-control" runat="server" class="form-control" placeholder="ที่อยู่"></asp:TextBox>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <asp:UpdatePanel ID="pnlProvincelist" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="จังหวัด"></asp:Label>
                                                        <asp:DropDownList ID="ProvinceList" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ProvinceList_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:UpdatePanel ID="pnlAmphurlist" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="อำเภอ"></asp:Label>
                                                        <asp:DropDownList ID="AmphurList" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="AmphurList_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:UpdatePanel ID="pnlTumbollist" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="ตำบล"></asp:Label>
                                                        <asp:DropDownList ID="Tumbol" CssClass="form-control" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:Label runat="server" Text="รหัสไปรษณีย์"></asp:Label>
                                                <asp:TextBox ID="zipcode" CssClass="form-control" runat="server" class="form-control" MaxLength="5" MinLength="5"></asp:TextBox>
                                            </div>
                                        </div>

                                        <hr class="tall" />

                                        <div class="form-group">
                                            <div class="col-md-11">
                                                <asp:Label runat="server" Text="ที่อยุ่ปัจจุบัน"></asp:Label>
                                                <asp:TextBox ID="Mailingaddress" CssClass="form-control" runat="server" class="form-control" placeholder="ที่อยู่"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div id="mailingprovince" class="col-md-3">
                                            <asp:UpdatePanel ID="pnlMailingprovince" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label runat="server" Text="จังหวัด"></asp:Label>
                                                    <asp:DropDownList ID="ProvinceList5" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ProvinceList5_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            </div>
                                            <div id="mailingamphur" class="col-md-3">
                                                <asp:UpdatePanel ID="pnlMailingamphurlist" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="อำเภอ"></asp:Label>
                                                        <asp:DropDownList ID="AmphurList4" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="AmphurList4_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div id="mailingtumbol" class="col-md-3">
                                                <asp:UpdatePanel ID="pnlMailingTumbol" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" Text="ตำบล"></asp:Label>
                                                        <asp:DropDownList ID="tumbolList3" CssClass="form-control" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:Label runat="server" Text="รหัสไปรษณีย์"></asp:Label>
                                                <asp:TextBox ID="mailingzipcode" CssClass="form-control" runat="server" class="form-control" MaxLength="5" MinLength="5"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!--ช่องทางติดต่อ-->
                                        <asp:Panel ID="addContact" runat="server" Visible="true">
                                            <asp:UpdatePanel ID="pnlrptContact" runat="server">
                                                <ContentTemplate>
                                            <asp:Label ID="lblContID" runat="server" Text="0" Visible="false"></asp:Label>
                                            <asp:Label ID="lblOF_ID" runat="server" Text="0" Visible="false"></asp:Label>
                                            <div class="form-group">
                                                <asp:Label ID="lblContactID" runat="server" Text="0" Visible="false"></asp:Label>
                                                <div class="col-md-8">
                                                    <asp:Label runat="server" Text="ข้อมูลการติดต่อ"></asp:Label>
                                                    <table class="table table-padding table-bordered table-striped mb-none" id="contactzone">
                                                        <thead>
                                                            <tr>
                                                                <th class="tbl-th" width="22%">ประเภท</th>
                                                                <th class="tbl-th">รายละเอียด</th>
                                                                <th class="tbl-th" width="15%">เครื่องมือ</th>
                                                            </tr>

                                                        </thead>
                                                        <tbody>
                                                            <asp:Repeater ID="rptContact" runat="server" OnItemDataBound="rptCantact_ItemDataBound" OnItemCommand="rptCantact_ItemCommand">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="pnlConnectionlist" runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:DropDownList ID="ddlConnectionList" CssClass="form-control" runat="server"></asp:DropDownList>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtContact" CssClass="form-control" runat="server" class="form-control"></asp:TextBox>
                                                                            <asp:Label ID="lblOFContactID" runat="server" Text="0" Visible="false"></asp:Label>
                                                                        </td>
                                                                        <td class="actions th">
                                                                            <asp:LinkButton ID="delete" CommandName="delete" runat="server" Text=" delete" CssClass="fa fa-trash-o" OnClick="delete_Click"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tbody>
                                                    </table>
                                                    <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary btn-float btn-sm" Text="เพิ่ม" OnClick="btnAdd_Click" />
                                                </div>
                                            </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--ข้อมูลครอบครัว-->
                        <div class="form-group dst-bottom">
                            <div class="panel panel-default dst-bottom">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-groupe dst-icon"></i>ข้อมูลครอบครัว</h4>
                                </div>
                                <div class="feature-box secundary">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:UpdatePanel ID="pnlrptFamily" runat="server">
                                                    <ContentTemplate>
                                                <asp:Panel ID="FamilyPanel" runat="server" Visible="true">
                                                    <asp:Label ID="lblFamID" runat="server" Text="0" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblFamOF_ID" runat="server" Text="0" Visible="false"></asp:Label>
                                                    <table class="table table-padding table-bordered table-striped mb-none" id="tbFamily">
                                                        <thead>
                                                            <tr>
                                                                <th class="tbl-th">ข้อมูลครอบครัว</th>
                                                                <th class="tbl-th" width="8%">เครื่องมือ</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <asp:Repeater ID="rptFamily" runat="server" OnItemDataBound="rptFamily_ItemDataBound" OnItemCommand="rptFamily_ItemCommand">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <div class="col-md-2">
                                                                                    <asp:Label runat="server" Text="คำนำหน้า"></asp:Label>
                                                                                    <asp:UpdatePanel ID="pnlFamilyti" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <asp:DropDownList ID="ddlFamilyTI" CssClass="form-control" runat="server"></asp:DropDownList>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <asp:Label runat="server" Text="ชื่อ"></asp:Label>
                                                                                    <asp:TextBox ID="txtFamName" CssClass="form-control" runat="server"></asp:TextBox>
                                                                                    <asp:Label ID="lblOFFAM" runat="server" Text="0" Visible="false"></asp:Label>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <asp:Label runat="server" Text="นามสกุล"></asp:Label>
                                                                                    <asp:TextBox ID="txtFamSurname" CssClass="form-control" runat="server"></asp:TextBox>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <asp:UpdatePanel ID="pnlFamilyrelation" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <asp:Label runat="server" Text="ความสัมพันธ์"></asp:Label>
                                                                                            <asp:DropDownList CssClass="form-control" ID="ddlFamRelation" runat="server"></asp:DropDownList>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </div>
                                                                            </div>
                                                                            </div>
                                                                        <uc1:UCContact ID="OFcontact" runat="server" />
                                                                        </td>
                                                                        <td class="actions th">
                                                                            <asp:LinkButton ID="btnFamDelete" CommandName="btnFamDelete" runat="server" Text=" delete" CssClass="fa fa-trash-o" OnClick="btnFamDelete_Click"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tbody>
                                                    </table>
                                                    <asp:Button ID="btnFamAdd" runat="server" CssClass="btn btn-primary btn-float btn-sm" Text="เพิ่ม" OnClick="btnFamAdd_Click" />
                                                </asp:Panel>
                                                     </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--ข้อมูลการศึกษา-->
                        <div class="form-group dst-bottom">
                            <div class="panel panel-default dst-bottom">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-graduation-cap dst-icon"></i>ข้อมูลการศึกษา</h4>
                                </div>
                                <div class="feature-box secundary">
                                    <asp:UpdatePanel ID="pnlrptEdu" runat="server">
                                        <ContentTemplate>
                                        
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="lblOFEduID" runat="server" Text="0" Visible="false"></asp:Label>
                                                <asp:Label ID="lblEduID" runat="server" Text="0" Visible="false"></asp:Label>
                                                <table class="table table-padding table-bordered table-striped mb-none" id="edu">
                                                    <thead>
                                                        <tr>
                                                            <th class="tbl-th" width="15%">ระดับ</th>
                                                            <th class="tbl-th" width="30%">ชื่อสถานศึกษา</th>
                                                            <th class="tbl-th" width="26%">สาขาวิชา</th>
                                                            <th class="tbl-th" width="8%">เกรด</th>
                                                            <th class="tbl-th" width="8%">ปีเริ่ม</th>
                                                            <th class="tbl-th" width="8%">ปีจบ</th>
                                                            <th class="tbl-th" width="8%">เครื่องมือ</th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
                                                        <asp:Repeater runat="server" ID="rptEdu" OnItemDataBound="rptEdu_ItemDataBound" OnItemCommand="rptEdu_ItemCommand">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="Degree" runat="server" class="form-control"></asp:TextBox>
                                                                        <asp:Label ID="lblEduIDCont" runat="server" Text="0" Visible="false"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="Insittution" runat="server" class="form-control"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="Graduate" runat="server" class="form-control"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="Grade" runat="server" class="form-control"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="StartYear" runat="server" class="form-control"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="EndYear" runat="server" class="form-control"></asp:TextBox>
                                                                    </td>
                                                                    <td class="actions th">
                                                                        <asp:LinkButton ID="btnEDelete" CommandName="btnEDelete" runat="server" Text=" delete" CssClass="fa fa-trash-o" OnClick="btnEduDelete_Click"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                                <asp:Button ID="btnAddEdu" runat="server" CssClass="btn btn-primary btn-float btn-sm" Text="เพิ่ม" OnClick="btnAddEdu_Click" />
                                            </div>
                                        </div>
                                    </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                        <!--ข้อมูลความสามารถพิเศษ-->
                        <div class="form-group dst-bottom">
                            <div class="panel panel-default dst-bottom">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-list-alt dst-icon"></i>ข้อมูลความสามารถพิเศษ</h4>
                                </div>
                                <div class="feature-box secundary">
                                    <div class="panel-body">
                                        <div class="form-group">

                                            <div class="box" align="center">
                                                <asp:Label ID="lblSpecialsSkill" runat="server" Text="0" Visible="false"></asp:Label>
                                                <table class="table table-padding table-bordered table-striped mb-none" id="">
                                                    <thead>

                                                        <tr>
                                                            <th rowspan="2" class="tbl-th" width="60%" style="text-align: center">ความสามารถพิเศษ</th>
                                                            <th colspan="4" class="tbl-th">ระดับ</th>
                                                        </tr>

                                                        <tr align="center">
                                                            <td>ไม่มี</td>
                                                            <td>พอใช้</td>
                                                            <td>ดี</td>
                                                            <td>ดีมาก</td>
                                                        </tr>

                                                    </thead>
                                                    <tbody class="accordion-body collapse in" aria-expanded="true" align="center">
                                                        <asp:Repeater ID="Special_Skill" runat="server" OnItemDataBound="Special_Skill_ItemDataBound">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <%--<div class="checkbox-custom checkbox-default">
                                                                <asp:CheckBox ID="Skill_Check" runat="server" />
                                                                <label for="Skillcheckbox"></label>
                                                                
                                                            </div>--%>
                                                                        <asp:Label ID="lblMS_SP" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div class="radio-custom" align="center">
                                                                            <asp:RadioButton ID="RadioButton1" runat="server" GroupName="0"></asp:RadioButton>
                                                                            <label for="RadioButton1"></label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="radio-custom" align="center">
                                                                            <asp:RadioButton ID="Basicradio" runat="server" GroupName="0"></asp:RadioButton>
                                                                            <label for="Basicradio"></label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="radio-custom" align="center">
                                                                            <asp:RadioButton ID="moderateradio" runat="server" GroupName="0"></asp:RadioButton>
                                                                            <label for="moderateradio"></label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="radio-custom" align="center">
                                                                            <asp:RadioButton ID="Expertradio" runat="server" GroupName="0"></asp:RadioButton>
                                                                            <label for="Expertradio"></label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--ข้อมูลการอบรม-->
                        <div class="form-group dst-bottom">
                            <div class="panel panel-default dst-bottom">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-file-text-o dst-icon"></i>ข้อมูลการอบรม</h4>
                                </div>
                                <div class="feature-box secundary">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:UpdatePanel ID="pnlrptTrain" runat="server">
                                                    <ContentTemplate>
                                                <asp:Label ID="lblTrainIDCont" runat="server" Text="0" Visible="false"></asp:Label>
                                                <asp:Label ID="lblTID" runat="server" Text="0" Visible="false"></asp:Label>
                                                <table class="table table-padding table-bordered table-striped mb-none" id="">
                                                    <thead>
                                                        <tr>
                                                            <th class="tbl-th" width="35%">หัวข้อที่เข้าฝึกอบรม</th>
                                                            <th class="tbl-th" width="32%">หน่วยงานที่ให้การฝึกอบรม</th>
                                                            <th class="tbl-th" width="13%">ช่วงเวลาที่ฝึกอบรม(ด/พ.ศ.)</th>
                                                            <th class="tbl-th" width="8%">เครื่องมือ</th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
                                                        <asp:Repeater ID="rptTrain" runat="server" OnItemDataBound="rptTrain_ItemDataBound" OnItemCommand="rptTrain_ItemCommand">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtProgram_name" runat="server" class="form-control"></asp:TextBox>
                                                                        <asp:Label ID="lblTrainID" runat="server" Text="0" Visible="false"></asp:Label></td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtTrain_by" runat="server" class="form-control"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtTrain_date" runat="server" class="form-control"></asp:TextBox>
                                                                    </td>
                                                                    <td class="actions th">
                                                                        <asp:LinkButton ID="btnDeleteTrain" CommandName="btnDeleteTrain" runat="server" Text=" delete" CssClass="fa fa-trash-o"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                                <asp:Button ID="btnAddTrain" runat="server" CssClass="btn btn-primary btn-float btn-sm" Text="เพิ่ม" OnClick="btnAddTrain_Click" />
                                                </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--ข้อมูลรางวัล-->
                        <div class="form-group dst-bottom">
                            <div class="panel panel-default dst-bottom">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-star dst-icon"></i>ข้อมูลรางวัล</h4>
                                </div>
                                <div class="feature-box secundary">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:UpdatePanel ID="pnlrptAward" runat="server">
                                                    <ContentTemplate>
                                                    
                                                <asp:Label ID="lblContAwardID" runat="server" Text="0" Visible="false"></asp:Label>
                                                <asp:Label ID="lblOFAwardID" runat="server" Text="0" Visible="false"></asp:Label>
                                                <table class="table table-padding table-bordered table-striped mb-none" id="">
                                                    <thead>
                                                        <tr>
                                                            <th class="tbl-th" width="35%">รางวัล</th>
                                                            <th class="tbl-th" width="32%">หน่วยงานที่มอบให้</th>
                                                            <th class="tbl-th" width="13%">ช่วงเวลาที่ได้(ด/พ.ศ.)</th>
                                                            <th class="tbl-th" width="8%">เครื่องมือ</th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
                                                        <asp:Repeater ID="rptAward" runat="server" OnItemDataBound="rptAward_ItemDataBound" OnItemCommand="rptAward_ItemCommand">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtA_name" runat="server" class="form-control"></asp:TextBox>
                                                                        <asp:Label ID="lblAwardID" runat="server" Text="0" Visible="false"></asp:Label></td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtA_by" runat="server" class="form-control"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtA_date" runat="server" class="form-control"></asp:TextBox>
                                                                    </td>
                                                                    <td class="actions th">
                                                                        <asp:LinkButton ID="btnAwardDelete" CommandName="btnAwardDelete" runat="server" Text=" delete" CssClass="fa fa-trash-o"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                                <asp:Button ID="btnaddAward" runat="server" CssClass="btn btn-primary btn-float btn-sm" Text="เพิ่ม" OnClick="btnaddAward_Click" />
                                                        </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--ข้อมูลประวัติการทำงาน-->
                        <div class="form-group dst-bottom">
                            <div class="panel panel-default dst-bottom">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-folder-open dst-icon"></i>ประวัติการทำงาน</h4>
                                </div>
                                <div class="feature-box secundary">
                                    <div class="panel-body">
                                        <!--ประวัติทำงาน-->
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:UpdatePanel ID="pnlrptCH" runat="server">
                                                    <ContentTemplate>
                                                    
                                                <asp:Label ID="lblCHIDCont" runat="server" Text="0" Visible="false"></asp:Label>
                                                <asp:Label ID="lblOFCHID" runat="server" Text="0" Visible="false"></asp:Label>
                                                <table class="table table-padding table-bordered table-striped mb-none">
                                                    <thead>
                                                        <tr>
                                                            <th class="tbl-th" width="30%">ชื่อบริษัท</th>
                                                            <th class="tbl-th" width="20%">ตำแหน่ง</th>
                                                            <th class="tbl-th" width="10%">ปีเริ่มต้น</th>
                                                            <th class="tbl-th" width="10%">ปีสิ้นสุด</th>
                                                            <th class="tbl-th" width="10%">เงินเดือน</th>
                                                            <th class="tbl-th" width="8%">เครื่องมือ</th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
                                                        <asp:Repeater ID="rptCH" runat="server" OnItemDataBound="rptCH_ItemDataBound" OnItemCommand="rptCH_ItemCommand">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtWorkPlace" runat="server" class="form-control"></asp:TextBox>
                                                                        <asp:Label ID="lblCHID" runat="server" Text="0" Visible="false"></asp:Label></td>
                                                                    <td>
                                                                        <asp:UpdatePanel ID="pnlCH" runat="server">
                                                                            <ContentTemplate>
                                                                                <asp:DropDownList ID="ddlCRposition" runat="server" CssClass="form-control"></asp:DropDownList>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCR_start" runat="server" class="form-control"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCR_end" runat="server" class="form-control"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCR_slary" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </td>
                                                                    <td class="actions th">
                                                                        <asp:LinkButton ID="btnCHdelete" CommandName="btnCHdelete" runat="server" Text=" delete" CssClass="fa fa-trash-o"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                                <asp:Button ID="btnaddCH" runat="server" CssClass="btn btn-primary btn-float btn-sm" Text="เพิ่ม" OnClick="btnaddCH_Click" />
                                                        </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--ข้อมูลบุคคลอ้างอิง-->
                        <div class="form-group dst-bottom">
                            <div class="panel panel-default dst-bottom">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-share-alt dst-icon"></i>ข้อมูลบุคคลอ้างอิง</h4>
                                </div>
                                <div class="feature-box secundary">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <asp:Label ID="lblRefID" runat="server" Text="0" Visible="false"></asp:Label></td>
                                            <div class="col-md-3">
                                                <asp:Label runat="server" Text="ชื่อ"></asp:Label>
                                                <asp:TextBox ID="txtRefName" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:Label runat="server" Text="นามสกุล"></asp:Label>
                                                <asp:TextBox ID="txtRefSurname" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:Label runat="server" Text="ตำแหน่ง"></asp:Label>
                                                <asp:TextBox ID="txtRePosition" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:Label runat="server" Text="หน่วยงาน/บริษัท"></asp:Label>
                                                <asp:TextBox ID="txtRefWP" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <asp:UpdatePanel ID="pnluccContact" runat="server">
                                                    <ContentTemplate>
                                                        <uc1:UCContact ID="RefContact" runat="server" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group dst-bottom">
                            <div class="panel panel-default dst-bottom">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-share-alt dst-icon"></i>Upload Resume</h4>
                                </div>
                                <div class="form-group">
                                <div class="row"">
                                    <div class="col-md-12" style="text-align: center;">
                                        <asp:FileUpload ID="FileUpload1" runat="server"></asp:FileUpload>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <asp:Button ID="OFsave" CssClass="btn btn-primary" runat="server" Text="SAVE" OnClick="OFsave_Click" />
                                    <asp:Button ID ="btnOFcancle" CssClass="btn btn-danger" runat="server" Text="Cancle" OnClick="btnOFcancle_Click" />
                                </div>
                            </div>
                        </div>
                    </panel>
        </div>





        <!--Edit-->
        <div class=" container ">
            <div id="collapsefive-edit" class="accordion-body collapse" aria-expanded="true">

                <div class="panel-heading">
                    <h4 class="panel-bottom">Edit Officer</h4>
                </div>
                <div class="panel-body">
                    <div class="tabs">
                        <ul class="nav nav-tabs nav-justify">
                            <li class="active">
                                <a href="#e-account" data-toggle="tab" class="text-center" aria-expanded="true">
                                    <span class="badge hidden-xs">1</span>ข้อมูลส่วนตัว</a>
                            </li>
                            <li class="">
                                <a href="#e-profile" data-toggle="tab" class="text-center" aria-expanded="false">
                                    <span class="badge hidden-xs">2</span>ข้อมูลติดต่อ</a>
                            </li>
                            <li class="">
                                <a href="#e-confirm" data-toggle="tab" class="text-center" aria-expanded="false">
                                    <span class="badge hidden-xs">3</span>ข้อมูลครอบครัว</a>
                            </li>
                        </ul>

                        <!--Tab 1-->
                        <!--ส่วนกรอกข้อมูล-->

                        <div class="tab-content">
                            <div id="e-account" class="tab-pane active">
                                <!--รูปภาพ-->
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control ">Photo Upload :</label>
                                    <div class="col-md-6">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="fa fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-exists">Change</span>
                                                    <span class="fileupload-new">Select file</span>
                                                    <input type="file" />
                                                </span>
                                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--คำนำหน้า ชื่อ นามสกุล ชื่อเล่น TH -->
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control" for="inputDefault">ชื่อ-นามสกุล :</label>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <select class="form-control">
                                                <option value="">นาย</option>
                                                <option value="">นาง</option>
                                                <option value="">นางสาว</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="of_eth_name" runat="server" class="form-control"></asp:TextBox>
                                            <asp:Label ID="lblOfethname" runat="server" Text="0" Visible="false"></asp:Label>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="of_eth_surname" runat="server" class="form-control"></asp:TextBox>
                                            <asp:Label ID="lblOfethsurname" runat="server" Text="0" Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control" for="inputDefault">ชื่อเล่น :</label>
                                    <div class="col-md-2 ">
                                        <asp:TextBox ID="of_eth_nickname" runat="server" class="form-control"></asp:TextBox>
                                        <asp:Label ID="lblOfethnickname" runat="server" Text="0" Visible="false"></asp:Label>
                                    </div>
                                </div>
                                <!--คำนำหน้า ชื่อ นามสกุล ชื่อเล่น ENG -->
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control" for="inputDefault">Name-Surname :</label>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <select class="form-control">
                                                <option value="">นาย</option>
                                                <option value="">นาง</option>
                                                <option value="">นางสาว</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="of_een_name" runat="server" class="form-control"></asp:TextBox>
                                            <asp:Label ID="lblOfeenname" runat="server" Text="0" Visible="false"></asp:Label>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="of_een_surnme" runat="server" class="form-control"></asp:TextBox>
                                            <asp:Label ID="lblOfeensurname" runat="server" Text="0" Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control" for="inputDefault">Nickname :</label>
                                    <div class="col-md-2 ">
                                        <asp:TextBox ID="of_een_nickkname" runat="server" class="form-control"></asp:TextBox>
                                        <asp:Label ID="lblOfeennickname" runat="server" Text="0" Visible="false"></asp:Label>
                                    </div>
                                </div>
                                <!--ตำแหน่อง เงินเดือน -->
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control" for="inputDefault">ตำแหน่ง :</label>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <asp:TextBox ID="of_etype" runat="server" class="form-control"></asp:TextBox>
                                            <asp:Label ID="lblOfetype" runat="server" Text="0" Visible="false"></asp:Label>
                                        </div>
                                        <label class="col-md-1 lbl-control" for="inputDefault">เงินเดือน :</label>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="of_esalary" runat="server" class="form-control"></asp:TextBox>
                                            <asp:Label ID="lblOfesalary" runat="server" Text="0" Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <!--ข้อมูลส่วนตัว-->
                                <!--ช่องวันที่-->
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control ">สถานที่เกิด :</label>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <select class="form-control" data-plugin-multiselect="" id="ebrithplace">
                                                <option value="cheese" selected="">จังหวัด</option>
                                                <option value="tomatoes">อำเภอ</option>
                                            </select>
                                        </div>
                                        <label class="col-md-2 lbl-control">เพศ :</label>
                                        <div class="col-md-2">
                                            <select class="form-control" data-plugin-multiselect="" id="esex">
                                                <option value="cheese" selected="">ชาย</option>
                                                <option value="tomatoes">หญิง</option>

                                            </select>
                                        </div>
                                        <label class="col-md-1 lbl-control">กรุ๊ปเลือด :</label>
                                        <div class="col-md-2">
                                            <select class="form-control" data-plugin-multiselect="" id="eblood">
                                                <option value="cheese" selected="">A</option>
                                                <option value="tomatoes">B</option>
                                                <option value="tomatoes">O</option>
                                                <option value="tomatoes">AB</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control">สัญชาติ :</label>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <select class="form-control" data-plugin-multiselect="" id="enationality">
                                                <option value="cheese" selected="">อำเถอ</option>
                                                <option value="tomatoes">จังหวัด</option>
                                            </select>
                                        </div>
                                        <label class="col-md-2 lbl-control">เชื้อชาติ :</label>
                                        <div class="col-md-2">
                                            <select class="form-control" data-plugin-multiselect="" id="eethnicity">
                                                <option value="cheese" selected="">ไทย</option>
                                                <option value="tomatoes">จเมริกัน</option>
                                            </select>
                                        </div>
                                        <label class="col-md-1 lbl-control">ศาสนา :</label>
                                        <div class="col-md-2">
                                            <select class="form-control" data-plugin-multiselect="" id="ereligion">
                                                <option value="cheese" selected="">พุทธ</option>
                                                <option value="tomatoes">คริส</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control" for="inputDefault">เลขที่บัตรประชาชน :</label>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <asp:TextBox ID="of_eidc" runat="server" class="form-control"></asp:TextBox>
                                            <asp:Label ID="lblOfeidc" runat="server" Text="0" Visible="false"></asp:Label>
                                        </div>
                                        <!--วันออกบัตร--->
                                        <!--วัยหมดอายุ-->
                                        <label class="col-md-2 lbl-control">สถานที่ออกบัตร :</label>
                                        <div class="col-md-2">
                                            <select class="form-control" data-plugin-multiselect="" id="eidc_ex_pl">
                                                <option value="cheese" selected="">อำเถอ</option>
                                                <option value="tomatoes">จังหวัด</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control" for="textareaDefault">โรคประจำตัว :</label>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="of_medic" runat="server" class="form-control"></asp:TextBox>
                                        <asp:Label ID="lblOfemedic" runat="server" Text="0" Visible="false"></asp:Label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control" for="inputDefault">น้ำหนัก (ก.ก.) :</label>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <asp:TextBox ID="eweight" runat="server" class="form-control"></asp:TextBox>
                                            <asp:Label ID="lbleweight" runat="server" Text="0" Visible="false"></asp:Label>
                                        </div>
                                        <label class="col-md-3 lbl-control" for="inputDefault">ส่วนสูง (ซ.ม.) :</label>
                                        <div class="col-md-1">
                                            <asp:TextBox ID="eheight" runat="server" class="form-control"></asp:TextBox>
                                            <asp:Label ID="lbleheight" runat="server" Text="0" Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control" for="inputDefauit">สถาณภาพสมรส :</label>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <select class="form-control" data-plugin-multiselect="" id="emarital">
                                                <option value="cheese" selected="">โสด</option>
                                                <option value="tomatoes">แต่งงาน</option>
                                                <option value="tomatoes">หย่าร้าง</option>
                                            </select>
                                        </div>
                                        <label class="col-md-2 lbl-control">สถานภาพทางทหาร :</label>
                                        <div class="col-md-2">
                                            <select class="form-control" data-plugin-multiselect="" id="emilitary">
                                                <option value="cheese" selected="">ปกติ</option>
                                                <option value="tomatoes">ไม่ปกติ</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: center;">
                                            <a id="of_edit1" class="btn btn-primary" href="javascript:__doPostBack('ctl00$ContentPlaceHolder1$btnaddadmin','')">SAVE</a>
                                            <a id="of_cancle_edit1" class="btn btn-danger" href="javascript:__doPostBack('ctl00$ContentPlaceHolder1$btnaddadmin','')">CANCLE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Tab2-->
                            <div id="e-profile" class="tab-pane">
                                <!--ข้อมูลการติดต่อ-->
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control" for="textareaDefault">ที่อยู่ตามทะเบียนบ้าน :</label>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="ehomeadd" runat="server" class="form-control"></asp:TextBox>
                                        <asp:Label ID="lblehomeadd" runat="server" Text="0" Visible="false"></asp:Label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control" for="textareaDefault">ที่อยู่ปัจจุบัน :</label>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="emailingadd" runat="server" class="form-control"></asp:TextBox>
                                        <asp:Label ID="lblemailingadd" runat="server" Text="0" Visible="false"></asp:Label>
                                    </div>
                                </div>
                                <!--ช่องทางติดต่อ-->
                                <div class="form-group">
                                    <label class="col-md-2 lbl-control" for="inputDefault">ข้อมูลการติดต่อ :</label>
                                    <div class="col-md-8">
                                        <table class="table table-padding table-bordered table-striped mb-none" id="econtactzone">
                                            <thead>
                                                <tr>
                                                    <th class="tbl-th" width="22%">ประเภท</th>
                                                    <th class="tbl-th">รายละเอียด</th>
                                                    <th class="tbl-th" width="15%">เครื่องมือ</th>
                                                </tr>

                                            </thead>
                                            <tbody id="econtacttype" class="accordion-body collapse in" aria-expanded="true">
                                                <tr>
                                                    <td>
                                                        <select class="form-control">
                                                            <option value="">E-mail</option>
                                                            <option value="">Facebook</option>
                                                            <option value="">Facebook</option>
                                                            <option value="">Line</option>
                                                            <option value="">Mobile Phone</option>
                                                            <option value="">Phone</option>
                                                            <option value="">Twitter</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="text" value="" class="form-control" /></td>
                                                    <td class="actions th">
                                                        <a href="#" class="on-default remove-row"><i class="fa fa-trash-o">ลบ</i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <button class="btn btn-primary btn-float btn-sm" data-toggle="collapse" data-parent="#accordion" href="#collapsecontact" aria-expanded="true">เพิ่ม</button>
                                    </div>
                                </div>
                                <!--ปุ่มเซฟ-->
                                <div class="row">
                                    <div class="col-md-12" style="text-align: center;">
                                        <a id="of_edit2" class="btn btn-primary" href="javascript:__doPostBack('ctl00$ContentPlaceHolder1$btnaddadmin','')">SAVE</a>
                                        <a id="of_cancle_edit2" class="btn btn-danger" href="javascript:__doPostBack('ctl00$ContentPlaceHolder1$btnaddadmin','')">CANCLE</a>
                                    </div>
                                </div>
                            </div>
                            <!--Tab3-->
                            <div id="e-confirm" class="tab-pane">
                                <!--ข้อมูลครอบครัว-->
                                <div class="form-group">
                                    <label class="col-md-1 lbl-control" for="inputDefault">บิดา :</label>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <select class="form-control">
                                                <option value="">นาย</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="eFaname" runat="server" class="form-control"></asp:TextBox>
                                            <asp:Label ID="lbleFaname" runat="server" Text="0" Visible="false"></asp:Label>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="eFasurname" runat="server" class="form-control"></asp:TextBox>
                                            <asp:Label ID="lbleFasurname" runat="server" Text="0" Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-1 lbl-control" for="inputDefault">มารดา :</label>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <select class="form-control">
                                                <option value="">นาง</option>
                                                <option value="">นางสาว</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="eManame" runat="server" class="form-control"></asp:TextBox>
                                            <asp:Label ID="lbleManame" runat="server" Text="0" Visible="false"></asp:Label>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="eMasurname" runat="server" class="form-control"></asp:TextBox>
                                            <asp:Label ID="lbleMasurname" runat="server" Text="0" Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-1 lbl-control" for="inputDefault">พี่น้อง :</label>
                                    <div class="col-md-10">
                                        <table class="table table-padding table-bordered table-striped mb-none" id="ebrethren">
                                            <thead>
                                                <tr>
                                                    <th class="tbl-th" width="15%">คำนำหน้า</th>
                                                    <th class="tbl-th">ชื่อ</th>
                                                    <th class="tbl-th">นามสกุล</th>
                                                    <th class="tbl-th">ความสัมพันธ์</th>
                                                    <th class="tbl-th" width="10%">เครื่องมือ</th>
                                                </tr>

                                            </thead>
                                            <tbody id="ebrethren_TI" class="accordion-body collapse in" aria-expanded="true">
                                                <tr>
                                                    <td>
                                                        <select class="form-control">
                                                            <option value="">นาย</option>
                                                            <option value="">นาง</option>
                                                            <option value="">นางสาว</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="ebreth_name" runat="server" class="form-control"></asp:TextBox>
                                                        <asp:Label ID="lbleBrethname" runat="server" Text="0" Visible="false"></asp:Label></td>
                                                    <td>
                                                        <asp:TextBox ID="ebreth_surname" runat="server" class="form-control"></asp:TextBox>
                                                        <asp:Label ID="lbleBrethSurname" runat="server" Text="0" Visible="false"></asp:Label></td>
                                                    <td>
                                                        <select class="form-control">
                                                            <option value="">พี่</option>
                                                            <option value="">น้อง</option>
                                                        </select>
                                                    </td>
                                                    <td class="actions th">
                                                        <a href="#" class="on-default remove-row"><i class="fa fa-trash-o">ลบ</i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <button class="btn btn-primary btn-float btn-sm" data-toggle="collapse" data-parent="#accordion" href="#collapsecontact" aria-expanded="true">เพิ่ม</button>
                                    </div>
                                </div>
                                <!--ปุ่มเซฟ-->
                                <div class="row">
                                    <div class="col-md-12" style="text-align: center;">
                                        <a id="of_edit3" class="btn btn-primary" href="javascript:__doPostBack('ctl00$ContentPlaceHolder1$btnaddadmin','')">SAVE</a>
                                        <a id="of_cancle_edit3" class="btn btn-danger" href="javascript:__doPostBack('ctl00$ContentPlaceHolder1$btnaddadmin','')">CANCLE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

</asp:Content>
