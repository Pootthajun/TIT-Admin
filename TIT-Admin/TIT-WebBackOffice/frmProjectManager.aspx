﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmProjectManager.aspx.cs" Inherits="TIT_WebBackoffice.frmProjectManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Project Manager | TiT</title>
    <!-- Basic -->
    <meta charset="utf-8" />
    <title>Team | Porto - Responsive HTML5 Template 3.7.0</title>
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Porto - Responsive HTML5 Template" />
    <meta name="author" content="okler.net" />

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css" />

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.css" />
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css" media="screen" />
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css" media="screen" />
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.css" media="screen" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css" />
    <link rel="stylesheet" href="css/theme-elements.css" />
    <link rel="stylesheet" href="css/theme-blog.css" />
    <link rel="stylesheet" href="css/theme-shop.css" />
    <link rel="stylesheet" href="css/theme-animate.css" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="css/skins/default.css" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" />

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <section class="page-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="frmHome.aspx">Home</a></li>
                            <li class="active">Project Menager</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>Project Menager</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="panel-body">
                <h4 class="panel-title">
                    <button class="btn btn-primary" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="true" aria-hidden="False">Manage Project</button>
                </h4>
            </div>
        </div>
        <div class="container">
            <div class="col-md-4">
                <div class="team-item thumbnail">
                    <a href="frmProject.aspx" class="thumb-info team">
                        <img class="img-responsive" alt="" src="img/team/team-1.jpg" />
                        <span class="thumb-info-title">
                            <span class="thumb-info-inner">Cocomax</span>
                            <span class="thumb-info-type">John Doe</span>
                        </span>
                    </a>
                    <span class="thumb-info-caption">
                        <p style="height: 90px;">CCM : แบรนเว็บไซต์ </p>
                    </span>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="post-meta">
                                <span><i class="fa fa-calendar"></i> November 5, 2016 - November 5, 2017</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="team-item thumbnail">
                    <a href="frmProject.aspx" class="thumb-info team">
                        <img class="img-responsive" alt="" src="img/team/team-1.jpg" />
                        <span class="thumb-info-title">
                            <span class="thumb-info-inner">Starbuck</span>
                            <span class="thumb-info-type">Jane Doe</span>
                        </span>
                    </a>
                    <span class="thumb-info-caption">
                        <p style="height: 90px;">SB :ระบบสตอกสินค้า</p>
                    </span>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="post-meta">
                                <span><i class="fa fa-calendar"></i> January 5, 2017 - January 5, 2021</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="team-item thumbnail">
                    <a href="frmProject.aspx" class="thumb-info team">
                        <img class="img-responsive" alt="" src="img/team/team-1.jpg" />
                        <span class="thumb-info-title">
                            <span class="thumb-info-inner">PTT</span>
                            <span class="thumb-info-type">Jane Doe</span>
                        </span>
                    </a>
                    <span class="thumb-info-caption">
                        <p style="height: 90px;">PTT :ระบบเช็คสภาพ</p>
                    </span>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="post-meta">
                                <span><i class="fa fa-calendar"></i> January 30, 2017 - January 30, 2021</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div id="collapsefive" class="accordion-body collapse" aria-expanded="true">
                <div class="feature-box secundary">
                    <div class="form-group dst-bottom">
                        <div class="panel panel-default dst-bottom">
                            <div class="panel-heading">
                                <h4 class="panel-title"><i class="fa fa-user dst-icon"></i>Assigne</h4>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label>เลือกโปรเจค</label>
                                    <select class="form-control input-sm mb-md">
                                        <option>Cocomax</option>
                                        <option>Starbucks</option>
                                        <option>PTT</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <table class="table table-padding table-bordered table-striped mb-none" id="edu">
                                        <thead>
                                            <tr>
                                                <th class="tbl-th">ฝ่ายงาน</th>
                                                <th class="tbl-th">พนักงาน</th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <label>ออกแบบดาต้าเบส</label>
                                                </td>
                                                <td>
                                                    <select class="form-control input-sm mb-md">
                                                        <option>Ana</option>
                                                        <option>Lexi</option>
                                                        <option>Vince</option>
                                                        <option>Adam</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>ออกแบบหน้าเว็บ</label>
                                                </td>
                                                <td>
                                                    <select class="form-control input-sm mb-md">
                                                        <option>Ana</option>
                                                        <option>Lexi</option>
                                                        <option>Vince</option>
                                                        <option>Adam</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>Programing</label>
                                                </td>
                                                <td>
                                                    <select class="form-control input-sm mb-md">
                                                        <option>Ana</option>
                                                        <option>Lexi</option>
                                                        <option>Vince</option>
                                                        <option>Adam</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>ดูแลระบบ</label>
                                                </td>
                                                <td>
                                                    <select class="form-control input-sm mb-md">
                                                        <option>Ana</option>
                                                        <option>Lexi</option>
                                                        <option>Vince</option>
                                                        <option>Adam</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>ออกแบบดาต้าเบส</label>
                                                </td>
                                                <td>
                                                    <select class="form-control input-sm mb-md">
                                                        <option>Ana</option>
                                                        <option>Lexi</option>
                                                        <option>Vince</option>
                                                        <option>Adam</option>
                                                        <option>Coco</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="form-group">
                                        <div class="row">
                                            <div style="text-align: center;" class="col-md-12">
                                                <asp:Button ID="PMsave" CssClass="btn btn-primary" runat="server" Text="SAVE" />
                                                <asp:Button ID="btnOFcancle" CssClass="btn btn-danger" runat="server" Text="Cancle" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- Vendor -->
    <script src="vendor/jquery/jquery.js"></script>
    <script src="vendor/jquery.appear/jquery.appear.js"></script>
    <script src="vendor/jquery.easing/jquery.easing.js"></script>
    <script src="vendor/jquery-cookie/jquery-cookie.js"></script>
    <script src="vendor/bootstrap/bootstrap.js"></script>
    <script src="vendor/common/common.js"></script>
    <script src="vendor/jquery.validation/jquery.validation.js"></script>
    <script src="vendor/jquery.stellar/jquery.stellar.js"></script>
    <script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="vendor/jquery.gmap/jquery.gmap.js"></script>
    <script src="vendor/isotope/jquery.isotope.js"></script>
    <script src="vendor/owlcarousel/owl.carousel.js"></script>
    <script src="vendor/jflickrfeed/jflickrfeed.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.js"></script>
    <script src="vendor/vide/vide.js"></script>
    <!-- Theme Base, Components and Settings -->
    <script src="js/theme.js"></script>

    <!-- Admin Extension Specific Page Vendor -->
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/jquery-placeholder/jquery-placeholder.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/jquery-ui/jquery-ui.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/select2/js/select2.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/bootstrap-timepicker/bootstrap-timepicker.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/fuelux/js/spinner.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/dropzone/dropzone.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/bootstrap-markdown/js/markdown.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/bootstrap-markdown/js/to-markdown.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/bootstrap-markdown/js/bootstrap-markdown.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/codemirror/lib/codemirror.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/codemirror/addon/selection/active-line.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/codemirror/addon/edit/matchbrackets.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/codemirror/mode/javascript/javascript.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/codemirror/mode/xml/xml.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/codemirror/mode/css/css.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/summernote/summernote.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/ios7-switch/ios7-switch.js"></script>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/bootstrap-confirmation/bootstrap-confirmation.js"></script>

    <!-- Admin Extension -->
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/javascripts/theme.admin.extension.js"></script>

    <!-- Admin Extension Examples -->
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/javascripts/forms/examples.advanced.form.js"></script>

    <!-- Theme Custom -->
    <script src="js/custom.js"></script>

    <!-- Theme Initialization Files -->
    <script src="js/theme.init.js"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="js/theme.js"></script>

    <!-- Theme Custom -->
    <script src="js/custom.js"></script>

    <!-- Theme Initialization Files -->
    <script src="js/theme.init.js"></script>
</asp:Content>
