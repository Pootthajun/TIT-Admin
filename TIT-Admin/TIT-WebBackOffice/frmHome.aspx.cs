﻿using LinqDB.ConnectDB;
using LinqDB.TABLE;
using System;
using System.Data;
using System.Web.UI.WebControls;

namespace TIT_WebBackoffice
{
    public partial class frmHome : System.Web.UI.Page
    {
        backofficeBL BL = new backofficeBL();
        private Repeater rpt;

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        protected void btnCancel_Click(object sender, EventArgs e)
        {
          
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataTable dt = TablePrice.Get_ContactDT();
            TransactionDB trans = new TransactionDB();
            ExecuteDataInfo ret = new ExecuteDataInfo();
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                
                TbCusContactLinqDB CusContLnq = new TbCusContactLinqDB();

                CusContLnq.GetDataByPK(Convert.ToInt16(lblContID.Text), trans.Trans);
               
                     //CusContLnq.CUS_CONTACT_ID = Convert.ToInt64(dt.Rows[i]["CUS_Contact_ID"].ToString());
                     CusContLnq.CT_TYPE_ID = Convert.ToInt64(dt.Rows[i]["CT_TYPE_ID"].ToString());
                     CusContLnq.CONTACT_DETAIL =dt.Rows[i]["Contact_Detail"].ToString();


                if (CusContLnq.CUS_CONTACT_ID == 0)
                {
                    ret = CusContLnq.InsertData("Username", trans.Trans);
                }
                else
                {
                    ret = CusContLnq.UpdateData("Username", trans.Trans);
                }

            }

            if (ret.IsSuccess == true)
            {
                trans.CommitTransaction();
            }
            else
            {
                trans.RollbackTransaction();
            }

            //DataTable dt = new DataTable();
            //dt = UCCustomerContact.GetContactDT;


        }
    }
}