﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using LinqDB.ConnectDB;
using LinqDB.TABLE;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace TIT_WebBackoffice
{

    public partial class frmStaff : System.Web.UI.Page
    {
        backofficeBL BL = new backofficeBL();
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BL.Bind_BL_Province(ProvinceList);
                Bind_MailingProvinceList();
                ProvinceList5_SelectedIndexChanged(null, null);
                AmphurList4_SelectedIndexChanged(null, null);
                ddlDepartment_SelectedIndexChanged(null, null);
                BindData();
                BL.Bind_BL_ThTitle(DropDownthTI);
                DropDownthTI_SelectedIndexChanged(null, null);
                Bind_Sex(SexList);
                Bind_Blood(BloodtypeList);
                Bind_National(NationalityList);
                Bind_Ethnicity(EthnicityList);
                Bind_Religion(ReligionList);
                Bind_Marital(MaritalList);
                Bind_Military(MilitaryList);
                Bind_OFstatus(OfficerStatus);
                BL.Bind_BL_Department(ddlDepartment);
                Bind_Position(JobList);
                EDUCATION_ID = "1";
                OF_TRAIN_ID = "1";
                OF_AW_ID = "1";
                OF_CH_ID = "1";
            }
        }
        protected void ProvinceList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ProvinceList.SelectedIndex > 0)
            {
                BL.Bind_BL_Amphur(AmphurList, ProvinceList.SelectedValue);
                AmphurList_SelectedIndexChanged(null, null);
            }
        }
        protected void AmphurList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BL.Bind_BL_Tumbol(Tumbol, AmphurList.SelectedValue);
        }
        //Mailing province Amphur Tumbol
        public void Bind_MailingProvinceList()
        {
            BL.Bind_BL_Province(ProvinceList5);
        }
        protected void ProvinceList5_SelectedIndexChanged(object sender, EventArgs e)
        {
            BL.Bind_BL_Amphur(AmphurList4, ProvinceList5.SelectedValue);
            AmphurList4_SelectedIndexChanged(null, null);
        }

        protected void AmphurList4_SelectedIndexChanged(object sender, EventArgs e)
        {
            BL.Bind_BL_Tumbol(tumbolList3, AmphurList4.SelectedValue);
        }
        //insert data
        protected void OFsave_Click(object sender, EventArgs e)
        {
            TransactionDB trans = new TransactionDB();
            TbOfficerLinqDB OfficerLnq = new TbOfficerLinqDB();

            OfficerLnq.GetDataByPK(Convert.ToInt16(lblOffficerID.Text), trans.Trans);

            ExecuteDataInfo ret = new ExecuteDataInfo();

            //OfficerLnq.OF_IMG = Uploadpic.FileBytes;
            OfficerLnq.TI_ID = Convert.ToInt64(DropDownthTI.SelectedValue);
            OfficerLnq.OF_NAME = of_th_name.Text;
            OfficerLnq.OF_SURNAME = of_th_surname.Text;
            OfficerLnq.OF_NICKNAME = of_th_nickname.Text;
            OfficerLnq.OF_EN_NAME = of_en_name.Text;
            OfficerLnq.OF_EN_SURNAME = of_en_surname.Text;
            OfficerLnq.OF_EN_NICKNAME = of_en_nickname.Text;
            OfficerLnq.DEPARTMENT_ID = Convert.ToInt64(ddlDepartment.SelectedValue);
            //OfficerLnq.POSITION_ID = Convert.ToInt64(JobList.SelectedValue);
            OfficerLnq.STATUS_ID = Convert.ToInt64(OfficerStatus.SelectedValue);
            OfficerLnq.OF_SALARY = Int64.Parse(of_salary.Text);
            OfficerLnq.GENDER_ID = Convert.ToInt64(SexList.SelectedValue);
            OfficerLnq.BLOOD_ID = Convert.ToInt64(BloodtypeList.SelectedValue);
            OfficerLnq.NAT_ID = Convert.ToInt64(NationalityList.SelectedValue);
            OfficerLnq.ETH_ID = Convert.ToInt64(EthnicityList.SelectedValue);
            OfficerLnq.RELIGION_ID = Convert.ToInt64(ReligionList.SelectedValue);
            OfficerLnq.OF_BRITH = DateTime.Parse(OF_Brith.Text, CultureInfo.InvariantCulture);
            OfficerLnq.OF_PID = of_idc.Text;
            OfficerLnq.WEIGHT = Int16.Parse(weight.Text);
            OfficerLnq.HEIGHT = Int16.Parse(height.Text);
            OfficerLnq.MR_ID = Convert.ToInt64(MaritalList.SelectedValue);
            OfficerLnq.MILITARY_ID = Convert.ToInt64(MilitaryList.SelectedValue);
            OfficerLnq.HOME_ADDRESS = Homeaddress.Text;
            OfficerLnq.HOME_PROVINCE_ID = Convert.ToInt64(ProvinceList.SelectedValue).ToString();
            OfficerLnq.HOME_AMPHUR_ID = Convert.ToInt64(AmphurList.SelectedValue).ToString();
            OfficerLnq.HOME_TUMBOL_ID = Convert.ToInt64(Tumbol.SelectedValue).ToString();
            OfficerLnq.HOME_ZIPCODE = zipcode.Text;
            OfficerLnq.MAILING_ADDRESS = Mailingaddress.Text;
            OfficerLnq.MAILING_PROVINCE_ID = Convert.ToInt64(ProvinceList5.SelectedValue).ToString();
            OfficerLnq.MAILING_AMPHUR_ID = Convert.ToInt64(AmphurList4.SelectedValue).ToString();
            OfficerLnq.MAILING_TUMBOL_ID = Convert.ToInt64(tumbolList3.SelectedValue).ToString();
            OfficerLnq.MAILING_ZIPCODE = mailingzipcode.Text;


            if (OfficerLnq.OF_ID == 0)
            {
                ret = OfficerLnq.InsertData("User", trans.Trans);
                DataTable dt = Get_Contact();
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    TbOfContactLinqDB ContLnq = new TbOfContactLinqDB();
                    ContLnq.GetDataByPK(Convert.ToInt16(lblContID.Text), trans.Trans);
                    ContLnq.OF_ID = Convert.ToInt64(OfficerLnq.OF_ID);
                    ContLnq.CT_TYPE_ID = Convert.ToInt64(dt.Rows[i]["CT_TYPE_ID"].ToString());
                    ContLnq.CONTACT = dt.Rows[i]["CONTACT"].ToString();

                    if (ContLnq.CONTACT_ID == 0)
                    {
                        ret = ContLnq.InsertData("Username", trans.Trans);
                    }
                    else
                    {
                        ret = ContLnq.UpdateData("Username", trans.Trans);
                    }
                }
                if (ret.IsSuccess == true)
                {
                    TbOfFamilyLinqDB FamilyLnq = new TbOfFamilyLinqDB();
                    DataTable dt1 = Get_Family();
                    for (int i = 0; i <= dt1.Rows.Count - 1; i++)
                    {
                        TbOfContactLinqDB ContLnq = new TbOfContactLinqDB();
                        FamilyLnq.GetDataByPK(Convert.ToInt16(lblFamID.Text), trans.Trans);
                        FamilyLnq.OF_ID = Convert.ToInt64(OfficerLnq.OF_ID);
                        FamilyLnq.TI_ID = Convert.ToInt64(dt1.Rows[i]["TI_ID"].ToString());
                        FamilyLnq.NAME = dt1.Rows[i]["NAME"].ToString();
                        FamilyLnq.SURNAME = dt1.Rows[i]["SURNAME"].ToString();
                        FamilyLnq.FR_ID = Convert.ToInt64(dt1.Rows[i]["FR_ID"].ToString());

                        if (FamilyLnq.FAM_ID == 0)
                        {
                            ret = FamilyLnq.InsertData("Username", trans.Trans);
                        }
                        else
                        {
                            ret = FamilyLnq.UpdateData("Username", trans.Trans);
                        }
                    }
                    if (ret.IsSuccess == true)
                    {
                        TbOfEducationLinqDB EduLnq = new TbOfEducationLinqDB();
                        DataTable dt2 = Get_Edu();
                        for (int i = 0; i <= dt2.Rows.Count - 1; i++)
                        {
                            EduLnq.GetDataByPK(Convert.ToInt16(lblEduID.Text), trans.Trans);
                            EduLnq.OF_ID = Convert.ToInt64(OfficerLnq.OF_ID);
                            EduLnq.INSTITUTION = dt2.Rows[i]["INSTITUTION"].ToString();
                            EduLnq.GRADUATE = dt2.Rows[i]["GRADUATE"].ToString();
                            EduLnq.DEGREE = dt2.Rows[i]["DEGREE"].ToString();
                            EduLnq.GRADE = Convert.ToDouble(dt2.Rows[i]["GRADE"].ToString());
                            EduLnq.START_YEAR = dt2.Rows[i]["START_YEAR"].ToString();
                            EduLnq.END_YEAR = dt2.Rows[i]["END_YEAR"].ToString();

                            if (EduLnq.EDU_ID == 0)
                            {
                                ret = EduLnq.InsertData("Username", trans.Trans);
                            }
                            else
                            {
                                ret = EduLnq.UpdateData("Username", trans.Trans);
                            }
                        }
                        if (ret.IsSuccess == true)
                        {
                            TbOfTrainedLinqDB TrainLnq = new TbOfTrainedLinqDB();
                            DataTable dt3 = Get_Train();
                            for (int i = 0; i <= dt3.Rows.Count - 1; i++)
                            {
                                TrainLnq.GetDataByPK(Convert.ToInt16(lblTID.Text), trans.Trans);
                                TrainLnq.OF_ID = Convert.ToInt64(OfficerLnq.OF_ID);
                                TrainLnq.PROGRAM_NAME = dt3.Rows[i]["PROGRAM_NAME"].ToString();
                                TrainLnq.TRAIN_DATE = dt3.Rows[i]["TRAIN_DATE"].ToString();
                                TrainLnq.TRAINED_BY = dt3.Rows[i]["TRAINED_BY"].ToString();
                                if (TrainLnq.TRIANED_ID == 0)
                                {
                                    ret = TrainLnq.InsertData("Username", trans.Trans);
                                }
                                else
                                {
                                    ret = TrainLnq.UpdateData("Username", trans.Trans);
                                }
                            }
                            if (ret.IsSuccess == true)
                            {
                                TbOfAwardLinqDB AwardLnq = new TbOfAwardLinqDB();
                                DataTable dt4 = Get_AW();
                                for (int i = 0; i <= dt4.Rows.Count - 1; i++)
                                {
                                    AwardLnq.GetDataByPK(Convert.ToInt16(lblOFAwardID.Text), trans.Trans);
                                    AwardLnq.OF_ID = Convert.ToInt64(OfficerLnq.OF_ID);
                                    AwardLnq.AW_NAME = dt4.Rows[i]["AW_NAME"].ToString();
                                    AwardLnq.AW_DATE = dt4.Rows[i]["AW_DATE"].ToString();
                                    AwardLnq.PST_BY = dt4.Rows[i]["PST_BY"].ToString();
                                    if (AwardLnq.AW_ID == 0)
                                    {
                                        ret = AwardLnq.InsertData("Username", trans.Trans);
                                    }
                                    else
                                    {
                                        ret = AwardLnq.UpdateData("Username", trans.Trans);
                                    }
                                }
                                if (ret.IsSuccess == true)
                                {
                                    TbOfCarreerHistoryLinqDB CHLnq = new TbOfCarreerHistoryLinqDB();
                                    DataTable dt5 = Get_CH();
                                    for (int i = 0; i <= dt5.Rows.Count - 1; i++)
                                    {
                                        CHLnq.GetDataByPK(Convert.ToInt16(lblOFCHID.Text), trans.Trans);
                                        CHLnq.OF_ID = Convert.ToInt64(OfficerLnq.OF_ID);
                                        CHLnq.WORK_PLACE = dt5.Rows[i]["WORK_PLACE"].ToString();
                                        CHLnq.OF_TYPE_ID = Convert.ToInt64(dt5.Rows[i]["OF_TYPE_ID"].ToString());
                                        CHLnq.START_YEAR = dt5.Rows[i]["START_YEAR"].ToString();
                                        CHLnq.END_YEAR = dt5.Rows[i]["END_YEAR"].ToString();
                                        if (CHLnq.CH_ID == 0)
                                        {
                                            ret = CHLnq.InsertData("Username", trans.Trans);
                                        }
                                        else
                                        {
                                            ret = CHLnq.UpdateData("Username", trans.Trans);
                                        }
                                    }
                                    if (ret.IsSuccess == true)
                                    {
                                        TbReferencesLinqDB RefLnq = new TbReferencesLinqDB();
                                        RefLnq.GetDataByPK(Convert.ToInt16(lblRefID.Text), trans.Trans);
                                        RefLnq.OF_ID = Convert.ToInt64(OfficerLnq.OF_ID);
                                        RefLnq.REFERENCE_NAME = txtRefName.Text;
                                        RefLnq.REFERENCE_SURNAME = txtRefSurname.Text;
                                        RefLnq.REFERENCE_WP = txtRefWP.Text;
                                        if (ret.IsSuccess == true)
                                        {
                                            ret = RefLnq.InsertData("Username", trans.Trans);
                                            //TbSpecialSkill SpkLnq = new TbSpecialSkill();
                                            //DataTable dt6 = Get_SPK();
                                            //SpkLnq.OF_ID = Convert.ToInt64(OfficerLnq.OF_ID);
                                            //SpkLnq.MS_ID = lblMS_SP.Text;
                                            //for (int i = 0; i <= dt5.Rows.Count - 1; i++)
                                            //{
                                            //    if (RadioButton1.Checked)
                                            //    {SpkLnq.SP_LEVEL = "1";
                                            //    }
                                            //    if (RadioButton2.Checked)
                                            //    {SpkLnq.SP_LEVEL = "2";
                                            //    }
                                            //    if (RadioButton3.Checked)
                                            //    {SpkLnq.SP_LEVEL = "3";
                                            //    }
                                            //    if (RadioButton4.Checked)
                                            //    {SpkLnq.SP_LEVEL = "4";
                                            //    }
                                            // }
                                            //else
                                            //{
                                            //    ret = SpkLnq.UpdateData("Username", trans.Trans);
                                            //}
                                        }
                                        else
                                        {
                                            ret = RefLnq.UpdateData("Username", trans.Trans);
                                        }
                                    }
                                    else
                                    {
                                        trans.RollbackTransaction();
                                    }
                                }
                                else
                                {
                                    trans.RollbackTransaction();
                                }
                            }
                            else
                            {
                                trans.RollbackTransaction();
                            }
                        }
                        else
                        {
                            trans.RollbackTransaction();
                        }
                    }
                    else
                    {
                        trans.RollbackTransaction();
                    }
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                ret = OfficerLnq.UpdateData("User", trans.Trans);
            }
            if (ret.IsSuccess == true)
            {
                //lblOffficerID.Text = OfficerLnq.OF_ID.ToString();
                //lblContactID.Text = ContLnq.CONTACT_ID.ToString();
                //lblFamilyID.Text = FamilyLnq.FAM_ID.ToString();
                trans.CommitTransaction();
            }
            else
            {
                trans.RollbackTransaction();
            }
        }

        public void Bind_Contact()
        {
            String sql = "SELECT CUS_Contact_ID,c.CT_TYPE_ID";
            sql += " ,Contact_Detail,CP_ID, ct.TYPE_NAME  FROM TB_CUS_CONTACT as c  left join TB_CT_TYPE as ct on c.CT_TYPE_ID = ct.CT_TYPE_ID";
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            OfficerList.DataSource = dt;
            OfficerList.DataBind();
        }
        protected void Contact_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblMS_SP = (Label)e.Item.FindControl("lblMS_SP");

            lblMS_SP.Text = dr["MS_NAME"].ToString();
        }
        public void Clear()
        {
            lblContID.Text = "0";
            lblFamID.Text = "0";
            lblOFEduID.Text = "0";
            lblTrainIDCont.Text = "0";
            lblContAwardID.Text = "0";
        }
        private ExecuteDataInfo SaveSpecialSkill(TransactionDB trans)
        {
            ExecuteDataInfo ret = new ExecuteDataInfo();
            TbOfSpecialSkillLinqDB SpecialSkillLnq = new TbOfSpecialSkillLinqDB();
            //SpecialSkillLnq.GetDataByPK(Convert.ToInt16(lblSpecialsSkill.Text), trans.Trans);

            //DataRow dr = 

            return ret;
        }

        protected void BindData()
        {
            //Bind repeater show data
            String sql = "select o.OF_TYPE_ID, o.OF_ID, o.OF_NAME, t.OF_TYPE_NAME";
            sql += " from TB_OFFICER as o";
            sql += " inner join TB_OF_TYPE as t on o.OF_TYPE_ID = t.OF_TYPE_ID";
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            OfficerList.DataSource = dt;
            OfficerList.DataBind();
            //Bind repeater special skill
            String sql2 = "select ms.MS_NAME, ms.SKILL_TYPE_ID, t.SKILL_TYPE_ID";
            sql2 += " from TB_MS_SKILL as ms";
            sql2 += " inner join TB_SKILL_TYPE as t on ms.SKILL_TYPE_ID = t.SKILL_TYPE_ID";
            DataTable dt2 = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql2);
            Special_Skill.DataSource = dt2;
            Special_Skill.DataBind();
            //Bind repeater add Contact
            string sql3 = "select c.CONTACT_ID, ct.CT_TYPE_ID, c.CONTACT, c.CT_TYPE_ID,c.OF_ID";
            sql3 += " from TB_OF_CONTACT as c";
            sql3 += " inner join TB_CT_TYPE as ct on c.CT_TYPE_ID = ct.CT_TYPE_ID";
            sql3 += " where c.OF_ID =" + CT_ID;
            DataTable dt3 = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql3);

            rptContact.DataSource = dt3;
            rptContact.DataBind();
            //Bind repeater add family
            string sql4 = "select f.NAME,f.SURNAME,f.TI_ID,f.FAM_ID,f.FR_ID";
            sql4 += " from TB_OF_FAMILY as f";
            sql4 += " inner join TB_TITLE as t on t.TI_ID = f.TI_ID";
            sql4 += " where c.CP_ID =" + FAM_ID;
            DataTable dt4 = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql4);

            rptFamily.DataSource = dt4;
            rptFamily.DataBind();
            //Bind repeater add Edu
            string sql5 = "select e.EDU_ID, e.INSTITUTION,e.GRADUATE,e.DEGREE,e.START_YEAR,e.END_YEAR,e.GRADE,e.OF_ID";
            sql5 += " from TB_OF_EDUCATION as e";
            sql5 += " inner join TB_OFFICER as o on o.OF_ID = e.OF_ID";
            sql5 += " where e.OF_ID =" + EDUCATION_ID;
            DataTable dt5 = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql5);

            rptEdu.DataSource = dt5;
            rptEdu.DataBind();
            //Bind repeater add trained
            string sql6 = "select t.TRIANED_ID,t.OF_ID,t.PROGRAM_NAME,t.TRAIN_DATE,t.TRAINED_BY";
            sql6 += " from TB_OF_TRAINED as t";
            sql6 += " inner join TB_OFFICER as o on o.OF_ID = t.OF_ID";
            sql6 += " where t.OF_ID =" + OF_TRAIN_ID;
            DataTable dt6 = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql6);

            rptTrain.DataSource = dt6;
            rptTrain.DataBind();
            //Bind repeater add award
            string sql7 = "select a.AW_NAME,a.AW_DATE,a.PST_BY,a.AW_ID";
            sql7 += " from TB_OF_AWARD as a";
            sql7 += " inner join TB_OFFICER as o on o.OF_ID = a.OF_ID";
            sql7 += " where a.OF_ID =" + OF_AW_ID;
            DataTable dt7 = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql7);

            rptAward.DataSource = dt7;
            rptAward.DataBind();
            //Bind repeater add carreer history
            string sql8 = "select ch.WORK_PLACE,ch.OF_TYPE_ID,ch.START_YEAR,ch.END_YEAR,ch.CH_ID";
            sql8 += " from TB_OF_CARREER_HISTORY as ch";
            sql8 += " inner join TB_OFFICER as o on o.OF_ID = ch.OF_ID";
            sql8 += " where ch.OF_ID =" + OF_CH_ID;
            DataTable dt8 = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql8);

            rptCH.DataSource = dt8;
            rptCH.DataBind();


        }
        //Repeater
        protected void OfficerList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblOfficer_ID = (Label)e.Item.FindControl("lblOfficer_ID");
            Label lblOfficer_Name = (Label)e.Item.FindControl("lblOfficer_Name");
            Label lblOfficer_Position = (Label)e.Item.FindControl("lblOfficer_Position");

            lblOfficer_ID.Text = dr["OF_ID"].ToString();
            lblOfficer_Name.Text = dr["OF_NAME"].ToString();
            lblOfficer_Position.Text = dr["OF_TYPE_NAME"].ToString();
        }
        //Title
       protected void	DropDownthTI_SelectedIndexChanged(object sender, EventArgs e)
        {
            BL.Bind_BL_EnTitle(ddlEnTitle, DropDownthTI.SelectedIndex);
        }

        //Job Type
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BL.Bind_BL_Position(JobList, Int16.Parse(ddlDepartment.SelectedValue));
        }
        //Carreer History job type
        public void Bind_CRjobtype(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select OF_TYPE_NAME, OF_TYPE_ID from TB_OF_TYPE");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือก", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["OF_TYPE_NAME"].ToString(), dt.Rows[i]["OF_TYPE_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //Sex / Gender
        public void Bind_Sex(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select GENDER_NAME, GENDER_ID from TB_GENDER");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือก", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["GENDER_NAME"].ToString(), dt.Rows[i]["GENDER_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //Officer status
        public void Bind_OFstatus(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select STATUS_ID,STATUS_NAME from TB_OF_STATUS");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือก", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["STATUS_NAME"].ToString(), dt.Rows[i]["STATUS_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //Blood Type
        public void Bind_Blood(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select BLOOD_NAME, BLOOD_ID from TB_OF_BLOOD");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือก", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["BLOOD_NAME"].ToString(), dt.Rows[i]["BLOOD_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //National
        public void Bind_National(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select NAT_ID, NAT_NAME from TB_NATIONALITY");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือก", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["NAT_NAME"].ToString(), dt.Rows[i]["NAT_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //Ethnicity
        public void Bind_Ethnicity(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select ETH_ID, ETH_NAME from TB_ETHNICITY");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือก", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["ETH_NAME"].ToString(), dt.Rows[i]["ETH_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //Religion
        public void Bind_Religion(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select RELIGION_ID, RELIGION_NAME from TB_RELIGION");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือก", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["RELIGION_NAME"].ToString(), dt.Rows[i]["RELIGION_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }

        //Marital
        public void Bind_Marital(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select MR_ID, MR_STATUS from TB_MARITAL_STATUS");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือก", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["MR_STATUS"].ToString(), dt.Rows[i]["MR_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //Family relation
        public void Bind_FamRelationship(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("select FR_ID,F_RELATIONSHIP from TB_FAM_RELATIONSHIP");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือก...", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["F_RELATIONSHIP"].ToString(), dt.Rows[i]["FR_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //Military
        public void Bind_Military(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("Select MILITARY_ID, MILITARY_STATUS from TB_OF_MILITARY_STATUS");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือก", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["MILITARY_STATUS"].ToString(), dt.Rows[i]["MILITARY_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        //Position
        public void Bind_Position(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("select POSITION_ID,POSITION_CODE,POSITION_NAME from TB_POSITION");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือกตำแหน่ง...", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["POSITION_NAME"].ToString(), dt.Rows[i]["POSITION_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
        protected void Special_Skill_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblMS_SP = (Label)e.Item.FindControl("lblMS_SP");

            lblMS_SP.Text = dr["MS_NAME"].ToString();
        }
        //Repeater add contact
        public string CT_ID
        {
            get { return (lblOF_ID.Attributes["OF_ID"]); }
            set { lblOF_ID.Attributes["OF_ID"] = value; }
        }
        public string OF_Contact_ID
        {
            get { return (lblOF_ID.Attributes["CONTACT_ID"]); }
            set { lblOF_ID.Attributes["CONTACT_ID"] = value; }
        }
        protected void rptCantact_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            DropDownList ddlConnectionList = (DropDownList)e.Item.FindControl("ddlConnectionList");
            TextBox txtContact = (TextBox)e.Item.FindControl("txtContact");
            Label lblOFContactID = (Label)e.Item.FindControl("lblOFContactID");
            LinkButton delete = (LinkButton)e.Item.FindControl("delete");

            BL.Bind_BL_CtType(ddlConnectionList);

            lblOFContactID.Attributes["index"] = (e.Item.ItemIndex).ToString();
            if (!string.IsNullOrEmpty(dr["CT_TYPE_ID"].ToString()))
            {

                ddlConnectionList.SelectedValue = dr["CT_TYPE_ID"].ToString();
            }
            txtContact.Text = dr["CONTACT"].ToString();
            lblOFContactID.Text = dr["CONTACT_ID"].ToString();

            delete.CommandArgument = (e.Item.ItemIndex).ToString();
        }

        protected void delete_Click(object sender, EventArgs e)
        {

        }

        protected void rptCantact_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            LinkButton delete = (LinkButton)e.Item.FindControl("delete");

            switch (e.CommandName)
            {
                case "delete":
                    DataTable dt = Current_Data();
                    dt.Rows.RemoveAt(Convert.ToInt32(delete.CommandArgument));
                    rptContact.DataSource = dt;
                    rptContact.DataBind();

                    break;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //Clear();
            DataTable dt = Current_Data();
            // อ่าน repeater
            dt.Rows.Add();
            rptContact.DataSource = dt;
            rptContact.DataBind();
        }
        protected DataTable Current_Data()
        {
            string sql = "select c.CONTACT_ID, ct.CT_TYPE_ID, c.CONTACT, c.CT_TYPE_ID";
            sql += " from TB_OF_CONTACT as c";
            sql += " inner join TB_CT_TYPE as ct on c.CT_TYPE_ID = ct.CT_TYPE_ID";
            sql += " where 0=1";
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            DataRow DR;
            dt.Columns.Add("index");
            foreach (RepeaterItem item in rptContact.Items)
            {
                DropDownList ddlConnectionList = (DropDownList)item.FindControl("ddlConnectionList");
                TextBox txtContact = (TextBox)item.FindControl("txtContact");
                Label lblOFContactID = (Label)item.FindControl("lblOFContactID");

                DR = dt.NewRow();
                DR["index"] = lblOFContactID.Attributes["index"];

                if (lblOFContactID.Text == "")
                {
                    DR["CONTACT_ID"] = 0;
                }
                else
                {
                    DR["CONTACT_ID"] = lblOFContactID.Text;
                }
                if (ddlConnectionList.SelectedValue != "")
                {
                    DR["CT_TYPE_ID"] = ddlConnectionList.SelectedValue;
                }
                DR["CONTACT"] = txtContact.Text;
                dt.Rows.Add(DR);

            }

            return dt;
        }
        //insert repeater contact
        public DataTable ContactDT
        {
            get { return GetContact(rptContact); }
            set
            {
                dt = value;
                rptContact.DataSource = dt;
                rptContact.DataBind();
                //SetContact(dt);
            }
        }
        public DataTable dt_Contact
        {
            get { return (DataTable)ViewState["dt_Contact"]; }
            set { ViewState["dt_Contact"] = value; }
        }
        public DataTable dt_Contact_Detail
        {
            get { return (DataTable)ViewState["dt_Contact_Detail"]; }
            set { ViewState["dt_Contact_Detail"] = value; }
        }
        public void Set_rpt_Contact(DataTable dt)
        {
            rptContact.DataSource = dt;
            rptContact.DataBind();
        }
        public DataTable GetContact(Repeater rpt)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CONTACT_ID");
            dt.Columns.Add("CT_TYPE_ID");
            dt.Columns.Add("CONTACT");
            dt.Columns.Add("TYPE_NAME");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptContact.Items.Count - 1; i++)
            {
                DropDownList ddlConnectionList = (DropDownList)rptContact.FindControl("ddlConnectionList");
                TextBox txtContact = (TextBox)rptContact.FindControl("txtContact");

                dr = dt.NewRow();
                dr["CONTACT_ID"] = dt.Rows.Count + 1;
                dr["CT_TYPE_ID"] = ddlConnectionList.SelectedValue;
                dr["CONTACT"] = txtContact.Text;
                dr["TYPE_NAME"] = ddlConnectionList.SelectedItem.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public DataTable Get_Contact()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CONTACT_ID");
            dt.Columns.Add("CT_TYPE_ID");
            dt.Columns.Add("CONTACT");
            dt.Columns.Add("TYPE_NAME");

            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptContact.Items.Count - 1; i++)
            {
                DropDownList ddlConnectionList = (DropDownList)rptContact.Items[i].FindControl("ddlConnectionList");
                TextBox txtContact = (TextBox)rptContact.Items[i].FindControl("txtContact");

                dr = dt.NewRow();
                dr["CONTACT_ID"] = i + 1;
                dr["CT_TYPE_ID"] = ddlConnectionList.SelectedValue;
                dr["CONTACT"] = txtContact.Text;
                dr["TYPE_NAME"] = ddlConnectionList.SelectedItem.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public DataTable Get_ContactDT_Detail(int Con_id)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CONTACT_ID");
            dt.Columns.Add("OF_ID");
            dt.Columns.Add("CT_TYPE_ID");
            dt.Columns.Add("CONTACT");
            dt.Columns.Add("TYPE_NAME");

            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptContact.Items.Count - 1; i++)
            {
                DropDownList ddlConnectionList = (DropDownList)rptContact.Items[i].FindControl("ddlConnectionList");
                TextBox txtContact = (TextBox)rptContact.Items[i].FindControl("txtContact");

                dr = dt.NewRow();
                dr["CONTACT_ID"] = i + 1;
                dr["OF_ID"] = Con_id;
                dr["CT_TYPE_ID"] = ddlConnectionList.SelectedValue;
                dr["CONTACT"] = txtContact.Text;
                dr["TYPE_NAME"] = ddlConnectionList.SelectedItem.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        //Repeater add family
        public string FAM_ID
        {
            get { return (lblFamOF_ID.Attributes["OF_ID"]); }
            set { lblFamOF_ID.Attributes["OF_ID"] = value; }
        }
        public string OF_Family_ID
        {
            get { return (lblFamOF_ID.Attributes["FAM_ID"]); }
            set { lblFamOF_ID.Attributes["FAM_ID"] = value; }
        }
        protected void rptFamily_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            DropDownList ddlFamilyTI = (DropDownList)e.Item.FindControl("ddlFamilyTI");
            TextBox txtFamName = (TextBox)e.Item.FindControl("txtFamName");
            TextBox txtFamSurname = (TextBox)e.Item.FindControl("txtFamSurname");
            Label lblOFFAM = (Label)e.Item.FindControl("lblOFFAM");
            DropDownList ddlFamRelation = (DropDownList)e.Item.FindControl("ddlFamRelation");
            UCContact contact = (UCContact)e.Item.FindControl("contact");
            LinkButton btnFamDelete = (LinkButton)e.Item.FindControl("btnFamDelete");


            BL.Bind_BL_ThTitle(ddlFamilyTI);
            Bind_FamRelationship(ddlFamRelation);

            lblOFFAM.Attributes["index"] = (e.Item.ItemIndex).ToString();
            if (!string.IsNullOrEmpty(dr["TI_ID"].ToString()))
            {

                ddlFamilyTI.SelectedValue = dr["TI_ID"].ToString();
                ddlFamRelation.SelectedValue = dr["FR_ID"].ToString();
            }

            txtFamName.Text = dr["NAME"].ToString();
            txtFamSurname.Text = dr["SURNAME"].ToString();
            lblOFFAM.Text = dr["FAM_ID"].ToString();

            btnFamDelete.CommandArgument = (e.Item.ItemIndex).ToString();
        }

        protected void rptFamily_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            LinkButton btnFamDelete = (LinkButton)e.Item.FindControl("btnFamDelete");

            switch (e.CommandName)
            {
                case "btnFamDelete":
                    DataTable dt = Current_Data_Fam();
                    dt.Rows.RemoveAt(Convert.ToInt32(btnFamDelete.CommandArgument));

                    rptFamily.DataSource = dt;
                    rptFamily.DataBind();

                    break;
            }
        }
        protected DataTable Current_Data_Fam()
        {
            string sql = "select f.NAME,f.SURNAME,f.TI_ID,f.FAM_ID,f.FR_ID";
            sql += " from TB_OF_FAMILY as f";
            sql += " inner join TB_TITLE as t on t.TI_ID = f.TI_ID";
            sql += " where 0=1";
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            DataRow DR;
            dt.Columns.Add("index");
            foreach (RepeaterItem item in rptFamily.Items)
            {
                DropDownList ddlFamilyTI = (DropDownList)item.FindControl("ddlFamilyTI");
                TextBox txtFamName = (TextBox)item.FindControl("txtFamName");
                TextBox txtFamSurname = (TextBox)item.FindControl("txtFamSurname");
                Label lblOFFAM = (Label)item.FindControl("lblOFFAM");
                DropDownList ddlFamRelation = (DropDownList)item.FindControl("ddlFamRelation");
                LinkButton btnFamDelete = (LinkButton)item.FindControl("btnFamDelete");

                DR = dt.NewRow();
                DR["index"] = lblOFFAM.Attributes["index"];

                if (lblOFFAM.Text == "")
                {
                    DR["FAM_ID"] = 0;
                }
                else
                {
                    DR["FAM_ID"] = lblOFFAM.Text;
                }
                if (ddlFamilyTI.SelectedValue != "")
                {
                    DR["TI_ID"] = ddlFamilyTI.SelectedValue;
                }
                DR["FR_ID"] = ddlFamRelation.SelectedValue;
                DR["NAME"] = txtFamName.Text;
                DR["SURNAME"] = txtFamSurname.Text;
                dt.Rows.Add(DR);

            }
            return dt;
        }

        protected void btnFamAdd_Click(object sender, EventArgs e)
        {
            //Clear();
            DataTable dt = Current_Data_Fam();
            // อ่าน repeater
            dt.Rows.Add();
            rptFamily.DataSource = dt;
            rptFamily.DataBind();
        }

        protected void btnFamDelete_Click(object sender, EventArgs e)
        {

        }
        //insert fam
        public DataTable FamilyDT
        {
            get { return GetFam(rptFamily); }
            set
            {
                dt = value;
                rptFamily.DataSource = dt;
                rptFamily.DataBind();
                //SetContact(dt);
            }
        }
        public DataTable dt_Family
        {
            get { return (DataTable)ViewState["dt_Family"]; }
            set { ViewState["dt_Family"] = value; }
        }
        public DataTable dt_Familyt_Detail
        {
            get { return (DataTable)ViewState["dt_Familyt_Detail"]; }
            set { ViewState["dt_Familyt_Detail"] = value; }
        }
        public void Set_rpt_family(DataTable dt)
        {
            rptFamily.DataSource = dt;
            rptFamily.DataBind();
        }
        public DataTable GetFam(Repeater rpt)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("FAM_ID");
            dt.Columns.Add("TI_ID");
            dt.Columns.Add("NAME");
            dt.Columns.Add("SURNAME");
            dt.Columns.Add("FR_ID");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptFamily.Items.Count - 1; i++)
            {
                DropDownList ddlFamilyTI = (DropDownList)rptFamily.FindControl("ddlFamilyTI");
                TextBox txtFamName = (TextBox)rptFamily.FindControl("txtFamName");
                TextBox txtFamSurname = (TextBox)rptFamily.FindControl("txtFamSurname");
                DropDownList ddlFamRelation = (DropDownList)rptFamily.FindControl("ddlFamRelation");

                dr = dt.NewRow();
                dr["FAM_ID"] = dt.Rows.Count + 1;
                dr["TI_ID"] = ddlFamilyTI.SelectedValue;
                dr["NAME"] = txtFamName.Text;
                dr["TI_NAME_TH"] = ddlFamilyTI.SelectedItem.Text;
                dr["SURNAME"] = txtFamSurname.Text;
                dr["FR_ID"] = ddlFamRelation.SelectedValue;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public DataTable Get_Family()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("FAM_ID");
            dt.Columns.Add("TI_ID");
            dt.Columns.Add("NAME");
            dt.Columns.Add("SURNAME");
            dt.Columns.Add("FR_ID");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptFamily.Items.Count - 1; i++)
            {
                DropDownList ddlFamilyTI = (DropDownList)rptFamily.Items[i].FindControl("ddlFamilyTI");
                TextBox txtFamName = (TextBox)rptFamily.Items[i].FindControl("txtFamName");
                TextBox txtFamSurname = (TextBox)rptFamily.Items[i].FindControl("txtFamSurname");
                DropDownList ddlFamRelation = (DropDownList)rptFamily.Items[i].FindControl("ddlFamRelation");

                dr = dt.NewRow();
                dr["FAM_ID"] = dt.Rows.Count + 1;
                dr["TI_ID"] = ddlFamilyTI.SelectedValue;
                dr["NAME"] = txtFamName.Text;
                //dr["TI_NAME_TH"] = ddlFamilyTI.SelectedItem.Text;
                dr["SURNAME"] = txtFamSurname.Text;
                dr["FR_ID"] = ddlFamRelation.SelectedValue;
                //dr["F_RELATIONSHIP"] = ddlFamRelation.SelectedItem.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        //repeater edu
        public string EDUCATION_ID
        {
            get { return (lblOFEduID.Attributes["OF_ID"]); }
            set { lblOFEduID.Attributes["OF_ID"] = value; }
        }
        public string OF_EDU_ID
        {
            get { return (lblOFEduID.Attributes["FAM_ID"]); }
            set { lblOFEduID.Attributes["FAM_ID"] = value; }
        }
        protected void rptEdu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }
            DataRowView dr = (DataRowView)e.Item.DataItem;

            TextBox Insittution = (TextBox)e.Item.FindControl("Insittution");
            TextBox Graduate = (TextBox)e.Item.FindControl("Graduate");
            TextBox Degree = (TextBox)e.Item.FindControl("Degree");
            TextBox Grade = (TextBox)e.Item.FindControl("Grade");
            TextBox StartYear = (TextBox)e.Item.FindControl("StartYear");
            TextBox EndYear = (TextBox)e.Item.FindControl("EndYear");
            Label lblEduIDCont = (Label)e.Item.FindControl("lblEduIDCont");
            LinkButton btnEDelete = (LinkButton)e.Item.FindControl("btnEDelete");


            lblEduID.Attributes["index"] = (e.Item.ItemIndex).ToString();
            //if (!string.IsNullOrEmpty(dr["CT_TYPE_ID"].ToString()))
            //{

            //    ddlConnectionList.SelectedValue = dr["CT_TYPE_ID"].ToString();
            //}

            Insittution.Text = dr["INSTITUTION"].ToString();
            lblEduIDCont.Text = dr["EDU_ID"].ToString();
            Graduate.Text = dr["GRADUATE"].ToString();
            Degree.Text = dr["DEGREE"].ToString();
            Grade.Text = dr["GRADE"].ToString();
            StartYear.Text = dr["START_YEAR"].ToString();
            EndYear.Text = dr["END_YEAR"].ToString();

            btnEDelete.CommandArgument = (e.Item.ItemIndex).ToString();
        }

        protected void rptEdu_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            LinkButton btnEDelete = (LinkButton)e.Item.FindControl("btnEDelete");
            switch (e.CommandName)
            {
                case "btnEDelete":
                    DataTable dt = Current_Data_Edu();
                    dt.Rows.RemoveAt(Convert.ToInt32(btnEDelete.CommandArgument));

                    rptEdu.DataSource = dt;
                    rptEdu.DataBind();

                    break;
            }
        }

        protected void btnAddEdu_Click(object sender, EventArgs e)
        {
            //Clear();
            DataTable dt = Current_Data_Edu();
            // อ่าน repeater
            dt.Rows.Add();
            rptEdu.DataSource = dt;
            rptEdu.DataBind();
        }
        protected DataTable Current_Data_Edu()
        {
            string sql = "select e.EDU_ID, e.INSTITUTION,e.GRADUATE,e.DEGREE,e.START_YEAR,e.END_YEAR,e.GRADE,e.OF_ID";
            sql += " from TB_OF_EDUCATION as e";
            sql += " inner join TB_OFFICER as o on o.OF_ID = e.OF_ID";
            sql += " where e.OF_ID =" + EDUCATION_ID;
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            DataRow DR;
            dt.Columns.Add("index");
            foreach (RepeaterItem item in rptEdu.Items)
            {
                TextBox Insittution = (TextBox)item.FindControl("Insittution");
                TextBox Graduate = (TextBox)item.FindControl("Graduate");
                TextBox Degree = (TextBox)item.FindControl("Degree");
                TextBox Grade = (TextBox)item.FindControl("Grade");
                TextBox StartYear = (TextBox)item.FindControl("StartYear");
                TextBox EndYear = (TextBox)item.FindControl("EndYear");
                Label lblEduIDCont = (Label)item.FindControl("lblEduIDCont");
                DR = dt.NewRow();
                DR["index"] = lblEduIDCont.Attributes["index"];
                if (lblEduIDCont.Text == "")
                {
                    DR["EDU_ID"] = 0;
                }
                else
                {
                    DR["EDU_ID"] = lblEduIDCont.Text;
                }
                //if (ddlConnectionList.SelectedValue != "")
                //{
                //    DR["CT_TYPE_ID"] = ddlConnectionList.SelectedValue;
                //}
                DR["INSTITUTION"] = Insittution.Text;
                DR["GRADUATE"] = Graduate.Text;
                DR["DEGREE"] = Degree.Text;
                DR["START_YEAR"] = StartYear.Text;
                DR["END_YEAR"] = EndYear.Text;
                dt.Rows.Add(DR);
            }
            return dt;
        }

        protected void btnEduDelete_Click(object sender, EventArgs e)
        {

        }
        //insert edu
        public DataTable EduDT
        {
            get { return GetEdu(rptEdu); }
            set
            {
                dt = value;
                rptEdu.DataSource = dt;
                rptEdu.DataBind();
                //SetContact(dt);
            }
        }
        public DataTable dt_Edu
        {
            get { return (DataTable)ViewState["dt_Edu"]; }
            set { ViewState["dt_Edu"] = value; }
        }
        public DataTable dt_Edu_Detail
        {
            get { return (DataTable)ViewState["dt_Edu_Detail"]; }
            set { ViewState["dt_Edu_Detail"] = value; }
        }
        public void Set_rpt_Edu(DataTable dt)
        {
            rptEdu.DataSource = dt;
            rptEdu.DataBind();
        }
        public DataTable GetEdu(Repeater rpt)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("EDU_ID");
            dt.Columns.Add("INSTITUTION");
            dt.Columns.Add("GRADUATE");
            dt.Columns.Add("DEGREE");
            dt.Columns.Add("START_YEAR");
            dt.Columns.Add("END_YEAR");
            dt.Columns.Add("GRADE");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptEdu.Items.Count - 1; i++)
            {
                TextBox Insittution = (TextBox)rptEdu.FindControl("Insittution");
                TextBox Graduate = (TextBox)rptEdu.FindControl("Graduate");
                TextBox Degree = (TextBox)rptEdu.FindControl("Degree");
                TextBox Grade = (TextBox)rptEdu.FindControl("Grade");
                TextBox StartYear = (TextBox)rptEdu.FindControl("StartYear");
                TextBox EndYear = (TextBox)rptEdu.FindControl("EndYear");

                dr = dt.NewRow();
                dr["EDU_ID"] = dt.Rows.Count + 1;
                dr["INSTITUTION"] = Insittution.Text;
                dr["GRADUATE"] = Graduate.Text;
                dr["DEGREE"] = Degree.Text;
                dr["START_YEAR"] = StartYear.Text;
                dr["END_YEAR"] = EndYear.Text;
                dr["GRADE"] = Grade.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public DataTable Get_Edu()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("EDU_ID");
            dt.Columns.Add("INSTITUTION");
            dt.Columns.Add("GRADUATE");
            dt.Columns.Add("DEGREE");
            dt.Columns.Add("START_YEAR");
            dt.Columns.Add("END_YEAR");
            dt.Columns.Add("GRADE");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptEdu.Items.Count - 1; i++)
            {
                TextBox Insittution = (TextBox)rptEdu.Items[i].FindControl("Insittution");
                TextBox Graduate = (TextBox)rptEdu.Items[i].FindControl("Graduate");
                TextBox Degree = (TextBox)rptEdu.Items[i].FindControl("Degree");
                TextBox Grade = (TextBox)rptEdu.Items[i].FindControl("Grade");
                TextBox StartYear = (TextBox)rptEdu.Items[i].FindControl("StartYear");
                TextBox EndYear = (TextBox)rptEdu.Items[i].FindControl("EndYear");

                dr = dt.NewRow();
                dr["EDU_ID"] = dt.Rows.Count + 1;
                dr["INSTITUTION"] = Insittution.Text;
                dr["GRADUATE"] = Graduate.Text;
                dr["DEGREE"] = Degree.Text;
                dr["START_YEAR"] = StartYear.Text;
                dr["END_YEAR"] = EndYear.Text;
                dr["GRADE"] = Grade.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public DataTable Get_Edu_Detail(int Con_id)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("EDU_ID");
            dt.Columns.Add("INSTITUTION");
            dt.Columns.Add("GRADUATE");
            dt.Columns.Add("DEGREE");
            dt.Columns.Add("START_YEAR");
            dt.Columns.Add("END_YEAR");
            dt.Columns.Add("GRADE");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptEdu.Items.Count - 1; i++)
            {
                TextBox Insittution = (TextBox)rptEdu.FindControl("Insittution");
                TextBox Graduate = (TextBox)rptEdu.FindControl("Graduate");
                TextBox Degree = (TextBox)rptEdu.FindControl("Degree");
                TextBox Grade = (TextBox)rptEdu.FindControl("Grade");
                TextBox StartYear = (TextBox)rptEdu.FindControl("StartYear");
                TextBox EndYear = (TextBox)rptEdu.FindControl("EndYear");

                dr = dt.NewRow();
                dr["EDU_ID"] = dt.Rows.Count + 1;
                dr["INSTITUTION"] = Insittution.Text;
                dr["GRADUATE"] = Graduate.Text;
                dr["DEGREE"] = Degree.Text;
                dr["START_YEAR"] = StartYear.Text;
                dr["END_YEAR"] = EndYear.Text;
                dr["GRADE"] = Grade.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        //Train Repeater
        public string OF_TRAIN_ID
        {
            get { return (lblTID.Attributes["OF_ID"]); }
            set { lblTID.Attributes["OF_ID"] = value; }
        }
        public string TRAIN_ID
        {
            get { return (lblTID.Attributes["FAM_ID"]); }
            set { lblTID.Attributes["FAM_ID"] = value; }
        }
        protected void rptTrain_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }
            DataRowView dr = (DataRowView)e.Item.DataItem;
            TextBox txtProgram_name = (TextBox)e.Item.FindControl("txtProgram_name");
            TextBox txtTrain_by = (TextBox)e.Item.FindControl("txtTrain_by");
            TextBox txtTrain_date = (TextBox)e.Item.FindControl("txtTrain_date");
            Label lblTrainID = (Label)e.Item.FindControl("lblTrainID");
            LinkButton btnDeleteTrain = (LinkButton)e.Item.FindControl("btnDeleteTrain");

            lblTrainID.Attributes["index"] = (e.Item.ItemIndex).ToString();

            txtProgram_name.Text = dr["PROGRAM_NAME"].ToString();
            txtTrain_by.Text = dr["TRAINED_BY"].ToString();
            txtTrain_date.Text = dr["TRAIN_DATE"].ToString();
            lblTrainID.Text = dr["TRIANED_ID"].ToString();

            btnDeleteTrain.CommandArgument = (e.Item.ItemIndex).ToString();
        }

        protected void rptTrain_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            LinkButton btnDeleteTrain = (LinkButton)e.Item.FindControl("btnDeleteTrain");

            switch (e.CommandName)
            {
                case "btnDeleteTrain":
                    DataTable dt = current_Data_Train();
                    dt.Rows.RemoveAt(Convert.ToInt32(btnDeleteTrain.CommandArgument));

                    rptTrain.DataSource = dt;
                    rptTrain.DataBind();

                    break;
            }
        }
        protected void btnAddTrain_Click(object sender, EventArgs e)
        {
            //Clear();
            DataTable dt = current_Data_Train();
            // อ่าน repeater
            dt.Rows.Add();
            rptTrain.DataSource = dt;
            rptTrain.DataBind();
        }
        protected DataTable current_Data_Train()
        {
            string sql6 = "select t.TRIANED_ID,t.OF_ID,t.PROGRAM_NAME,t.TRAIN_DATE,t.TRAINED_BY";
            sql6 += " from TB_OF_TRAINED as t";
            sql6 += " inner join TB_OFFICER as o on o.OF_ID = t.OF_ID";
            sql6 += " where t.OF_ID =" + OF_TRAIN_ID;
            DataTable dt6 = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql6);

            DataRow DR;
            dt6.Columns.Add("index");
            foreach (RepeaterItem item in rptTrain.Items)
            {
                TextBox txtProgram_name = (TextBox)item.FindControl("txtProgram_name");
                TextBox txtTrain_by = (TextBox)item.FindControl("txtTrain_by");
                TextBox txtTrain_date = (TextBox)item.FindControl("txtTrain_date");
                Label lblTrainID = (Label)item.FindControl("lblTrainID");
                DR = dt6.NewRow();
                DR["index"] = lblTrainID.Attributes["index"];
                if (lblTrainID.Text == "")
                {
                    DR["TRIANED_ID"] = 0;
                }
                else
                {
                    DR["TRIANED_ID"] = lblTrainID.Text;
                }
                //if (ddlConnectionList.SelectedValue != "")
                //{
                //    DR["CT_TYPE_ID"] = ddlConnectionList.SelectedValue;
                //}
                DR["PROGRAM_NAME"] = txtProgram_name.Text;
                DR["TRAINED_BY"] = txtTrain_by.Text;
                DR["TRAIN_DATE"] = txtTrain_date.Text;
                dt6.Rows.Add(DR);
            }
            return dt6;
        }
        //insert train
        public DataTable TrainDT
        {
            get { return GetTrain(rptTrain); }
            set
            {
                dt = value;
                rptTrain.DataSource = dt;
                rptTrain.DataBind();
                //SetContact(dt);
            }
        }
        public DataTable dt_Train
        {
            get { return (DataTable)ViewState["dt_Train"]; }
            set { ViewState["dt_Train"] = value; }
        }
        public DataTable dt_Train_Detail
        {
            get { return (DataTable)ViewState["dt_Train_Detail"]; }
            set { ViewState["dt_Train_Detail"] = value; }
        }
        public void Set_rpt_Train(DataTable dt)
        {
            rptTrain.DataSource = dt;
            rptTrain.DataBind();
        }
        public DataTable GetTrain(Repeater rpt)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("TRIANED_ID");
            dt.Columns.Add("PROGRAM_NAME");
            dt.Columns.Add("TRAIN_DATE");
            dt.Columns.Add("TRAINED_BY");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptTrain.Items.Count - 1; i++)
            {
                TextBox txtProgram_name = (TextBox)rptTrain.FindControl("txtProgram_name");
                TextBox txtTrain_by = (TextBox)rptTrain.FindControl("txtTrain_by");
                TextBox txtTrain_date = (TextBox)rptTrain.FindControl("txtTrain_date");

                dr = dt.NewRow();
                dr["TRIANED_ID"] = dt.Rows.Count + 1;
                dr["PROGRAM_NAME"] = txtProgram_name.Text;
                dr["TRAIN_DATE"] = txtTrain_by.Text;
                dr["TRAINED_BY"] = txtTrain_date.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public DataTable Get_Train()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("TRIANED_ID");
            dt.Columns.Add("PROGRAM_NAME");
            dt.Columns.Add("TRAIN_DATE");
            dt.Columns.Add("TRAINED_BY");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptTrain.Items.Count - 1; i++)
            {
                TextBox txtProgram_name = (TextBox)rptTrain.Items[i].FindControl("txtProgram_name");
                TextBox txtTrain_by = (TextBox)rptTrain.Items[i].FindControl("txtTrain_by");
                TextBox txtTrain_date = (TextBox)rptTrain.Items[i].FindControl("txtTrain_date");

                dr = dt.NewRow();
                dr["TRIANED_ID"] = dt.Rows.Count + 1;
                dr["PROGRAM_NAME"] = txtProgram_name.Text;
                dr["TRAIN_DATE"] = txtTrain_by.Text;
                dr["TRAINED_BY"] = txtTrain_date.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public DataTable Get_Train_Detail(int Con_id)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("TRIANED_ID");
            dt.Columns.Add("PROGRAM_NAME");
            dt.Columns.Add("TRAIN_DATE");
            dt.Columns.Add("TRAINED_BY");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptTrain.Items.Count - 1; i++)
            {
                TextBox txtProgram_name = (TextBox)rptTrain.FindControl("txtProgram_name");
                TextBox txtTrain_by = (TextBox)rptTrain.FindControl("txtTrain_by");
                TextBox txtTrain_date = (TextBox)rptTrain.FindControl("txtTrain_date");

                dr = dt.NewRow();
                dr["TRIANED_ID"] = dt.Rows.Count + 1;
                dr["PROGRAM_NAME"] = txtProgram_name.Text;
                dr["TRAIN_DATE"] = txtTrain_by.Text;
                dr["TRAINED_BY"] = txtTrain_date.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        //repeater add award
        public string OF_AW_ID
        {
            get { return (lblOFAwardID.Attributes["OF_ID"]); }
            set { lblOFAwardID.Attributes["OF_ID"] = value; }
        }
        public string AW_ID
        {
            get { return (lblOFAwardID.Attributes["AW_ID"]); }
            set { lblOFAwardID.Attributes["AW_ID"] = value; }
        }
        protected void rptAward_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }
            DataRowView dr = (DataRowView)e.Item.DataItem;

            TextBox txtA_name = (TextBox)e.Item.FindControl("txtA_name");
            TextBox txtA_by = (TextBox)e.Item.FindControl("txtA_by");
            TextBox txtA_date = (TextBox)e.Item.FindControl("txtA_date");
            Label lblAwardID = (Label)e.Item.FindControl("lblAwardID");
            LinkButton btnAwardDelete = (LinkButton)e.Item.FindControl("btnAwardDelete");

            lblAwardID.Attributes["index"] = (e.Item.ItemIndex).ToString();

            txtA_name.Text = dr["AW_NAME"].ToString();
            txtA_by.Text = dr["PST_BY"].ToString();
            txtA_date.Text = dr["AW_DATE"].ToString();
            lblAwardID.Text = dr["AW_ID"].ToString();

            btnAwardDelete.CommandArgument = (e.Item.ItemIndex).ToString();
        }

        protected void rptAward_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            LinkButton btnAwardDelete = (LinkButton)e.Item.FindControl("btnAwardDelete");

            switch (e.CommandName)
            {
                case "btnAwardDelete":
                    DataTable dt = current_Data_Award();
                    dt.Rows.RemoveAt(Convert.ToInt32(btnAwardDelete.CommandArgument));

                    rptAward.DataSource = dt;
                    rptAward.DataBind();

                    break;
            }
        }

        protected void btnaddAward_Click(object sender, EventArgs e)
        {
            //Clear();
            DataTable dt = current_Data_Award();
            // อ่าน repeater
            dt.Rows.Add();
            rptAward.DataSource = dt;
            rptAward.DataBind();
        }
        protected DataTable current_Data_Award()
        {
            string sql7 = "select a.AW_NAME,a.AW_DATE,a.PST_BY,a.AW_ID";
            sql7 += " from TB_OF_AWARD as a";
            sql7 += " inner join TB_OFFICER as o on o.OF_ID = a.OF_ID";
            sql7 += " where a.OF_ID =" + OF_AW_ID;
            DataTable dt7 = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql7);

            DataRow DR;
            dt7.Columns.Add("index");
            foreach (RepeaterItem item in rptAward.Items)
            {
                TextBox txtA_name = (TextBox)item.FindControl("txtA_name");
                TextBox txtA_by = (TextBox)item.FindControl("txtA_by");
                TextBox txtA_date = (TextBox)item.FindControl("txtA_date");
                Label lblAwardID = (Label)item.FindControl("lblAwardID");
                DR = dt7.NewRow();
                DR["index"] = lblAwardID.Attributes["index"];
                if (lblAwardID.Text == "")
                {
                    DR["AW_ID"] = 0;
                }
                else
                {
                    DR["AW_ID"] = lblAwardID.Text;
                }
                //if (ddlConnectionList.SelectedValue != "")
                //{
                //    DR["CT_TYPE_ID"] = ddlConnectionList.SelectedValue;
                //}
                DR["AW_NAME"] = txtA_name.Text;
                DR["PST_BY"] = txtA_by.Text;
                DR["AW_DATE"] = txtA_date.Text;
                dt7.Rows.Add(DR);
            }
            return dt7;
        }
        //insert award
        public DataTable AWDT
        {
            get { return GetAW(rptAward); }
            set
            {
                dt = value;
                rptAward.DataSource = dt;
                rptAward.DataBind();
                //SetContact(dt);
            }
        }
        public DataTable dt_AW
        {
            get { return (DataTable)ViewState["dt_AW"]; }
            set { ViewState["dt_AW"] = value; }
        }
        public DataTable dt_AW_Detail
        {
            get { return (DataTable)ViewState["dt_AW_Detail"]; }
            set { ViewState["dt_AW_Detail"] = value; }
        }
        public void Set_rpt_AW(DataTable dt)
        {
            rptAward.DataSource = dt;
            rptAward.DataBind();
        }
        public DataTable GetAW(Repeater rpt)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("AW_ID");
            dt.Columns.Add("AW_NAME");
            dt.Columns.Add("AW_DATE");
            dt.Columns.Add("PST_BY");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptAward.Items.Count - 1; i++)
            {
                TextBox txtA_name = (TextBox)rptAward.FindControl("txtA_name");
                TextBox txtA_by = (TextBox)rptAward.FindControl("txtA_by");
                TextBox txtA_date = (TextBox)rptAward.FindControl("txtA_date");

                dr = dt.NewRow();
                dr["AW_ID"] = dt.Rows.Count + 1;
                dr["AW_NAME"] = txtA_name.Text;
                dr["AW_DATE"] = txtA_date.Text;
                dr["PST_BY"] = txtA_by.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public DataTable Get_AW()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("AW_ID");
            dt.Columns.Add("AW_NAME");
            dt.Columns.Add("AW_DATE");
            dt.Columns.Add("PST_BY");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptAward.Items.Count - 1; i++)
            {
                TextBox txtA_name = (TextBox)rptAward.Items[i].FindControl("txtA_name");
                TextBox txtA_by = (TextBox)rptAward.Items[i].FindControl("txtA_by");
                TextBox txtA_date = (TextBox)rptAward.Items[i].FindControl("txtA_date");

                dr = dt.NewRow();
                dr["AW_ID"] = dt.Rows.Count + 1;
                dr["AW_NAME"] = txtA_name.Text;
                dr["AW_DATE"] = txtA_date.Text;
                dr["PST_BY"] = txtA_by.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public DataTable Get_AW_Detail(int Con_id)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("AW_ID");
            dt.Columns.Add("AW_NAME");
            dt.Columns.Add("AW_DATE");
            dt.Columns.Add("PST_BY");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptAward.Items.Count - 1; i++)
            {
                TextBox txtA_name = (TextBox)rptAward.FindControl("txtA_name");
                TextBox txtA_by = (TextBox)rptAward.FindControl("txtA_by");
                TextBox txtA_date = (TextBox)rptAward.FindControl("txtA_date");

                dr = dt.NewRow();
                dr["AW_ID"] = dt.Rows.Count + 1;
                dr["AW_NAME"] = txtA_name.Text;
                dr["AW_DATE"] = txtA_date.Text;
                dr["PST_BY"] = txtA_by.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        //repeater carree history
        public string OF_CH_ID
        {
            get { return (lblOFCHID.Attributes["OF_ID"]); }
            set { lblOFCHID.Attributes["OF_ID"] = value; }
        }
        public string CH_ID
        {
            get { return (lblOFCHID.Attributes["CH_ID"]); }
            set { lblOFCHID.Attributes["CH_ID"] = value; }
        }
        protected void rptCH_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }
            DataRowView dr = (DataRowView)e.Item.DataItem;
            DropDownList ddlCRposition = (DropDownList)e.Item.FindControl("ddlCRposition");
            TextBox txtWorkPlace = (TextBox)e.Item.FindControl("txtWorkPlace");
            TextBox txtCR_start = (TextBox)e.Item.FindControl("txtCR_start");
            TextBox txtCR_end = (TextBox)e.Item.FindControl("txtCR_end");
            Label lblCHID = (Label)e.Item.FindControl("lblCHID");
            LinkButton btnCHdelete = (LinkButton)e.Item.FindControl("btnCHdelete");

            Bind_Position(ddlCRposition);

            lblCHID.Attributes["index"] = (e.Item.ItemIndex).ToString();
            if (!string.IsNullOrEmpty(dr["OF_TYPE_ID"].ToString()))
            {

                ddlCRposition.SelectedValue = dr["OF_TYPE_ID"].ToString();
            }

            txtWorkPlace.Text = dr["WORK_PLACE"].ToString();
            txtCR_start.Text = dr["START_YEAR"].ToString();
            txtCR_end.Text = dr["END_YEAR"].ToString();
            lblCHID.Text = dr["CH_ID"].ToString();

            btnCHdelete.CommandArgument = (e.Item.ItemIndex).ToString();
        }

        protected void rptCH_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            LinkButton btnCHdelete = (LinkButton)e.Item.FindControl("btnCHdelete");

            switch (e.CommandName)
            {
                case "btnCHdelete":
                    DataTable dt = current_Data_CH();
                    dt.Rows.RemoveAt(Convert.ToInt32(btnCHdelete.CommandArgument));

                    rptCH.DataSource = dt;
                    rptCH.DataBind();

                    break;
            }
        }
        protected DataTable current_Data_CH()
        {
            string sql8 = "select ch.WORK_PLACE,ch.OF_TYPE_ID,ch.START_YEAR,ch.END_YEAR,ch.CH_ID";
            sql8 += " from TB_OF_CARREER_HISTORY as ch";
            sql8 += " inner join TB_OFFICER as o on o.OF_ID = ch.OF_ID";
            sql8 += " where 0=1";
            DataTable dt8 = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql8);

            DataRow DR;
            dt8.Columns.Add("index");
            foreach (RepeaterItem item in rptCH.Items)
            {
                TextBox txtWorkPlace = (TextBox)item.FindControl("txtWorkPlace");
                DropDownList ddlCRposition = (DropDownList)item.FindControl("ddlCRposition");
                TextBox txtCR_start = (TextBox)item.FindControl("txtCR_start");
                TextBox txtCR_end = (TextBox)item.FindControl("txtCR_end");
                Label lblCHID = (Label)item.FindControl("lblCHID");
                DR = dt8.NewRow();
                DR["index"] = lblCHID.Attributes["index"];
                if (lblCHID.Text == "")
                {
                    DR["CH_ID"] = 0;
                }
                else
                {
                    DR["CH_ID"] = lblCHID.Text;
                }
                if (ddlCRposition.SelectedValue != "")
                {
                    DR["OF_TYPE_ID"] = ddlCRposition.SelectedValue;
                }
                DR["WORK_PLACE"] = txtWorkPlace.Text;
                DR["START_YEAR"] = txtCR_start.Text;
                DR["END_YEAR"] = txtCR_end.Text;
                dt8.Rows.Add(DR);
            }
            return dt8;
        }

        protected void btnaddCH_Click(object sender, EventArgs e)
        {
            //Clear();
            DataTable dt = current_Data_CH();
            // อ่าน repeater
            dt.Rows.Add();
            rptCH.DataSource = dt;
            rptCH.DataBind();
        }
        //insert ch
        public DataTable CHDT
        {
            get { return GetCH(rptCH); }
            set
            {
                dt = value;
                rptCH.DataSource = dt;
                rptCH.DataBind();
                //SetContact(dt);
            }
        }
        public DataTable dt_CH
        {
            get { return (DataTable)ViewState["dt_CH"]; }
            set { ViewState["dt_CH"] = value; }
        }
        public DataTable dt_CH_Detail
        {
            get { return (DataTable)ViewState["dt_CH_Detail"]; }
            set { ViewState["dt_CH_Detail"] = value; }
        }

        public object SelectedValue { get; private set; }

        public void Set_rpt_CH(DataTable dt)
        {
            rptCH.DataSource = dt;
            rptCH.DataBind();
        }
        public DataTable GetCH(Repeater rpt)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CH_ID");
            dt.Columns.Add("WORK_PLACE");
            dt.Columns.Add("OF_TYPE_ID");
            dt.Columns.Add("START_YEAR");
            dt.Columns.Add("END_YEAR");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptCH.Items.Count - 1; i++)
            {
                TextBox txtWorkPlace = (TextBox)rptCH.FindControl("txtWorkPlace");
                DropDownList ddlCRposition = (DropDownList)rptCH.FindControl("ddlCRposition");
                TextBox txtCR_start = (TextBox)rptCH.FindControl("txtCR_start");
                TextBox txtCR_end = (TextBox)rptCH.FindControl("txtCR_end");
                TextBox txtCR_slary = (TextBox)rptCH.FindControl("txtCR_slary");

                dr = dt.NewRow();
                dr["CH_ID"] = dt.Rows.Count + 1;
                dr["WORK_PLACE"] = txtWorkPlace.Text;
                dr["OF_TYPE_ID"] = ddlCRposition.SelectedValue;
                dr["START_YEAR"] = txtCR_start.Text;
                dr["END_YEAR"] = txtCR_end.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public DataTable Get_CH()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CH_ID");
            dt.Columns.Add("WORK_PLACE");
            dt.Columns.Add("OF_TYPE_ID");
            dt.Columns.Add("START_YEAR");
            dt.Columns.Add("END_YEAR");
            DataRow dr = default(DataRow);
            for (int i = 0; i <= rptCH.Items.Count - 1; i++)
            {
                TextBox txtWorkPlace = (TextBox)rptCH.Items[i].FindControl("txtWorkPlace");
                DropDownList ddlCRposition = (DropDownList)rptCH.Items[i].FindControl("ddlCRposition");
                TextBox txtCR_start = (TextBox)rptCH.Items[i].FindControl("txtCR_start");
                TextBox txtCR_end = (TextBox)rptCH.Items[i].FindControl("txtCR_end");
                TextBox txtCR_slary = (TextBox)rptCH.Items[i].FindControl("txtCR_slary");

                dr = dt.NewRow();
                dr["CH_ID"] = dt.Rows.Count + 1;
                dr["OF_TYPE_ID"] = ddlCRposition.SelectedValue;
                dr["WORK_PLACE"] = txtWorkPlace.Text;
                dr["START_YEAR"] = txtCR_start.Text;
                dr["END_YEAR"] = txtCR_end.Text;
                dt.Rows.Add(dr);
            }
            return dt;
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            btnCreate.Visible = false;
            if (btnCreate.Visible == false)
            {
                pnlCreate.Visible = true;
            }
        }

        protected void btnOFcancle_Click(object sender, EventArgs e)
        {
            pnlCreate.Visible = false;
            btnCreate.Visible = true;
            //if (pnlCreate.Visible == false)
            //{
            //    pnlCreate.Visible = true;
            //    Clear();
            //    btnCreate.Visible = true;

            //}
        }
        //Upload Picture
        protected void btnAddPic_Click(object sender, EventArgs e)
        {

                if (FileUpload1.PostedFile != null)
                {
                    string FileName = fileupload.FileName.ToString();
                    FileUpload1.SaveAs(Server.MapPath("img/staff/" + FileName)); //เก็บใส่โฟลเดอร์
                    image.ImageUrl = fileupload.FileName.ToString();

                    Session["Picture"] = "img/staff/" + FileName;
            }

        }
        //Cancel Picture
        protected void btnCancelPic_Click(object sender, EventArgs e)
        {

                image.ImageUrl = "";
        }

    }
}