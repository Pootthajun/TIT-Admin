﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmSettingMilitaryStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind_Data();
        }

        public void Bind_Data()
        {
            string sql = "select MILITARY_ID,MILITARY_STATUS from TB_OF_MILITARY_STATUS";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            rptMilitaryStatus.DataSource = dt;
            rptMilitaryStatus.DataBind();
        }

        protected void rptMilitaryStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblMilitaryStatus = (Label)e.Item.FindControl("lblMilitaryStatus");

            lblMilitaryStatus.Text = dr["MILITARY_STATUS"].ToString();
        }

        protected void btnMLSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                TransactionDB trans = new TransactionDB();
                TbOfMilitaryStatusLinqDB MilitaryLnq = new TbOfMilitaryStatusLinqDB();

                MilitaryLnq.GetDataByPK(Convert.ToInt16(lblMSID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();
                MilitaryLnq.MILITARY_STATUS = txtML.Text;
                if (MilitaryLnq.MILITARY_ID == 0)
                {
                    ret = MilitaryLnq.InsertData("User", trans.Trans);
                }
                else
                {
                    ret = MilitaryLnq.UpdateData("User", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblMSID.Text = MilitaryLnq.MILITARY_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
            Clear();
            Bind_Data();
        }
        private void Clear()
        {
            txtML.Text = "";
            Bind_Data();
        }
        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtML.Text))
            {
                alertmsg("กรุณาระบุสถานภาพทางทหาร");
                ret = false;
            }
            return ret;
        }

        protected void rptMilitaryStatus_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblMilitaryStatus = (Label)e.Item.FindControl("lblMilitaryStatus");
            Label lblID = (Label)e.Item.FindControl("lblID");

            if (e.CommandName == "edit")
            {
                lblMSID.Text = lblID.Text;
                txtML.Text = lblMilitaryStatus.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbOfMilitaryStatusLinqDB MilitaryLnq = new TbOfMilitaryStatusLinqDB();

                ExecuteDataInfo ret = MilitaryLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_Data();
            }
        }
    }
}