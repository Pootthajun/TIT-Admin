﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmActivity.aspx.cs" Inherits="TIT_WebBackoffice.frmActivity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Activity | TiT</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <section class="page-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="frmHome.aspx">Home</a></li>
                            <li class="active">Activity</li>

                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>Activity</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <asp:Panel ID="pnlCustomerList" runat="server" Visible="True">
                <%--<div class="form-group">
                    <div class="col-md-12">
                        <div class="featured-box featured-box-secundary default textalign">
                            <div class="box-content">
                                <h3>ค้นหารายชื่อกิจกรรม</h3>

                                <div class="row dst-bottom">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label>ชื่อกิจกรรม</label>
                                            <input type="text" value="" class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            <label>ชื่อโครงการ</label>
                                            <input type="text" value="" class="form-control">
                                        </div>
                                        </div>
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label>วันเริ่มต้นกิจกรรม</label>
                                            <input type="text" value="" class="form-control">
                                        </div>
                                        <div class="col-md-3">
                                            <label>วันสิ้นสุดกิจกรรม</label>
                                            <input type="text" value="" class="form-control">
                                        </div>
                                        <div class="col-md-3">
                                            <label>ประเภทของลูกค้า</label>
                                            <asp:DropDownList ID="ddlCusTypeSearch" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row lbl-control">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="button" value="Search.." class="btn btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>

                <section class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-bottom">Activity List</h1>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <!--ตารางแสดงผลลูกค้า-->
                            <div class="col-md-12">
                                <table id="tbCustomer" class="table table-bordered table-striped mb-none">
                                    <thead>
                                        <tr class="bg-gray">
                                            <%--<th class="tbl-th" width="10%">รหัสโครงการ</th>--%>
                                            <th class="tbl-th th">ชื่อกิจกรรม</th>
                                            <%--<th class="tbl-th">ชื่อลูกค้า</th>--%>
                                            <th class="tbl-th" width="12%">วันเริ่มต้นกิจกรรม</th>
                                            <th class="tbl-th" width="12%">วันสิ้นสุดกิจกรรม</th>
                                            <th class="tbl-th" width="10%">จำนวนวัน</th>
                                            <th class="tbl-th" width="12%">เครื่องมือ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%--<asp:Repeater ID="rptProjectList" runat="server" OnItemDataBound="rptProjectList_ItemDataBound">
                                            <ItemTemplate>--%>
                                        <tr>
                                            <%--<td align="center" class="actions th">TEST</td>--%>
                                            <td align="center" class="actions th">TEST</td>
                                            <td align="center" class="actions th">01-01-2017</td>
                                            <td align="center" class="actions th">31-12-2017</td>
                                            <td align="center" class="actions th">TEST</td>
                                            <td class="actions th" align="center">
                                                <a href="#"><i class="fa fa-plus distance"></i></a>
                                                <a href="#"><i class="fa fa-pencil-square-o distance"></i></a>
                                                <a href="#"><i class="fa fa-trash-o distance"></i></a>
                                            </td>
                                        </tr>
                                        <%--</ItemTemplate>
                                        </asp:Repeater>--%>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </asp:Panel>
            <asp:Button ID="btnCreate" runat="server" CssClass="btn btn-primary" Text="CREATE" OnClick="btnCreate_Click" />

            <asp:Panel ID="pnlCreate" runat="server" Visible="False">
                <h4 class="panel-bottom">Creat New Activity</h4>
                <div class="panel-body">
                    <!--ข้อมูลลูกค้า-->
                    <div class="form-group dst-bottom">
                        <div class="panel panel-default dst-bottom">
                            <div class="panel-heading">
                                <h4 class="panel-title"><i class="fa fa-building dst-icon"></i>ข้อมูลโครงการ</h4>
                            </div>
                            <div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <asp:Label ID="Label1" runat="server" Text="0" Visible="false"></asp:Label>
                                        <div class="col-md-6">
                                            <asp:Label ID="Label2" runat="server" Text="ชื่อกิจกรรม"></asp:Label>
                                            <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:Label ID="Label3" runat="server" Text="ชื่อโครงการ"></asp:Label>
                                            <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control" MaxLength="150"></asp:TextBox>
                                        </div>
                                    </div>
                                    <%--<div class="form-group">
                                        <div class="col-md-6">
                                            <asp:Label ID="lblCus_Type" runat="server" Text="ชื่อลูกค้า"></asp:Label>
                                            <asp:DropDownList ID="ddlCusType" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-6">
                                            <label>ผู้จัดการโครงการ</label>
                                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>--%>
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label>วันเริ่มต้นกิจกรรม</label>
                                            <div class="input-daterange input-group" data-plugin-datepicker="">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                                <asp:TextBox ID="TextBox4" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>วันสิ้นสุดกิจกรรม</label>
                                            <div class="input-daterange input-group" data-plugin-datepicker="">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                                <asp:TextBox ID="TextBox5" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group" align="center">
                        <asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="btnSave_Click"/>
                        <asp:Button ID="btnCancel" runat="server" Text="CANCEL" CssClass="btn btn-danger" OnClick="btnCancel_Click" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </form>
</asp:Content>
