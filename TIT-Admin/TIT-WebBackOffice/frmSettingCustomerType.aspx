﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmSettingCustomerType.aspx.cs" Inherits="TIT_WebBackoffice.frmSettingCusType" %>
<%@ Register Src="~/UCContactPerson.ascx" TagPrefix="uc2" TagName="UCContactPerson" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Customer Type | TiT</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="upPnl" runat="server">
            <ContentTemplate>
                <section class="page-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="breadcrumb">
                                    <li><a href="#">Master</a></li>
                                    <li class="active">Customer Type</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h1>Customer Type</h1>
                            </div>
                        </div>
                    </div>
                </section>
                
                <div class="container">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-bottom">Customers List</h4>
                        </div>
                        
                        <div class="panel-body">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped mb-none panel-bottom">
                                    <thead>
                                        <tr>
                                            <th class="tbl-th" width="30%">รหัสประเภทของลูกค้า</th>
                                            <th class="tbl-th">ประเภทของลูกค้า</th>
                                            <th class="tbl-th" width="12%">เครื่องมือ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptCustomerType" runat="server" OnItemDataBound="rptCustomerType_ItemDataBound" OnItemCommand="rptCustomerType_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="actions th">
                                                        <asp:Label ID="lblID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUS_Type_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblCustomerTypeCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUS_Type_Code") %>'></asp:Label></td>
                                                    <td class="actions th">
                                                        <asp:Label ID="lblCustomerTypeName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUS_Type_Name") %>'></asp:Label><br />
                                                    </td>
                                                    <td class="actions th" align="center">
                                                        <asp:LinkButton ID="edit" runat="server" Text=" Edit" CssClass="fa fa-pencil-square-o" CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CUS_Type_ID") %>'></asp:LinkButton>
                                                        <asp:LinkButton ID="delete" runat="server" Text=" Delete" CssClass="fa fa-trash-o" CommandName="delete" OnClientClick='javascript:return confirm("Are you sure you want to delete?")' 
                                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CUS_Type_ID") %>'></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>

                    <asp:Panel ID="pnlCusType" runat="server">
                        <div class="panel panel-default dst-bottom">
                            <div class="panel-heading">
                                <h4 class="panel-title"><i class="fa fa-group distance"></i> เพิ่ม / แก้ไขประเภทของลูกค้า
                                </h4>
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                    <asp:Label ID="lblCusTypeID" runat="server" Text="0" Visible="false"></asp:Label>
                                    <label class="lbl-control col-md-2">รหัสประเภทของลูกค้า :</label>
                                    <div class="col-md-2">
                                        <asp:TextBox ID="txtCustomerTypeCode" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <label class="lbl-control col-md-2">ประเภทของลูกค้า :</label>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtCustomerTypeName" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" align="center">
                                        <asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnCancel" runat="server" Text="CANCEL" CssClass="btn btn-danger" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</asp:Content>
