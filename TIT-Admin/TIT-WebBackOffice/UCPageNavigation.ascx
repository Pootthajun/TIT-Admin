﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPageNavigation.ascx.cs" Inherits="TIT_WebBackoffice.UCPageNavigation" %>
<style>
    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 10px 0 10px -30px;
        border-radius: 4px;
    }

        .pagination ul {
            display: inline-block;
            margin-bottom: 0;
            margin-left: 0;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,0.05);
            box-shadow: 0 1px 2px rgba(0,0,0,0.05);
        }

            .pagination ul > li {
                display: inline;
            }

        .pagination btn :first-child {
            border-left-width: 1px;
            -webkit-border-bottom-left-radius: 4px;
            border-bottom-left-radius: 4px;
            -webkit-border-top-left-radius: 4px;
            border-top-left-radius: 4px;
            -moz-border-radius-bottomleft: 4px;
            -moz-border-radius-topleft: 4px;
        }

        .pagination btn :last-child {
            border-right-width: 1px;
            -webkit-border-bottom-right-radius: 4px;
            border-bottom-right-radius: 4px;
            -webkit-border-top-right-radius: 4px;
            border-top-right-radius: 4px;
            -moz-border-radius-bottomright: 4px;
            -moz-border-radius-topright: 4px;
        }

        .pagination btn {
            float: left;
            padding: 4px 12px;
            line-height: 20px;
            text-decoration: none;
            background-color: #a8abb1;
            border: 1px solid #ddd;
            border-left-width: 0;
        }

        .pagination .btn.focus, .pagination .btn:focus, .pagination .btn:hover {
            color: #fff;
            text-decoration: none;
            background-color: #93959b;
        }

    .current-Page {
        color: #fff;
        text-decoration: none;
        background-color: #93959b;
    }
</style>

<div class="pagination">
    <asp:UpdatePanel ID="UDPMain" runat="Server">
        <ContentTemplate>
            <ul>
                <li>
                    <asp:Button ID="btnFirst" SourceName="" PageSize="20" CurrentPage="0" MaximunPageCount="4" CssClass="btn" runat="server" Text="<<" ToolTip="First Page"></asp:Button></li>
                <li>
                    <asp:Button ID="btnBack" runat="server" Text="<" ToolTip="Previous Page" CssClass="btn"></asp:Button></li>

                <asp:Repeater ID="rptPage" runat="server">
                    <ItemTemplate>
                        <li>
                            <asp:Button ID="btnPage" runat="server" Text="1" ToolTip="Go to Page" CssClass="btn"></asp:Button></li>
                    </ItemTemplate>
                </asp:Repeater>

                <li>
                    <asp:Button ID="btnNext" runat="server" Text=">" ToolTip="Next Page" CssClass="btn"></asp:Button></li>
                <li>
                    <asp:Button ID="btnLast" runat="server" Text=">>" ToolTip="Last Page" CssClass="btn"></asp:Button></li>
            </ul>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
