﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCContactPerson.ascx.cs" Inherits="TIT_WebBackoffice.UCContactPerson" %>
<%@ Register Src="~/UCCustomerContact.ascx" TagPrefix="uc2" TagName="UCCustomerContact" %>
<style>
    .text-red {
        color: #d64742 !important;
    }
</style>
<asp:TextBox ID="txtCP_CusID" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtCP_BrnID" runat="server" Visible="false"></asp:TextBox>
<asp:Label ID="lblCP_ID" runat="server" Text="0" Visible="false"></asp:Label>
<table class="table table-striped">
    <tbody>
        <asp:Repeater ID="rptContactPerson" runat="server" OnItemDataBound="rptContactPerson_ItemDataBound" OnItemCommand="rptContactPerson_ItemCommand">
            <ItemTemplate>
                <tr class="bg-tr bd-top">
                    <td></td>
                    <td>
                        <asp:LinkButton ID="delete" CommandName="delete" runat="server" CssClass="fa fa-close btn-float text-red"></asp:LinkButton>
                    </td>

                </tr>
                <tr>
                    <td class="bg-tr bold lbl-control" width="15%">ผู้ติดต่อ :</td>
                    <td class="bg-td">
                        <div class="form-group">
                            <div class="col-md-2">
                                <label>คำนำหน้า</label>
                                <asp:DropDownList ID="ddlTitle" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="col-md-5">
                                <label>ชื่อ</label>
                                <asp:TextBox ID="txtCPName" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:Label ID="lblContPerson" runat="server" Text="0" Visible="false"></asp:Label>
                            </div>
                            <div class="col-md-5">
                                <label>นามสกุล</label>
                                <asp:TextBox ID="txtCPSurname" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>แผนก</label>
                                <asp:DropDownList ID="ddlDepartment" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <div class="col-md-6">
                                <label>ตำแหน่ง</label>
                                <asp:DropDownList ID="ddlPosition" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="bg-tr lbl-control bold" width="15%">ช่องทางการติดต่อ :</td>
                    <td class="bg-td">
                        <uc2:UCCustomerContact ID="contact_test" runat="server" />
                    </td>
                </tr>
                <tr class="bg-tr">
                    <td class="bg-td">
                        <br />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </tbody>
</table>
<asp:Button ID="btnNewContactPerson" runat="server" CssClass="dst-icon btn btn-primary dst-bottom" Text="เพิ่มผู้ติดต่อใหม่" OnClick="btnNewContactPerson_Click"></asp:Button>
