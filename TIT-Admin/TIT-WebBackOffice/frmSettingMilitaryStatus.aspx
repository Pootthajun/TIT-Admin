﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmSettingMilitaryStatus.aspx.cs" Inherits="TIT_WebBackoffice.frmSettingMilitaryStatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Military Status | TiT</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <section class="page-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="breadcrumb">
                                    <li><a href="#">Master</a></li>
                                    <li class="active">Military Status</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h1>Position</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="container">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-bottom">Military Status List</h4>
                        </div>

                        <div class="panel-body">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped mb-none panel-bottom">
                                    <thead>
                                        <tr>
                                            <th class="tbl-th">สถานภาพทางทหาร</th>
                                            <th class="tbl-th" width="12%">เครื่องมือ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptMilitaryStatus" runat="server" OnItemDataBound="rptMilitaryStatus_ItemDataBound" OnItemCommand="rptMilitaryStatus_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="actions th">
                                                        <asp:Label ID="lblID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MILITARY_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblMilitaryStatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MILITARY_STATUS") %>'></asp:Label>
                                                    </td>
                                                    <td class="actions th" align="center">
                                                        <asp:LinkButton ID="btnMLedit" runat="server" Text=" Edit" CssClass="fa fa-pencil-square-o" CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "MILITARY_ID") %>'></asp:LinkButton>
                                                        <asp:LinkButton ID="btnMLdelete" runat="server" Text=" Delete" CssClass="fa fa-trash-o" CommandName="delete" OnClientClick='javascript:return confirm("Are you sure you want to delete?")' 
                                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "MILITARY_ID") %>'></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>

                    <asp:Panel ID="pnlPosition" runat="server">
                        <div class="panel panel-default dst-bottom">
                            <div class="panel-heading">
                                <h4 class="panel-title"><i class="fa fa-group distance"></i>เพิ่ม / แก้ไขสถานภาพทางทหาร
                                </h4>
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                    <asp:Label ID="lblMSID" runat="server" Text="0" Visible="false"></asp:Label>
                                    <div class="col-md-3">
                                    <label>สถานภาพทางทหาร :</label>
                                        <asp:TextBox ID="txtML" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" align="center">
                                        <asp:Button ID="btnMLSave" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="btnMLSave_Click" />
                                        <asp:Button ID="btnMLCancel" runat="server" Text="CANCEL" CssClass="btn btn-danger" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
    </form>
</asp:Content>
