﻿using System;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmProject : System.Web.UI.Page
	{
        backofficeBL BL = new backofficeBL();

        protected void Page_Load(object sender, EventArgs e)
		{
            if (!IsPostBack)
            {
                //BL.Bind_BL_Province(Cus_ProvinceList);
                //BL.Bind_BL_CustomerType(ddlCusType);
                //BL.Bind_BL_CustomerType(DropDownList1);

            }
        }

        
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            btnCreate.Visible = false;
            if (btnCreate.Visible == false)
            {
                pnlCreate.Visible = true;
                pnlCustomerList.Visible = false;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlCreate.Visible = false;
            if (pnlCreate.Visible == false)
            {
                pnlCustomerList.Visible = true;
                //Clear();
                btnCreate.Visible = true;

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            pnlCreate.Visible = false;
            if (pnlCreate.Visible == false)
            {
                pnlCustomerList.Visible = true;
                //Clear();
                btnCreate.Visible = true;

            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Button3.Visible = false;
            if (Button3.Visible == false)
            {
                Panel1.Visible = true;
                //pnlCustomerList.Visible = false;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            if (Panel1.Visible == false)
            {
                Button1.Visible = true;

            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            if (Panel1.Visible == false)
            {
                Button2.Visible = true;

            }
        }
    }
}