﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmHome.aspx.cs" Inherits="TIT_WebBackoffice.frmHome" %>

<%@ Register Src="~/UCCustomerContact.ascx" TagPrefix="uc1" TagName="UCCustomerContact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Home | TiT</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <div class="container">
            <asp:Panel ID="pnlCreate" runat="server" Visible="True">
                <h4 class="panel-bottom">Creat New Invoice</h4>
                <div class="form-group">
                    <asp:Label ID="lblContID" runat="server" Text="0" Visible="false"></asp:Label>
                    <uc1:UCCustomerContact ID="TablePrice" runat="server" />
                </div>
                <div class="col-md-12">
                    <div class="form-group" align="center">
                        <asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="CANCEL" CssClass="btn btn-danger" OnClick="btnCancel_Click" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </form>
</asp:Content>
