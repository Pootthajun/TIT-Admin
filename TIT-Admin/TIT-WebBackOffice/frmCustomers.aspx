﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmCustomers.aspx.cs" Inherits="TIT_WebBackoffice.frmCustomers" %>

<%@ Register Src="~/UCContactPerson.ascx" TagPrefix="uc1" TagName="UCContactPerson" %>
<%@ Register Src="~/UCCustomerContact.ascx" TagPrefix="uc1" TagName="UCCustomerContact" %>
<%--<%@ Register Src="~/UCPageNavigation.ascx" TagPrefix="uc1" TagName="UCPageNavigation" %>--%>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Customers | TiT</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="upPnl" runat="server">
            <ContentTemplate>
                <section class="page-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="breadcrumb">
                                    <li><a href="frmHome.aspx">Home</a></li>
                                    <li class="active">Customer</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h1>Customer</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="container">
                    <!--หัวตาราง ส่วนการค้นหา-->
                    <asp:Panel ID="pnlCustomerList" runat="server" Visible="True">
                        <div class="form-group dst-bottom">
                            <div class="panel panel-default dst-bottom">
                                <div class="panel-heading">
                                    <h4 class="panel-title bold"><i class="fa fa-search dst-icon bold"></i>ค้นหารายชื่อลูกค้า
                                    </h4>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label>ชื่อย่อลูกค้า</label>
                                            <asp:TextBox ID="txtSearch_CusCode" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-4">
                                            <label>ชื่อลูกค้า</label>
                                            <asp:TextBox ID="txtSearch_CusName" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-3">
                                            <label>เลขประจำตัวผู้เสียภาษี</label>
                                            <asp:TextBox ID="txtSearch_CusTaxID" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-3">
                                            <label>ประเภทของลูกค้า</label>
                                            <asp:DropDownList ID="ddlCusTypeSearch" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group lbl-control">
                                        <div class="col-md-12">       
                                            <asp:Button ID="Search" runat="server" CssClass="btn btn-primary" Text="Search.." OnClick="Search_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <section class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-bottom">Customer List</h2>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <!--ตารางแสดงผลลูกค้า-->
                                    <div class="col-md-12">
                                        <table id="tbCustomer" class="table table-bordered table-striped mb-none">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th class="tbl-th" width="8%">ชื่อย่อ</th>
                                                    <th class="tbl-th">ชื่อลูกค้า</th>
                                                    <th class="tbl-th" width="15%">เลขประจำตัวผู้เสียภาษี</th>
                                                    <th class="tbl-th" width="18%">ประเภทลูกค้า</th>
                                                    <th class="tbl-th" width="12%">เครื่องมือ</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server" OnItemDataBound="rptList_ItemDataBound" OnItemCommand="rptList_ItemCommand">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblCUS_ID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUS_ID") %>'></asp:Label>
                                                                <asp:Label ID="lblCUS_Code" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUS_Code") %>'></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="lblCUS_Name" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUS_Name") %>'></asp:Label></td>
                                                            <td align="center">
                                                                <asp:Label ID="lblCUS_TaxID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUS_TaxID") %>'></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="lblCUS_Type_ID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUS_Type_ID") %>'></asp:Label>
                                                                <asp:Label ID="lblCUS_Type_Name" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUS_Type_Name") %>'></asp:Label></td>
                                                                
                                                            <td class="actions th" align="center">
                                                                <asp:LinkButton ID="edit" runat="server" Text=" edit" CssClass="fa fa-pencil-square-o" OnClick="edit_Click"  CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CUS_ID") %>'></asp:LinkButton>
                                                                <asp:LinkButton ID="delete" runat="server" Text=" delete" CssClass="fa fa-trash-o" CommandName="delete" OnClientClick='javascript:return confirm("Are you sure you want to delete?")' 
                                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CUS_ID") %>'></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                    <%--<uc1:UCPageNavigation runat="server" ID="Pager" PageSize="10" />--%>
                                </div>
                            </div>
                        </section>
                    </asp:Panel>
                    <asp:Button ID="btnCreate" runat="server" CssClass="btn btn-primary" Text="CREATE" OnClick="btnCreate_Click" />

                    <asp:Panel ID="pnlCreate" runat="server" Visible="False">
                        <%--<h4 class="panel-bottom">Create New Customer</h4>--%>
                        <div class="panel-body">
                            <!--ข้อมูลลูกค้า-->
                            <div class="form-group dst-bottom">
                                <div class="panel panel-default dst-bottom">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><i class="fa fa-building dst-icon"></i>ข้อมูลลูกค้า</h4>
                                    </div>
                                    <div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <asp:Label ID="lblCusID" runat="server" Text="0" Visible="false"></asp:Label>
                                                <asp:Label ID="lblCC" runat="server" Text="0" Visible="false"></asp:Label>
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblCusCode" runat="server" Text="ชื่อย่อลูกค้า"></asp:Label>
                                                    <asp:TextBox ID="txtCusCode" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:Label ID="lblCusName" runat="server" Text="ชื่อลูกค้า"></asp:Label>
                                                    <asp:TextBox ID="txtCusName" runat="server" CssClass="form-control" MaxLength="150"></asp:TextBox>
                                                </div>
                                                <div class="col-md-3">
                                                    <asp:Label ID="lblCus_Type" runat="server" Text="ประเภทลูกค้า"></asp:Label>
                                                    <asp:DropDownList ID="ddlCusType" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblTaxID" runat="server" Text="เลขประจำตัวผู้เสียภาษี"></asp:Label>
                                                    <asp:TextBox ID="txtTaxID" runat="server" CssClass="form-control" MaxLength="13"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="lblAddress" runat="server" Text="0" Visible="false"></asp:Label>
                                                <div class="col-sm-7">
                                                    <label>ที่อยู่</label>
                                                    <asp:TextBox ID="txtCusAddress" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label>จังหวัด</label>
                                                    <asp:DropDownList ID="Cus_ProvinceList" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Cus_ProvinceList_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>เขต/อำเภอ</label>
                                                    <asp:DropDownList ID="Cus_AmphurList" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Cus_AmphurList_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>แขวง/ตำบล</label>
                                                    <asp:DropDownList ID="Cus_TumbolList" CssClass="form-control" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>รหัสไปรษณีย์</label>
                                                    <asp:TextBox ID="txtPostalCode" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>รายละเอียดเพิ่มเติม</label>
                                                    <textarea class="form-control" rows="3" id="txtDetail" runat="server"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group dst-bottom">
                                <div class="panel panel-default dst-bottom">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><i class="fa fa-group dst-icon"></i>ข้อมูลผู้ติดต่อ
                                        </h4>
                                    </div>

                                    <div class="panel-body">
                                        <uc1:UCContactPerson ID="contactperson" runat="server" />                                   
                                    </div>
                                </div>
                            </div>

                            <!--สาขา-->
                            <div class="form-group dst-bottom">
                                <div class="panel panel-default dst-bottom">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><i class="fa fa-sitemap dst-icon"></i>ข้อมูลสาขา</h4>
                                    </div>

                                    <div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <h5>ชื่อ/ที่อยู่สาขา</h5>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="lblBranchID" runat="server" Text="0" Visible="false"></asp:Label>
                                                            <asp:TextBox ID="txtBrnCusID" runat="server" Visible="false"></asp:TextBox>

                                                            <div class="col-sm-9">
                                                                <label>ชื่อสาขา</label>
                                                                <asp:TextBox ID="txtBranch" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <label>เลขประจำตัวผู้เสียภาษี</label>
                                                                <asp:TextBox ID="txtTaxIDBrn" runat="server" CssClass="form-control" MaxLength="13"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-9">
                                                                <label>ที่อยู่</label>
                                                                <asp:TextBox ID="txtAddressBrn" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-3">
                                                                <label>จังหวัด</label>
                                                                <asp:DropDownList ID="Brn_ProvinceList" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Brn_ProvinceList_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>เขต/อำเภอ</label>
                                                                <asp:DropDownList ID="Brn_AmphurList" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Brn_AmphurList_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>แขวง/ตำบล</label>
                                                                <asp:DropDownList ID="Brn_TumbolList" CssClass="form-control" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <asp:Label ID="Label2" runat="server" Text="0" Visible="false"></asp:Label>
                                                                <label>รหัสไปรษณีย์</label>
                                                                <asp:TextBox ID="txtPostalCode2" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--ข้อมูลผู้ติดต่อของสาขา-->
                                            <div class="form-group">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <h5>ข้อมูลผู้ติดต่อของสาขา</h5>
                                                        </div>
                                                        <asp:Label ID="lblContPerson_ID" runat="server" Text="0" Visible="false"></asp:Label>
                                                        <uc1:UCContactPerson ID="ContactPersonBrn" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group" align="center">
                                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="SAVE" CssClass="btn btn-primary" />
                                <asp:Button ID="btnCancel" runat="server" Text="CANCEL" CssClass="btn btn-danger" OnClick="btnCancel_Click" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
    <script src="http://preview.oklerthemes.com/porto-admin/edge/assets/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
</asp:Content>
