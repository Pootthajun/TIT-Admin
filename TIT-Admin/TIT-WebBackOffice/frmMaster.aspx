﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="frmMaster.aspx.cs" Inherits="TIT_WebBackoffice.frmMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="page-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="frmHome.aspx">Home</a></li>
                        <li class="active">Master</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1>Master</h1>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
