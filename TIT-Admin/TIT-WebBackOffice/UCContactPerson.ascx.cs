﻿using System;
using System.Web.UI.WebControls;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class UCContactPerson : System.Web.UI.UserControl
    {
        backofficeBL BL = new backofficeBL();
        DataTable dt = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlDepartment_SelectedIndexChanged(null, null);
                BindData_ContactPerson(null);
                BindData_ContactPersonBrn(null, null);
            }
        }

        public string CP_ID
        {
            get { return (lblCP_ID.Attributes["CP_ID"]); }
            set { lblCP_ID.Attributes["CP_ID"] = value; }
        }
        public string TI_ID
        {
            get { return (lblCP_ID.Attributes["TI_ID"]); }
            set { lblCP_ID.Attributes["TI_ID"] = value; }
        }
        public string POSITION_ID
        {
            get { return (lblCP_ID.Attributes["POSITION_ID"]); }
            set { lblCP_ID.Attributes["POSITION_ID"] = value; }
        }
        public string DEPARTMENT_ID
        {
            get { return (lblCP_ID.Attributes["DEPARTMENT_ID"]); }
            set { lblCP_ID.Attributes["DEPARTMENT_ID"] = value; }
        }
        public DataTable dt_CUS_Contact_Detail
        {
            get { return (DataTable)ViewState["dt_CUS_Contact_Detail"]; }
            set { ViewState["dt_CUS_Contact_Detail"] = value; }
        }
        public DataTable dt_CUS_Contact_H
        {
            get { return (DataTable)ViewState["dt_CUS_Contact_H"]; }
            set { ViewState["dt_CUS_Contact_H"] = value; }

           // 

        }

        public DataTable dtCustContact() {

            DataTable dt = new DataTable();
            dt.Columns.Add("title_id",typeof(long));
            dt.Columns.Add("person_first_name");
            dt.Columns.Add("person_surname");
            dt.Columns.Add("contact_person_id", typeof(long));
            dt.Columns.Add("department_id", typeof(long));
            dt.Columns.Add("position_id", typeof(long));
            dt.Columns.Add("contact_list", typeof(DataTable));

            foreach (RepeaterItem itm in rptContactPerson.Items) {
                DropDownList ddlTitle = (DropDownList)itm.FindControl("ddlTitle");
                Label lblContPerson = (Label)itm.FindControl("lblContPerson");
                TextBox txtCPName = (TextBox)itm.FindControl("txtCPName");
                TextBox txtCPSurname = (TextBox)itm.FindControl("txtCPSurname");
                DropDownList ddlDepartment = (DropDownList)itm.FindControl("ddlDepartment");
                DropDownList ddlPosition = (DropDownList)itm.FindControl("ddlPosition");
                UCCustomerContact contact_test = (UCCustomerContact)itm.FindControl("contact_test");

                DataRow dr = dt.NewRow();
                dr["title_id"] = Convert.ToInt64(ddlTitle.SelectedValue);
                dr["person_first_name"] = txtCPName.Text;
                dr["person_surname"] = txtCPSurname.Text;
                dr["contact_person_id"] = Convert.ToInt64(lblContPerson.Text);
                dr["department_id"] = Convert.ToInt64(ddlDepartment.SelectedValue);
                dr["position_id"] = Convert.ToInt64(ddlPosition.SelectedValue);
                dr["contact_list"] = contact_test.ContactDT;

                dt.Rows.Add(dr);
            }
            return dt;
        }

        public void BindData_ContactPerson(String Cus_ID)
        {
            string sql = "select cp.CP_ID,cp.TI_ID, CP_Name, CP_Surname, cp.POSITION_ID, cp.DEPARTMENT_ID,CUS_ID ,BRN_ID";
            sql += " from TB_CONTACT_PERSON as cp ";
            sql += " where cp.CUS_ID =" + Cus_ID + " AND BRN_ID IS NULL ";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);

            rptContactPerson.DataSource = dt;
            rptContactPerson.DataBind();
        }

        public void BindData_ContactPersonBrn(String Cus_ID,String Brn_ID)
        {
            string sql = "select cp.CP_ID,cp.TI_ID, CP_Name, CP_Surname, cp.POSITION_ID, cp.DEPARTMENT_ID,CUS_ID ,BRN_ID";
            sql += " from TB_CONTACT_PERSON as cp ";
            sql += " where cp.CUS_ID =" + Cus_ID + " AND BRN_ID =" + Brn_ID +" ";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);

            rptContactPerson.DataSource = dt;
            rptContactPerson.DataBind();
        }

        protected void rptContactPerson_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }
            DataRowView dr = (DataRowView)e.Item.DataItem;

            DropDownList ddlTitle = (DropDownList)e.Item.FindControl("ddlTitle");
            TextBox txtCPName = (TextBox)e.Item.FindControl("txtCPName");
            TextBox txtCPSurname = (TextBox)e.Item.FindControl("txtCPSurname");
            DropDownList ddlPosition = (DropDownList)e.Item.FindControl("ddlPosition");
            DropDownList ddlDepartment = (DropDownList)e.Item.FindControl("ddlDepartment");
            Repeater rptContact = (Repeater)e.Item.FindControl("rptContact");
            Label lblContactID = (Label)e.Item.FindControl("lblContactID");
            Label lblContPerson = (Label)e.Item.FindControl("lblContPerson");
            LinkButton delete = (LinkButton)e.Item.FindControl("delete");
            Button addNewContact = (Button)e.Item.FindControl("addNewContact");
            UCContactPerson UCContactPerson = (UCContactPerson)e.Item.FindControl("UCContactPerson");
            UCCustomerContact UCContact_Detail = (UCCustomerContact)e.Item.FindControl("contact_test");

            if (dt_CUS_Contact_Detail != null)
            {
                dt_CUS_Contact_Detail.DefaultView.RowFilter = "CP_ID = '" + dr["CP_ID"].ToString() + "'";
                UCContact_Detail.Set_Rpt_Customers(dt_CUS_Contact_Detail.DefaultView.ToTable());
            }

            BL.Bind_BL_ThTitle(ddlTitle);

            lblCP_ID.Attributes["index"] = (e.Item.ItemIndex).ToString();
            if (!string.IsNullOrEmpty(dr["TI_ID"].ToString()))
            {
                ddlTitle.SelectedValue = dr["TI_ID"].ToString();
            }

            BL.Bind_BL_Department(ddlDepartment);
            lblCP_ID.Attributes["index"] = (e.Item.ItemIndex).ToString();
            if (!string.IsNullOrEmpty(dr["DEPARTMENT_ID"].ToString()))
            {
                ddlDepartment.SelectedValue = dr["DEPARTMENT_ID"].ToString();
            }

            Bind_Position(ddlPosition);

            lblCP_ID.Attributes["index"] = (e.Item.ItemIndex).ToString();
            if (!string.IsNullOrEmpty(dr["POSITION_ID"].ToString()))
            {
                ddlPosition.SelectedValue = dr["POSITION_ID"].ToString();
            }

            txtCPName.Text = dr["CP_Name"].ToString();
            txtCPSurname.Text = dr["CP_Surname"].ToString();

            delete.CommandArgument = (e.Item.ItemIndex).ToString();

            string CP_ID = dr["CP_ID"].ToString();
            UCContact_Detail.BindData_Contact(CP_ID);

            if (CP_ID != "")
            {
                lblContPerson.Text = dr["CP_ID"].ToString();
            }
            else
            {
                lblContPerson.Text = "0";
            }
           
        }
        protected void rptContactPerson_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            LinkButton delete = (LinkButton)e.Item.FindControl("delete");

            switch (e.CommandName)
            {
                case "delete":
                    DataTable dt = Current_Data();
                    
                    dt.Rows.RemoveAt(Convert.ToInt32(delete.CommandArgument));

                    rptContactPerson.DataSource = dt;
                    rptContactPerson.DataBind();

                    break;
            }
        }

        protected void btnNewContactPerson_Click(object sender, EventArgs e)
        {
            //Clear();
            ViewState["dt_CUS_Contact_H"] = null;
            ViewState["dt_CUS_Contact_Detail"] = null;
            DataTable dt = Current_Data();
            // อ่าน repeater
            dt.Rows.Add();
            rptContactPerson.DataSource = dt;
            rptContactPerson.DataBind();
        }
        public void Get_Current_Data_H_Detail()
        {
            ViewState["dt_CUS_Contact_H"] = null;
            ViewState["dt_CUS_Contact_Detail"] = null;
            DataTable dt = Current_Data();
        }

        protected DataTable Current_Data()
        {
            string sql = "select cp.CP_ID,cp.TI_ID, CP_Name, CP_Surname, cp.POSITION_ID, cp.DEPARTMENT_ID";
            sql += " from TB_CONTACT_PERSON as cp ";
            sql += " inner join TB_TITLE as t on cp.TI_ID = t.TI_ID ";
            sql += " where 0=1";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            DataRow DR;
            dt.Columns.Add("index");
            int i = 1;
            foreach (RepeaterItem item in rptContactPerson.Items)
            {
                DropDownList ddlTitle = (DropDownList)item.FindControl("ddlTitle");
                TextBox txtCPName = (TextBox)item.FindControl("txtCPName");
                TextBox txtCPSurname = (TextBox)item.FindControl("txtCPSurname");
                DropDownList ddlPosition = (DropDownList)item.FindControl("ddlPosition");
                DropDownList ddlDepartment = (DropDownList)item.FindControl("ddlDepartment");
                Label lblContPerson = (Label)item.FindControl("lblContPerson");
                UCContactPerson UCContactPerson = (UCContactPerson)item.FindControl("UCContactPerson");
                UCCustomerContact UCContact_Detail = (UCCustomerContact)item.FindControl("contact_test");
             
                DR = dt.NewRow();

                int id_Con = i;
                //DR["index"] = lblContPerson.Attributes["index"];
                DR["index"] = id_Con;
                if (lblContPerson.Text == "")
                {
                    DR["CP_ID"] = 0;
                }
                else
                {
                    DR["CP_ID"] = id_Con;
                    lblContPerson.Text = DR["CP_ID"].ToString();
                }
                if (ddlTitle.SelectedValue != "")
                {
                    DR["TI_ID"] = ddlTitle.SelectedIndex;
                }
                if (ddlPosition.SelectedValue != "")
                {
                    DR["POSITION_ID"] = ddlPosition.SelectedIndex;
                }
                if (ddlDepartment.SelectedValue != "")
                {
                    DR["DEPARTMENT_ID"] = ddlDepartment.SelectedIndex;
                }
                DR["CP_Name"] = txtCPName.Text;
                DR["CP_Surname"] = txtCPSurname.Text;
                
                dt.Rows.Add(DR);
                DataTable dt_GetContactDT_Detail =  UCContact_Detail.Get_ContactDT_Detail(Convert.ToInt32(DR["CP_ID"]));
              
                if (dt_CUS_Contact_Detail == null)
                {
                    if (dt_GetContactDT_Detail.Rows.Count > 0)
                    {
                        ViewState["dt_CUS_Contact_Detail"] = dt_GetContactDT_Detail;
                    }
                }
                else
                {
                    DataTable tem_dt = dt_CUS_Contact_Detail;
                    for (int j = 0; j <= dt_GetContactDT_Detail.Rows.Count - 1; j++)
                    {
                        DR = tem_dt.NewRow();
                        DR["CUS_Contact_ID"] = dt_GetContactDT_Detail.Rows[j]["CUS_Contact_ID"].ToString ();
                        DR["CP_ID"] = dt_GetContactDT_Detail.Rows[j]["CP_ID"].ToString();
                        DR["CT_TYPE_ID"] = dt_GetContactDT_Detail.Rows[j]["CT_TYPE_ID"].ToString();
                        DR["Contact_Detail"] = dt_GetContactDT_Detail.Rows[j]["Contact_Detail"].ToString();
                        DR["TYPE_NAME"] = dt_GetContactDT_Detail.Rows[j]["TYPE_NAME"].ToString();
                        tem_dt.Rows.Add(DR);
                    }
                    ViewState["dt_CUS_Contact_Detail"] = tem_dt;
                }
                i++;
            }
            ViewState["dt_CUS_Contact_H"] = dt;
            return dt;
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BL.Bind_BL_Position(ddlPosition, Convert.ToInt64(ddlDepartment.SelectedValue));
        }

        public void Bind_Position(DropDownList ddl)
        {
            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable("select POSITION_ID,POSITION_CODE,POSITION_NAME from TB_POSITION");

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("กรุณาเลือกตำแหน่ง...", ""));
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(dt.Rows[i]["POSITION_NAME"].ToString(), dt.Rows[i]["POSITION_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
        }
    }
}