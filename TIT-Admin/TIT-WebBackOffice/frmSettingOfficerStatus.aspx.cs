﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqDB.TABLE;
using LinqDB.ConnectDB;
using System.Data;

namespace TIT_WebBackoffice
{
    public partial class frmSettingOfficerStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind_Data();
        }

        public void Bind_Data()
        {
            string sql = "select STATUS_ID,STATUS_NAME from TB_OF_STATUS";

            DataTable dt = LinqDB.ConnectDB.SqlDB.ExecuteTable(sql);
            rptOFstatus.DataSource = dt;
            rptOFstatus.DataBind();
        }

        protected void rptOFstatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = (DataRowView)e.Item.DataItem;

            Label lblOFstatus = (Label)e.Item.FindControl("lblOFstatus");

            lblOFstatus.Text = dr["STATUS_NAME"].ToString();
        }

        protected void btnOFstatusSave_Click(object sender, EventArgs e)
        {
            if (validate() == true)
            {
                TransactionDB trans = new TransactionDB();
                TbOfStatusLinqDB StatuslLnq = new TbOfStatusLinqDB();

                StatuslLnq.GetDataByPK(Convert.ToInt16(lblOFstatusID.Text), trans.Trans);

                ExecuteDataInfo ret = new ExecuteDataInfo();
                StatuslLnq.STATUS_NAME = txtOFstatus.Text;
                if (StatuslLnq.STATUS_ID == 0)
                {
                    ret = StatuslLnq.InsertData("User", trans.Trans);
                }
                else
                {
                    ret = StatuslLnq.UpdateData("User", trans.Trans);
                }
                if (ret.IsSuccess == true)
                {
                    lblOFstatusID.Text = StatuslLnq.STATUS_ID.ToString();
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
            }
            else
            {
                alertmsg("ไม่สามารถบันทึกข้อมูลได้");
            }
            Clear();
            Bind_Data();
        }
        private void Clear()
        {
            txtOFstatus.Text = "";
            Bind_Data();
        }
        private void alertmsg(string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('" + msg + "');", true);
        }

        private bool validate()
        {
            bool ret = true;
            if (string.IsNullOrEmpty(txtOFstatus.Text))
            {
                alertmsg("กรุณาระบุสถานะพนักงาน");
                ret = false;
            }
            return ret;
        }

        protected void rptOFstatus_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label lblOFstatus = (Label)e.Item.FindControl("lblOFstatus");
            Label lblID = (Label)e.Item.FindControl("lblID");

            if (e.CommandName == "edit")
            {
                lblOFstatusID.Text = lblID.Text;
                txtOFstatus.Text = lblOFstatus.Text;
            }
            if (e.CommandName == "delete")
            {
                TransactionDB trans = new TransactionDB();
                TbOfStatusLinqDB StatuslLnq = new TbOfStatusLinqDB();

                ExecuteDataInfo ret = StatuslLnq.DeleteByPK(Convert.ToInt16(lblID.Text), trans.Trans);
                if (ret.IsSuccess == true)
                {
                    trans.CommitTransaction();
                }
                else
                {
                    trans.RollbackTransaction();
                }
                Bind_Data();
            }
        }
    }
}