Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_OF_CONTACT table LinqDB.
    '[Create by  on May, 22 2017]
    Public Class TbOfContactLinqDB
        Public sub TbOfContactLinqDB()

        End Sub 
        ' TB_OF_CONTACT
        Const _tableName As String = "TB_OF_CONTACT"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _CONTACT_ID As Long = 0
        Dim _FAM_ID As  System.Nullable(Of Long) 
        Dim _OF_ID As  System.Nullable(Of Long) 
        Dim _RF_ID As  System.Nullable(Of Long) 
        Dim _CT_TYPE_ID As  System.Nullable(Of Long) 
        Dim _CONTACT As  String  = ""
        Dim _CREATED_BY As  String  = ""
        Dim _CREATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_CONTACT_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property CONTACT_ID() As Long
            Get
                Return _CONTACT_ID
            End Get
            Set(ByVal value As Long)
               _CONTACT_ID = value
            End Set
        End Property 
        <Column(Storage:="_FAM_ID", DbType:="BigInt")>  _
        Public Property FAM_ID() As  System.Nullable(Of Long) 
            Get
                Return _FAM_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _FAM_ID = value
            End Set
        End Property 
        <Column(Storage:="_OF_ID", DbType:="BigInt")>  _
        Public Property OF_ID() As  System.Nullable(Of Long) 
            Get
                Return _OF_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _OF_ID = value
            End Set
        End Property 
        <Column(Storage:="_RF_ID", DbType:="BigInt")>  _
        Public Property RF_ID() As  System.Nullable(Of Long) 
            Get
                Return _RF_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _RF_ID = value
            End Set
        End Property 
        <Column(Storage:="_CT_TYPE_ID", DbType:="BigInt")>  _
        Public Property CT_TYPE_ID() As  System.Nullable(Of Long) 
            Get
                Return _CT_TYPE_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _CT_TYPE_ID = value
            End Set
        End Property 
        <Column(Storage:="_CONTACT", DbType:="VarChar(250)")>  _
        Public Property CONTACT() As  String 
            Get
                Return _CONTACT
            End Get
            Set(ByVal value As  String )
               _CONTACT = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100)")>  _
        Public Property CREATED_BY() As  String 
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As  String )
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime")>  _
        Public Property CREATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _CONTACT_ID = 0
            _FAM_ID = Nothing
            _OF_ID = Nothing
            _RF_ID = Nothing
            _CT_TYPE_ID = Nothing
            _CONTACT = ""
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_OF_CONTACT table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _CONTACT_ID = DB.GetNextID("CONTACT_ID",tableName, trans)
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_OF_CONTACT table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _CONTACT_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("CONTACT_ID = @_CONTACT_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_OF_CONTACT table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_OF_CONTACT table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cCONTACT_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_CONTACT_ID", cCONTACT_ID)
                Return doDelete("CONTACT_ID = @_CONTACT_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_OF_CONTACT by specified CONTACT_ID key is retrieved successfully.
        '/// <param name=cCONTACT_ID>The CONTACT_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cCONTACT_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_CONTACT_ID", cCONTACT_ID)
            Return doChkData("CONTACT_ID = @_CONTACT_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_OF_CONTACT by specified CONTACT_ID key is retrieved successfully.
        '/// <param name=cCONTACT_ID>The CONTACT_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cCONTACT_ID As Long, trans As SQLTransaction) As TbOfContactLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_CONTACT_ID", cCONTACT_ID)
            Return doGetData("CONTACT_ID = @_CONTACT_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_OF_CONTACT by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_OF_CONTACT table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_OF_CONTACT table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_OF_CONTACT table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(9) As SqlParameter
            cmbParam(0) = New SqlParameter("@_CONTACT_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _CONTACT_ID

            cmbParam(1) = New SqlParameter("@_FAM_ID", SqlDbType.BigInt)
            If _FAM_ID IsNot Nothing Then 
                cmbParam(1).Value = _FAM_ID.Value
            Else
                cmbParam(1).Value = DBNull.value
            End IF

            cmbParam(2) = New SqlParameter("@_OF_ID", SqlDbType.BigInt)
            If _OF_ID IsNot Nothing Then 
                cmbParam(2).Value = _OF_ID.Value
            Else
                cmbParam(2).Value = DBNull.value
            End IF

            cmbParam(3) = New SqlParameter("@_RF_ID", SqlDbType.BigInt)
            If _RF_ID IsNot Nothing Then 
                cmbParam(3).Value = _RF_ID.Value
            Else
                cmbParam(3).Value = DBNull.value
            End IF

            cmbParam(4) = New SqlParameter("@_CT_TYPE_ID", SqlDbType.BigInt)
            If _CT_TYPE_ID IsNot Nothing Then 
                cmbParam(4).Value = _CT_TYPE_ID.Value
            Else
                cmbParam(4).Value = DBNull.value
            End IF

            cmbParam(5) = New SqlParameter("@_CONTACT", SqlDbType.VarChar)
            If _CONTACT.Trim <> "" Then 
                cmbParam(5).Value = _CONTACT
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            If _CREATED_BY.Trim <> "" Then 
                cmbParam(6).Value = _CREATED_BY
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            If _CREATED_DATE.Value.Year > 1 Then 
                cmbParam(7).Value = _CREATED_DATE.Value
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(8).Value = _UPDATED_BY
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(9).Value = _UPDATED_DATE.Value
            Else
                cmbParam(9).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_OF_CONTACT by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("CONTACT_ID")) = False Then _CONTACT_ID = Convert.ToInt64(Rdr("CONTACT_ID"))
                        If Convert.IsDBNull(Rdr("FAM_ID")) = False Then _FAM_ID = Convert.ToInt64(Rdr("FAM_ID"))
                        If Convert.IsDBNull(Rdr("OF_ID")) = False Then _OF_ID = Convert.ToInt64(Rdr("OF_ID"))
                        If Convert.IsDBNull(Rdr("RF_ID")) = False Then _RF_ID = Convert.ToInt64(Rdr("RF_ID"))
                        If Convert.IsDBNull(Rdr("CT_TYPE_ID")) = False Then _CT_TYPE_ID = Convert.ToInt64(Rdr("CT_TYPE_ID"))
                        If Convert.IsDBNull(Rdr("CONTACT")) = False Then _CONTACT = Rdr("CONTACT").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_OF_CONTACT by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbOfContactLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("CONTACT_ID")) = False Then _CONTACT_ID = Convert.ToInt64(Rdr("CONTACT_ID"))
                        If Convert.IsDBNull(Rdr("FAM_ID")) = False Then _FAM_ID = Convert.ToInt64(Rdr("FAM_ID"))
                        If Convert.IsDBNull(Rdr("OF_ID")) = False Then _OF_ID = Convert.ToInt64(Rdr("OF_ID"))
                        If Convert.IsDBNull(Rdr("RF_ID")) = False Then _RF_ID = Convert.ToInt64(Rdr("RF_ID"))
                        If Convert.IsDBNull(Rdr("CT_TYPE_ID")) = False Then _CT_TYPE_ID = Convert.ToInt64(Rdr("CT_TYPE_ID"))
                        If Convert.IsDBNull(Rdr("CONTACT")) = False Then _CONTACT = Rdr("CONTACT").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_OF_CONTACT
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (CONTACT_ID, FAM_ID, OF_ID, RF_ID, CT_TYPE_ID, CONTACT, CREATED_BY, CREATED_DATE)"
                Sql += " OUTPUT INSERTED.CONTACT_ID, INSERTED.FAM_ID, INSERTED.OF_ID, INSERTED.RF_ID, INSERTED.CT_TYPE_ID, INSERTED.CONTACT, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_CONTACT_ID" & ", "
                sql += "@_FAM_ID" & ", "
                sql += "@_OF_ID" & ", "
                sql += "@_RF_ID" & ", "
                sql += "@_CT_TYPE_ID" & ", "
                sql += "@_CONTACT" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_OF_CONTACT
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "FAM_ID = " & "@_FAM_ID" & ", "
                Sql += "OF_ID = " & "@_OF_ID" & ", "
                Sql += "RF_ID = " & "@_RF_ID" & ", "
                Sql += "CT_TYPE_ID = " & "@_CT_TYPE_ID" & ", "
                Sql += "CONTACT = " & "@_CONTACT" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_OF_CONTACT
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_OF_CONTACT
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT CONTACT_ID, FAM_ID, OF_ID, RF_ID, CT_TYPE_ID, CONTACT, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
