Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_CUSTOMER table LinqDB.
    '[Create by  on June, 2 2017]
    Public Class TbCustomerLinqDB
        Public sub TbCustomerLinqDB()

        End Sub 
        ' TB_CUSTOMER
        Const _tableName As String = "TB_CUSTOMER"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _CUS_ID As Long = 0
        Dim _CUS_CODE As  String  = ""
        Dim _CUS_NAME As  String  = ""
        Dim _CUS_TYPE_ID As  System.Nullable(Of Long) 
        Dim _CUS_CREATE_YEAR As  String  = ""
        Dim _CUS_CREATE_MONTH As  String  = ""
        Dim _CUS_RUNNING_NO As  String  = ""
        Dim _CUS_TAXID As  String  = ""
        Dim _CUS_ADDRESS As  String  = ""
        Dim _PROVINCE_ID As  String  = ""
        Dim _AMPHUR_ID As  String  = ""
        Dim _TUMBOL_ID As  String  = ""
        Dim _POSTAL_CODE As  String  = ""
        Dim _CUSTOMER_DETAIL As  String  = ""
        Dim _CREATED_BY As  String  = ""
        Dim _CREATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_CUS_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property CUS_ID() As Long
            Get
                Return _CUS_ID
            End Get
            Set(ByVal value As Long)
               _CUS_ID = value
            End Set
        End Property 
        <Column(Storage:="_CUS_CODE", DbType:="NVarChar(20)")>  _
        Public Property CUS_CODE() As  String 
            Get
                Return _CUS_CODE
            End Get
            Set(ByVal value As  String )
               _CUS_CODE = value
            End Set
        End Property 
        <Column(Storage:="_CUS_NAME", DbType:="VarChar(150)")>  _
        Public Property CUS_NAME() As  String 
            Get
                Return _CUS_NAME
            End Get
            Set(ByVal value As  String )
               _CUS_NAME = value
            End Set
        End Property 
        <Column(Storage:="_CUS_TYPE_ID", DbType:="BigInt")>  _
        Public Property CUS_TYPE_ID() As  System.Nullable(Of Long) 
            Get
                Return _CUS_TYPE_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _CUS_TYPE_ID = value
            End Set
        End Property 
        <Column(Storage:="_CUS_CREATE_YEAR", DbType:="NVarChar(8)")>  _
        Public Property CUS_CREATE_YEAR() As  String 
            Get
                Return _CUS_CREATE_YEAR
            End Get
            Set(ByVal value As  String )
               _CUS_CREATE_YEAR = value
            End Set
        End Property 
        <Column(Storage:="_CUS_CREATE_MONTH", DbType:="NVarChar(4)")>  _
        Public Property CUS_CREATE_MONTH() As  String 
            Get
                Return _CUS_CREATE_MONTH
            End Get
            Set(ByVal value As  String )
               _CUS_CREATE_MONTH = value
            End Set
        End Property 
        <Column(Storage:="_CUS_RUNNING_NO", DbType:="NVarChar(100)")>  _
        Public Property CUS_RUNNING_NO() As  String 
            Get
                Return _CUS_RUNNING_NO
            End Get
            Set(ByVal value As  String )
               _CUS_RUNNING_NO = value
            End Set
        End Property 
        <Column(Storage:="_CUS_TAXID", DbType:="VarChar(50)")>  _
        Public Property CUS_TAXID() As  String 
            Get
                Return _CUS_TAXID
            End Get
            Set(ByVal value As  String )
               _CUS_TAXID = value
            End Set
        End Property 
        <Column(Storage:="_CUS_ADDRESS", DbType:="VarChar(500)")>  _
        Public Property CUS_ADDRESS() As  String 
            Get
                Return _CUS_ADDRESS
            End Get
            Set(ByVal value As  String )
               _CUS_ADDRESS = value
            End Set
        End Property 
        <Column(Storage:="_PROVINCE_ID", DbType:="NVarChar(4)")>  _
        Public Property PROVINCE_ID() As  String 
            Get
                Return _PROVINCE_ID
            End Get
            Set(ByVal value As  String )
               _PROVINCE_ID = value
            End Set
        End Property 
        <Column(Storage:="_AMPHUR_ID", DbType:="NVarChar(8)")>  _
        Public Property AMPHUR_ID() As  String 
            Get
                Return _AMPHUR_ID
            End Get
            Set(ByVal value As  String )
               _AMPHUR_ID = value
            End Set
        End Property 
        <Column(Storage:="_TUMBOL_ID", DbType:="NVarChar(12)")>  _
        Public Property TUMBOL_ID() As  String 
            Get
                Return _TUMBOL_ID
            End Get
            Set(ByVal value As  String )
               _TUMBOL_ID = value
            End Set
        End Property 
        <Column(Storage:="_POSTAL_CODE", DbType:="VarChar(5)")>  _
        Public Property POSTAL_CODE() As  String 
            Get
                Return _POSTAL_CODE
            End Get
            Set(ByVal value As  String )
               _POSTAL_CODE = value
            End Set
        End Property 
        <Column(Storage:="_CUSTOMER_DETAIL", DbType:="VarChar(500)")>  _
        Public Property CUSTOMER_DETAIL() As  String 
            Get
                Return _CUSTOMER_DETAIL
            End Get
            Set(ByVal value As  String )
               _CUSTOMER_DETAIL = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100)")>  _
        Public Property CREATED_BY() As  String 
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As  String )
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime")>  _
        Public Property CREATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _CUS_ID = 0
            _CUS_CODE = ""
            _CUS_NAME = ""
            _CUS_TYPE_ID = Nothing
            _CUS_CREATE_YEAR = ""
            _CUS_CREATE_MONTH = ""
            _CUS_RUNNING_NO = ""
            _CUS_TAXID = ""
            _CUS_ADDRESS = ""
            _PROVINCE_ID = ""
            _AMPHUR_ID = ""
            _TUMBOL_ID = ""
            _POSTAL_CODE = ""
            _CUSTOMER_DETAIL = ""
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_CUSTOMER table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_CUSTOMER table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _CUS_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("CUS_ID = @_CUS_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_CUSTOMER table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_CUSTOMER table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cCUS_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_CUS_ID", cCUS_ID)
                Return doDelete("CUS_ID = @_CUS_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_CUSTOMER by specified CUS_ID key is retrieved successfully.
        '/// <param name=cCUS_ID>The CUS_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cCUS_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_CUS_ID", cCUS_ID)
            Return doChkData("CUS_ID = @_CUS_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_CUSTOMER by specified CUS_ID key is retrieved successfully.
        '/// <param name=cCUS_ID>The CUS_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cCUS_ID As Long, trans As SQLTransaction) As TbCustomerLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_CUS_ID", cCUS_ID)
            Return doGetData("CUS_ID = @_CUS_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_CUSTOMER by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_CUSTOMER table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _CUS_ID = dt.Rows(0)("CUS_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_CUSTOMER table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_CUSTOMER table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(17) As SqlParameter
            cmbParam(0) = New SqlParameter("@_CUS_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _CUS_ID

            cmbParam(1) = New SqlParameter("@_CUS_CODE", SqlDbType.NVarChar)
            If _CUS_CODE.Trim <> "" Then 
                cmbParam(1).Value = _CUS_CODE
            Else
                cmbParam(1).Value = DBNull.value
            End IF

            cmbParam(2) = New SqlParameter("@_CUS_NAME", SqlDbType.VarChar)
            If _CUS_NAME.Trim <> "" Then 
                cmbParam(2).Value = _CUS_NAME
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_CUS_TYPE_ID", SqlDbType.BigInt)
            If _CUS_TYPE_ID IsNot Nothing Then 
                cmbParam(3).Value = _CUS_TYPE_ID.Value
            Else
                cmbParam(3).Value = DBNull.value
            End IF

            cmbParam(4) = New SqlParameter("@_CUS_CREATE_YEAR", SqlDbType.NVarChar)
            If _CUS_CREATE_YEAR.Trim <> "" Then 
                cmbParam(4).Value = _CUS_CREATE_YEAR
            Else
                cmbParam(4).Value = DBNull.value
            End IF

            cmbParam(5) = New SqlParameter("@_CUS_CREATE_MONTH", SqlDbType.NVarChar)
            If _CUS_CREATE_MONTH.Trim <> "" Then 
                cmbParam(5).Value = _CUS_CREATE_MONTH
            Else
                cmbParam(5).Value = DBNull.value
            End IF

            cmbParam(6) = New SqlParameter("@_CUS_RUNNING_NO", SqlDbType.NVarChar)
            If _CUS_RUNNING_NO.Trim <> "" Then 
                cmbParam(6).Value = _CUS_RUNNING_NO
            Else
                cmbParam(6).Value = DBNull.value
            End IF

            cmbParam(7) = New SqlParameter("@_CUS_TAXID", SqlDbType.VarChar)
            If _CUS_TAXID.Trim <> "" Then 
                cmbParam(7).Value = _CUS_TAXID
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_CUS_ADDRESS", SqlDbType.VarChar)
            If _CUS_ADDRESS.Trim <> "" Then 
                cmbParam(8).Value = _CUS_ADDRESS
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_PROVINCE_ID", SqlDbType.NVarChar)
            If _PROVINCE_ID.Trim <> "" Then 
                cmbParam(9).Value = _PROVINCE_ID
            Else
                cmbParam(9).Value = DBNull.value
            End IF

            cmbParam(10) = New SqlParameter("@_AMPHUR_ID", SqlDbType.NVarChar)
            If _AMPHUR_ID.Trim <> "" Then 
                cmbParam(10).Value = _AMPHUR_ID
            Else
                cmbParam(10).Value = DBNull.value
            End IF

            cmbParam(11) = New SqlParameter("@_TUMBOL_ID", SqlDbType.NVarChar)
            If _TUMBOL_ID.Trim <> "" Then 
                cmbParam(11).Value = _TUMBOL_ID
            Else
                cmbParam(11).Value = DBNull.value
            End IF

            cmbParam(12) = New SqlParameter("@_POSTAL_CODE", SqlDbType.VarChar)
            If _POSTAL_CODE.Trim <> "" Then 
                cmbParam(12).Value = _POSTAL_CODE
            Else
                cmbParam(12).Value = DBNull.value
            End If

            cmbParam(13) = New SqlParameter("@_CUSTOMER_DETAIL", SqlDbType.VarChar)
            If _CUSTOMER_DETAIL.Trim <> "" Then 
                cmbParam(13).Value = _CUSTOMER_DETAIL
            Else
                cmbParam(13).Value = DBNull.value
            End If

            cmbParam(14) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            If _CREATED_BY.Trim <> "" Then 
                cmbParam(14).Value = _CREATED_BY
            Else
                cmbParam(14).Value = DBNull.value
            End If

            cmbParam(15) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            If _CREATED_DATE.Value.Year > 1 Then 
                cmbParam(15).Value = _CREATED_DATE.Value
            Else
                cmbParam(15).Value = DBNull.value
            End If

            cmbParam(16) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(16).Value = _UPDATED_BY
            Else
                cmbParam(16).Value = DBNull.value
            End If

            cmbParam(17) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(17).Value = _UPDATED_DATE.Value
            Else
                cmbParam(17).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_CUSTOMER by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("CUS_ID")) = False Then _CUS_ID = Convert.ToInt64(Rdr("CUS_ID"))
                        If Convert.IsDBNull(Rdr("CUS_Code")) = False Then _CUS_Code = Rdr("CUS_Code").ToString()
                        If Convert.IsDBNull(Rdr("CUS_Name")) = False Then _CUS_Name = Rdr("CUS_Name").ToString()
                        If Convert.IsDBNull(Rdr("CUS_Type_ID")) = False Then _CUS_Type_ID = Convert.ToInt64(Rdr("CUS_Type_ID"))
                        If Convert.IsDBNull(Rdr("CUS_Create_Year")) = False Then _CUS_Create_Year = Rdr("CUS_Create_Year").ToString()
                        If Convert.IsDBNull(Rdr("CUS_Create_Month")) = False Then _CUS_Create_Month = Rdr("CUS_Create_Month").ToString()
                        If Convert.IsDBNull(Rdr("CUS_Running_No")) = False Then _CUS_Running_No = Rdr("CUS_Running_No").ToString()
                        If Convert.IsDBNull(Rdr("CUS_TaxID")) = False Then _CUS_TaxID = Rdr("CUS_TaxID").ToString()
                        If Convert.IsDBNull(Rdr("CUS_Address")) = False Then _CUS_Address = Rdr("CUS_Address").ToString()
                        If Convert.IsDBNull(Rdr("PROVINCE_ID")) = False Then _PROVINCE_ID = Rdr("PROVINCE_ID").ToString()
                        If Convert.IsDBNull(Rdr("AMPHUR_ID")) = False Then _AMPHUR_ID = Rdr("AMPHUR_ID").ToString()
                        If Convert.IsDBNull(Rdr("TUMBOL_ID")) = False Then _TUMBOL_ID = Rdr("TUMBOL_ID").ToString()
                        If Convert.IsDBNull(Rdr("Postal_Code")) = False Then _Postal_Code = Rdr("Postal_Code").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Detail")) = False Then _Customer_Detail = Rdr("Customer_Detail").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_CUSTOMER by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbCustomerLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("CUS_ID")) = False Then _CUS_ID = Convert.ToInt64(Rdr("CUS_ID"))
                        If Convert.IsDBNull(Rdr("CUS_Code")) = False Then _CUS_Code = Rdr("CUS_Code").ToString()
                        If Convert.IsDBNull(Rdr("CUS_Name")) = False Then _CUS_Name = Rdr("CUS_Name").ToString()
                        If Convert.IsDBNull(Rdr("CUS_Type_ID")) = False Then _CUS_Type_ID = Convert.ToInt64(Rdr("CUS_Type_ID"))
                        If Convert.IsDBNull(Rdr("CUS_Create_Year")) = False Then _CUS_Create_Year = Rdr("CUS_Create_Year").ToString()
                        If Convert.IsDBNull(Rdr("CUS_Create_Month")) = False Then _CUS_Create_Month = Rdr("CUS_Create_Month").ToString()
                        If Convert.IsDBNull(Rdr("CUS_Running_No")) = False Then _CUS_Running_No = Rdr("CUS_Running_No").ToString()
                        If Convert.IsDBNull(Rdr("CUS_TaxID")) = False Then _CUS_TaxID = Rdr("CUS_TaxID").ToString()
                        If Convert.IsDBNull(Rdr("CUS_Address")) = False Then _CUS_Address = Rdr("CUS_Address").ToString()
                        If Convert.IsDBNull(Rdr("PROVINCE_ID")) = False Then _PROVINCE_ID = Rdr("PROVINCE_ID").ToString()
                        If Convert.IsDBNull(Rdr("AMPHUR_ID")) = False Then _AMPHUR_ID = Rdr("AMPHUR_ID").ToString()
                        If Convert.IsDBNull(Rdr("TUMBOL_ID")) = False Then _TUMBOL_ID = Rdr("TUMBOL_ID").ToString()
                        If Convert.IsDBNull(Rdr("Postal_Code")) = False Then _Postal_Code = Rdr("Postal_Code").ToString()
                        If Convert.IsDBNull(Rdr("Customer_Detail")) = False Then _Customer_Detail = Rdr("Customer_Detail").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_CUSTOMER
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (CUS_CODE, CUS_NAME, CUS_TYPE_ID, CUS_CREATE_YEAR, CUS_CREATE_MONTH, CUS_RUNNING_NO, CUS_TAXID, CUS_ADDRESS, PROVINCE_ID, AMPHUR_ID, TUMBOL_ID, POSTAL_CODE, CUSTOMER_DETAIL, CREATED_BY, CREATED_DATE)"
                Sql += " OUTPUT INSERTED.CUS_ID, INSERTED.CUS_CODE, INSERTED.CUS_NAME, INSERTED.CUS_TYPE_ID, INSERTED.CUS_CREATE_YEAR, INSERTED.CUS_CREATE_MONTH, INSERTED.CUS_RUNNING_NO, INSERTED.CUS_TAXID, INSERTED.CUS_ADDRESS, INSERTED.PROVINCE_ID, INSERTED.AMPHUR_ID, INSERTED.TUMBOL_ID, INSERTED.POSTAL_CODE, INSERTED.CUSTOMER_DETAIL, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_CUS_CODE" & ", "
                sql += "@_CUS_NAME" & ", "
                sql += "@_CUS_TYPE_ID" & ", "
                sql += "@_CUS_CREATE_YEAR" & ", "
                sql += "@_CUS_CREATE_MONTH" & ", "
                sql += "@_CUS_RUNNING_NO" & ", "
                sql += "@_CUS_TAXID" & ", "
                sql += "@_CUS_ADDRESS" & ", "
                sql += "@_PROVINCE_ID" & ", "
                sql += "@_AMPHUR_ID" & ", "
                sql += "@_TUMBOL_ID" & ", "
                sql += "@_POSTAL_CODE" & ", "
                sql += "@_CUSTOMER_DETAIL" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_CUSTOMER
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "CUS_CODE = " & "@_CUS_CODE" & ", "
                Sql += "CUS_NAME = " & "@_CUS_NAME" & ", "
                Sql += "CUS_TYPE_ID = " & "@_CUS_TYPE_ID" & ", "
                Sql += "CUS_CREATE_YEAR = " & "@_CUS_CREATE_YEAR" & ", "
                Sql += "CUS_CREATE_MONTH = " & "@_CUS_CREATE_MONTH" & ", "
                Sql += "CUS_RUNNING_NO = " & "@_CUS_RUNNING_NO" & ", "
                Sql += "CUS_TAXID = " & "@_CUS_TAXID" & ", "
                Sql += "CUS_ADDRESS = " & "@_CUS_ADDRESS" & ", "
                Sql += "PROVINCE_ID = " & "@_PROVINCE_ID" & ", "
                Sql += "AMPHUR_ID = " & "@_AMPHUR_ID" & ", "
                Sql += "TUMBOL_ID = " & "@_TUMBOL_ID" & ", "
                Sql += "POSTAL_CODE = " & "@_POSTAL_CODE" & ", "
                Sql += "CUSTOMER_DETAIL = " & "@_CUSTOMER_DETAIL" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_CUSTOMER
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_CUSTOMER
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT CUS_ID, CUS_CODE, CUS_NAME, CUS_TYPE_ID, CUS_CREATE_YEAR, CUS_CREATE_MONTH, CUS_RUNNING_NO, CUS_TAXID, CUS_ADDRESS, PROVINCE_ID, AMPHUR_ID, TUMBOL_ID, POSTAL_CODE, CUSTOMER_DETAIL, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
