Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_AMPHUR table LinqDB.
    '[Create by  on April, 5 2017]
    Public Class TbAmphurLinqDB
        Public sub TbAmphurLinqDB()

        End Sub 
        ' TB_AMPHUR
        Const _tableName As String = "TB_AMPHUR"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _AMPHUR_ID As String = ""
        Dim _AMPHUR_NAME As  String  = ""
        Dim _PROVINCE_ID As  String  = ""
        Dim _LAT As  System.Nullable(Of Double) 
        Dim _LON As  System.Nullable(Of Double) 

        'Generate Field Property 
        <Column(Storage:="_AMPHUR_ID", DbType:="NVarChar(8) NOT NULL ",CanBeNull:=false)>  _
        Public Property AMPHUR_ID() As String
            Get
                Return _AMPHUR_ID
            End Get
            Set(ByVal value As String)
               _AMPHUR_ID = value
            End Set
        End Property 
        <Column(Storage:="_AMPHUR_NAME", DbType:="NVarChar(510)")>  _
        Public Property AMPHUR_NAME() As  String 
            Get
                Return _AMPHUR_NAME
            End Get
            Set(ByVal value As  String )
               _AMPHUR_NAME = value
            End Set
        End Property 
        <Column(Storage:="_PROVINCE_ID", DbType:="NVarChar(4)")>  _
        Public Property PROVINCE_ID() As  String 
            Get
                Return _PROVINCE_ID
            End Get
            Set(ByVal value As  String )
               _PROVINCE_ID = value
            End Set
        End Property 
        <Column(Storage:="_LAT", DbType:="Float")>  _
        Public Property LAT() As  System.Nullable(Of Double) 
            Get
                Return _LAT
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _LAT = value
            End Set
        End Property 
        <Column(Storage:="_LON", DbType:="Float")>  _
        Public Property LON() As  System.Nullable(Of Double) 
            Get
                Return _LON
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _LON = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _AMPHUR_ID = ""
            _AMPHUR_NAME = ""
            _PROVINCE_ID = ""
            _LAT = Nothing
            _LON = Nothing
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_AMPHUR table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _AMPHUR_ID = DB.GetNextID("AMPHUR_ID",tableName, trans)
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_AMPHUR table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _AMPHUR_ID > 0 Then 

                    Return doUpdate("AMPHUR_ID = @_AMPHUR_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_AMPHUR table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_AMPHUR table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cAMPHUR_ID As String, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_AMPHUR_ID", cAMPHUR_ID)
                Return doDelete("AMPHUR_ID = @_AMPHUR_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_AMPHUR by specified AMPHUR_ID key is retrieved successfully.
        '/// <param name=cAMPHUR_ID>The AMPHUR_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cAMPHUR_ID As String, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_AMPHUR_ID", cAMPHUR_ID)
            Return doChkData("AMPHUR_ID = @_AMPHUR_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_AMPHUR by specified AMPHUR_ID key is retrieved successfully.
        '/// <param name=cAMPHUR_ID>The AMPHUR_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cAMPHUR_ID As String, trans As SQLTransaction) As TbAmphurLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_AMPHUR_ID", cAMPHUR_ID)
            Return doGetData("AMPHUR_ID = @_AMPHUR_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_AMPHUR by specified PROVINCE_ID key is retrieved successfully.
        '/// <param name=cPROVINCE_ID>The PROVINCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPROVINCE_ID(cPROVINCE_ID As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_PROVINCE_ID", cPROVINCE_ID) 
            Return doChkData("PROVINCE_ID = @_PROVINCE_ID", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_AMPHUR by specified PROVINCE_ID key is retrieved successfully.
        '/// <param name=cPROVINCE_ID>The PROVINCE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByPROVINCE_ID(cPROVINCE_ID As String, cAMPHUR_ID As String, trans As SQLTransaction) As Boolean
            Dim cmdPara(2)  As SQLParameter
            cmdPara(0) = DB.SetText("@_PROVINCE_ID", cPROVINCE_ID) 
            cmdPara(1) = DB.SetBigInt("@_AMPHUR_ID", cAMPHUR_ID) 
            Return doChkData("PROVINCE_ID = @_PROVINCE_ID And AMPHUR_ID <> @_AMPHUR_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_AMPHUR by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_AMPHUR table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_AMPHUR table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_AMPHUR table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(4) As SqlParameter
            cmbParam(0) = New SqlParameter("@_AMPHUR_ID", SqlDbType.NVarChar)
            cmbParam(0).Value = _AMPHUR_ID

            cmbParam(1) = New SqlParameter("@_AMPHUR_NAME", SqlDbType.NVarChar)
            If _AMPHUR_NAME.Trim <> "" Then 
                cmbParam(1).Value = _AMPHUR_NAME
            Else
                cmbParam(1).Value = DBNull.value
            End IF

            cmbParam(2) = New SqlParameter("@_PROVINCE_ID", SqlDbType.NVarChar)
            If _PROVINCE_ID.Trim <> "" Then 
                cmbParam(2).Value = _PROVINCE_ID
            Else
                cmbParam(2).Value = DBNull.value
            End IF

            cmbParam(3) = New SqlParameter("@_LAT", SqlDbType.Float)
            If _LAT IsNot Nothing Then 
                cmbParam(3).Value = _LAT.Value
            Else
                cmbParam(3).Value = DBNull.value
            End IF

            cmbParam(4) = New SqlParameter("@_LON", SqlDbType.Float)
            If _LON IsNot Nothing Then 
                cmbParam(4).Value = _LON.Value
            Else
                cmbParam(4).Value = DBNull.value
            End IF

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_AMPHUR by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("AMPHUR_ID")) = False Then _AMPHUR_ID = Rdr("AMPHUR_ID").ToString()
                        If Convert.IsDBNull(Rdr("AMPHUR_NAME")) = False Then _AMPHUR_NAME = Rdr("AMPHUR_NAME").ToString()
                        If Convert.IsDBNull(Rdr("PROVINCE_ID")) = False Then _PROVINCE_ID = Rdr("PROVINCE_ID").ToString()
                        If Convert.IsDBNull(Rdr("LAT")) = False Then _LAT = Convert.ToDouble(Rdr("LAT"))
                        If Convert.IsDBNull(Rdr("LON")) = False Then _LON = Convert.ToDouble(Rdr("LON"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_AMPHUR by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbAmphurLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("AMPHUR_ID")) = False Then _AMPHUR_ID = Rdr("AMPHUR_ID").ToString()
                        If Convert.IsDBNull(Rdr("AMPHUR_NAME")) = False Then _AMPHUR_NAME = Rdr("AMPHUR_NAME").ToString()
                        If Convert.IsDBNull(Rdr("PROVINCE_ID")) = False Then _PROVINCE_ID = Rdr("PROVINCE_ID").ToString()
                        If Convert.IsDBNull(Rdr("LAT")) = False Then _LAT = Convert.ToDouble(Rdr("LAT"))
                        If Convert.IsDBNull(Rdr("LON")) = False Then _LON = Convert.ToDouble(Rdr("LON"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_AMPHUR
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (AMPHUR_ID, AMPHUR_NAME, PROVINCE_ID, LAT, LON)"
                Sql += " OUTPUT INSERTED.AMPHUR_ID, INSERTED.AMPHUR_NAME, INSERTED.PROVINCE_ID, INSERTED.LAT, INSERTED.LON"
                Sql += " VALUES("
                sql += "@_AMPHUR_ID" & ", "
                sql += "@_AMPHUR_NAME" & ", "
                sql += "@_PROVINCE_ID" & ", "
                sql += "@_LAT" & ", "
                sql += "@_LON"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_AMPHUR
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "AMPHUR_NAME = " & "@_AMPHUR_NAME" & ", "
                Sql += "PROVINCE_ID = " & "@_PROVINCE_ID" & ", "
                Sql += "LAT = " & "@_LAT" & ", "
                Sql += "LON = " & "@_LON" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_AMPHUR
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_AMPHUR
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT AMPHUR_ID, AMPHUR_NAME, PROVINCE_ID, LAT, LON FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
