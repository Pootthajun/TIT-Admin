Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_CONTACT_PERSON table LinqDB.
    '[Create by  on April, 28 2017]
    Public Class TbContactPersonLinqDB
        Public sub TbContactPersonLinqDB()

        End Sub 
        ' TB_CONTACT_PERSON
        Const _tableName As String = "TB_CONTACT_PERSON"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _CP_ID As Long = 0
        Dim _TI_ID As  System.Nullable(Of Long) 
        Dim _CP_NAME As  String  = ""
        Dim _CP_SURNAME As String = ""
        Dim _POSITION_ID As System.Nullable(Of Long)
        Dim _DEPARTMENT_ID As System.Nullable(Of Long)
        'Dim _CP_POSITION As  String  = ""
        'Dim _CP_DEPARTMENT As  String  = ""
        Dim _CUS_ID As  System.Nullable(Of Long) 
        Dim _BRN_ID As  System.Nullable(Of Long) 
        Dim _CREATED_BY As  String  = ""
        Dim _CREATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_CP_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property CP_ID() As Long
            Get
                Return _CP_ID
            End Get
            Set(ByVal value As Long)
               _CP_ID = value
            End Set
        End Property 
        <Column(Storage:="_TI_ID", DbType:="BigInt")>  _
        Public Property TI_ID() As  System.Nullable(Of Long) 
            Get
                Return _TI_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _TI_ID = value
            End Set
        End Property 
        <Column(Storage:="_CP_NAME", DbType:="VarChar(50)")>  _
        Public Property CP_NAME() As  String 
            Get
                Return _CP_NAME
            End Get
            Set(ByVal value As  String )
               _CP_NAME = value
            End Set
        End Property 
        <Column(Storage:="_CP_SURNAME", DbType:="VarChar(50)")>  _
        Public Property CP_SURNAME() As  String 
            Get
                Return _CP_SURNAME
            End Get
            Set(ByVal value As  String )
               _CP_SURNAME = value
            End Set
        End Property
        <Column(Storage:="_POSITION_ID", DbType:="BigInt")>
        Public Property POSITION_ID() As System.Nullable(Of Long)
            Get
                Return _POSITION_ID
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _POSITION_ID = value
            End Set
        End Property
        <Column(Storage:="_DEPARTMENT_ID", DbType:="BigInt")>
        Public Property DEPARTMENT_ID() As System.Nullable(Of Long)
            Get
                Return _DEPARTMENT_ID
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _DEPARTMENT_ID = value
            End Set
        End Property
        <Column(Storage:="_CUS_ID", DbType:="BigInt")>  _
        Public Property CUS_ID() As  System.Nullable(Of Long) 
            Get
                Return _CUS_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _CUS_ID = value
            End Set
        End Property 
        <Column(Storage:="_BRN_ID", DbType:="BigInt")>  _
        Public Property BRN_ID() As  System.Nullable(Of Long) 
            Get
                Return _BRN_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _BRN_ID = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100)")>  _
        Public Property CREATED_BY() As  String 
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As  String )
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime")>  _
        Public Property CREATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _CP_ID = 0
            _TI_ID = Nothing
            _CP_NAME = ""
            _CP_SURNAME = ""
            _POSITION_ID = Nothing
            _DEPARTMENT_ID = Nothing
            _CUS_ID = Nothing
            _BRN_ID = Nothing
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_CONTACT_PERSON table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _CP_ID = DB.GetNextID("CP_ID",tableName, trans)
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_CONTACT_PERSON table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _CP_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("CP_ID = @_CP_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_CONTACT_PERSON table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_CONTACT_PERSON table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cCP_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_CP_ID", cCP_ID)
                Return doDelete("CUS_ID = @_CP_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_CONTACT_PERSON by specified CP_ID key is retrieved successfully.
        '/// <param name=cCP_ID>The CP_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cCP_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_CP_ID", cCP_ID)
            Return doChkData("CP_ID = @_CP_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_CONTACT_PERSON by specified CP_ID key is retrieved successfully.
        '/// <param name=cCP_ID>The CP_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cCP_ID As Long, trans As SQLTransaction) As TbContactPersonLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_CP_ID", cCP_ID)
            Return doGetData("CP_ID = @_CP_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_CONTACT_PERSON by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_CONTACT_PERSON table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_CONTACT_PERSON table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_CONTACT_PERSON table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(11) As SqlParameter
            cmbParam(0) = New SqlParameter("@_CP_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _CP_ID

            cmbParam(1) = New SqlParameter("@_TI_ID", SqlDbType.BigInt)
            If _TI_ID IsNot Nothing Then 
                cmbParam(1).Value = _TI_ID.Value
            Else
                cmbParam(1).Value = DBNull.value
            End IF

            cmbParam(2) = New SqlParameter("@_CP_NAME", SqlDbType.VarChar)
            If _CP_NAME.Trim <> "" Then 
                cmbParam(2).Value = _CP_NAME
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_CP_SURNAME", SqlDbType.VarChar)
            If _CP_SURNAME.Trim <> "" Then 
                cmbParam(3).Value = _CP_SURNAME
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_POSITION_ID", SqlDbType.BigInt)
            If _POSITION_ID IsNot Nothing Then
                cmbParam(4).Value = _POSITION_ID.Value
            Else
                cmbParam(4).Value = DBNull.Value
            End If

            cmbParam(5) = New SqlParameter("@_DEPARTMENT_ID", SqlDbType.BigInt)
            If _DEPARTMENT_ID IsNot Nothing Then
                cmbParam(5).Value = _DEPARTMENT_ID.Value
            Else
                cmbParam(5).Value = DBNull.Value
            End If

            cmbParam(6) = New SqlParameter("@_CUS_ID", SqlDbType.BigInt)
            If _CUS_ID IsNot Nothing Then 
                cmbParam(6).Value = _CUS_ID.Value
            Else
                cmbParam(6).Value = DBNull.value
            End IF

            cmbParam(7) = New SqlParameter("@_BRN_ID", SqlDbType.BigInt)
            If _BRN_ID IsNot Nothing Then 
                cmbParam(7).Value = _BRN_ID.Value
            Else
                cmbParam(7).Value = DBNull.value
            End IF

            cmbParam(8) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            If _CREATED_BY.Trim <> "" Then 
                cmbParam(8).Value = _CREATED_BY
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            If _CREATED_DATE.Value.Year > 1 Then 
                cmbParam(9).Value = _CREATED_DATE.Value
            Else
                cmbParam(9).Value = DBNull.value
            End If

            cmbParam(10) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(10).Value = _UPDATED_BY
            Else
                cmbParam(10).Value = DBNull.value
            End If

            cmbParam(11) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(11).Value = _UPDATED_DATE.Value
            Else
                cmbParam(11).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_CONTACT_PERSON by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("CP_ID")) = False Then _CP_ID = Convert.ToInt64(Rdr("CP_ID"))
                        If Convert.IsDBNull(Rdr("TI_ID")) = False Then _TI_ID = Convert.ToInt64(Rdr("TI_ID"))
                        If Convert.IsDBNull(Rdr("CP_Name")) = False Then _CP_Name = Rdr("CP_Name").ToString()
                        If Convert.IsDBNull(Rdr("CP_Surname")) = False Then _CP_Surname = Rdr("CP_Surname").ToString()
                        If Convert.IsDBNull(Rdr("POSITION_ID")) = False Then _POSITION_ID = Convert.ToInt64(Rdr("POSITION_ID"))
                        If Convert.IsDBNull(Rdr("DEPARTMENT_ID")) = False Then _DEPARTMENT_ID = Convert.ToInt64(Rdr("DEPARTMENT_ID"))
                        If Convert.IsDBNull(Rdr("CUS_ID")) = False Then _CUS_ID = Convert.ToInt64(Rdr("CUS_ID"))
                        If Convert.IsDBNull(Rdr("BRN_ID")) = False Then _BRN_ID = Convert.ToInt64(Rdr("BRN_ID"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_CONTACT_PERSON by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbContactPersonLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("CP_ID")) = False Then _CP_ID = Convert.ToInt64(Rdr("CP_ID"))
                        If Convert.IsDBNull(Rdr("TI_ID")) = False Then _TI_ID = Convert.ToInt64(Rdr("TI_ID"))
                        If Convert.IsDBNull(Rdr("CP_Name")) = False Then _CP_Name = Rdr("CP_Name").ToString()
                        If Convert.IsDBNull(Rdr("CP_Surname")) = False Then _CP_Surname = Rdr("CP_Surname").ToString()
                        If Convert.IsDBNull(Rdr("POSITION_ID")) = False Then _POSITION_ID = Convert.ToInt64(Rdr("POSITION_ID"))
                        If Convert.IsDBNull(Rdr("DEPARTMENT_ID")) = False Then _DEPARTMENT_ID = Convert.ToInt64(Rdr("DEPARTMENT_ID"))
                        If Convert.IsDBNull(Rdr("CUS_ID")) = False Then _CUS_ID = Convert.ToInt64(Rdr("CUS_ID"))
                        If Convert.IsDBNull(Rdr("BRN_ID")) = False Then _BRN_ID = Convert.ToInt64(Rdr("BRN_ID"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_CONTACT_PERSON
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & TableName & " (CP_ID, TI_ID, CP_NAME, CP_SURNAME, POSITION_ID, DEPARTMENT_ID, CUS_ID, BRN_ID, CREATED_BY, CREATED_DATE)"
                Sql += " OUTPUT INSERTED.CP_ID, INSERTED.TI_ID, INSERTED.CP_NAME, INSERTED.CP_SURNAME, INSERTED.POSITION_ID, INSERTED.DEPARTMENT_ID, INSERTED.CUS_ID, INSERTED.BRN_ID, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_CP_ID" & ", "
                sql += "@_TI_ID" & ", "
                sql += "@_CP_NAME" & ", "
                sql += "@_CP_SURNAME" & ", "
                Sql += "@_POSITION_ID" & ", "
                Sql += "@_DEPARTMENT_ID" & ", "
                Sql += "@_CUS_ID" & ", "
                sql += "@_BRN_ID" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_CONTACT_PERSON
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "TI_ID = " & "@_TI_ID" & ", "
                Sql += "CP_NAME = " & "@_CP_NAME" & ", "
                Sql += "CP_SURNAME = " & "@_CP_SURNAME" & ", "
                Sql += "POSITION_ID = " & "@_POSITION_ID" & ", "
                Sql += "DEPARTMENT_ID = " & "@_DEPARTMENT_ID" & ", "
                Sql += "CUS_ID = " & "@_CUS_ID" & ", "
                Sql += "BRN_ID = " & "@_BRN_ID" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_CONTACT_PERSON
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_CONTACT_PERSON
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT CP_ID, TI_ID, CP_NAME, CP_SURNAME, POSITION_ID, DEPARTMENT_ID, CUS_ID, BRN_ID, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & TableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
