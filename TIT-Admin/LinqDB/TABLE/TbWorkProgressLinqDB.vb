Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_WORK_PROGRESS table LinqDB.
    '[Create by  on March, 29 2017]
    Public Class TbWorkProgressLinqDB
        Public sub TbWorkProgressLinqDB()

        End Sub 
        ' TB_WORK_PROGRESS
        Const _tableName As String = "TB_WORK_PROGRESS"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _WP_ID As Long = 0
        Dim _OF_ID As System.Nullable(Of Long)
        Dim _PROJECT_ID As  System.Nullable(Of Long) 
        Dim _SITE As  String  = ""
        Dim _DESCRIPTION As  String  = ""
        Dim _PLAN_START As  System.Nullable(Of Date)  = New DateTime(1,1,1)
        Dim _PLAN_FINISH As  System.Nullable(Of Date)  = New DateTime(1,1,1)
        Dim _ACTUAL_START As  System.Nullable(Of Date)  = New DateTime(1,1,1)
        Dim _ACTUAL_FINISH As  System.Nullable(Of Date)  = New DateTime(1,1,1)
        Dim _STATUS As  String  = ""
        Dim _DETAIL_OF_PROGRESS As  String  = ""
        Dim _ASSIGNED_BY As String = ""
        Dim _ISSUE As  String  = ""
        Dim _PROCESS As  String  = ""
        Dim _RESULT As String = ""
        Dim _UPDATED_BY As String = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _CREATED_BY As  String  = ""
        Dim _CREATED_DATE As System.Nullable(Of DateTime) = New DateTime(1, 1, 1)

        'Generate Field Property 
        <Column(Storage:="_WP_ID", DbType:="BigInt NOT NULL ", CanBeNull:=False)>
        Public Property WP_ID() As Long
            Get
                Return _WP_ID
            End Get
            Set(ByVal value As Long)
                _WP_ID = value
            End Set
        End Property
        <Column(Storage:="_OF_ID", DbType:="BigInt")>
        Public Property OF_ID() As System.Nullable(Of Long)
            Get
                Return _OF_ID
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _OF_ID = value
            End Set
        End Property
        <Column(Storage:="_PROJECT_ID", DbType:="BigInt")>  _
        Public Property PROJECT_ID() As  System.Nullable(Of Long) 
            Get
                Return _PROJECT_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _PROJECT_ID = value
            End Set
        End Property 
        <Column(Storage:="_SITE", DbType:="VarChar(50)")>  _
        Public Property SITE() As  String 
            Get
                Return _SITE
            End Get
            Set(ByVal value As  String )
               _SITE = value
            End Set
        End Property 
        <Column(Storage:="_DESCRIPTION", DbType:="VarChar(250)")>  _
        Public Property DESCRIPTION() As  String 
            Get
                Return _DESCRIPTION
            End Get
            Set(ByVal value As  String )
               _DESCRIPTION = value
            End Set
        End Property 
        <Column(Storage:="_PLAN_START", DbType:="Date")>  _
        Public Property PLAN_START() As  System.Nullable(Of Date) 
            Get
                Return _PLAN_START
            End Get
            Set(ByVal value As  System.Nullable(Of Date) )
               _PLAN_START = value
            End Set
        End Property 
        <Column(Storage:="_PLAN_FINISH", DbType:="Date")>  _
        Public Property PLAN_FINISH() As  System.Nullable(Of Date) 
            Get
                Return _PLAN_FINISH
            End Get
            Set(ByVal value As  System.Nullable(Of Date) )
               _PLAN_FINISH = value
            End Set
        End Property 
        <Column(Storage:="_ACTUAL_START", DbType:="Date")>  _
        Public Property ACTUAL_START() As  System.Nullable(Of Date) 
            Get
                Return _ACTUAL_START
            End Get
            Set(ByVal value As  System.Nullable(Of Date) )
               _ACTUAL_START = value
            End Set
        End Property 
        <Column(Storage:="_ACTUAL_FINISH", DbType:="Date")>  _
        Public Property ACTUAL_FINISH() As  System.Nullable(Of Date) 
            Get
                Return _ACTUAL_FINISH
            End Get
            Set(ByVal value As  System.Nullable(Of Date) )
               _ACTUAL_FINISH = value
            End Set
        End Property 
        <Column(Storage:="_STATUS", DbType:="VarChar(50)")>  _
        Public Property STATUS() As  String 
            Get
                Return _STATUS
            End Get
            Set(ByVal value As  String )
               _STATUS = value
            End Set
        End Property 
        <Column(Storage:="_DETAIL_OF_PROGRESS", DbType:="VarChar(250)")>  _
        Public Property DETAIL_OF_PROGRESS() As  String 
            Get
                Return _DETAIL_OF_PROGRESS
            End Get
            Set(ByVal value As  String )
               _DETAIL_OF_PROGRESS = value
            End Set
        End Property
        <Column(Storage:="_ASSIGNED_BY", DbType:="VarChar(50)")>
        Public Property ASSIGNED_BY() As String
            Get
                Return _ASSIGNED_BY
            End Get
            Set(ByVal value As String)
                _ASSIGNED_BY = value
            End Set
        End Property
        <Column(Storage:="_ISSUE", DbType:="VarChar(250)")>  _
        Public Property ISSUE() As  String 
            Get
                Return _ISSUE
            End Get
            Set(ByVal value As  String )
               _ISSUE = value
            End Set
        End Property 
        <Column(Storage:="_PROCESS", DbType:="VarChar(250)")>  _
        Public Property PROCESS() As  String 
            Get
                Return _PROCESS
            End Get
            Set(ByVal value As  String )
               _PROCESS = value
            End Set
        End Property
        <Column(Storage:="_RESULT", DbType:="VarChar(250)")>
        Public Property RESULT() As String
            Get
                Return _RESULT
            End Get
            Set(ByVal value As String)
                _RESULT = value
            End Set
        End Property
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>
        Public Property UPDATED_BY() As String
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As String)
                _UPDATED_BY = value
            End Set
        End Property
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100)")>  _
        Public Property CREATED_BY() As  String 
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As  String )
               _CREATED_BY = value
            End Set
        End Property
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime")>
        Public Property CREATED_DATE() As System.Nullable(Of DateTime)
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As System.Nullable(Of DateTime))
                _CREATED_DATE = value
            End Set
        End Property


        'Clear All Data
        Private Sub ClearData()
            _WP_ID = 0
            _OF_ID = Nothing
            _PROJECT_ID = Nothing
            _SITE = ""
            _DESCRIPTION = ""
            _PLAN_START = New DateTime(1,1,1)
            _PLAN_FINISH = New DateTime(1,1,1)
            _ACTUAL_START = New DateTime(1,1,1)
            _ACTUAL_FINISH = New DateTime(1,1,1)
            _STATUS = ""
            _DETAIL_OF_PROGRESS = ""
            _ASSIGNED_BY = ""
            _ISSUE = ""
            _PROCESS = ""
            _RESULT = ""
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1, 1, 1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_WORK_PROGRESS table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _WP_ID = DB.GetNextID("WP_ID",tableName, trans)
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_WORK_PROGRESS table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _WP_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("WP_ID = @_WP_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_WORK_PROGRESS table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_WORK_PROGRESS table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cWP_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_WP_ID", cWP_ID)
                Return doDelete("WP_ID = @_WP_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_WORK_PROGRESS by specified WP_ID key is retrieved successfully.
        '/// <param name=cWP_ID>The WP_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cWP_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_WP_ID", cWP_ID)
            Return doChkData("WP_ID = @_WP_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_WORK_PROGRESS by specified WP_ID key is retrieved successfully.
        '/// <param name=cWP_ID>The WP_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cWP_ID As Long, trans As SQLTransaction) As TbWorkProgressLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_WP_ID", cWP_ID)
            Return doGetData("WP_ID = @_WP_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_WORK_PROGRESS by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_WORK_PROGRESS table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_WORK_PROGRESS table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_WORK_PROGRESS table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(18) As SqlParameter
            cmbParam(0) = New SqlParameter("@_WP_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _WP_ID

            cmbParam(1) = New SqlParameter("@_OF_ID", SqlDbType.BigInt)
            If _OF_ID IsNot Nothing Then
                cmbParam(1).Value = _OF_ID.Value
            Else
                cmbParam(1).Value = DBNull.Value
            End If

            cmbParam(2) = New SqlParameter("@_PROJECT_ID", SqlDbType.BigInt)
            If _PROJECT_ID IsNot Nothing Then
                cmbParam(2).Value = _PROJECT_ID.Value
            Else
                cmbParam(2).Value = DBNull.Value
            End If

            cmbParam(3) = New SqlParameter("@_SITE", SqlDbType.VarChar)
            If _SITE.Trim <> "" Then
                cmbParam(3).Value = _SITE
            Else
                cmbParam(3).Value = DBNull.Value
            End If

            cmbParam(4) = New SqlParameter("@_DESCRIPTION", SqlDbType.VarChar)
            If _DESCRIPTION.Trim <> "" Then
                cmbParam(4).Value = _DESCRIPTION
            Else
                cmbParam(4).Value = DBNull.Value
            End If

            cmbParam(5) = New SqlParameter("@_PLAN_START", SqlDbType.Date)
            If _PLAN_START.Value.Year > 1 Then
                cmbParam(5).Value = _PLAN_START.Value
            Else
                cmbParam(5).Value = DBNull.Value
            End If

            cmbParam(6) = New SqlParameter("@_PLAN_FINISH", SqlDbType.Date)
            If _PLAN_FINISH.Value.Year > 1 Then
                cmbParam(6).Value = _PLAN_FINISH.Value
            Else
                cmbParam(6).Value = DBNull.Value
            End If

            cmbParam(7) = New SqlParameter("@_ACTUAL_START", SqlDbType.Date)
            If _ACTUAL_START.Value.Year > 1 Then
                cmbParam(7).Value = _ACTUAL_START.Value
            Else
                cmbParam(7).Value = DBNull.Value
            End If

            cmbParam(8) = New SqlParameter("@_ACTUAL_FINISH", SqlDbType.Date)
            If _ACTUAL_FINISH.Value.Year > 1 Then
                cmbParam(8).Value = _ACTUAL_FINISH.Value
            Else
                cmbParam(8).Value = DBNull.Value
            End If

            cmbParam(9) = New SqlParameter("@_STATUS", SqlDbType.VarChar)
            If _STATUS.Trim <> "" Then
                cmbParam(9).Value = _STATUS
            Else
                cmbParam(9).Value = DBNull.Value
            End If

            cmbParam(10) = New SqlParameter("@_DETAIL_OF_PROGRESS", SqlDbType.VarChar)
            If _DETAIL_OF_PROGRESS.Trim <> "" Then
                cmbParam(10).Value = _DETAIL_OF_PROGRESS
            Else
                cmbParam(10).Value = DBNull.Value
            End If

            cmbParam(11) = New SqlParameter("@_ASSIGNED_BY", SqlDbType.BigInt)
            If _ASSIGNED_BY IsNot Nothing Then
                cmbParam(11).Value = _ASSIGNED_BY
            Else
                cmbParam(11).Value = DBNull.Value
            End If

            cmbParam(12) = New SqlParameter("@_ISSUE", SqlDbType.VarChar)
            If _ISSUE.Trim <> "" Then
                cmbParam(12).Value = _ISSUE
            Else
                cmbParam(12).Value = DBNull.Value
            End If

            cmbParam(13) = New SqlParameter("@_PROCESS", SqlDbType.VarChar)
            If _PROCESS.Trim <> "" Then
                cmbParam(13).Value = _PROCESS
            Else
                cmbParam(13).Value = DBNull.Value
            End If

            cmbParam(14) = New SqlParameter("@_RESULT", SqlDbType.VarChar)
            If _RESULT.Trim <> "" Then
                cmbParam(14).Value = _RESULT
            Else
                cmbParam(14).Value = DBNull.Value
            End If

            cmbParam(15) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then
                cmbParam(15).Value = _UPDATED_BY
            Else
                cmbParam(15).Value = DBNull.Value
            End If

            cmbParam(16) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then
                cmbParam(16).Value = _UPDATED_DATE.Value
            Else
                cmbParam(16).Value = DBNull.Value
            End If

            cmbParam(17) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            If _CREATED_BY.Trim <> "" Then
                cmbParam(17).Value = _CREATED_BY
            Else
                cmbParam(17).Value = DBNull.Value
            End If

            cmbParam(18) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            If _CREATED_DATE.Value.Year > 1 Then
                cmbParam(18).Value = _CREATED_DATE.Value
            Else
                cmbParam(18).Value = DBNull.Value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_WORK_PROGRESS by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("WP_ID")) = False Then _WP_ID = Convert.ToInt64(Rdr("WP_ID"))
                        If Convert.IsDBNull(Rdr("OF_ID")) = False Then _OF_ID = Convert.ToInt64(Rdr("OF_ID"))
                        If Convert.IsDBNull(Rdr("PROJECT_ID")) = False Then _PROJECT_ID = Convert.ToInt64(Rdr("PROJECT_ID"))
                        If Convert.IsDBNull(Rdr("SITE")) = False Then _SITE = Rdr("SITE").ToString()
                        If Convert.IsDBNull(Rdr("DESCRIPTION")) = False Then _DESCRIPTION = Rdr("DESCRIPTION").ToString()
                        If Convert.IsDBNull(Rdr("PLAN_START")) = False Then _PLAN_START = Convert.ToDateTime(Rdr("PLAN_START"))
                        If Convert.IsDBNull(Rdr("PLAN_FINISH")) = False Then _PLAN_FINISH = Convert.ToDateTime(Rdr("PLAN_FINISH"))
                        If Convert.IsDBNull(Rdr("ACTUAL_START")) = False Then _ACTUAL_START = Convert.ToDateTime(Rdr("ACTUAL_START"))
                        If Convert.IsDBNull(Rdr("ACTUAL_FINISH")) = False Then _ACTUAL_FINISH = Convert.ToDateTime(Rdr("ACTUAL_FINISH"))
                        If Convert.IsDBNull(Rdr("STATUS")) = False Then _STATUS = Rdr("STATUS").ToString()
                        If Convert.IsDBNull(Rdr("DETAIL_OF_PROGRESS")) = False Then _DETAIL_OF_PROGRESS = Rdr("DETAIL_OF_PROGRESS").ToString()
                        If Convert.IsDBNull(Rdr("ASSIGNED_BY")) = False Then _ASSIGNED_BY = Convert.ToInt64(Rdr("ASSIGNED_BY"))
                        If Convert.IsDBNull(Rdr("ISSUE")) = False Then _ISSUE = Rdr("ISSUE").ToString()
                        If Convert.IsDBNull(Rdr("PROCESS")) = False Then _PROCESS = Rdr("PROCESS").ToString()
                        If Convert.IsDBNull(Rdr("RESULT")) = False Then _RESULT = Rdr("RESULT").ToString()
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _UPDATED_BY = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _CREATED_DATE = Convert.ToDateTime(Rdr("created_date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_WORK_PROGRESS by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbWorkProgressLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("WP_ID")) = False Then _WP_ID = Convert.ToInt64(Rdr("WP_ID"))
                        If Convert.IsDBNull(Rdr("OF_ID")) = False Then _OF_ID = Convert.ToInt64(Rdr("OF_ID"))
                        If Convert.IsDBNull(Rdr("PROJECT_ID")) = False Then _PROJECT_ID = Convert.ToInt64(Rdr("PROJECT_ID"))
                        If Convert.IsDBNull(Rdr("SITE")) = False Then _SITE = Rdr("SITE").ToString()
                        If Convert.IsDBNull(Rdr("DESCRIPTION")) = False Then _DESCRIPTION = Rdr("DESCRIPTION").ToString()
                        If Convert.IsDBNull(Rdr("PLAN_START")) = False Then _PLAN_START = Convert.ToDateTime(Rdr("PLAN_START"))
                        If Convert.IsDBNull(Rdr("PLAN_FINISH")) = False Then _PLAN_FINISH = Convert.ToDateTime(Rdr("PLAN_FINISH"))
                        If Convert.IsDBNull(Rdr("ACTUAL_START")) = False Then _ACTUAL_START = Convert.ToDateTime(Rdr("ACTUAL_START"))
                        If Convert.IsDBNull(Rdr("ACTUAL_FINISH")) = False Then _ACTUAL_FINISH = Convert.ToDateTime(Rdr("ACTUAL_FINISH"))
                        If Convert.IsDBNull(Rdr("STATUS")) = False Then _STATUS = Rdr("STATUS").ToString()
                        If Convert.IsDBNull(Rdr("DETAIL_OF_PROGRESS")) = False Then _DETAIL_OF_PROGRESS = Rdr("DETAIL_OF_PROGRESS").ToString()
                        If Convert.IsDBNull(Rdr("ASSIGNED_BY")) = False Then _ASSIGNED_BY = Convert.ToInt64(Rdr("ASSIGNED_BY"))
                        If Convert.IsDBNull(Rdr("ISSUE")) = False Then _ISSUE = Rdr("ISSUE").ToString()
                        If Convert.IsDBNull(Rdr("PROCESS")) = False Then _PROCESS = Rdr("PROCESS").ToString()
                        If Convert.IsDBNull(Rdr("RESULT")) = False Then _RESULT = Rdr("RESULT").ToString()
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _UPDATED_BY = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _CREATED_DATE = Convert.ToDateTime(Rdr("created_date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_WORK_PROGRESS
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & TableName & " (WP_ID, OF_ID, PROJECT_ID, SITE, DESCRIPTION, PLAN_START, PLAN_FINISH, ACTUAL_START, ACTUAL_FINISH, STATUS, DETAIL_OF_PROGRESS, ASSIGNED_BY, ISSUE, PROCESS, RESULT, CREATED_BY, CREATED_DATE)"
                Sql += " OUTPUT INSERTED.WP_ID, INSERTED.OF_ID, INSERTED.PROJECT_ID, INSERTED.SITE, INSERTED.DESCRIPTION, INSERTED.PLAN_START, INSERTED.PLAN_FINISH, INSERTED.ACTUAL_START, INSERTED.ACTUAL_FINISH, INSERTED.STATUS, INSERTED.DETAIL_OF_PROGRESS, INSERTED.ASSIGNED_BY, INSERTED.ISSUE, INSERTED.PROCESS, INSERTED.RESULT, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE, INSERTED.CREATED_BY, INSERTED.CREATED_DATE"
                Sql += " VALUES("
                Sql += "@_WP_ID" & ", "
                Sql += "@_OF_ID" & ", "
                Sql += "@_PROJECT_ID" & ", "
                sql += "@_SITE" & ", "
                sql += "@_DESCRIPTION" & ", "
                sql += "@_PLAN_START" & ", "
                sql += "@_PLAN_FINISH" & ", "
                sql += "@_ACTUAL_START" & ", "
                sql += "@_ACTUAL_FINISH" & ", "
                sql += "@_STATUS" & ", "
                sql += "@_DETAIL_OF_PROGRESS" & ", "
                Sql += "@_ASSIGNED_BY" & ", "
                Sql += "@_ISSUE" & ", "
                sql += "@_PROCESS" & ", "
                sql += "@_RESULT" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_WORK_PROGRESS
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & TableName & " SET "
                Sql += "OF_ID = " & "@_OF_ID" & ", "
                Sql += "PROJECT_ID = " & "@_PROJECT_ID" & ", "
                Sql += "SITE = " & "@_SITE" & ", "
                Sql += "DESCRIPTION = " & "@_DESCRIPTION" & ", "
                Sql += "PLAN_START = " & "@_PLAN_START" & ", "
                Sql += "PLAN_FINISH = " & "@_PLAN_FINISH" & ", "
                Sql += "ACTUAL_START = " & "@_ACTUAL_START" & ", "
                Sql += "ACTUAL_FINISH = " & "@_ACTUAL_FINISH" & ", "
                Sql += "STATUS = " & "@_STATUS" & ", "
                Sql += "DETAIL_OF_PROGRESS = " & "@_DETAIL_OF_PROGRESS" & ", "
                Sql += "ASSIGNED_BY = " & "@_ASSIGNED_BY" & ", "
                Sql += "ISSUE = " & "@_ISSUE" & ", "
                Sql += "PROCESS = " & "@_PROCESS" & ", "
                Sql += "RESULT = " & "@_RESULT" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" + ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" & ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_WORK_PROGRESS
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_WORK_PROGRESS
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT WP_ID, OF_ID, PROJECT_ID, SITE, DESCRIPTION, PLAN_START, PLAN_FINISH, ACTUAL_START, ACTUAL_FINISH, STATUS, DETAIL_OF_PROGRESS, ASSIGNED_BY, ISSUE, PROCESS, RESULT, UPDATED_BY, UPDATED_DATE, CREATED_BY, CREATED_DATE FROM " & TableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
