Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_STAFF_PAYMENT table LinqDB.
    '[Create by  on March, 29 2017]
    Public Class TbStaffPaymentLinqDB
        Public sub TbStaffPaymentLinqDB()

        End Sub 
        ' TB_STAFF_PAYMENT
        Const _tableName As String = "TB_STAFF_PAYMENT"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _PAYYEAR As  System.Nullable(Of Long) 
        Dim _PAYMONTH As  System.Nullable(Of Long) 
        Dim _PAYDAY As  System.Nullable(Of Long) 
        Dim _PAYTONAME As  String  = ""
        Dim _TAXID As  String  = ""
        Dim _PAYAMOUNT As System.Nullable(Of Double)
        Dim _HOLDINGTAX As System.Nullable(Of Double)
        Dim _HOLDINGTAXNO As String = ""
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As System.Nullable(Of DateTime) = New DateTime(1, 1, 1)
        Dim _UPDATED_BY As String = ""
        Dim _UPDATED_DATE As System.Nullable(Of DateTime) = New DateTime(1, 1, 1)

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ", CanBeNull:=False)>
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
                _ID = value
            End Set
        End Property
        <Column(Storage:="_PAYYEAR", DbType:="Int")>
        Public Property PAYYEAR() As System.Nullable(Of Long)
            Get
                Return _PAYYEAR
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _PAYYEAR = value
            End Set
        End Property
        <Column(Storage:="_PAYMONTH", DbType:="Int")>
        Public Property PAYMONTH() As System.Nullable(Of Long)
            Get
                Return _PAYMONTH
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _PAYMONTH = value
            End Set
        End Property
        <Column(Storage:="_PAYDAY", DbType:="Int")>
        Public Property PAYDAY() As System.Nullable(Of Long)
            Get
                Return _PAYDAY
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _PAYDAY = value
            End Set
        End Property
        <Column(Storage:="_PAYTONAME", DbType:="VarChar(50)")>
        Public Property PAYTONAME() As String
            Get
                Return _PAYTONAME
            End Get
            Set(ByVal value As String)
                _PAYTONAME = value
            End Set
        End Property
        <Column(Storage:="_TAXID", DbType:="VarChar(20)")>
        Public Property TAXID() As String
            Get
                Return _TAXID
            End Get
            Set(ByVal value As String)
                _TAXID = value
            End Set
        End Property
        <Column(Storage:="_PAYAMOUNT", DbType:="Double")>
        Public Property PAYAMOUNT() As System.Nullable(Of Double)
            Get
                Return _PAYAMOUNT
            End Get
            Set(ByVal value As System.Nullable(Of Double))
                _PAYAMOUNT = value
            End Set
        End Property
        <Column(Storage:="_HOLDINGTAX", DbType:="Double")>
        Public Property HOLDINGTAX() As System.Nullable(Of Double)
            Get
                Return _HOLDINGTAX
            End Get
            Set(ByVal value As System.Nullable(Of Double))
                _HOLDINGTAX = value
            End Set
        End Property
        <Column(Storage:="_HOLDINGTAXNO", DbType:="VarChar(20)")>
        Public Property HOLDINGTAXNO() As String
            Get
                Return _HOLDINGTAXNO
            End Get
            Set(ByVal value As String)
                _HOLDINGTAXNO = value
            End Set
        End Property
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100)")>
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
                _CREATED_BY = value
            End Set
        End Property
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime")>
        Public Property CREATED_DATE() As System.Nullable(Of DateTime)
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As System.Nullable(Of DateTime))
                _CREATED_DATE = value
            End Set
        End Property
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>
        Public Property UPDATED_BY() As String
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As String)
                _UPDATED_BY = value
            End Set
        End Property
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>
        Public Property UPDATED_DATE() As System.Nullable(Of DateTime)
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As System.Nullable(Of DateTime))
                _UPDATED_DATE = value
            End Set
        End Property


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _PAYYEAR = Nothing
            _PAYMONTH = Nothing
            _PAYDAY = Nothing
            _PAYTONAME = ""
            _TAXID = ""
            _HOLDINGTAXNO = ""
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1, 1, 1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1, 1, 1)
        End Sub

        'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SqlTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIf(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SqlTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_STAFF_PAYMENT table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                _CREATED_BY = CreatedBy
                _CREATED_DATE = DateTime.Now
                Return doInsert(trans)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is updated to TB_STAFF_PAYMENT table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                If _ID > 0 Then
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is updated to TB_STAFF_PAYMENT table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SqlTransaction, cmbParm() As SqlParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is deleted from TB_STAFF_PAYMENT table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                Dim p(1) As SqlParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the record of TB_STAFF_PAYMENT by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SqlTransaction) As Boolean
            Dim p(1) As SqlParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_STAFF_PAYMENT by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SqlTransaction) As TbStaffPaymentLinqDB
            Dim p(1) As SqlParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_STAFF_PAYMENT by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_STAFF_PAYMENT table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SqlTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt As DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = False
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_STAFF_PAYMENT table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SqlTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> "" Then

                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString()
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 & ex.ToString()
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_STAFF_PAYMENT table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            Dim sql As String = SqlDelete & tmpWhere
            If whText.Trim() <> "" Then

                Try
                    ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                    If ret.IsSuccess = False Then
                        _error = MessageResources.MSGED001
                    Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                    End If
                Catch ex As ApplicationException
                    _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                    ret.IsSuccess = False
                    ret.ErrorMessage = _error
                    ret.SqlStatement = sql
                Catch ex As Exception
                    _error = " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                    ret.IsSuccess = False
                    ret.ErrorMessage = _error
                    ret.SqlStatement = sql
                End Try
            Else
                _error = MessageResources.MSGED003 & "### SQL: " & sql
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                ret.SqlStatement = sql
            End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(12) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_PAYYEAR", SqlDbType.Int)
            If _PAYYEAR IsNot Nothing Then
                cmbParam(1).Value = _PAYYEAR.Value
            Else
                cmbParam(1).Value = DBNull.Value
            End If

            cmbParam(2) = New SqlParameter("@_PAYMONTH", SqlDbType.Int)
            If _PAYMONTH IsNot Nothing Then
                cmbParam(2).Value = _PAYMONTH.Value
            Else
                cmbParam(2).Value = DBNull.Value
            End If

            cmbParam(3) = New SqlParameter("@_PAYDAY", SqlDbType.Int)
            If _PAYDAY IsNot Nothing Then
                cmbParam(3).Value = _PAYDAY.Value
            Else
                cmbParam(3).Value = DBNull.Value
            End If

            cmbParam(4) = New SqlParameter("@_PAYTONAME", SqlDbType.VarChar)
            If _PAYTONAME.Trim <> "" Then
                cmbParam(4).Value = _PAYTONAME
            Else
                cmbParam(4).Value = DBNull.Value
            End If

            cmbParam(5) = New SqlParameter("@_TAXID", SqlDbType.VarChar)
            If _TAXID.Trim <> "" Then
                cmbParam(5).Value = _TAXID
            Else
                cmbParam(5).Value = DBNull.Value
            End If



            cmbParam(8) = New SqlParameter("@_HOLDINGTAXNO", SqlDbType.VarChar)
            If _HOLDINGTAXNO.Trim <> "" Then
                cmbParam(8).Value = _HOLDINGTAXNO
            Else
                cmbParam(8).Value = DBNull.Value
            End If

            cmbParam(9) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            If _CREATED_BY.Trim <> "" Then
                cmbParam(9).Value = _CREATED_BY
            Else
                cmbParam(9).Value = DBNull.Value
            End If

            cmbParam(10) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            If _CREATED_DATE.Value.Year > 1 Then
                cmbParam(10).Value = _CREATED_DATE.Value
            Else
                cmbParam(10).Value = DBNull.Value
            End If

            cmbParam(11) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then
                cmbParam(11).Value = _UPDATED_BY
            Else
                cmbParam(11).Value = DBNull.Value
            End If

            cmbParam(12) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then
                cmbParam(12).Value = _UPDATED_DATE.Value
            Else
                cmbParam(12).Value = DBNull.Value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_STAFF_PAYMENT by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData = False
            If whText.Trim() <> "" Then
                Dim Rdr As SqlDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _ID = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("PayYear")) = False Then _PAYYEAR = Convert.ToInt32(Rdr("PayYear"))
                        If Convert.IsDBNull(Rdr("PayMonth")) = False Then _PAYMONTH = Convert.ToInt32(Rdr("PayMonth"))
                        If Convert.IsDBNull(Rdr("PayDay")) = False Then _PAYDAY = Convert.ToInt32(Rdr("PayDay"))
                        If Convert.IsDBNull(Rdr("PayToName")) = False Then _PAYTONAME = Rdr("PayToName").ToString()
                        If Convert.IsDBNull(Rdr("TaxID")) = False Then _TAXID = Rdr("TaxID").ToString()
                        If Convert.IsDBNull(Rdr("PayAmount")) = False Then _PAYAMOUNT = Convert.ToDouble(Rdr("PayAmount"))
                        If Convert.IsDBNull(Rdr("HoldingTax")) = False Then _HOLDINGTAX = Convert.ToDouble(Rdr("HoldingTax"))
                        If Convert.IsDBNull(Rdr("HoldingTaxNo")) = False Then _HOLDINGTAXNO = Rdr("HoldingTaxNo").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _CREATED_BY = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _CREATED_DATE = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _UPDATED_BY = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _UPDATED_DATE = Convert.ToDateTime(Rdr("updated_date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_STAFF_PAYMENT by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As TbStaffPaymentLinqDB
            ClearData()
            _haveData = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SqlDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _ID = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("PayYear")) = False Then _PAYYEAR = Convert.ToInt32(Rdr("PayYear"))
                        If Convert.IsDBNull(Rdr("PayMonth")) = False Then _PAYMONTH = Convert.ToInt32(Rdr("PayMonth"))
                        If Convert.IsDBNull(Rdr("PayDay")) = False Then _PAYDAY = Convert.ToInt32(Rdr("PayDay"))
                        If Convert.IsDBNull(Rdr("PayToName")) = False Then _PAYTONAME = Rdr("PayToName").ToString()
                        If Convert.IsDBNull(Rdr("TaxID")) = False Then _TAXID = Rdr("TaxID").ToString()
                        If Convert.IsDBNull(Rdr("PayAmount")) = False Then _PAYAMOUNT = Convert.ToDouble(Rdr("PayAmount"))
                        If Convert.IsDBNull(Rdr("HoldingTax")) = False Then _HOLDINGTAX = Convert.ToDouble(Rdr("HoldingTax"))
                        If Convert.IsDBNull(Rdr("HoldingTaxNo")) = False Then _HoldingTaxNo = Rdr("HoldingTaxNo").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_STAFF_PAYMENT
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (PAYYEAR, PAYMONTH, PAYDAY, PAYTONAME, TAXID, PAYAMOUNT, HOLDINGTAX, HOLDINGTAXNO, CREATED_BY, CREATED_DATE)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.PAYYEAR, INSERTED.PAYMONTH, INSERTED.PAYDAY, INSERTED.PAYTONAME, INSERTED.TAXID, INSERTED.PAYAMOUNT, INSERTED.HOLDINGTAX, INSERTED.HOLDINGTAXNO, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_PAYYEAR" & ", "
                sql += "@_PAYMONTH" & ", "
                sql += "@_PAYDAY" & ", "
                sql += "@_PAYTONAME" & ", "
                sql += "@_TAXID" & ", "
                sql += "@_PAYAMOUNT" & ", "
                sql += "@_HOLDINGTAX" & ", "
                sql += "@_HOLDINGTAXNO" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_STAFF_PAYMENT
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "PAYYEAR = " & "@_PAYYEAR" & ", "
                Sql += "PAYMONTH = " & "@_PAYMONTH" & ", "
                Sql += "PAYDAY = " & "@_PAYDAY" & ", "
                Sql += "PAYTONAME = " & "@_PAYTONAME" & ", "
                Sql += "TAXID = " & "@_TAXID" & ", "
                Sql += "PAYAMOUNT = " & "@_PAYAMOUNT" & ", "
                Sql += "HOLDINGTAX = " & "@_HOLDINGTAX" & ", "
                Sql += "HOLDINGTAXNO = " & "@_HOLDINGTAXNO" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_STAFF_PAYMENT
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_STAFF_PAYMENT
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, PAYYEAR, PAYMONTH, PAYDAY, PAYTONAME, TAXID, PAYAMOUNT, HOLDINGTAX, HOLDINGTAXNO, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
