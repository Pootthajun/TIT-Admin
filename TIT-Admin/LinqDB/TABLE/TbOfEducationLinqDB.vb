Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_OF_EDUCATION table LinqDB.
    '[Create by  on May, 22 2017]
    Public Class TbOfEducationLinqDB
        Public sub TbOfEducationLinqDB()

        End Sub 
        ' TB_OF_EDUCATION
        Const _tableName As String = "TB_OF_EDUCATION"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _EDU_ID As Long = 0
        Dim _OF_ID As  System.Nullable(Of Long) 
        Dim _INSTITUTION As  String  = ""
        Dim _GRADUATE As  String  = ""
        Dim _DEGREE As  String  = ""
        Dim _START_YEAR As  String  = ""
        Dim _END_YEAR As  String  = ""
        Dim _GRADE As  System.Nullable(Of Double) 
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _CREATED_BY As  String  = ""
        Dim _CREATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""

        'Generate Field Property 
        <Column(Storage:="_EDU_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property EDU_ID() As Long
            Get
                Return _EDU_ID
            End Get
            Set(ByVal value As Long)
               _EDU_ID = value
            End Set
        End Property 
        <Column(Storage:="_OF_ID", DbType:="BigInt")>  _
        Public Property OF_ID() As  System.Nullable(Of Long) 
            Get
                Return _OF_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _OF_ID = value
            End Set
        End Property 
        <Column(Storage:="_INSTITUTION", DbType:="VarChar(200)")>  _
        Public Property INSTITUTION() As  String 
            Get
                Return _INSTITUTION
            End Get
            Set(ByVal value As  String )
               _INSTITUTION = value
            End Set
        End Property 
        <Column(Storage:="_GRADUATE", DbType:="VarChar(250)")>  _
        Public Property GRADUATE() As  String 
            Get
                Return _GRADUATE
            End Get
            Set(ByVal value As  String )
               _GRADUATE = value
            End Set
        End Property 
        <Column(Storage:="_DEGREE", DbType:="NVarChar(200)")>  _
        Public Property DEGREE() As  String 
            Get
                Return _DEGREE
            End Get
            Set(ByVal value As  String )
               _DEGREE = value
            End Set
        End Property 
        <Column(Storage:="_START_YEAR", DbType:="NVarChar(8)")>  _
        Public Property START_YEAR() As  String 
            Get
                Return _START_YEAR
            End Get
            Set(ByVal value As  String )
               _START_YEAR = value
            End Set
        End Property 
        <Column(Storage:="_END_YEAR", DbType:="NVarChar(8)")>  _
        Public Property END_YEAR() As  String 
            Get
                Return _END_YEAR
            End Get
            Set(ByVal value As  String )
               _END_YEAR = value
            End Set
        End Property 
        <Column(Storage:="_GRADE", DbType:="Float")>  _
        Public Property GRADE() As  System.Nullable(Of Double) 
            Get
                Return _GRADE
            End Get
            Set(ByVal value As  System.Nullable(Of Double) )
               _GRADE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100)")>  _
        Public Property CREATED_BY() As  String 
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As  String )
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime")>  _
        Public Property CREATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _EDU_ID = 0
            _OF_ID = Nothing
            _INSTITUTION = ""
            _GRADUATE = ""
            _DEGREE = ""
            _START_YEAR = ""
            _END_YEAR = ""
            _GRADE = Nothing
            _UPDATED_DATE = New DateTime(1,1,1)
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_OF_EDUCATION table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _EDU_ID = DB.GetNextID("EDU_ID",tableName, trans)
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_OF_EDUCATION table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _EDU_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("EDU_ID = @_EDU_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_OF_EDUCATION table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_OF_EDUCATION table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cEDU_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_EDU_ID", cEDU_ID)
                Return doDelete("EDU_ID = @_EDU_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_OF_EDUCATION by specified EDU_ID key is retrieved successfully.
        '/// <param name=cEDU_ID>The EDU_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cEDU_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_EDU_ID", cEDU_ID)
            Return doChkData("EDU_ID = @_EDU_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_OF_EDUCATION by specified EDU_ID key is retrieved successfully.
        '/// <param name=cEDU_ID>The EDU_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cEDU_ID As Long, trans As SQLTransaction) As TbOfEducationLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_EDU_ID", cEDU_ID)
            Return doGetData("EDU_ID = @_EDU_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_OF_EDUCATION by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_OF_EDUCATION table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_OF_EDUCATION table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_OF_EDUCATION table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(11) As SqlParameter
            cmbParam(0) = New SqlParameter("@_EDU_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _EDU_ID

            cmbParam(1) = New SqlParameter("@_OF_ID", SqlDbType.BigInt)
            If _OF_ID IsNot Nothing Then 
                cmbParam(1).Value = _OF_ID.Value
            Else
                cmbParam(1).Value = DBNull.value
            End IF

            cmbParam(2) = New SqlParameter("@_INSTITUTION", SqlDbType.VarChar)
            If _INSTITUTION.Trim <> "" Then 
                cmbParam(2).Value = _INSTITUTION
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_GRADUATE", SqlDbType.VarChar)
            If _GRADUATE.Trim <> "" Then 
                cmbParam(3).Value = _GRADUATE
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_DEGREE", SqlDbType.NVarChar)
            If _DEGREE.Trim <> "" Then 
                cmbParam(4).Value = _DEGREE
            Else
                cmbParam(4).Value = DBNull.value
            End IF

            cmbParam(5) = New SqlParameter("@_START_YEAR", SqlDbType.NVarChar)
            If _START_YEAR.Trim <> "" Then 
                cmbParam(5).Value = _START_YEAR
            Else
                cmbParam(5).Value = DBNull.value
            End IF

            cmbParam(6) = New SqlParameter("@_END_YEAR", SqlDbType.NVarChar)
            If _END_YEAR.Trim <> "" Then 
                cmbParam(6).Value = _END_YEAR
            Else
                cmbParam(6).Value = DBNull.value
            End IF

            cmbParam(7) = New SqlParameter("@_GRADE", SqlDbType.Float)
            If _GRADE IsNot Nothing Then 
                cmbParam(7).Value = _GRADE.Value
            Else
                cmbParam(7).Value = DBNull.value
            End IF

            cmbParam(8) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(8).Value = _UPDATED_DATE.Value
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            If _CREATED_BY.Trim <> "" Then 
                cmbParam(9).Value = _CREATED_BY
            Else
                cmbParam(9).Value = DBNull.value
            End If

            cmbParam(10) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            If _CREATED_DATE.Value.Year > 1 Then 
                cmbParam(10).Value = _CREATED_DATE.Value
            Else
                cmbParam(10).Value = DBNull.value
            End If

            cmbParam(11) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(11).Value = _UPDATED_BY
            Else
                cmbParam(11).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_OF_EDUCATION by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("EDU_ID")) = False Then _EDU_ID = Convert.ToInt64(Rdr("EDU_ID"))
                        If Convert.IsDBNull(Rdr("OF_ID")) = False Then _OF_ID = Convert.ToInt64(Rdr("OF_ID"))
                        If Convert.IsDBNull(Rdr("INSTITUTION")) = False Then _INSTITUTION = Rdr("INSTITUTION").ToString()
                        If Convert.IsDBNull(Rdr("GRADUATE")) = False Then _GRADUATE = Rdr("GRADUATE").ToString()
                        If Convert.IsDBNull(Rdr("DEGREE")) = False Then _DEGREE = Rdr("DEGREE").ToString()
                        If Convert.IsDBNull(Rdr("START_YEAR")) = False Then _START_YEAR = Rdr("START_YEAR").ToString()
                        If Convert.IsDBNull(Rdr("END_YEAR")) = False Then _END_YEAR = Rdr("END_YEAR").ToString()
                        If Convert.IsDBNull(Rdr("GRADE")) = False Then _GRADE = Convert.ToDouble(Rdr("GRADE"))
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_OF_EDUCATION by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbOfEducationLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("EDU_ID")) = False Then _EDU_ID = Convert.ToInt64(Rdr("EDU_ID"))
                        If Convert.IsDBNull(Rdr("OF_ID")) = False Then _OF_ID = Convert.ToInt64(Rdr("OF_ID"))
                        If Convert.IsDBNull(Rdr("INSTITUTION")) = False Then _INSTITUTION = Rdr("INSTITUTION").ToString()
                        If Convert.IsDBNull(Rdr("GRADUATE")) = False Then _GRADUATE = Rdr("GRADUATE").ToString()
                        If Convert.IsDBNull(Rdr("DEGREE")) = False Then _DEGREE = Rdr("DEGREE").ToString()
                        If Convert.IsDBNull(Rdr("START_YEAR")) = False Then _START_YEAR = Rdr("START_YEAR").ToString()
                        If Convert.IsDBNull(Rdr("END_YEAR")) = False Then _END_YEAR = Rdr("END_YEAR").ToString()
                        If Convert.IsDBNull(Rdr("GRADE")) = False Then _GRADE = Convert.ToDouble(Rdr("GRADE"))
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_OF_EDUCATION
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (EDU_ID, OF_ID, INSTITUTION, GRADUATE, DEGREE, START_YEAR, END_YEAR, GRADE, CREATED_BY, CREATED_DATE)"
                Sql += " OUTPUT INSERTED.EDU_ID, INSERTED.OF_ID, INSERTED.INSTITUTION, INSERTED.GRADUATE, INSERTED.DEGREE, INSERTED.START_YEAR, INSERTED.END_YEAR, INSERTED.GRADE, INSERTED.UPDATED_DATE, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY"
                Sql += " VALUES("
                sql += "@_EDU_ID" & ", "
                sql += "@_OF_ID" & ", "
                sql += "@_INSTITUTION" & ", "
                sql += "@_GRADUATE" & ", "
                sql += "@_DEGREE" & ", "
                sql += "@_START_YEAR" & ", "
                sql += "@_END_YEAR" & ", "
                sql += "@_GRADE" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_OF_EDUCATION
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "OF_ID = " & "@_OF_ID" & ", "
                Sql += "INSTITUTION = " & "@_INSTITUTION" & ", "
                Sql += "GRADUATE = " & "@_GRADUATE" & ", "
                Sql += "DEGREE = " & "@_DEGREE" & ", "
                Sql += "START_YEAR = " & "@_START_YEAR" & ", "
                Sql += "END_YEAR = " & "@_END_YEAR" & ", "
                Sql += "GRADE = " & "@_GRADE" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_OF_EDUCATION
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_OF_EDUCATION
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT EDU_ID, OF_ID, INSTITUTION, GRADUATE, DEGREE, START_YEAR, END_YEAR, GRADE, UPDATED_DATE, CREATED_BY, CREATED_DATE, UPDATED_BY FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
