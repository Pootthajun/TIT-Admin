Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_OFFICER table LinqDB.
    '[Create by  on May, 25 2017]
    Public Class TbOfficerLinqDB
        Public sub TbOfficerLinqDB()

        End Sub 
        ' TB_OFFICER
        Const _tableName As String = "TB_OFFICER"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _OF_ID As Long = 0
        Dim _OF_IMG() As Byte
        Dim _TI_ID As  System.Nullable(Of Long) 
        Dim _OF_NAME As  String  = ""
        Dim _OF_SURNAME As  String  = ""
        Dim _OF_NICKNAME As  String  = ""
        Dim _OF_EN_NAME As  String  = ""
        Dim _OF_EN_SURNAME As  String  = ""
        Dim _OF_EN_NICKNAME As  String  = ""
        Dim _DEPARTMENT_ID As  System.Nullable(Of Long) 
        Dim _OF_SALARY As  System.Nullable(Of Long) 
        Dim _STATUS_ID As  System.Nullable(Of Long) 
        Dim _OF_BRITH As  System.Nullable(Of Date)  = New DateTime(1,1,1)
        Dim _GENDER_ID As  System.Nullable(Of Long) 
        Dim _BLOOD_ID As  System.Nullable(Of Long) 
        Dim _NAT_ID As  System.Nullable(Of Long) 
        Dim _ETH_ID As  System.Nullable(Of Long) 
        Dim _RELIGION_ID As  System.Nullable(Of Long) 
        Dim _OF_PID As  String  = ""
        Dim _DATE_OF_ISSUE As  System.Nullable(Of Date)  = New DateTime(1,1,1)
        Dim _DATE_OF_EXPIRY As  System.Nullable(Of Date)  = New DateTime(1,1,1)
        Dim _MEDICAL_PLOBLEMS As  String  = ""
        Dim _WEIGHT As  System.Nullable(Of Long) 
        Dim _HEIGHT As  System.Nullable(Of Long) 
        Dim _MR_ID As  System.Nullable(Of Long) 
        Dim _MILITARY_ID As  System.Nullable(Of Long) 
        Dim _HOME_ADDRESS As  String  = ""
        Dim _MAILING_ADDRESS As  String  = ""
        Dim _HOME_PROVINCE_ID As  String  = ""
        Dim _HOME_AMPHUR_ID As  String  = ""
        Dim _HOME_TUMBOL_ID As  String  = ""
        Dim _MAILING_PROVINCE_ID As  String  = ""
        Dim _MAILING_AMPHUR_ID As  String  = ""
        Dim _MAILING_TUMBOL_ID As  String  = ""
        Dim _HOME_ZIPCODE As  String  = ""
        Dim _MAILING_ZIPCODE As  String  = ""
        Dim _WP_ID As  System.Nullable(Of Long) 
        Dim _OF_RUNNING_NO As  System.Nullable(Of Long) 
        Dim _OF_CREATE_YEAR As  String  = ""
        Dim _OF_CREATE_MONTH As  String  = ""
        Dim _CREATED_BY As  String  = ""
        Dim _CREATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)
        Dim _UPDATED_BY As  String  = ""
        Dim _UPDATED_DATE As  System.Nullable(Of DateTime)  = New DateTime(1,1,1)

        'Generate Field Property 
        <Column(Storage:="_OF_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property OF_ID() As Long
            Get
                Return _OF_ID
            End Get
            Set(ByVal value As Long)
               _OF_ID = value
            End Set
        End Property 
        <Column(Storage:="_OF_IMG", DbType:="IMAGE")>  _
        Public Property OF_IMG() As  Byte() 
            Get
                Return _OF_IMG
            End Get
            Set(ByVal value() As Byte)
               _OF_IMG = value
            End Set
        End Property 
        <Column(Storage:="_TI_ID", DbType:="BigInt")>  _
        Public Property TI_ID() As  System.Nullable(Of Long) 
            Get
                Return _TI_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _TI_ID = value
            End Set
        End Property 
        <Column(Storage:="_OF_NAME", DbType:="VarChar(100)")>  _
        Public Property OF_NAME() As  String 
            Get
                Return _OF_NAME
            End Get
            Set(ByVal value As  String )
               _OF_NAME = value
            End Set
        End Property 
        <Column(Storage:="_OF_SURNAME", DbType:="VarChar(100)")>  _
        Public Property OF_SURNAME() As  String 
            Get
                Return _OF_SURNAME
            End Get
            Set(ByVal value As  String )
               _OF_SURNAME = value
            End Set
        End Property 
        <Column(Storage:="_OF_NICKNAME", DbType:="VarChar(50)")>  _
        Public Property OF_NICKNAME() As  String 
            Get
                Return _OF_NICKNAME
            End Get
            Set(ByVal value As  String )
               _OF_NICKNAME = value
            End Set
        End Property 
        <Column(Storage:="_OF_EN_NAME", DbType:="VarChar(100)")>  _
        Public Property OF_EN_NAME() As  String 
            Get
                Return _OF_EN_NAME
            End Get
            Set(ByVal value As  String )
               _OF_EN_NAME = value
            End Set
        End Property 
        <Column(Storage:="_OF_EN_SURNAME", DbType:="VarChar(100)")>  _
        Public Property OF_EN_SURNAME() As  String 
            Get
                Return _OF_EN_SURNAME
            End Get
            Set(ByVal value As  String )
               _OF_EN_SURNAME = value
            End Set
        End Property 
        <Column(Storage:="_OF_EN_NICKNAME", DbType:="VarChar(50)")>  _
        Public Property OF_EN_NICKNAME() As  String 
            Get
                Return _OF_EN_NICKNAME
            End Get
            Set(ByVal value As  String )
               _OF_EN_NICKNAME = value
            End Set
        End Property 
        <Column(Storage:="_DEPARTMENT_ID", DbType:="BigInt")>  _
        Public Property DEPARTMENT_ID() As  System.Nullable(Of Long) 
            Get
                Return _DEPARTMENT_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _DEPARTMENT_ID = value
            End Set
        End Property 
        <Column(Storage:="_OF_SALARY", DbType:="Int")>  _
        Public Property OF_SALARY() As  System.Nullable(Of Long) 
            Get
                Return _OF_SALARY
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _OF_SALARY = value
            End Set
        End Property 
        <Column(Storage:="_STATUS_ID", DbType:="BigInt")>  _
        Public Property STATUS_ID() As  System.Nullable(Of Long) 
            Get
                Return _STATUS_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _STATUS_ID = value
            End Set
        End Property 
        <Column(Storage:="_OF_BRITH", DbType:="Date")>  _
        Public Property OF_BRITH() As  System.Nullable(Of Date) 
            Get
                Return _OF_BRITH
            End Get
            Set(ByVal value As  System.Nullable(Of Date) )
               _OF_BRITH = value
            End Set
        End Property 
        <Column(Storage:="_GENDER_ID", DbType:="BigInt")>  _
        Public Property GENDER_ID() As  System.Nullable(Of Long) 
            Get
                Return _GENDER_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _GENDER_ID = value
            End Set
        End Property 
        <Column(Storage:="_BLOOD_ID", DbType:="BigInt")>  _
        Public Property BLOOD_ID() As  System.Nullable(Of Long) 
            Get
                Return _BLOOD_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _BLOOD_ID = value
            End Set
        End Property 
        <Column(Storage:="_NAT_ID", DbType:="BigInt")>  _
        Public Property NAT_ID() As  System.Nullable(Of Long) 
            Get
                Return _NAT_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _NAT_ID = value
            End Set
        End Property 
        <Column(Storage:="_ETH_ID", DbType:="BigInt")>  _
        Public Property ETH_ID() As  System.Nullable(Of Long) 
            Get
                Return _ETH_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _ETH_ID = value
            End Set
        End Property 
        <Column(Storage:="_RELIGION_ID", DbType:="BigInt")>  _
        Public Property RELIGION_ID() As  System.Nullable(Of Long) 
            Get
                Return _RELIGION_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _RELIGION_ID = value
            End Set
        End Property 
        <Column(Storage:="_OF_PID", DbType:="NVarChar(26)")>  _
        Public Property OF_PID() As  String 
            Get
                Return _OF_PID
            End Get
            Set(ByVal value As  String )
               _OF_PID = value
            End Set
        End Property 
        <Column(Storage:="_DATE_OF_ISSUE", DbType:="Date")>  _
        Public Property DATE_OF_ISSUE() As  System.Nullable(Of Date) 
            Get
                Return _DATE_OF_ISSUE
            End Get
            Set(ByVal value As  System.Nullable(Of Date) )
               _DATE_OF_ISSUE = value
            End Set
        End Property 
        <Column(Storage:="_DATE_OF_EXPIRY", DbType:="Date")>  _
        Public Property DATE_OF_EXPIRY() As  System.Nullable(Of Date) 
            Get
                Return _DATE_OF_EXPIRY
            End Get
            Set(ByVal value As  System.Nullable(Of Date) )
               _DATE_OF_EXPIRY = value
            End Set
        End Property 
        <Column(Storage:="_MEDICAL_PLOBLEMS", DbType:="VarChar(200)")>  _
        Public Property MEDICAL_PLOBLEMS() As  String 
            Get
                Return _MEDICAL_PLOBLEMS
            End Get
            Set(ByVal value As  String )
               _MEDICAL_PLOBLEMS = value
            End Set
        End Property 
        <Column(Storage:="_WEIGHT", DbType:="Int")>  _
        Public Property WEIGHT() As  System.Nullable(Of Long) 
            Get
                Return _WEIGHT
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _WEIGHT = value
            End Set
        End Property 
        <Column(Storage:="_HEIGHT", DbType:="Int")>  _
        Public Property HEIGHT() As  System.Nullable(Of Long) 
            Get
                Return _HEIGHT
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _HEIGHT = value
            End Set
        End Property 
        <Column(Storage:="_MR_ID", DbType:="BigInt")>  _
        Public Property MR_ID() As  System.Nullable(Of Long) 
            Get
                Return _MR_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _MR_ID = value
            End Set
        End Property 
        <Column(Storage:="_MILITARY_ID", DbType:="BigInt")>  _
        Public Property MILITARY_ID() As  System.Nullable(Of Long) 
            Get
                Return _MILITARY_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _MILITARY_ID = value
            End Set
        End Property 
        <Column(Storage:="_HOME_ADDRESS", DbType:="VarChar(200)")>  _
        Public Property HOME_ADDRESS() As  String 
            Get
                Return _HOME_ADDRESS
            End Get
            Set(ByVal value As  String )
               _HOME_ADDRESS = value
            End Set
        End Property 
        <Column(Storage:="_MAILING_ADDRESS", DbType:="VarChar(200)")>  _
        Public Property MAILING_ADDRESS() As  String 
            Get
                Return _MAILING_ADDRESS
            End Get
            Set(ByVal value As  String )
               _MAILING_ADDRESS = value
            End Set
        End Property 
        <Column(Storage:="_HOME_PROVINCE_ID", DbType:="NVarChar(4)")>  _
        Public Property HOME_PROVINCE_ID() As  String 
            Get
                Return _HOME_PROVINCE_ID
            End Get
            Set(ByVal value As  String )
               _HOME_PROVINCE_ID = value
            End Set
        End Property 
        <Column(Storage:="_HOME_AMPHUR_ID", DbType:="NVarChar(8)")>  _
        Public Property HOME_AMPHUR_ID() As  String 
            Get
                Return _HOME_AMPHUR_ID
            End Get
            Set(ByVal value As  String )
               _HOME_AMPHUR_ID = value
            End Set
        End Property 
        <Column(Storage:="_HOME_TUMBOL_ID", DbType:="NVarChar(12)")>  _
        Public Property HOME_TUMBOL_ID() As  String 
            Get
                Return _HOME_TUMBOL_ID
            End Get
            Set(ByVal value As  String )
               _HOME_TUMBOL_ID = value
            End Set
        End Property 
        <Column(Storage:="_MAILING_PROVINCE_ID", DbType:="NVarChar(4)")>  _
        Public Property MAILING_PROVINCE_ID() As  String 
            Get
                Return _MAILING_PROVINCE_ID
            End Get
            Set(ByVal value As  String )
               _MAILING_PROVINCE_ID = value
            End Set
        End Property 
        <Column(Storage:="_MAILING_AMPHUR_ID", DbType:="NVarChar(8)")>  _
        Public Property MAILING_AMPHUR_ID() As  String 
            Get
                Return _MAILING_AMPHUR_ID
            End Get
            Set(ByVal value As  String )
               _MAILING_AMPHUR_ID = value
            End Set
        End Property 
        <Column(Storage:="_MAILING_TUMBOL_ID", DbType:="NVarChar(12)")>  _
        Public Property MAILING_TUMBOL_ID() As  String 
            Get
                Return _MAILING_TUMBOL_ID
            End Get
            Set(ByVal value As  String )
               _MAILING_TUMBOL_ID = value
            End Set
        End Property 
        <Column(Storage:="_HOME_ZIPCODE", DbType:="VarChar(5)")>  _
        Public Property HOME_ZIPCODE() As  String 
            Get
                Return _HOME_ZIPCODE
            End Get
            Set(ByVal value As  String )
               _HOME_ZIPCODE = value
            End Set
        End Property 
        <Column(Storage:="_MAILING_ZIPCODE", DbType:="VarChar(5)")>  _
        Public Property MAILING_ZIPCODE() As  String 
            Get
                Return _MAILING_ZIPCODE
            End Get
            Set(ByVal value As  String )
               _MAILING_ZIPCODE = value
            End Set
        End Property 
        <Column(Storage:="_WP_ID", DbType:="BigInt")>  _
        Public Property WP_ID() As  System.Nullable(Of Long) 
            Get
                Return _WP_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _WP_ID = value
            End Set
        End Property 
        <Column(Storage:="_OF_RUNNING_NO", DbType:="Int")>  _
        Public Property OF_RUNNING_NO() As  System.Nullable(Of Long) 
            Get
                Return _OF_RUNNING_NO
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _OF_RUNNING_NO = value
            End Set
        End Property 
        <Column(Storage:="_OF_CREATE_YEAR", DbType:="NVarChar(8)")>  _
        Public Property OF_CREATE_YEAR() As  String 
            Get
                Return _OF_CREATE_YEAR
            End Get
            Set(ByVal value As  String )
               _OF_CREATE_YEAR = value
            End Set
        End Property 
        <Column(Storage:="_OF_CREATE_MONTH", DbType:="NVarChar(4)")>  _
        Public Property OF_CREATE_MONTH() As  String 
            Get
                Return _OF_CREATE_MONTH
            End Get
            Set(ByVal value As  String )
               _OF_CREATE_MONTH = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100)")>  _
        Public Property CREATED_BY() As  String 
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As  String )
               _CREATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime")>  _
        Public Property CREATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _CREATED_DATE = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>  _
        Public Property UPDATED_BY() As  String 
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As  String )
               _UPDATED_BY = value
            End Set
        End Property 
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>  _
        Public Property UPDATED_DATE() As  System.Nullable(Of DateTime) 
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As  System.Nullable(Of DateTime) )
               _UPDATED_DATE = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _OF_ID = 0
             _OF_IMG = Nothing
            _TI_ID = Nothing
            _OF_NAME = ""
            _OF_SURNAME = ""
            _OF_NICKNAME = ""
            _OF_EN_NAME = ""
            _OF_EN_SURNAME = ""
            _OF_EN_NICKNAME = ""
            _DEPARTMENT_ID = Nothing
            _OF_SALARY = Nothing
            _STATUS_ID = Nothing
            _OF_BRITH = New DateTime(1,1,1)
            _GENDER_ID = Nothing
            _BLOOD_ID = Nothing
            _NAT_ID = Nothing
            _ETH_ID = Nothing
            _RELIGION_ID = Nothing
            _OF_PID = ""
            _DATE_OF_ISSUE = New DateTime(1,1,1)
            _DATE_OF_EXPIRY = New DateTime(1,1,1)
            _MEDICAL_PLOBLEMS = ""
            _WEIGHT = Nothing
            _HEIGHT = Nothing
            _MR_ID = Nothing
            _MILITARY_ID = Nothing
            _HOME_ADDRESS = ""
            _MAILING_ADDRESS = ""
            _HOME_PROVINCE_ID = ""
            _HOME_AMPHUR_ID = ""
            _HOME_TUMBOL_ID = ""
            _MAILING_PROVINCE_ID = ""
            _MAILING_AMPHUR_ID = ""
            _MAILING_TUMBOL_ID = ""
            _HOME_ZIPCODE = ""
            _MAILING_ZIPCODE = ""
            _WP_ID = Nothing
            _OF_RUNNING_NO = Nothing
            _OF_CREATE_YEAR = ""
            _OF_CREATE_MONTH = ""
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1,1,1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1,1,1)
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_OFFICER table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                _created_by = CreatedBy
                _created_date = DateTime.Now
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_OFFICER table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _OF_ID > 0 Then 
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("OF_ID = @_OF_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_OFFICER table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_OFFICER table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cOF_ID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_OF_ID", cOF_ID)
                Return doDelete("OF_ID = @_OF_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_OFFICER by specified OF_ID key is retrieved successfully.
        '/// <param name=cOF_ID>The OF_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cOF_ID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_OF_ID", cOF_ID)
            Return doChkData("OF_ID = @_OF_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_OFFICER by specified OF_ID key is retrieved successfully.
        '/// <param name=cOF_ID>The OF_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cOF_ID As Long, trans As SQLTransaction) As TbOfficerLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_OF_ID", cOF_ID)
            Return doGetData("OF_ID = @_OF_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_OFFICER by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_OFFICER table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _OF_ID = dt.Rows(0)("OF_ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_OFFICER table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_OFFICER table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(43) As SqlParameter
            cmbParam(0) = New SqlParameter("@_OF_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _OF_ID

            If _OF_IMG IsNot Nothing Then 
                cmbParam(1) = New SqlParameter("@_OF_IMG",SqlDbType.Image, _OF_IMG.Length)
                cmbParam(1).Value = _OF_IMG
            Else
                cmbParam(1) = New SqlParameter("@_OF_IMG", SqlDbType.Image)
                cmbParam(1).Value = DBNull.value
            End If

            cmbParam(2) = New SqlParameter("@_TI_ID", SqlDbType.BigInt)
            If _TI_ID IsNot Nothing Then 
                cmbParam(2).Value = _TI_ID.Value
            Else
                cmbParam(2).Value = DBNull.value
            End IF

            cmbParam(3) = New SqlParameter("@_OF_NAME", SqlDbType.VarChar)
            If _OF_NAME.Trim <> "" Then 
                cmbParam(3).Value = _OF_NAME
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_OF_SURNAME", SqlDbType.VarChar)
            If _OF_SURNAME.Trim <> "" Then 
                cmbParam(4).Value = _OF_SURNAME
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_OF_NICKNAME", SqlDbType.VarChar)
            If _OF_NICKNAME.Trim <> "" Then 
                cmbParam(5).Value = _OF_NICKNAME
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_OF_EN_NAME", SqlDbType.VarChar)
            If _OF_EN_NAME.Trim <> "" Then 
                cmbParam(6).Value = _OF_EN_NAME
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_OF_EN_SURNAME", SqlDbType.VarChar)
            If _OF_EN_SURNAME.Trim <> "" Then 
                cmbParam(7).Value = _OF_EN_SURNAME
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_OF_EN_NICKNAME", SqlDbType.VarChar)
            If _OF_EN_NICKNAME.Trim <> "" Then 
                cmbParam(8).Value = _OF_EN_NICKNAME
            Else
                cmbParam(8).Value = DBNull.value
            End If

            cmbParam(9) = New SqlParameter("@_DEPARTMENT_ID", SqlDbType.BigInt)
            If _DEPARTMENT_ID IsNot Nothing Then 
                cmbParam(9).Value = _DEPARTMENT_ID.Value
            Else
                cmbParam(9).Value = DBNull.value
            End IF

            cmbParam(10) = New SqlParameter("@_OF_SALARY", SqlDbType.Int)
            If _OF_SALARY IsNot Nothing Then 
                cmbParam(10).Value = _OF_SALARY.Value
            Else
                cmbParam(10).Value = DBNull.value
            End IF

            cmbParam(11) = New SqlParameter("@_STATUS_ID", SqlDbType.BigInt)
            If _STATUS_ID IsNot Nothing Then 
                cmbParam(11).Value = _STATUS_ID.Value
            Else
                cmbParam(11).Value = DBNull.value
            End IF

            cmbParam(12) = New SqlParameter("@_OF_BRITH", SqlDbType.Date)
            If _OF_BRITH.Value.Year > 1 Then 
                cmbParam(12).Value = _OF_BRITH.Value
            Else
                cmbParam(12).Value = DBNull.value
            End If

            cmbParam(13) = New SqlParameter("@_GENDER_ID", SqlDbType.BigInt)
            If _GENDER_ID IsNot Nothing Then 
                cmbParam(13).Value = _GENDER_ID.Value
            Else
                cmbParam(13).Value = DBNull.value
            End IF

            cmbParam(14) = New SqlParameter("@_BLOOD_ID", SqlDbType.BigInt)
            If _BLOOD_ID IsNot Nothing Then 
                cmbParam(14).Value = _BLOOD_ID.Value
            Else
                cmbParam(14).Value = DBNull.value
            End IF

            cmbParam(15) = New SqlParameter("@_NAT_ID", SqlDbType.BigInt)
            If _NAT_ID IsNot Nothing Then 
                cmbParam(15).Value = _NAT_ID.Value
            Else
                cmbParam(15).Value = DBNull.value
            End IF

            cmbParam(16) = New SqlParameter("@_ETH_ID", SqlDbType.BigInt)
            If _ETH_ID IsNot Nothing Then 
                cmbParam(16).Value = _ETH_ID.Value
            Else
                cmbParam(16).Value = DBNull.value
            End IF

            cmbParam(17) = New SqlParameter("@_RELIGION_ID", SqlDbType.BigInt)
            If _RELIGION_ID IsNot Nothing Then 
                cmbParam(17).Value = _RELIGION_ID.Value
            Else
                cmbParam(17).Value = DBNull.value
            End IF

            cmbParam(18) = New SqlParameter("@_OF_PID", SqlDbType.NVarChar)
            If _OF_PID.Trim <> "" Then 
                cmbParam(18).Value = _OF_PID
            Else
                cmbParam(18).Value = DBNull.value
            End IF

            cmbParam(19) = New SqlParameter("@_DATE_OF_ISSUE", SqlDbType.Date)
            If _DATE_OF_ISSUE.Value.Year > 1 Then 
                cmbParam(19).Value = _DATE_OF_ISSUE.Value
            Else
                cmbParam(19).Value = DBNull.value
            End If

            cmbParam(20) = New SqlParameter("@_DATE_OF_EXPIRY", SqlDbType.Date)
            If _DATE_OF_EXPIRY.Value.Year > 1 Then 
                cmbParam(20).Value = _DATE_OF_EXPIRY.Value
            Else
                cmbParam(20).Value = DBNull.value
            End If

            cmbParam(21) = New SqlParameter("@_MEDICAL_PLOBLEMS", SqlDbType.VarChar)
            If _MEDICAL_PLOBLEMS.Trim <> "" Then 
                cmbParam(21).Value = _MEDICAL_PLOBLEMS
            Else
                cmbParam(21).Value = DBNull.value
            End If

            cmbParam(22) = New SqlParameter("@_WEIGHT", SqlDbType.Int)
            If _WEIGHT IsNot Nothing Then 
                cmbParam(22).Value = _WEIGHT.Value
            Else
                cmbParam(22).Value = DBNull.value
            End IF

            cmbParam(23) = New SqlParameter("@_HEIGHT", SqlDbType.Int)
            If _HEIGHT IsNot Nothing Then 
                cmbParam(23).Value = _HEIGHT.Value
            Else
                cmbParam(23).Value = DBNull.value
            End IF

            cmbParam(24) = New SqlParameter("@_MR_ID", SqlDbType.BigInt)
            If _MR_ID IsNot Nothing Then 
                cmbParam(24).Value = _MR_ID.Value
            Else
                cmbParam(24).Value = DBNull.value
            End IF

            cmbParam(25) = New SqlParameter("@_MILITARY_ID", SqlDbType.BigInt)
            If _MILITARY_ID IsNot Nothing Then 
                cmbParam(25).Value = _MILITARY_ID.Value
            Else
                cmbParam(25).Value = DBNull.value
            End IF

            cmbParam(26) = New SqlParameter("@_HOME_ADDRESS", SqlDbType.VarChar)
            If _HOME_ADDRESS.Trim <> "" Then 
                cmbParam(26).Value = _HOME_ADDRESS
            Else
                cmbParam(26).Value = DBNull.value
            End If

            cmbParam(27) = New SqlParameter("@_MAILING_ADDRESS", SqlDbType.VarChar)
            If _MAILING_ADDRESS.Trim <> "" Then 
                cmbParam(27).Value = _MAILING_ADDRESS
            Else
                cmbParam(27).Value = DBNull.value
            End If

            cmbParam(28) = New SqlParameter("@_HOME_PROVINCE_ID", SqlDbType.NVarChar)
            If _HOME_PROVINCE_ID.Trim <> "" Then 
                cmbParam(28).Value = _HOME_PROVINCE_ID
            Else
                cmbParam(28).Value = DBNull.value
            End IF

            cmbParam(29) = New SqlParameter("@_HOME_AMPHUR_ID", SqlDbType.NVarChar)
            If _HOME_AMPHUR_ID.Trim <> "" Then 
                cmbParam(29).Value = _HOME_AMPHUR_ID
            Else
                cmbParam(29).Value = DBNull.value
            End IF

            cmbParam(30) = New SqlParameter("@_HOME_TUMBOL_ID", SqlDbType.NVarChar)
            If _HOME_TUMBOL_ID.Trim <> "" Then 
                cmbParam(30).Value = _HOME_TUMBOL_ID
            Else
                cmbParam(30).Value = DBNull.value
            End IF

            cmbParam(31) = New SqlParameter("@_MAILING_PROVINCE_ID", SqlDbType.NVarChar)
            If _MAILING_PROVINCE_ID.Trim <> "" Then 
                cmbParam(31).Value = _MAILING_PROVINCE_ID
            Else
                cmbParam(31).Value = DBNull.value
            End IF

            cmbParam(32) = New SqlParameter("@_MAILING_AMPHUR_ID", SqlDbType.NVarChar)
            If _MAILING_AMPHUR_ID.Trim <> "" Then 
                cmbParam(32).Value = _MAILING_AMPHUR_ID
            Else
                cmbParam(32).Value = DBNull.value
            End IF

            cmbParam(33) = New SqlParameter("@_MAILING_TUMBOL_ID", SqlDbType.NVarChar)
            If _MAILING_TUMBOL_ID.Trim <> "" Then 
                cmbParam(33).Value = _MAILING_TUMBOL_ID
            Else
                cmbParam(33).Value = DBNull.value
            End IF

            cmbParam(34) = New SqlParameter("@_HOME_ZIPCODE", SqlDbType.VarChar)
            If _HOME_ZIPCODE.Trim <> "" Then 
                cmbParam(34).Value = _HOME_ZIPCODE
            Else
                cmbParam(34).Value = DBNull.value
            End If

            cmbParam(35) = New SqlParameter("@_MAILING_ZIPCODE", SqlDbType.VarChar)
            If _MAILING_ZIPCODE.Trim <> "" Then 
                cmbParam(35).Value = _MAILING_ZIPCODE
            Else
                cmbParam(35).Value = DBNull.value
            End If

            cmbParam(36) = New SqlParameter("@_WP_ID", SqlDbType.BigInt)
            If _WP_ID IsNot Nothing Then 
                cmbParam(36).Value = _WP_ID.Value
            Else
                cmbParam(36).Value = DBNull.value
            End IF

            cmbParam(37) = New SqlParameter("@_OF_RUNNING_NO", SqlDbType.Int)
            If _OF_RUNNING_NO IsNot Nothing Then 
                cmbParam(37).Value = _OF_RUNNING_NO.Value
            Else
                cmbParam(37).Value = DBNull.value
            End IF

            cmbParam(38) = New SqlParameter("@_OF_CREATE_YEAR", SqlDbType.NVarChar)
            If _OF_CREATE_YEAR.Trim <> "" Then 
                cmbParam(38).Value = _OF_CREATE_YEAR
            Else
                cmbParam(38).Value = DBNull.value
            End IF

            cmbParam(39) = New SqlParameter("@_OF_CREATE_MONTH", SqlDbType.NVarChar)
            If _OF_CREATE_MONTH.Trim <> "" Then 
                cmbParam(39).Value = _OF_CREATE_MONTH
            Else
                cmbParam(39).Value = DBNull.value
            End IF

            cmbParam(40) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            If _CREATED_BY.Trim <> "" Then 
                cmbParam(40).Value = _CREATED_BY
            Else
                cmbParam(40).Value = DBNull.value
            End If

            cmbParam(41) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            If _CREATED_DATE.Value.Year > 1 Then 
                cmbParam(41).Value = _CREATED_DATE.Value
            Else
                cmbParam(41).Value = DBNull.value
            End If

            cmbParam(42) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then 
                cmbParam(42).Value = _UPDATED_BY
            Else
                cmbParam(42).Value = DBNull.value
            End If

            cmbParam(43) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then 
                cmbParam(43).Value = _UPDATED_DATE.Value
            Else
                cmbParam(43).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_OFFICER by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("OF_ID")) = False Then _OF_ID = Convert.ToInt64(Rdr("OF_ID"))
                        If Convert.IsDBNull(Rdr("OF_IMG")) = False Then _OF_IMG = CType(Rdr("OF_IMG"), Byte())
                        If Convert.IsDBNull(Rdr("TI_ID")) = False Then _TI_ID = Convert.ToInt64(Rdr("TI_ID"))
                        If Convert.IsDBNull(Rdr("OF_NAME")) = False Then _OF_NAME = Rdr("OF_NAME").ToString()
                        If Convert.IsDBNull(Rdr("OF_SURNAME")) = False Then _OF_SURNAME = Rdr("OF_SURNAME").ToString()
                        If Convert.IsDBNull(Rdr("OF_NICKNAME")) = False Then _OF_NICKNAME = Rdr("OF_NICKNAME").ToString()
                        If Convert.IsDBNull(Rdr("OF_EN_NAME")) = False Then _OF_EN_NAME = Rdr("OF_EN_NAME").ToString()
                        If Convert.IsDBNull(Rdr("OF_EN_SURNAME")) = False Then _OF_EN_SURNAME = Rdr("OF_EN_SURNAME").ToString()
                        If Convert.IsDBNull(Rdr("OF_EN_NICKNAME")) = False Then _OF_EN_NICKNAME = Rdr("OF_EN_NICKNAME").ToString()
                        If Convert.IsDBNull(Rdr("DEPARTMENT_ID")) = False Then _DEPARTMENT_ID = Convert.ToInt64(Rdr("DEPARTMENT_ID"))
                        If Convert.IsDBNull(Rdr("OF_SALARY")) = False Then _OF_SALARY = Convert.ToInt32(Rdr("OF_SALARY"))
                        If Convert.IsDBNull(Rdr("STATUS_ID")) = False Then _STATUS_ID = Convert.ToInt64(Rdr("STATUS_ID"))
                        If Convert.IsDBNull(Rdr("OF_BRITH")) = False Then _OF_BRITH = Convert.ToDateTime(Rdr("OF_BRITH"))
                        If Convert.IsDBNull(Rdr("GENDER_ID")) = False Then _GENDER_ID = Convert.ToInt64(Rdr("GENDER_ID"))
                        If Convert.IsDBNull(Rdr("BLOOD_ID")) = False Then _BLOOD_ID = Convert.ToInt64(Rdr("BLOOD_ID"))
                        If Convert.IsDBNull(Rdr("NAT_ID")) = False Then _NAT_ID = Convert.ToInt64(Rdr("NAT_ID"))
                        If Convert.IsDBNull(Rdr("ETH_ID")) = False Then _ETH_ID = Convert.ToInt64(Rdr("ETH_ID"))
                        If Convert.IsDBNull(Rdr("RELIGION_ID")) = False Then _RELIGION_ID = Convert.ToInt64(Rdr("RELIGION_ID"))
                        If Convert.IsDBNull(Rdr("OF_PID")) = False Then _OF_PID = Rdr("OF_PID").ToString()
                        If Convert.IsDBNull(Rdr("DATE_OF_ISSUE")) = False Then _DATE_OF_ISSUE = Convert.ToDateTime(Rdr("DATE_OF_ISSUE"))
                        If Convert.IsDBNull(Rdr("DATE_OF_EXPIRY")) = False Then _DATE_OF_EXPIRY = Convert.ToDateTime(Rdr("DATE_OF_EXPIRY"))
                        If Convert.IsDBNull(Rdr("MEDICAL_PLOBLEMS")) = False Then _MEDICAL_PLOBLEMS = Rdr("MEDICAL_PLOBLEMS").ToString()
                        If Convert.IsDBNull(Rdr("WEIGHT")) = False Then _WEIGHT = Convert.ToInt32(Rdr("WEIGHT"))
                        If Convert.IsDBNull(Rdr("HEIGHT")) = False Then _HEIGHT = Convert.ToInt32(Rdr("HEIGHT"))
                        If Convert.IsDBNull(Rdr("MR_ID")) = False Then _MR_ID = Convert.ToInt64(Rdr("MR_ID"))
                        If Convert.IsDBNull(Rdr("MILITARY_ID")) = False Then _MILITARY_ID = Convert.ToInt64(Rdr("MILITARY_ID"))
                        If Convert.IsDBNull(Rdr("HOME_ADDRESS")) = False Then _HOME_ADDRESS = Rdr("HOME_ADDRESS").ToString()
                        If Convert.IsDBNull(Rdr("MAILING_ADDRESS")) = False Then _MAILING_ADDRESS = Rdr("MAILING_ADDRESS").ToString()
                        If Convert.IsDBNull(Rdr("HOME_PROVINCE_ID")) = False Then _HOME_PROVINCE_ID = Rdr("HOME_PROVINCE_ID").ToString()
                        If Convert.IsDBNull(Rdr("HOME_AMPHUR_ID")) = False Then _HOME_AMPHUR_ID = Rdr("HOME_AMPHUR_ID").ToString()
                        If Convert.IsDBNull(Rdr("HOME_TUMBOL_ID")) = False Then _HOME_TUMBOL_ID = Rdr("HOME_TUMBOL_ID").ToString()
                        If Convert.IsDBNull(Rdr("MAILING_PROVINCE_ID")) = False Then _MAILING_PROVINCE_ID = Rdr("MAILING_PROVINCE_ID").ToString()
                        If Convert.IsDBNull(Rdr("MAILING_AMPHUR_ID")) = False Then _MAILING_AMPHUR_ID = Rdr("MAILING_AMPHUR_ID").ToString()
                        If Convert.IsDBNull(Rdr("MAILING_TUMBOL_ID")) = False Then _MAILING_TUMBOL_ID = Rdr("MAILING_TUMBOL_ID").ToString()
                        If Convert.IsDBNull(Rdr("HOME_ZIPCODE")) = False Then _HOME_ZIPCODE = Rdr("HOME_ZIPCODE").ToString()
                        If Convert.IsDBNull(Rdr("MAILING_ZIPCODE")) = False Then _MAILING_ZIPCODE = Rdr("MAILING_ZIPCODE").ToString()
                        If Convert.IsDBNull(Rdr("WP_ID")) = False Then _WP_ID = Convert.ToInt64(Rdr("WP_ID"))
                        If Convert.IsDBNull(Rdr("OF_Running_No")) = False Then _OF_Running_No = Convert.ToInt32(Rdr("OF_Running_No"))
                        If Convert.IsDBNull(Rdr("OF_Create_Year")) = False Then _OF_Create_Year = Rdr("OF_Create_Year").ToString()
                        If Convert.IsDBNull(Rdr("OF_Create_Month")) = False Then _OF_Create_Month = Rdr("OF_Create_Month").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_OFFICER by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbOfficerLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("OF_ID")) = False Then _OF_ID = Convert.ToInt64(Rdr("OF_ID"))
                        If Convert.IsDBNull(Rdr("OF_IMG")) = False Then _OF_IMG = CType(Rdr("OF_IMG"), Byte())
                        If Convert.IsDBNull(Rdr("TI_ID")) = False Then _TI_ID = Convert.ToInt64(Rdr("TI_ID"))
                        If Convert.IsDBNull(Rdr("OF_NAME")) = False Then _OF_NAME = Rdr("OF_NAME").ToString()
                        If Convert.IsDBNull(Rdr("OF_SURNAME")) = False Then _OF_SURNAME = Rdr("OF_SURNAME").ToString()
                        If Convert.IsDBNull(Rdr("OF_NICKNAME")) = False Then _OF_NICKNAME = Rdr("OF_NICKNAME").ToString()
                        If Convert.IsDBNull(Rdr("OF_EN_NAME")) = False Then _OF_EN_NAME = Rdr("OF_EN_NAME").ToString()
                        If Convert.IsDBNull(Rdr("OF_EN_SURNAME")) = False Then _OF_EN_SURNAME = Rdr("OF_EN_SURNAME").ToString()
                        If Convert.IsDBNull(Rdr("OF_EN_NICKNAME")) = False Then _OF_EN_NICKNAME = Rdr("OF_EN_NICKNAME").ToString()
                        If Convert.IsDBNull(Rdr("DEPARTMENT_ID")) = False Then _DEPARTMENT_ID = Convert.ToInt64(Rdr("DEPARTMENT_ID"))
                        If Convert.IsDBNull(Rdr("OF_SALARY")) = False Then _OF_SALARY = Convert.ToInt32(Rdr("OF_SALARY"))
                        If Convert.IsDBNull(Rdr("STATUS_ID")) = False Then _STATUS_ID = Convert.ToInt64(Rdr("STATUS_ID"))
                        If Convert.IsDBNull(Rdr("OF_BRITH")) = False Then _OF_BRITH = Convert.ToDateTime(Rdr("OF_BRITH"))
                        If Convert.IsDBNull(Rdr("GENDER_ID")) = False Then _GENDER_ID = Convert.ToInt64(Rdr("GENDER_ID"))
                        If Convert.IsDBNull(Rdr("BLOOD_ID")) = False Then _BLOOD_ID = Convert.ToInt64(Rdr("BLOOD_ID"))
                        If Convert.IsDBNull(Rdr("NAT_ID")) = False Then _NAT_ID = Convert.ToInt64(Rdr("NAT_ID"))
                        If Convert.IsDBNull(Rdr("ETH_ID")) = False Then _ETH_ID = Convert.ToInt64(Rdr("ETH_ID"))
                        If Convert.IsDBNull(Rdr("RELIGION_ID")) = False Then _RELIGION_ID = Convert.ToInt64(Rdr("RELIGION_ID"))
                        If Convert.IsDBNull(Rdr("OF_PID")) = False Then _OF_PID = Rdr("OF_PID").ToString()
                        If Convert.IsDBNull(Rdr("DATE_OF_ISSUE")) = False Then _DATE_OF_ISSUE = Convert.ToDateTime(Rdr("DATE_OF_ISSUE"))
                        If Convert.IsDBNull(Rdr("DATE_OF_EXPIRY")) = False Then _DATE_OF_EXPIRY = Convert.ToDateTime(Rdr("DATE_OF_EXPIRY"))
                        If Convert.IsDBNull(Rdr("MEDICAL_PLOBLEMS")) = False Then _MEDICAL_PLOBLEMS = Rdr("MEDICAL_PLOBLEMS").ToString()
                        If Convert.IsDBNull(Rdr("WEIGHT")) = False Then _WEIGHT = Convert.ToInt32(Rdr("WEIGHT"))
                        If Convert.IsDBNull(Rdr("HEIGHT")) = False Then _HEIGHT = Convert.ToInt32(Rdr("HEIGHT"))
                        If Convert.IsDBNull(Rdr("MR_ID")) = False Then _MR_ID = Convert.ToInt64(Rdr("MR_ID"))
                        If Convert.IsDBNull(Rdr("MILITARY_ID")) = False Then _MILITARY_ID = Convert.ToInt64(Rdr("MILITARY_ID"))
                        If Convert.IsDBNull(Rdr("HOME_ADDRESS")) = False Then _HOME_ADDRESS = Rdr("HOME_ADDRESS").ToString()
                        If Convert.IsDBNull(Rdr("MAILING_ADDRESS")) = False Then _MAILING_ADDRESS = Rdr("MAILING_ADDRESS").ToString()
                        If Convert.IsDBNull(Rdr("HOME_PROVINCE_ID")) = False Then _HOME_PROVINCE_ID = Rdr("HOME_PROVINCE_ID").ToString()
                        If Convert.IsDBNull(Rdr("HOME_AMPHUR_ID")) = False Then _HOME_AMPHUR_ID = Rdr("HOME_AMPHUR_ID").ToString()
                        If Convert.IsDBNull(Rdr("HOME_TUMBOL_ID")) = False Then _HOME_TUMBOL_ID = Rdr("HOME_TUMBOL_ID").ToString()
                        If Convert.IsDBNull(Rdr("MAILING_PROVINCE_ID")) = False Then _MAILING_PROVINCE_ID = Rdr("MAILING_PROVINCE_ID").ToString()
                        If Convert.IsDBNull(Rdr("MAILING_AMPHUR_ID")) = False Then _MAILING_AMPHUR_ID = Rdr("MAILING_AMPHUR_ID").ToString()
                        If Convert.IsDBNull(Rdr("MAILING_TUMBOL_ID")) = False Then _MAILING_TUMBOL_ID = Rdr("MAILING_TUMBOL_ID").ToString()
                        If Convert.IsDBNull(Rdr("HOME_ZIPCODE")) = False Then _HOME_ZIPCODE = Rdr("HOME_ZIPCODE").ToString()
                        If Convert.IsDBNull(Rdr("MAILING_ZIPCODE")) = False Then _MAILING_ZIPCODE = Rdr("MAILING_ZIPCODE").ToString()
                        If Convert.IsDBNull(Rdr("WP_ID")) = False Then _WP_ID = Convert.ToInt64(Rdr("WP_ID"))
                        If Convert.IsDBNull(Rdr("OF_Running_No")) = False Then _OF_Running_No = Convert.ToInt32(Rdr("OF_Running_No"))
                        If Convert.IsDBNull(Rdr("OF_Create_Year")) = False Then _OF_Create_Year = Rdr("OF_Create_Year").ToString()
                        If Convert.IsDBNull(Rdr("OF_Create_Month")) = False Then _OF_Create_Month = Rdr("OF_Create_Month").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _created_by = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _created_date = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _updated_by = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _updated_date = Convert.ToDateTime(Rdr("updated_date"))
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_OFFICER
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (OF_IMG, TI_ID, OF_NAME, OF_SURNAME, OF_NICKNAME, OF_EN_NAME, OF_EN_SURNAME, OF_EN_NICKNAME, DEPARTMENT_ID, OF_SALARY, STATUS_ID, OF_BRITH, GENDER_ID, BLOOD_ID, NAT_ID, ETH_ID, RELIGION_ID, OF_PID, DATE_OF_ISSUE, DATE_OF_EXPIRY, MEDICAL_PLOBLEMS, WEIGHT, HEIGHT, MR_ID, MILITARY_ID, HOME_ADDRESS, MAILING_ADDRESS, HOME_PROVINCE_ID, HOME_AMPHUR_ID, HOME_TUMBOL_ID, MAILING_PROVINCE_ID, MAILING_AMPHUR_ID, MAILING_TUMBOL_ID, HOME_ZIPCODE, MAILING_ZIPCODE, WP_ID, OF_RUNNING_NO, OF_CREATE_YEAR, OF_CREATE_MONTH, CREATED_BY, CREATED_DATE)"
                Sql += " OUTPUT INSERTED.OF_ID, INSERTED.OF_IMG, INSERTED.TI_ID, INSERTED.OF_NAME, INSERTED.OF_SURNAME, INSERTED.OF_NICKNAME, INSERTED.OF_EN_NAME, INSERTED.OF_EN_SURNAME, INSERTED.OF_EN_NICKNAME, INSERTED.DEPARTMENT_ID, INSERTED.OF_SALARY, INSERTED.STATUS_ID, INSERTED.OF_BRITH, INSERTED.GENDER_ID, INSERTED.BLOOD_ID, INSERTED.NAT_ID, INSERTED.ETH_ID, INSERTED.RELIGION_ID, INSERTED.OF_PID, INSERTED.DATE_OF_ISSUE, INSERTED.DATE_OF_EXPIRY, INSERTED.MEDICAL_PLOBLEMS, INSERTED.WEIGHT, INSERTED.HEIGHT, INSERTED.MR_ID, INSERTED.MILITARY_ID, INSERTED.HOME_ADDRESS, INSERTED.MAILING_ADDRESS, INSERTED.HOME_PROVINCE_ID, INSERTED.HOME_AMPHUR_ID, INSERTED.HOME_TUMBOL_ID, INSERTED.MAILING_PROVINCE_ID, INSERTED.MAILING_AMPHUR_ID, INSERTED.MAILING_TUMBOL_ID, INSERTED.HOME_ZIPCODE, INSERTED.MAILING_ZIPCODE, INSERTED.WP_ID, INSERTED.OF_RUNNING_NO, INSERTED.OF_CREATE_YEAR, INSERTED.OF_CREATE_MONTH, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE"
                Sql += " VALUES("
                sql += "@_OF_IMG" & ", "
                sql += "@_TI_ID" & ", "
                sql += "@_OF_NAME" & ", "
                sql += "@_OF_SURNAME" & ", "
                sql += "@_OF_NICKNAME" & ", "
                sql += "@_OF_EN_NAME" & ", "
                sql += "@_OF_EN_SURNAME" & ", "
                sql += "@_OF_EN_NICKNAME" & ", "
                sql += "@_DEPARTMENT_ID" & ", "
                sql += "@_OF_SALARY" & ", "
                sql += "@_STATUS_ID" & ", "
                sql += "@_OF_BRITH" & ", "
                sql += "@_GENDER_ID" & ", "
                sql += "@_BLOOD_ID" & ", "
                sql += "@_NAT_ID" & ", "
                sql += "@_ETH_ID" & ", "
                sql += "@_RELIGION_ID" & ", "
                sql += "@_OF_PID" & ", "
                sql += "@_DATE_OF_ISSUE" & ", "
                sql += "@_DATE_OF_EXPIRY" & ", "
                sql += "@_MEDICAL_PLOBLEMS" & ", "
                sql += "@_WEIGHT" & ", "
                sql += "@_HEIGHT" & ", "
                sql += "@_MR_ID" & ", "
                sql += "@_MILITARY_ID" & ", "
                sql += "@_HOME_ADDRESS" & ", "
                sql += "@_MAILING_ADDRESS" & ", "
                sql += "@_HOME_PROVINCE_ID" & ", "
                sql += "@_HOME_AMPHUR_ID" & ", "
                sql += "@_HOME_TUMBOL_ID" & ", "
                sql += "@_MAILING_PROVINCE_ID" & ", "
                sql += "@_MAILING_AMPHUR_ID" & ", "
                sql += "@_MAILING_TUMBOL_ID" & ", "
                sql += "@_HOME_ZIPCODE" & ", "
                sql += "@_MAILING_ZIPCODE" & ", "
                sql += "@_WP_ID" & ", "
                sql += "@_OF_RUNNING_NO" & ", "
                sql += "@_OF_CREATE_YEAR" & ", "
                sql += "@_OF_CREATE_MONTH" & ", "
                sql += "@_CREATED_BY" & ", "
                sql += "@_CREATED_DATE"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_OFFICER
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "OF_IMG = " & "@_OF_IMG" & ", "
                Sql += "TI_ID = " & "@_TI_ID" & ", "
                Sql += "OF_NAME = " & "@_OF_NAME" & ", "
                Sql += "OF_SURNAME = " & "@_OF_SURNAME" & ", "
                Sql += "OF_NICKNAME = " & "@_OF_NICKNAME" & ", "
                Sql += "OF_EN_NAME = " & "@_OF_EN_NAME" & ", "
                Sql += "OF_EN_SURNAME = " & "@_OF_EN_SURNAME" & ", "
                Sql += "OF_EN_NICKNAME = " & "@_OF_EN_NICKNAME" & ", "
                Sql += "DEPARTMENT_ID = " & "@_DEPARTMENT_ID" & ", "
                Sql += "OF_SALARY = " & "@_OF_SALARY" & ", "
                Sql += "STATUS_ID = " & "@_STATUS_ID" & ", "
                Sql += "OF_BRITH = " & "@_OF_BRITH" & ", "
                Sql += "GENDER_ID = " & "@_GENDER_ID" & ", "
                Sql += "BLOOD_ID = " & "@_BLOOD_ID" & ", "
                Sql += "NAT_ID = " & "@_NAT_ID" & ", "
                Sql += "ETH_ID = " & "@_ETH_ID" & ", "
                Sql += "RELIGION_ID = " & "@_RELIGION_ID" & ", "
                Sql += "OF_PID = " & "@_OF_PID" & ", "
                Sql += "DATE_OF_ISSUE = " & "@_DATE_OF_ISSUE" & ", "
                Sql += "DATE_OF_EXPIRY = " & "@_DATE_OF_EXPIRY" & ", "
                Sql += "MEDICAL_PLOBLEMS = " & "@_MEDICAL_PLOBLEMS" & ", "
                Sql += "WEIGHT = " & "@_WEIGHT" & ", "
                Sql += "HEIGHT = " & "@_HEIGHT" & ", "
                Sql += "MR_ID = " & "@_MR_ID" & ", "
                Sql += "MILITARY_ID = " & "@_MILITARY_ID" & ", "
                Sql += "HOME_ADDRESS = " & "@_HOME_ADDRESS" & ", "
                Sql += "MAILING_ADDRESS = " & "@_MAILING_ADDRESS" & ", "
                Sql += "HOME_PROVINCE_ID = " & "@_HOME_PROVINCE_ID" & ", "
                Sql += "HOME_AMPHUR_ID = " & "@_HOME_AMPHUR_ID" & ", "
                Sql += "HOME_TUMBOL_ID = " & "@_HOME_TUMBOL_ID" & ", "
                Sql += "MAILING_PROVINCE_ID = " & "@_MAILING_PROVINCE_ID" & ", "
                Sql += "MAILING_AMPHUR_ID = " & "@_MAILING_AMPHUR_ID" & ", "
                Sql += "MAILING_TUMBOL_ID = " & "@_MAILING_TUMBOL_ID" & ", "
                Sql += "HOME_ZIPCODE = " & "@_HOME_ZIPCODE" & ", "
                Sql += "MAILING_ZIPCODE = " & "@_MAILING_ZIPCODE" & ", "
                Sql += "WP_ID = " & "@_WP_ID" & ", "
                Sql += "OF_RUNNING_NO = " & "@_OF_RUNNING_NO" & ", "
                Sql += "OF_CREATE_YEAR = " & "@_OF_CREATE_YEAR" & ", "
                Sql += "OF_CREATE_MONTH = " & "@_OF_CREATE_MONTH" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_OFFICER
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_OFFICER
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT OF_ID, OF_IMG, TI_ID, OF_NAME, OF_SURNAME, OF_NICKNAME, OF_EN_NAME, OF_EN_SURNAME, OF_EN_NICKNAME, DEPARTMENT_ID, OF_SALARY, STATUS_ID, OF_BRITH, GENDER_ID, BLOOD_ID, NAT_ID, ETH_ID, RELIGION_ID, OF_PID, DATE_OF_ISSUE, DATE_OF_EXPIRY, MEDICAL_PLOBLEMS, WEIGHT, HEIGHT, MR_ID, MILITARY_ID, HOME_ADDRESS, MAILING_ADDRESS, HOME_PROVINCE_ID, HOME_AMPHUR_ID, HOME_TUMBOL_ID, MAILING_PROVINCE_ID, MAILING_AMPHUR_ID, MAILING_TUMBOL_ID, HOME_ZIPCODE, MAILING_ZIPCODE, WP_ID, OF_RUNNING_NO, OF_CREATE_YEAR, OF_CREATE_MONTH, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
