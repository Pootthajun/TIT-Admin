﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmMasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TIT_WebBackoffice.Default" %>

<%@ Register Src="~/UCOfficerContact.ascx" TagPrefix="uc1" TagName="UCOfficerContact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <form runat="server" id="form1">
        <div class="form-group">
            <label class="col-md-3 control-label">Default Datepicker</label>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input type="text" data-plugin-datepicker="" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <asp:Label ID="Label2" runat="server" Text="วันที่เสนอราคา"></asp:Label>
            <div class="input-daterange input-group" data-plugin-datepicker="">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <asp:TextBox ID="TextBox2" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Date range</label>
            <div class="col-md-6">
                <div class="input-daterange input-group" data-plugin-datepicker="">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input type="text" class="form-control" name="start"/>
                    <span class="input-group-addon">to</span>
                    <input type="text" class="form-control" name="end"/>
                </div>
            </div>
        </div>
    </form>
</asp:Content>
